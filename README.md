1. Скопировать ko3/application/conf/local_conf.php.example в local_conf.php
   и отредактировать.

2. Выставить права 777 папкам: 
   ko3/application/cache
   ko3/application/emails
   ko3/application/emails/finished
   ko3/application/emails/raw
   ko3/application/emails/raw_notify
   ko3/application/logs
   ko3/application/logs/custom
   ko3/application/source
   ko3/application/source/superclean
   ko3/application/source/superclean/raw
   ko3/assets/autodraw
   ko3/assets/superclean

3. Накатить миграции: 
   ./minion db:migrate:up

4. Установить шрифт Carlito

5. Установить PHP-либу IMagick.

6. Настроить крон:

   # Каждую минуту
   if [ $(ps aux | grep 'autodraw --action=queue' | grep -v grep | wc -l | tr -s "\n") -lt 5 ]; then ./minion --task=autodraw --action=queue --mode=daemon >/dev/null 2>&1; fi

   # Каждую минуту
   if [ $(ps aux | grep 'autodraw --action=status_change' | grep -v grep | wc -l | tr -s "\n") -lt 1 ]; then ./minion --task=autodraw --action=status_change --mode=daemon >/dev/null 2>&1; fi

   # Каждую минуту
   if [ $(ps aux | grep 'superclean --action=queue' | grep -v grep | wc -l | tr -s "\n") -lt 1 ]; then ./minion --task=superclean --action=queue --mode=daemon >/dev/null 2>&1; fi

   # Каждую минуту
   ./minion --task=sendmail

   # Раз в час
   ./minion --task=sendpurification

   # Раз в час. Желательно где-нибудь в 13 минуту часа
   ./minion --task=reminder --sendmail=true

   # Раз в час. Желательно где-нибудь в середине часа
   ./minion --task=superclean --action=update

   # Раз в час. Желательно где-нибудь в середине часа
   ./minion --task=autodraw --action=schedule

   # Раз в сутки. Желательно в 00:01
   ./minion --task=servicefee

   # Раз в сутки. Желательно в 00:05
   ./minion --task=autodraw --action=garbage

   # Раз в сутки. Желательно в 00:09
   ./minion --task=reminder
