var notyTimeout;

var createNoty = function (message, type, close) {
    clearTimeout(notyTimeout);
    var html = $('<div />').attr({class: 'alert alert-' + type + ' noty-alert'}).html(message);
    var holder = $('#noty-holder');
    holder.empty();
    html.hide().prependTo(holder).slideDown();
    notyTimeout = setTimeout(function () {
        $('#noty-holder>div.noty-alert').slideUp({
            queue: false,
            complete: function () {
                $(holder).empty();
            }
        });
    }, close * 1000);
};

var decl = function (intgr, expr) {
    intgr = Number(intgr);
    
    var count = intgr % 100;
    
    if (count >= 5 && count <= 20) {
        var result = intgr + ' ' + expr[2];
    } else {
        count = count % 10;
        if (count == 1) {
            result = intgr + ' ' + expr[0];
        } else if (count >= 2 && count <= 4) {
            result = intgr + ' ' + expr[1];
        } 
        else 
        {
            result = intgr + ' ' + expr[2];
        }
    }

    return result;
}