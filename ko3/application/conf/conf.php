<?php

return array(        
    'maintenance_mode' => TRUE, // Режим обслуживания сайта
    
    'kohana_init' => array(
        'base_url'   => '/',
        'index_file' => FALSE,
        'profiling'  => FALSE,
        'errors'     => FALSE,
      	'cache_dir'  => APPPATH.'cache'
    ),
    'site_url'               => 'http://test.ru.loc/magictools/',
    'service_email'          => 'e@magic-tools.ru',
    'kohana_environment'     => 'PRODUCTION',
    'auth_pepper'            => 'test',
    'cookie_salt'            => '0000000000',
    'cookie_env_mode'        => 'qwerty12345',
    'modules' => array(
//      'beanstalkd' => MODPATH . 'beanstalkd', // Beanstalkd (Message Queue) 
        'pheanstalk' => MODPATH . 'pheanstalk', // Beanstalkd (Message Queue) 
        'database'   => MODPATH . 'database',   // Database access
        'image'      => MODPATH . 'image',      // Image Helper
        'minion'     => MODPATH . 'minion',     // CLI Tasks
        'orm'        => MODPATH . 'orm',        // Object Relationship Mapping
        'migrations' => MODPATH . 'migrations', // Migrations Tool
        'tgsdk'      => MODPATH . 'tgsdk',      // Telegram SDK
    ),
    
    'ps_page' => 'default', // Страница оплаты
    
    'ps' => array(
        'yandex' => array(
            'key' => '0123456789AaBbCcDdEeFfGg'
        ),
    
        'prodamus' => array(
            'site'       => 'https://magic-tools.payform.ru/',
            'secret_key' => '2y2aw4oknnke80bp1a8fniwuuq7tdkwmmuq7vwi4nzbr8z1182ftbn6p8mhw3bhz'
        ),
    
        'cryptocloud' => array(
            'shop_id' => 'iEOgaL1wyTGGr2XA',
            'api_key' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MzE5NSwiZXhwIjo4ODA3NjA5OTAxOX0.oIucxRgWKsuvvHbJkWBT_S23gzsxcayRizhjLqMyTcU'
        ),
    ),
    
    'autodraw' => array(
        'queue'   => array(
            'ttr' => 2700
        ),
        'workers' => array(
            'default' => array(
                'public_path' => 'http://magic-tools.ru.loc/assets/autodraw/', // БЕЗ СЛЕША В НАЧАЛЕ!!!
                'instances'   => 15
            ),
            'w1' => array(
                'public_path' => 'http://magic-tools.ru.loc/assets/autodraw/', // БЕЗ СЛЕША В НАЧАЛЕ!!!
                'instances'   => 15
            ),
            'w2' => array(
                'public_path' => 'http://magic-tools.ru.loc/assets/autodraw/', // БЕЗ СЛЕША В НАЧАЛЕ!!!
                'instances'   => 10
            )
        )
    ),
    
    'autodraw_dir'           => DOCROOT .'assets/autodraw/',
    'autodraw_public_path'   => 'assets/autodraw/', // БЕЗ СЛЕША В НАЧАЛЕ!!!
    
    'superclean_dir'           => DOCROOT .'assets/superclean/',
    'superclean_raw_dir'       => DOCROOT .'ko3/application/source/superclean/raw/',
    'superclean_harmonize_img' => DOCROOT .'ko3/application/source/superclean/harmonize.jpg',
    'superclean_public_path'   => 'assets/superclean/', // БЕЗ СЛЕША В НАЧАЛЕ!!!
);
