<?php defined('SYSPATH') OR die('No direct script access.');

return array(
    
    // Контроллер
    'base' => array(
        
        // Экшен
        'index' => array(            
            'title'  => '&nbsp;&nbsp;Главная',
            'icon'   => 'glyphicon-dashboard',
            'href'   => URL::site('admin'),
            //'access' => array(Model_Account::STATUS_ADMIN, Model_Account::STATUS_USER),
        )
        
    ),
    
    // Контроллер
    'users' => array(
        
        // Экшен
        'index' => array(            
            'title' => '&nbsp;&nbsp;Пользователи',
            'icon'  => 'glyphicon-user',
            'href'  => URL::site('admin/users')          
        )
        
    ),
    
    // Контроллер
    'payments' => array(
        
        // Экшен
        'index' => array(            
            'title' => '&nbsp;&nbsp;Платежи',
            'icon'  => 'glyphicon-rub',
            'href'  => URL::site('admin/payments')          
        )
        
    ),
    
    // Контроллер
    'draw' => array(
        
        // Экшен
        'templates,templates_cat' => array(            
            'title' => '&nbsp;&nbsp;Шаблоны рисовалки',
            'icon'  => 'glyphicon-text-background',
            'href'  => URL::site('admin/draw/templates')          
        )
        
    ),
    
    // Контроллер
    'blog' => array(
            
        'title' => '&nbsp;&nbsp;Блог',
        'icon'  => 'glyphicon-comment',
        'sub'   => array(
        
            // Экшен
            'index' => array(     
                'title' => 'Записи', 
                'href'  => URL::site('admin/blog')
            ),
            
            // Экшен
            'create' => array(                
                'title' => 'Создать',
                'href'  => URL::site('admin/blog/create')
            ),
            
            // Экшен
            'cats,cat_add' => array(                
                'title' => 'Категории',
                'href'  => URL::site('admin/blog/cats')
            ),        

        ),
        
    ),
);
