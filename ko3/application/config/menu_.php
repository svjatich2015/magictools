<?php defined('SYSPATH') OR die('No direct script access.');

return array(
    
    // Контроллер
    'page' => array(
        
        // Экшен
        'index' => array(            
            'title'  => '&nbsp;&nbsp;С чего начать?',
            'icon'   => 'glyphicon-bookmark',
            'href'   => URL::site('pages/manual/index'),
        )
        
    ),
    
    // Контроллер
    'archecards' => array(
        
        // Экшен
        'index' => array(            
            'title' => '&nbsp;&nbsp;Архетип-карты',
            'icon'  => 'glyphicon-picture',
            'href'  => URL::site('/archecards/')         
        )
        
    ),
    
    // Контроллер
    'autocoach' => array(
        
        // Экшен
        'index' => array(            
            'title' => '&nbsp;&nbsp;Автокоуч&nbsp;&nbsp;<span class="label label-success">New</span>',
            'icon'  => 'glyphicon-picture',
            'href'  => URL::site('/autocoach/')         
        )
        
    ),
    
    // Контроллер
    /*
    'draw' => array(
            
        'title' => '&nbsp;&nbsp;Рисовалка',
        'icon'  => 'glyphicon-pencil',
        'sub'   => array(
        
            // Экшен
            'index' => array(     
                'title' => 'Быстрая отрисовка',           
                //'title' => '&nbsp;&nbsp;Отрисовка темы',
                //'icon'  => 'glyphicon-check',
                'href'  => URL::site('/draw/')
            ),
            
            // Экшен
            'clients' => array(                
                'title' => 'Отрисовка клиентов',
                //'title' => '&nbsp;&nbsp;Отрисовка клиентов',
                //'icon'  => 'glyphicon-user',
                'href'  => URL::site('/draw/clients/')
            ),    
            
            // Экшен
            'templates,templates_cat' => array(                
                'title'  => 'Отрисовка шаблонов<sup> &beta;</sup>',
                //'title' => '&nbsp;&nbsp;Отрисовка клиентов',
                //'icon'  => 'glyphicon-user',
                'href'   => URL::site('/draw/templates/'),
                //'access' => array(Model_Account::STATUS_ADMIN),
            ),        

        ),
        
    ),
    */
    
    // Контроллер
    'autodraw' => array(
        
        // Экшен
        'index' => array(            
            'title' => '&nbsp;&nbsp;Авторисовалка',
            'icon'  => 'glyphicon-font',
            'href'  => URL::site('/autodraw/')         
        )
        
    ),
    
    // Контроллер
    'draw' => array(
        
        // Экшен
        'index' => array(            
            'title' => '&nbsp;&nbsp;Рисовалка',
            'icon'  => 'glyphicon-pencil',
            'href'  => URL::site('/draw/')         
        )
        
    ),
    
    // Контроллер
    'superclean' => array(
        
        // Экшен
        'index' => array(            
            'title' => '&nbsp;&nbsp;СуперЧистка',
            'icon'  => 'glyphicon-erase',
            'href'  => URL::site('/superclean/')         
        )
        
    ),
    
    // Контроллер
    'clients' => array(
            
        'title' => '&nbsp;&nbsp;Клиенты',
        'icon'  => 'glyphicon-book',
        'sub'   => array(
        
            // Экшен
            'index' => array(     
                'title' => 'Список клиентов', 
                'href'  => URL::site('/clients/')
            ),
            
            // Экшен
            'add' => array(                
                'title' => 'Добавить клиента',
                'href'  => URL::site('/clients/add/')
            ),        

        ),
        
    ),
    /*
    // Контроллер
    'community' => array(
        
        // Экшен
        'index,qa' => array(            
            'title'  => '&nbsp;&nbsp;Блог',
            'icon'   => 'glyphicon-comment',
            'href'  => URL::site()          
        )
        
    ),
    */
    // Контроллер
    'support' => array(
        
        // Экшен
        'index' => array(            
            'title'  => '&nbsp;&nbsp;Служба поддержки',
            'icon'   => 'glyphicon-question-sign',
            'href'   => URL::site('/support/')
        )
        
    ),
    
    // Контроллер
    'admin' => array(
        
        // Экшен
        'index' => array(            
            'title'  => '&nbsp;&nbsp;Админ-панель',
            'icon'   => 'glyphicon-knight',
            'href'   => URL::site('/admin/'),
            'style'  => 'font-weight: bold;',
            'access' => array(Model_Account::STATUS_ADMIN),
        )
        
    ),
    
    // Контроллер
    //'help' => array(
    //    
        // Экшен
    //    'index' => array(            
    //        'title' => '&nbsp;&nbsp;Помощь',
    //        'icon'  => 'glyphicon-question-sign',
    //        'href'  => URL::site('/help/')            
    //    )
    //    
    //)
);
