<?php defined('SYSPATH') OR die('No direct script access.');

return array(
    
    // Контроллер
    'page' => array(
        
        // Экшен
        'index' => array(            
            'title'  => '&nbsp;&nbsp;С чего начать?',
            'icon'   => 'glyphicon-bookmark',
            'href'   => URL::site('pages/manual/index'),
        )
        
    ),
    
    // Контроллер
    'blog' => array(
        
        // Экшен
        'index,post,category,tag' => array(            
            'title'  => '&nbsp;&nbsp;Блог',
            'icon'   => 'glyphicon-comment',
            'href'   => URL::site(''),
        )
        
    ),
    
    // Контроллер
    'tools' => array(
            
        'title' => '&nbsp;&nbsp;Инструменты',
        'icon'  => 'glyphicon-briefcase',
        'sub'   => array(
        
            // Экшен
            'archecards' => array(     
                'title' => 'Архетип-карты', 
                'href'  => URL::site('/archecards/')
            ),
            
            // Экшен
            'autocoach' => array(                
                'title' => 'Автокоуч',
                'href'  => URL::site('/autocoach/')
            ),  
            
            // Экшен
            'autodraw' => array(                
                'title' => 'Авторисовалка',
                'href'  => URL::site('/autodraw/')
            ),  
            
            // Экшен
            'draw' => array(                
                'title' => 'Рисовалка',
                'href'  => URL::site('/draw/')
            ),  
            
            // Экшен
            'superclean' => array(                
                'title' => 'СуперЧистка',
                'href'  => URL::site('/superclean/')
            ),        

        ),
        
    ),
    
    // Контроллер
    'clients' => array(
            
        'title' => '&nbsp;&nbsp;Клиенты',
        'icon'  => 'glyphicon-book',
        'sub'   => array(
        
            // Экшен
            'index,edit' => array(     
                'title' => 'Список клиентов', 
                'href'  => URL::site('/clients/')
            ),
            
            // Экшен
            'add' => array(                
                'title' => 'Добавить клиента',
                'href'  => URL::site('/clients/add/')
            ),        

        ),
        
    ),
    /*
    // Контроллер
    'community' => array(
        
        // Экшен
        'index,qa' => array(            
            'title'  => '&nbsp;&nbsp;Блог',
            'icon'   => 'glyphicon-comment',
            'href'  => URL::site()          
        )
        
    ),
    */
    // Контроллер
    'support' => array(
        
        // Экшен
        'index' => array(            
            'title'  => '&nbsp;&nbsp;Служба поддержки',
            'icon'   => 'glyphicon-question-sign',
            'href'   => URL::site('/support/')
        )
        
    ),
    
    // Контроллер
    'admin' => array(
        
        // Экшен
        'index' => array(            
            'title'  => '&nbsp;&nbsp;Админ-панель',
            'icon'   => 'glyphicon-knight',
            'href'   => URL::site('/admin/'),
            'style'  => 'font-weight: bold;',
            'access' => array(Model_Account::STATUS_ADMIN),
        )
        
    ),
    
    // Контроллер
    //'help' => array(
    //    
        // Экшен
    //    'index' => array(            
    //        'title' => '&nbsp;&nbsp;Помощь',
    //        'icon'  => 'glyphicon-question-sign',
    //        'href'  => URL::site('/help/')            
    //    )
    //    
    //)
);
