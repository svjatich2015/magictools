<?php defined('SYSPATH') OR die('No direct script access.');
/**
* create_table($table_name, $fields, array('id' => TRUE, 'options' => ''))
* drop_table($table_name)
* rename_table($old_name, $new_name)
* add_column($table_name, $column_name, $params)
* rename_column($table_name, $column_name, $new_column_name)
* change_column($table_name, $column_name, $params)
* remove_column($table_name, $column_name)
* add_index($table_name, $index_name, $columns, $index_type = 'normal')
* remove_index($table_name, $index_name)
*/
class AddAutodrawClientSchedulesTable extends Migration
{
	public function up()
	{
                $this->query("
CREATE TABLE IF NOT EXISTS `AutodrawClientSchedules` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `clientId` INT(10) UNSIGNED NOT NULL,
  `period` TINYINT(1) NOT NULL DEFAULT 0,
  `minimize` TINYINT(1) NOT NULL DEFAULT 0,
  `updated` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  INDEX `fk_AutodrawClientSchedules_DrawClients1_idx` (`clientId` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_AutodrawClientSchedules_DrawClients1`
    FOREIGN KEY (`clientId`)
    REFERENCES `DrawClients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;");
	}
	
	public function down()
	{
                $this->query("DROP TABLE IF EXISTS `AutodrawClientSchedules`;");
	}
}