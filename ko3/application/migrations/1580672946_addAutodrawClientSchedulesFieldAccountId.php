<?php defined('SYSPATH') OR die('No direct script access.');
/**
* create_table($table_name, $fields, array('id' => TRUE, 'options' => ''))
* drop_table($table_name)
* rename_table($old_name, $new_name)
* add_column($table_name, $column_name, $params)
* rename_column($table_name, $column_name, $new_column_name)
* change_column($table_name, $column_name, $params)
* remove_column($table_name, $column_name)
* add_index($table_name, $index_name, $columns, $index_type = 'normal')
* remove_index($table_name, $index_name)
*/
class AddAutodrawClientSchedulesFieldAccountId extends Migration
{
	public function up()
	{
                $this->query("ALTER TABLE `AutodrawClientSchedules` "
                        . "ADD `accountId` INT(10) UNSIGNED NOT NULL AFTER `id`, "
                        . "ADD INDEX fk_AutodrawClientSchedules_Accounts1_idx (`accountId` ASC), "
                        . "ADD CONSTRAINT `fk_AutodrawClientSchedules_Accounts1` 
                                FOREIGN KEY (`accountId`) 
                                REFERENCES `Accounts` (`id`) 
                                ON DELETE NO ACTION 
                                ON UPDATE NO ACTION;");
	}
	
	public function down()
	{
                $this->query("ALTER TABLE `AutodrawClientSchedules` DROP `accountId`;");
	}
}