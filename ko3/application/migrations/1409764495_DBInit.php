<?php defined('SYSPATH') OR die('No direct script access.');

class DBInit extends Migration
{
	public function up()
	{
		$this->execute(file_get_contents(APPPATH.'migrations/dbinit.sql'));
	}
	
	public function down()
	{
		$this->execute("");
	}
}