SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Accounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Accounts` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `passwordSalt` CHAR(8) NOT NULL,
  `passwordHash` CHAR(32) NOT NULL,
  `balance` INT(10) NOT NULL DEFAULT 0,
  `expiried` INT(10) UNSIGNED NOT NULL,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DrawClients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DrawClients` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `isScheme` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `isBodies` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `isTemplates` TINYINT(1) UNSIGNED NOT NULL DEFAULT 2,
  `processed` INT(10) UNSIGNED NULL DEFAULT NULL,
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_DrawClients_Accounts_idx` (`accountId` ASC),
  CONSTRAINT `fk_DrawClients_Accounts`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DrawClientThemes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DrawClientThemes` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `clientId` INT(10) UNSIGNED NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_DrawClientThemes_DrawClients1_idx` (`clientId` ASC),
  CONSTRAINT `fk_DrawClientThemes_DrawClients1`
    FOREIGN KEY (`clientId`)
    REFERENCES `DrawClients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Invites`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Invites` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NOT NULL,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PasswordRecoveries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PasswordRecoveries` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `hash` VARCHAR(16) NOT NULL,
  `expiried` INT(10) UNSIGNED NOT NULL,
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `hash_UNIQUE` (`hash` ASC),
  INDEX `fk_PasswordRecoveries_Accounts1_idx` (`accountId` ASC),
  CONSTRAINT `fk_PasswordRecoveries_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Profiles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Profiles` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Profiles_Accounts1_idx` (`accountId` ASC),
  CONSTRAINT `fk_Profiles_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tools`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tools` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NOT NULL,
  `monthlyCost` INT UNSIGNED NOT NULL,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Slots`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Slots` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `toolId` INT(10) UNSIGNED NOT NULL,
  `num` INT(3) UNSIGNED NOT NULL,
  `monthlyCost` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Slots_Tools1_idx` (`toolId` ASC),
  CONSTRAINT `fk_Slots_Tools1`
    FOREIGN KEY (`toolId`)
    REFERENCES `Tools` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AccountsTools`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AccountsTools` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `toolId` INT(10) UNSIGNED NOT NULL,
  `slotsId` INT(10) UNSIGNED NULL DEFAULT NULL,
  `freeUntil` INT(10) UNSIGNED NOT NULL,
  `updated` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_AccountsTools_Accounts1_idx` (`accountId` ASC),
  INDEX `fk_AccountsTools_Tools1_idx` (`toolId` ASC),
  INDEX `fk_AccountsTools_Slots1_idx` (`slotsId` ASC),
  CONSTRAINT `fk_AccountsTools_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AccountsTools_Tools1`
    FOREIGN KEY (`toolId`)
    REFERENCES `Tools` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AccountsTools_Slots1`
    FOREIGN KEY (`slotsId`)
    REFERENCES `Slots` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Payments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Payments` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `type` TINYINT(1) UNSIGNED NOT NULL,
  `amount` INT UNSIGNED NOT NULL,
  `additional` TEXT NULL,
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_Payments_Accounts1_idx` (`accountId` ASC),
  CONSTRAINT `fk_Payments_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DrawTemplatesCategories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DrawTemplatesCategories` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  `orderNum` INT(10) UNSIGNED NOT NULL DEFAULT 1,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT 2,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DrawTemplates`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DrawTemplates` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `categoryId` INT(10) UNSIGNED NOT NULL,
  `title` VARCHAR(32) NOT NULL,
  `description` VARCHAR(128) NOT NULL,
  `type` INT(1) UNSIGNED NOT NULL DEFAULT 1,
  `orderNum` INT(10) UNSIGNED NOT NULL DEFAULT 1,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC),
  INDEX `fk_DrawTemplates_DrawTemplatesCategories1_idx` (`categoryId` ASC),
  CONSTRAINT `fk_DrawTemplates_DrawTemplatesCategories1`
    FOREIGN KEY (`categoryId`)
    REFERENCES `DrawTemplatesCategories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DrawThemes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DrawThemes` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DrawTemplatesThemes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DrawTemplatesThemes` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `templateId` INT(10) UNSIGNED NOT NULL,
  `themeId` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_DrawTemplatesThemes_DrawTemplates1_idx` (`templateId` ASC),
  INDEX `fk_DrawTemplatesThemes_DrawThemes1_idx` (`themeId` ASC),
  CONSTRAINT `fk_DrawTemplatesThemes_DrawTemplates1`
    FOREIGN KEY (`templateId`)
    REFERENCES `DrawTemplates` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DrawTemplatesThemes_DrawThemes1`
    FOREIGN KEY (`themeId`)
    REFERENCES `DrawThemes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DrawClientsTemplates`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DrawClientsTemplates` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `clientId` INT(10) UNSIGNED NOT NULL,
  `templateId` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_DrawClientsTemplates_DrawClients1_idx` (`clientId` ASC),
  INDEX `fk_DrawClientsTemplates_DrawTemplates1_idx` (`templateId` ASC),
  CONSTRAINT `fk_DrawClientsTemplates_DrawClients1`
    FOREIGN KEY (`clientId`)
    REFERENCES `DrawClients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DrawClientsTemplates_DrawTemplates1`
    FOREIGN KEY (`templateId`)
    REFERENCES `DrawTemplates` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autodraw`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autodraw` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `title` VARCHAR(45) NULL DEFAULT NULL,
  `name` VARCHAR(45) NOT NULL,
  `isScheme` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `isBodies` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `strainLevel` TINYINT(1) UNSIGNED NOT NULL,
  `type` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `processed` INT(10) UNSIGNED NULL,
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`),
  INDEX `fk_Autodraw_Accounts1_idx` (`accountId` ASC),
  CONSTRAINT `fk_Autodraw_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AutodrawThemes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AutodrawThemes` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `autodrawId` INT(10) UNSIGNED NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `color` VARCHAR(24) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_AutodrawThemes_Autodraw1_idx` (`autodrawId` ASC),
  CONSTRAINT `fk_AutodrawThemes_Autodraw1`
    FOREIGN KEY (`autodrawId`)
    REFERENCES `Autodraw` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AccountsToolsRemove`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AccountsToolsRemove` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `toolId` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_AccountsToolsRemove_Accounts1_idx` (`accountId` ASC),
  INDEX `fk_AccountsToolsRemove_Tools1_idx` (`toolId` ASC),
  CONSTRAINT `fk_AccountsToolsRemove_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AccountsToolsRemove_Tools1`
    FOREIGN KEY (`toolId`)
    REFERENCES `Tools` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SuperClean`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SuperClean` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `slotNum` INT(3) UNSIGNED NOT NULL,
  `incorrect` INT(2) UNSIGNED NOT NULL DEFAULT 0,
  `rawFileType` VARCHAR(4) NOT NULL,
  `created` INT(10) UNSIGNED NOT NULL,
  `processed` INT(10) UNSIGNED NOT NULL,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT 2,
  PRIMARY KEY (`id`),
  INDEX `fk_SuperClean_Accounts1_idx` (`accountId` ASC),
  UNIQUE INDEX `accountId_slotNum_UNIQUE` (`accountId` ASC, `slotNum` ASC),
  CONSTRAINT `fk_SuperClean_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autocoach`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autocoach` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `theme` TEXT NOT NULL,
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_Autocoach_Accounts1_idx` (`accountId` ASC),
  CONSTRAINT `fk_Autocoach_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Staff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Staff` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accountId` INT(10) UNSIGNED NOT NULL,
  `status` INT(1) UNSIGNED NOT NULL DEFAULT 2,
  PRIMARY KEY (`id`),
  INDEX `fk_Staff_Accounts1_idx` (`accountId` ASC),
  CONSTRAINT `fk_Staff_Accounts1`
    FOREIGN KEY (`accountId`)
    REFERENCES `Accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Permissions` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `controller` VARCHAR(45) NOT NULL,
  `action` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StaffPermissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `StaffPermissions` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `staffId` INT(10) UNSIGNED NOT NULL,
  `permissionId` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_StaffPermissions_Permissions1_idx` (`permissionId` ASC),
  INDEX `fk_StaffPermissions_Staff1_idx` (`staffId` ASC),
  CONSTRAINT `fk_StaffPermissions_Permissions1`
    FOREIGN KEY (`permissionId`)
    REFERENCES `Permissions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_StaffPermissions_Staff1`
    FOREIGN KEY (`staffId`)
    REFERENCES `Staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Data for table `Tools`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `Tools` (`id`, `code`, `monthlyCost`, `freeUntil`, `status`) VALUES (DEFAULT, 'draw', 1000, 0, 1);
INSERT INTO `Tools` (`id`, `code`, `monthlyCost`, `freeUntil`, `status`) VALUES (DEFAULT, 'superclean', 0, 0, 1);
INSERT INTO `Tools` (`id`, `code`, `monthlyCost`, `freeUntil`, `status`) VALUES (DEFAULT, 'archecards', 0, 0, 1);
INSERT INTO `Tools` (`id`, `code`, `monthlyCost`, `freeUntil`, `status`) VALUES (DEFAULT, 'autocoach', 0, 0, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Slots`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `Slots` (`id`, `toolId`, `num`, `monthlyCost`) VALUES (DEFAULT, 2, 1, 300);
INSERT INTO `Slots` (`id`, `toolId`, `num`, `monthlyCost`) VALUES (DEFAULT, 2, 3, 500);
INSERT INTO `Slots` (`id`, `toolId`, `num`, `monthlyCost`) VALUES (DEFAULT, 2, 5, 750);
INSERT INTO `Slots` (`id`, `toolId`, `num`, `monthlyCost`) VALUES (DEFAULT, 2, 10, 1000);
INSERT INTO `Slots` (`id`, `toolId`, `num`, `monthlyCost`) VALUES (DEFAULT, 2, 25, 2000);
INSERT INTO `Slots` (`id`, `toolId`, `num`, `monthlyCost`) VALUES (DEFAULT, 2, 50, 3500);
INSERT INTO `Slots` (`id`, `toolId`, `num`, `monthlyCost`) VALUES (DEFAULT, 2, 100, 5000);

COMMIT;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
