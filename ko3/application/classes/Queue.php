<?php defined('SYSPATH') or die('No direct script access.');

class Queue extends Kohana_Queue {
        
    /**
     * Задержка при выбрке
     * @const
     */
    const DEFAULT_DELAY = 0;       // без задержки

    /**
     * Приоритет сообщения
     * @const
     */
    const DEFAULT_PRIORITY = 1024; // 0 - самый срочный, макс. значение - 4294967295

    /**
     * Время на выполнение задачи, Time To Run
     * @const
     */
    const DEFAULT_TTR = 300;        // 5 минут

    /**
     * Время ожидания новой задачи, если очередь пуста
     * @const
     */
    const DEFAULT_TIMEOUT = 1;     // 1 секунда

    /**
     * Инстанцирование объекта очереди
     *
     * @param string $name
     * @param array $config
     * @throws Kohana_Exception
     * @return Kohana_Queue
     */
    public static function instance($name = 'default', $config = array())
    {
        if ( ! isset(self::$_instance[$name])) {
            if (empty(self::$_default_config)) {
                self::$_default_config = Conf::get_option('beanstalk');
            }

            $config = array_merge(self::$_default_config, $config);

            self::$_instance[$name] = new Queue($config);
        }

        return self::$_instance[$name];
    }        
}
