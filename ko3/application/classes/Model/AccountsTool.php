<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_AccountsTool extends ORM {
    
        const ACTION_PROLONG      = 1;
        const ACTION_UPDATE_SLOTS = 2;
        
        const MODE_NOT_ACTIVE = 0;
        const MODE_ACTIVE     = 1;
        const MODE_EXPIRIED   = 2;
        
        const TOOL_DRAW         = 1;        
        const TOOL_SUPERCLEAN   = 2;
        const TOOL_ARCHECARDS   = 3;
        const TOOL_AUTOCOACH    = 4;
        
        const AUTOPAY_ON  = 1;        
        const AUTOPAY_OFF = 2;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'AccountsTools';

}
