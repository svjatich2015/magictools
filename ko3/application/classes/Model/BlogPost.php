<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_BlogPost extends ORM {
        
        const STATUS_ACTIVE  = 1;        
        const STATUS_DRAFT   = 2;
        const STATUS_TRASH   = 8;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'BlogPosts';

        /**
         * Связи много к одному
         */
        protected $_belongs_to = array(
            'cat' => array(
                'model' => 'BlogCat',
                'foreign_key' => 'blogCatId',
            ),
        );

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'tags' => array(
                'model' => 'BlogTag',
                'through' => 'BlogPostsTags',
                'foreign_key' => 'blogPostId',
                'far_key' => 'blogTagId',
                
            ),
            'comments' => array(
                'model' => 'BlogComment',
                'foreign_key' => 'blogPostId',            
            )
        );

}
