<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Gift extends ORM {
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Gifts';

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'items' => array(
                'model' => 'GiftItem',
                'foreign_key' => 'giftId',
            )
        );

}
