<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_DrawTemplatesCategory extends ORM {
        
        const STATUS_ENABLED  = 1;
        const STATUS_DISABLED = 2;
        const STATUS_DELETED  = 9;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'DrawTemplatesCategories';

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'templates' => array(
                'model' => 'DrawTemplate',
                'foreign_key' => 'categoryId',
            )
        );

}
