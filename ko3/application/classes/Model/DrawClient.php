<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_DrawClient extends ORM {
        
        const STATUS_ACTIVE  = 1;
        const STATUS_DELETED = 10;
        
        const THEMES_MAX_CNT = 50;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'DrawClients';

        /**
         * Связи один к одному
         */
        protected $_has_one = array(
            'schedule' => array(
                'model' => 'AutodrawClientSchedule',
                'foreign_key' => 'clientId',
            ),
        );

        /**
         * Связи много к одному
         */
        protected $_belongs_to = array(
            'account' => array(
                'model' => 'Account',
                'foreign_key' => 'accountId',
            ),
        );

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'themes' => array(
                'model' => 'DrawClientTheme',
                'foreign_key' => 'clientId',
            ),
            'templates' => array(
                'model' => 'DrawTemplate',
                'through' => 'DrawClientsTemplates',
                'foreign_key' => 'clientId',
                'far_key' => 'templateId',
                
            )
        );

}
