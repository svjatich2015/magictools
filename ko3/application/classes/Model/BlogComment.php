<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_BlogComment extends ORM {
        
        const STATUS_ACTIVE   = 1;
        const STATUS_BANED    = 8;
        const STATUS_DELETED  = 9;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'BlogComments';

        /**
         * Связи много к одному
         */
        protected $_belongs_to = array(
            'account' => array(
                'model' => 'Account',
                'foreign_key' => 'accountId',
            ),
            'profile' => array(
                'model' => 'Profile',
                'foreign_key' => 'profileId',
            ),
            'post' => array(
                'model' => 'BlogPost',
                'foreign_key' => 'blogPostId',
            ),
        );
}
