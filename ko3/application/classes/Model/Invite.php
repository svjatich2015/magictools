<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Invite extends ORM {
        
        const STATUS_NEW  = 1;
        const STATUS_SEND = 2;
        const STATUS_USED = 9;

        /**
         * Имя таблицы
         */
        protected $_table_name = 'Invites';

}
