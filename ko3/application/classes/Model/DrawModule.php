<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_DrawModule extends ORM {
        
        const STATUS_ENABLED  = 1;
        const STATUS_DISABLED = 2;
        const STATUS_DELETED  = 9;
        
        const REGISTER = [1 => 'Birthday', 2 => 'Newyear', 3 => 'Life'];
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'DrawModules';
}
