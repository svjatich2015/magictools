<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Autodraw extends ORM {
        
        const QUEUE_NAME = 'MagicToolsAutodraw';        
        const QUEUE_STATUS_NAME = 'MagicToolsAutodrawStatusChange';
        
        const STATUS_COMPLETE  = 1;
        const STATUS_PROCESSED = 2;
        const STATUS_WAITING   = 3;
        
        const TYPE_FAST   = 1;
        const TYPE_CLIENT = 2;
        const TYPE_TPL    = 3;
        const TYPE_MODULE = 4;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Autodraw';

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'themes' => array(
                'model' => 'AutodrawTheme',
                'foreign_key' => 'autodrawId',
            )
        );

}
