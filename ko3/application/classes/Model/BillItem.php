<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_BillItem extends ORM {
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'BillItems';

        /**
         * Связи много к одному
         */
        protected $_belongs_to = array(
            'accountTool' => array(
                'model' => 'AccountsTool',
                'foreign_key' => 'accountToolId',
            )
        );
}
