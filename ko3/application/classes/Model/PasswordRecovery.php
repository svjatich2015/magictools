<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_PasswordRecovery extends ORM {
        
        const STATUS_ACTIVE  = 1;
        const STATUS_USED = 2;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'PasswordRecoveries';
        
}
