<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_BlogCat extends ORM {
        
        const STATUS_ACTIVE   = 1;
        const STATUS_DELETED  = 9;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'BlogCats';

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'posts' => array(
                'model' => 'BlogPost',
                'foreign_key' => 'blogCatId',            
            )
        );

}
