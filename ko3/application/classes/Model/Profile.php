<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Profile extends ORM {
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Profiles';

        /**
         * Связи-принадлежности (многие к одному)
         */
        protected $_belongs_to = array(
            'account' => array(
                'model' => 'Account',
                'foreign_key' => 'accountId',
            ),
        );

}
