<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Staff extends ORM {
        
        const STATUS_GOD = 1;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Staff';

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'permissions' => array(
                'model' => 'Permission',
                'through' => 'StaffPermissions',
                'foreign_key' => 'staffId',
                'far_key' => 'permissionId',
                
            )
        );

}
