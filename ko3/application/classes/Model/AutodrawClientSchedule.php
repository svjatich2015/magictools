<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_AutodrawClientSchedule extends ORM {
        
        const PERIOD_HOURLY = 1;
        const PERIOD_DAILY  = 2;
        const PERIOD_WEEKLY = 3;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'AutodrawClientSchedules';

}
