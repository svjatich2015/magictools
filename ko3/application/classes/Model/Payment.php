<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Payment extends ORM {
        
        const TYPE_MANUAL_PAY   = 1;
        const TYPE_CRYPTOCLOUD  = 6;
        const TYPE_PRODAMUS     = 7;
        const TYPE_YANDEX_CARD  = 8;
        const TYPE_YANDEX_MONEY = 9;
        
        const NOT_ACCEPTED = 0;
        const ACCEPTED     = 1;
        const CANCELED     = 9;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Payments';
        
        

}
