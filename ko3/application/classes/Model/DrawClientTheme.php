<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_DrawClientTheme extends ORM {
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'DrawClientThemes';

        /**
         * Связи много к одному
         */
        protected $_belongs_to = array(
            'client' => array(
                'model' => 'DrawClient',
                'foreign_key' => 'clientId',
            ),
        );

}
