<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_DrawTemplate extends ORM {
        
        const STATUS_ENABLED  = 1;
        const STATUS_DISABLED = 2;
        const STATUS_DELETED  = 9;
        
        const TYPE_SIMPLE     = 1;
        const TYPE_0_100      = 2;
        const TYPE_USER_0_100 = 3;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'DrawTemplates';

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'themes' => array(
                'model' => 'DrawTheme',
                'through' => 'DrawTemplatesThemes',
                'foreign_key' => 'templateId',
                'far_key' => 'themeId',
                
            )
        );

}
