<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_SuperClean extends ORM {
        
        const QUEUE_NAME = 'MagicToolsSuperClean';
        
        const STATUS_COMPLETE  = 1;
        const STATUS_PROCESSED = 2;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'SuperClean';
}
