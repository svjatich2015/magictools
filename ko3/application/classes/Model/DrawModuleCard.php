<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_DrawModuleCard extends ORM {
        
        const STATUS_ENABLED  = 1;
        const STATUS_DISABLED = 2;
        const STATUS_DELETED  = 9;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'DrawModuleCards';
}
