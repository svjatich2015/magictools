<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_SuperCleanSlots extends ORM {
        
        const STATUS_ACTIVE = 1;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'SuperCleanSlots';

}
