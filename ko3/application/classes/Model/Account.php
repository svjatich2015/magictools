<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Account extends ORM {
        
        const STATUS_ADMIN      = 0;        
        const STATUS_USER       = 1;
        const STATUS_BLOCKED    = 90;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Accounts';

        /**
         * Связи один к одному
         */
        protected $_has_one = array(
            'profile' => array(
                'model' => 'Profile',
                'foreign_key' => 'accountId',
            ),
        );

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'clients' => array(
                'model' => 'DrawClient',
                'foreign_key' => 'accountId',
            ),
            'payments' => array(
                'model' => 'Payment',
                'foreign_key' => 'accountId',
            ),
            'recoveries' => array(
                'model' => 'PasswordRecovery',
                'foreign_key' => 'accountId',
            ),
            'services' => array(
                'model' => 'AccountsTool',
                'foreign_key' => 'accountId'
                
            ),
            'tools' => array(
                'model' => 'Tool',
                'through' => 'AccountsTool',
                'foreign_key' => 'accountId',
                'far_key' => 'toolId',
                
            ),
            'toolsRemove' => array(
                'model' => 'Tool',
                'through' => 'AccountsToolsRemove',
                'foreign_key' => 'accountId',
                'far_key' => 'toolId',
                
            )
        );

}
