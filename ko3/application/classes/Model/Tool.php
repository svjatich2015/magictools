<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Tool extends ORM {
        
        const AUTODRAW   = 1;
        const SUPERCLEAN = 2;
        const ARCHECARDS = 3;
        const AUTOCOACH  = 4;
        
        const STATUS_PAID    = 1;        
        const STATUS_FREE    = 2;
        const STATUS_OFF     = 8;
        const STATUS_DELETED = 9;
        
        public static $list = [
            1 => 'Рисовалка и авторисовалка',
            2 => 'Суперчистка',
            3 => 'Архетип-карты',
            4 => 'Автокоуч'
        ];


        /**
         * Имя таблицы
         */
        protected $_table_name = 'Tools';

}
