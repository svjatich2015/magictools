<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Bill extends ORM {
        
        const STATUS_NEW      = 1;        
        const STATUS_ACCEPTED = 2;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Bills';

        /**
         * Связи один ко многим и многие ко многим
         */
        protected $_has_many = array(
            'items' => array(
                'model' => 'BillItem',
                'foreign_key' => 'billId',
            )
        );
}
