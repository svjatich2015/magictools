<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  ORM 
 */
class Model_Autocoach extends ORM {
        
        const STATUS_ACTIVE  = 1;
        const STATUS_PAUSED = 2;
        
        // Стоимость одного экземпляра Автокоуча в месяц
        const TOOL_INSTANCE_FEE = 100;
        
        /**
         * Имя таблицы
         */
        protected $_table_name = 'Autocoach';
}
