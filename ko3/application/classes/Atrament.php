<?php

class Atrament {
	
	public $canvas = DOCROOT .'/assets/sketcher.png';
	
	private $_image;
	private $_draw;
		
	private $_smoothing = 0;
	private $_weight = 0.2;	
	private $_thickness = 0.2;
	private $_opacity = 0.8;
	private $_minOpacity = 0.7;
	private $_maxOpacity = 0.9;
	private $_targetThickness = 0.2;
	private $_maxWeight = 0.5;
	
	private $_px;
	private $_py;
	
	public function __construct($name)
        {
		$this->_image = new \Imagick($this->canvas); 
		
		$this->_draw = new \ImagickDraw();
		$this->_draw->setFontFamily('Carlito'); // Шрифт
		$this->_draw->setFontSize(24); // Размер шрифта
		$this->_draw->setFillColor(new \ImagickPixel('#000')); // Цвет текста
		$this->_draw->setFontWeight(600); // Жирность текста
		$this->_draw->setTextAlignment(\Imagick::ALIGN_CENTER); // Выравнивание
		$this->_draw->setStrokeWidth(3);
		$this->_draw->annotation(200, 208, $name);
	}
	
	public function draw($pseudoTimeout, $strokeColor)
        {         
                try 
                {
                        $this->_smoothing = mt_rand(0.11, 0.99);

                        $this->_px = mt_rand(1, 400);
                        $this->_py = mt_rand(1, 400);

                        $this->_draw->setStrokeColor($strokeColor);
                        $this->_draw->setStrokeWidth($this->_thickness);
                        $this->_draw->setFillColor('transparent'); // Цвет текста
                        $this->_draw->setStrokeOpacity($this->_opacity);
                        $this->_draw->setStrokeAntiAlias(TRUE);

                        for($i = 0; $i < rand($pseudoTimeout, $pseudoTimeout * 10) * rand(1, 3); $i++) {	
                                $points = $this->_stroke();			
                                $this->_draw->bezier($points);
                        }
                } 
                catch (Exception $e) 
                {
                        Kohana::$log->add(Log::WARNING, 
                                'ImagickDraw error: "'.$e->getMessage().'"');
                }
	}
	
	public function getImage()
        {
		return $this->_image;
	}
	
	public function render()
        {
		return $this->_image->drawImage($this->_draw);
	}
	
	private function _stroke()
        {
		$randXY = $this->_mathRandXY();
                $x = $randXY[0];
                $y = $randXY[1];
		
		//calculate distance from previous point
		$rawDist = $this->_lineDistance($x, $y, $this->_px, $this->_py);		
		//now, here we scale the initial _smoothing factor by the raw distance
		//this means that when the mouse moves fast, there is more _smoothing
		//and when we're drawing small detailed stuff, we have more control
		//also we hard clip at 1
		$_smoothing = ($this->_smoothing > 0) ? ($this->_mathRand() * 0.33 + 0.66) : 0;
		//now, here we scale the initial smoothing factor by the raw distance
		//this means that when the mouse moves fast, there is more smoothing
		//and when we're drawing small detailed stuff, we have more control
		//also we hard clip at 1
		$_smoothingFactor = min(0.87, $_smoothing + ($rawDist - 60) / 3000);
		//calculate smoothed coordinates
		$mouseX = $x - ($x - $this->_px) * $_smoothingFactor;
		$mouseY = $y - ($y - $this->_py) * $_smoothingFactor;
		//recalculate distance from previous point, this time relative to the smoothed coords
		$dist = $this->_lineDistance($mouseX, $mouseY, $this->_px, $this->_py);
		//calculate target thickness based on the new distance
		$this->_targetThickness = ($dist - 1) / (50 - 1) * ($this->_maxWeight - $this->_weight) + $this->_weight;
		//approach the target gradually
		if ($this->_thickness > $this->_targetThickness) 
                {
			$this->_thickness -= 0.25;
			$this->_opacity = ($this->_opacity > $this->_minOpacity) ? $this->_opacity - 0.1 : $this->_opacity;
		} 
                else if ($this->_thickness < $this->_targetThickness) 
                {
			$this->_thickness += 0.25;
			$this->_opacity = ($this->_opacity < $this->_maxOpacity) ? $this->_opacity + 0.1 : $this->_opacity;
		}
		
		$this->_draw->setStrokeWidth($this->_thickness);
		$this->_draw->setStrokeOpacity($this->_opacity);
		
		$points = array(array('x' => $this->_px, 'y' => $this->_py));
	
		for($i = 1; $i < 4; $i++) 
                {			
			$points[] = array('x' => $x, 'y' => $y);
			$this->_px = $x;
			$this->_py = $y;
			
			$randXY = $this->_mathRandXY();
                        $x = $randXY[0];
                        $y = $randXY[1];
		}
		
		return $points;
	}
	
	private function _mathRandXY() 
        {
		$minX = ($this->_px > 100) ? $this->_px - 100 : 0;
		$maxX = ($this->_px < 300) ? $this->_px + 100 : 400;
		$minY = ($this->_py > 100) ? $this->_py - 100 : 0;
		$maxY = ($this->_py < 300) ? $this->_py + 100 : 400;
                
                return array(rand($minX, $maxX), rand($minY, $maxY));
	}
	
	private function _lineDistance($x1, $y1, $x2, $y2) 
        {
		$xs = pow($x2 - $x1, 2);
		$ys = pow($y2 - $y1, 2);
		return sqrt($xs + $ys);	
	}
	
	private function _mathRand($min = 0, $max = 32768) 
        {
		return (float) rand($min, $max) / (float) getrandmax();
	}
}