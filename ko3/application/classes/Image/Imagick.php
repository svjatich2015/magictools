<?php

class Image_Imagick extends Kohana_Image_Imagick {
        
        public function __construct($file) 
        {
                // Set using Image Driver
                Image::$_driver = 'Imagick';
                
                parent::__construct($file);
        }

        protected function _do_scale($width, $height, $bgRgb)
	{
                extract(Arr::extract($bgRgb, array('r', 'g', 'b')));
                
		// Create a RGB color for the background
		$color = sprintf('rgb(%d, %d, %d)', $r, $g, $b);
                
                $x = ($width > $this->width) ? round(($width - $this->width) / 2) : 0;
                $y = ($height > $this->height) ? round(($height - $this->height) / 2) : 0;

		// Create a new image for the background
		$background = new Imagick;
		$background->newImage($width, $height, new ImagickPixel($color));

		if ( ! $background->getImageAlphaChannel())
		{
			// Force the image to have an alpha channel
			$background->setImageAlphaChannel(Imagick::ALPHACHANNEL_SET);
		}

		// Clear the background image
		$background->setImageBackgroundColor(new ImagickPixel('transparent'));

		// Match the colorspace between the two images before compositing
		$background->setColorspace($this->im->getColorspace());

		if ($background->compositeImage($this->im, imagick::COMPOSITE_IN, $x, $y))
		{
			// Replace the current image with the new image
			$this->im = $background;
                        
			// Reset the width and height
			$this->width = $this->im->getImageWidth();
			$this->height = $this->im->getImageHeight();

			return TRUE;
		}

		return FALSE;
	}

	protected function _do_over(Image $image, $offset_x, $offset_y, $opacity)
	{
		// Convert the Image intance into an Imagick instance
		$watermark = new Imagick;
		$watermark->readImageBlob($image->render(), $image->file);

		if ($watermark->getImageAlphaChannel() !== Imagick::ALPHACHANNEL_ACTIVATE)
		{
			// Force the image to have an alpha channel
			$watermark->setImageAlphaChannel(Imagick::ALPHACHANNEL_OPAQUE);
		}

		if ($opacity < 100)
		{
			// NOTE: Using setImageOpacity will destroy current alpha channels!
			$watermark->evaluateImage(Imagick::EVALUATE_MULTIPLY, $opacity / 100, Imagick::CHANNEL_ALPHA);
		}

		// Match the colorspace between the two images before compositing
		// $watermark->setColorspace($this->im->getColorspace());

		// Apply the watermark to the image
		return $this->im->compositeImage($watermark, Imagick::COMPOSITE_IN, $offset_x, $offset_y);
	}

        protected function _do_text($text, $css, $offset_x, $offset_y, $gravity)
	{
                $image = new Imagick();
                
                if(is_array($css['color']))
                {
                        $css['color'] = $this->_get_rgbcolor_from_array($css['color']);
                }
                
                if(isset($css['background']) && is_array($css['background']))
                {
                        $css['background'] = $this->_get_rgbcolor_from_array($css['background']);
                }
                
                // Create a new drawing palette
                $draw = new \ImagickDraw();

                // Set font properties
                $draw->setFont($css['font-family']);
                $draw->setFontSize(intval($css['font-size']));
                $draw->setFillColor(new \ImagickPixel($css['color']));
                
                if(isset($css['background']))
                {
                        $draw->setTextUnderColor(new \ImagickPixel($css['background']));
                }

                // Position text of the image
                $draw->setGravity($gravity);

                // Draw text on the image
                return $this->im->annotateImage($draw, $offset_x, $offset_y, 0, $text);
	}

	protected function _get_rgbcolor_from_array(array $color)
	{
		if(! isset($color['r']) || ! isset($color['g']) || ! isset($color['b']))
                {
			throw new Kohana_Exception('Array is not valid RGB.');
                }
                
                extract(Arr::extract($css['color'], array('r', 'g', 'b')));

                // Create a RGB color
                return sprintf('rgb(%d, %d, %d)', $r, $g, $b);
	}

	/**
	 * Get the image type and format for an extension.
	 *
	 * @param   string  $extension  image extension: png, jpg, etc
	 * @return  string  IMAGETYPE_* constant
	 * @throws  Kohana_Exception
	 */
	protected function _get_imagetype($extension)
	{
		// Normalize the extension to a format
		$format = strtolower($extension);

		switch ($format)
		{
			case 'jpg':
			case 'jpe':
			case 'jpeg':
				$type = IMAGETYPE_JPEG;
			break;
			case 'gif':
				$type = IMAGETYPE_GIF;
			break;
			case 'png':
				$type = IMAGETYPE_PNG;
			break;
			case 'tif':
			case 'tiff':
				$type = IMAGETYPE_TIFF_II;
			break;
			case 'webp':
				$type = SELF::IMAGETYPE_WEBP;
			break;
			default:
				throw new Kohana_Exception('Installed ImageMagick does not support :type images',
					array(':type' => $extension));
			break;
		}

		return [$format, $type];
	}
}
