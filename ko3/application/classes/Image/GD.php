<?php

class Image_GD extends Kohana_Image_GD {
        
        public function __construct($file) 
        {
                // Set using Image Driver
                Image::$_driver = 'GD';
                
                parent::__construct($file);
        }        
}
