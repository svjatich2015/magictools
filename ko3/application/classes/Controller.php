<?php defined('SYSPATH') OR die('No direct script access.');

abstract class Controller extends Kohana_Controller {

	/**
	 * @var  boolean  auto change action via method
	 **/
	public $actions_via_method = TRUE;

	/**
	 * Executes the given action and calls the [Controller::before] and [Controller::after] methods.
	 *
	 * Can also be used to catch exceptions from actions in a single place.
	 *
	 * 1. Before the controller action is called, the [Controller::before] method
	 * will be called.
	 * 2. Next the controller action will be called.
	 * 3. After the controller action is called, the [Controller::after] method
	 * will be called.
	 *
	 * @throws  HTTP_Exception_404
	 * @return  Response
	 */
	public function execute()
	{
		// Execute the "before action" method
		$this->before();

		// Determine the action to use
		$action = 'action_'.$this->request->action();

		if($this->actions_via_method)
                {
                        $methods = array('POST', 'GET', 'PUT', 'PATCH', 'DELETE');
                        
                        if(isset($_REQUEST['_method']) AND 
                                in_array(strtoupper($_REQUEST['_method']), $methods))
                        {
                                $this->request->method($_REQUEST['_method']);
                        }
                        
                        $_action = strtolower($this->request->method()) . '_' . $action;
                        
                        if(method_exists($this, $_action))
                        {
                                $action = $_action;
                        }
                }

		// If the action doesn't exist, it's a 404
		if ( ! method_exists($this, $action))
		{
			throw HTTP_Exception::factory(404,
				'The requested URL :uri was not found on this server.',
				array(':uri' => $this->request->uri())
			)->request($this->request);
		}

		// Execute the action itself
		$this->{$action}();

		// Execute the "after action" method
		$this->after();

		// Return the response
		return $this->response;
	}
}
