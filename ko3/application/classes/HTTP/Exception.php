<?php

class HTTP_Exception extends Kohana_HTTP_Exception {

	/**
	 * Generate a Response for the current Exception
	 *
	 * @uses   Kohana_Exception::response()
	 * @return Response
	 */
	public function get_response()
	{
                $code = (isset($this->_code) && $this->_code === 404) ? 404 : 500;
                
                if (Kohana::$environment == Kohana::PRODUCTION)
                {
                        $response = Response::factory()
                                ->status($code)
                                ->body(View::factory('templates/error')->set('code', $code));
                        
                        return $response;
                }
                
                return Kohana_Exception::response($this);
	}
        
}
