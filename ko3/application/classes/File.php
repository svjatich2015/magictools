<?php defined('SYSPATH') OR die('No direct script access.');

class File extends Kohana_File {

	public static function scandir($directory)
	{
		$files = array();
                
                foreach (scandir($directory) as $file)
                {
                        if (! in_array($file, array('.', '..')))
                        {
                                $files[] = $file;
                        }
                }
                
                return $files;
	}
}
