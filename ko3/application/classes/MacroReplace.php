<?php defined('SYSPATH') or die('No direct script access.');
/**
 *   Хелпер, позволяющий реплейсить макросы даты и времени
 * 
 * @category  Helper
 * @author    Svjat Maslov
 */
class MacroReplace {
        
        private $string;
        
        private $map = array(
            'TODAY'         => '_getTodayStr',
            'TOMORROW'      => '_getTomorrowStr',
            'TOMORROW_FULL' => '_getTomorrowFullStr',
            'YEAR'          => '_getYearStr'
        );

        public function __construct($string)
        {
                $this->string = $string;
                $this->_replace();
        }
        
        private function _replace()
        {
                foreach ($this->map as $name => $func)
                {
                        $this->string = str_replace('{'. $name .'}', $this->$func(), $this->string);
                }
        }

        private function _getTodayStr()
        {
                return date('j') . ' ' . Date::genetiveRusMonth();
        }

        private function _getTodayFullStr()
        {
                return Date::genetiveRusDateByUT();
        }

        private function _getTomorrowStr()
        {
                $ut = time() + 86400;
                $monthNum = intval(date('n', $ut));
                return date('j', $ut) . ' ' . Date::genetiveRusMonth($monthNum);
        }

        private function _getTomorrowFullStr()
        {
                $ut = time() + 86400;
                return Date::genetiveRusDateByUT($ut);
        }

        private function _getYearStr()
        {
                return date('Y');
        }

        public function render()
        {
                return $this->string;
        }

}
