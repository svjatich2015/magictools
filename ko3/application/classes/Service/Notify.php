<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Notify extends Abstract_Service {
        
        public function createRaw($letter)
        {                                
                file_put_contents($this->_getNewEmailRawFilePath('raw_notify'), serialize($letter));
        }
        
        private function _getNewEmailRawFilePath($emailRawDir = 'raw')
        {                
                do
                {
                        $taskPath = APPPATH . '/emails/' . $emailRawDir . '/' . microtime(true);
                }
                while(file_exists($taskPath));
                
                return $taskPath;
        }
}
