<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Dir extends Abstract_Service {

        public function create($dir)
        {
                if($dir == '' || $dir == '/')
                {
                        return TRUE;
                }
                
                if(substr($dir, -1) == '/')
                {
                        $dir = substr($dir, 0, -1);
                }
                
                $parent = substr($dir, 0, strrpos($dir, '/'));
                
                if(! is_dir($parent) && ! $this->create($parent))
                {
                        return FALSE;
                }
                
                if(! is_dir($dir))
                {
                        @shell_exec('mkdir ' . $dir);
                }
                
                @shell_exec('chmod 0777 ' . $dir);
                
                return TRUE;
        }

        public function scan($dir, $default = array())
        {
                if(! is_dir($dir))
                {
                        return $default;
                }
                
                return array_diff(scandir($dir), array('.', '..'));
        }

        public function perm($file)
        {
                if(! is_file($file) && ! is_dir($file))
                {
                        return FALSE;
                }
                
                return intval(substr(sprintf('%o', fileperms($file)), -3));
        }
        
        public function createByAccount($accountId, $dir)
        {
                if(substr($dir, -1) !== '/')
                {
                        $dir .= '/';
                }
                
                $dir .= chunk_split(str_pad($accountId, 6, "0", STR_PAD_LEFT), 2, '/');

                if (! file_exists($dir) || ! is_dir($dir)) 
                {
                        if (! $this->create($dir)) 
                        {
                                return FALSE;
                        }
                }

                return (substr($dir, -1) !== '/') ? $dir . '/' : $dir;
        }

        public function createYMD($dir)
        {
                if(substr($dir, -1) !== '/')
                {
                        $dir .= '/';
                }
                
                $dir .= date('Y/m/d');

                if (! file_exists($dir) || ! is_dir($dir)) 
                {
                        if (! $this->create($dir)) 
                        {
                                return FALSE;
                        }
                }

                return $dir . '/';
        }

        public function deleteExpiredYMDRecursive($baseDir, $expire)
        {                
                foreach ($this->scan($baseDir) as $dir)
                {
                        $dirY = $baseDir . $dir;
                        
                        if ((int) $dir < (int) date('Y', $expire))
                        {
                                @shell_exec('rm -rf ' . $dirY);
                                continue;
                        }
                        else if ((int) $dir > (int) date('Y', $expire))
                        {
                                continue;
                        }
                        
                        foreach ($this->scan($dirY) as $dir)
                        {
                                $dirM = $dirY . '/' . $dir;
                                
                                if ((int) $dir < (int) date('n', $expire))
                                {
                                        @shell_exec('rm -rf ' . $dirM);
                                        continue;
                                }
                                else if ((int) $dir > (int) date('n', $expire))
                                {
                                        continue;
                                }

                                foreach ($this->scan($dirM) as $dir)
                                {
                                        if ((int) $dir < (int) date('j', $expire))
                                        {
                                                @shell_exec('rm -rf ' . $dirM . '/' . $dir);
                                        }
                                }
                        }
                }
        }

        public function deleteEmptyYMDRecursive($baseDir)
        {                
                foreach ($this->scan($baseDir) as $dir)
                {
                        $dirY = $baseDir . $dir;
                        
                        if(! is_dir($dirY))
                        {
                                continue;
                        }
                        
                        foreach ($this->scan($dirY) as $dir)
                        {
                                $dirM = $dirY . '/' . $dir;
                        
                                if(! is_dir($dirM))
                                {
                                        continue;
                                }

                                foreach ($this->scan($dirM) as $dir)
                                {
                                        $dirD = $dirM . '/' . $dir;
                                        
                                        if(! is_dir($dirD) || date('Y/m/d') == $dirD || count($this->scan($dirD)) > 0)
                                        {
                                                continue;
                                        }
                                        
                                        @shell_exec('rm -rf ' . $dirD);
                                }
                        
                                if (count($this->scan($dirM)) > 0)
                                {
                                        continue;
                                }
                                
                                @shell_exec('rm -rf ' . $dirM);
                        }
                        
                        if (count($this->scan($dirY)) > 0)
                        {
                                continue;
                        }
                        
                        @shell_exec('rm -rf ' . $dirY);
                }
        }
}
