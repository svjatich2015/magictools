<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Email extends Abstract_Service {
        
        public function send($to, $subject, $msg)
        {                
                $letter = array(
                    'text' => $this->_getReadyMsg($msg),
                    'encoding' => 'UTF-8',
                    'subject' => $subject,
                    'to' => $to
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
        
        private function _getNewEmailRawFilePath($emailRawDir = 'raw')
        {                
                do
                {
                        $taskPath = APPPATH . '/emails/' . $emailRawDir . '/' . microtime(true);
                }
                while(file_exists($taskPath));
                
                return $taskPath;
        }
        
        private function _getReadyMsg($msg)
        {
            if(! isset($msg['text']) || ! isset($msg['replace']))
            {
                return $msg;
            }
            
            return $this->_msgReplaceMacros($msg['text'], $msg['replace']);
        }
        
        private function _msgReplaceMacros($text, $replace)
        {
            foreach($replace as $k => $v)
            {
                $text = str_replace('{{' . $k . '}}', $v, $text);
            }
            
            return $text;
        }
}
