<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Superclean extends Abstract_Service {
        
        private $_img_types = array(
            'image/jpeg'  => 'jpg',
            'image/pjpeg' => 'jpg',
            'image/png'   => 'png',
            'image/tiff'  => 'tiff'
        );
        
        private $_tech_images = array(
            'empty'     => '/assets/superclean_empty.jpg?v=1.0.1',
            'processed' => '/assets/superclean_processed.jpg?v=1.0'
        );

        public function uploadImgBySlot($account, $slotNum, $file)
        {
                if(! $this->_isAllowedSlotByAccount($account, $slotNum) || ! $this->_isValidUploadedImg($file))
                {
                        return FALSE;
                }
                
                $slot = $this->_getSlot($account->id, $slotNum);
                $type = $this->_getUploadedImgType($file);
                
                if($slot)
                {
                        $this->_purgeImgsBySlot($slot);
                        $slot->rawFileType = $type;                       
                        $slot->tested = time();                        
                        $slot->processed = $slot->tested;                        
                        $slot->status = Model_SuperClean::STATUS_PROCESSED;
                        $slot->save();
                }
                else
                {
                        $slot = $this->_createSlot($account->id, $slotNum, $type);
                }
                
                $rawImg = $this->_saveUploadedImg($file, $slot);
                
                if(! $rawImg)
                {
                        return FALSE;
                }
                
                $queue    = Model_SuperClean::QUEUE_NAME;
                $params   = Arr::extract($slot->as_array(), array('id', 'accountId', 'slotNum', 'rawFileType'));
                $prioroty = Queue::DEFAULT_PRIORITY;
                $delay    = mt_rand(300, 600);
                
                Queue::instance()->add($queue, $params, $prioroty, $delay);
                
                return TRUE;
        }

        public function clearSlot($account, $slotNum)
        {
                $slot = $this->_getSlot($account->id, $slotNum);
                
                if($slot)
                {
                        $this->_purgeImgsBySlot($slot);
                        
                        $slot->delete();
                        
                        return TRUE;
                }
                
                return FALSE;
        }

        public function getSlotInfo($account, $slotNum)
        {
                if(! $this->_isAllowedSlotByAccount($account, $slotNum))
                {
                        return FALSE;
                }
                
                $_slot = $this->_getSlot($account->id, $slotNum);
                
                if(! $_slot)
                {
                        return FALSE;
                }
                
                $slot = array();                                
                $slot['status'] = ($_slot->status == Model_SuperClean::STATUS_PROCESSED) ? 'processed' : 'active';
                
                if($slot['status'] == 'active')
                {
                        $slot['img']       = $this-> _getImgSrc($_slot);
                        $slot['incorrect'] = $_slot->incorrect;
                        $slot['processed'] = date('d.m.Y H:i', $_slot->processed);
                }
                
                return $slot;
        }

        public function getSlotImg($slot, $public = TRUE)
        {
                return $this->_getImgSrc($slot, $public);
        }

        public function getSlotImgRaw($slot)
        {
                return $this->_getImgRaw($slot);
        }

        public function getSlotsList($account)
        {
                $accSlots = $this->_getActiveSlotsNumByAccountId($account->id);
                
                if(! $accSlots)
                {
                        return FALSE;
                }
                
                $slots = array();
                
                for ($i = 1; $i <= $accSlots; $i++)
                {
                        $slots[$i] = array('status' => 'empty', 'img' => $this->_tech_images['empty'], 'incorrect' => 0);
                }
                
                foreach (Repo::factory('SuperClean')->findAll(array('accountId', $account->id)) as $slot)
                {
                        if(! isset($slots[$slot->slotNum]))
                        {
                                continue;
                        }
                        
                        if($slot->status == Model_SuperClean::STATUS_PROCESSED)
                        {
                                $slots[$slot->slotNum]['status'] = 'processed';
                                $slots[$slot->slotNum]['img']    = $this->_tech_images['processed'];
                                continue;
                        }
                        
                        $slots[$slot->slotNum] = array(
                            'status'    => 'active',
                            'img'       => URL::site() . $this-> _getImgSrc($slot),
                            'incorrect' => $this->testIncorrectBySlot($slot),
                            'processed' => $slot->processed
                        );
                }
                
                return $slots;
        }

        public function getSlotsNumByAccounts()
        {
                $accsSettings = Repo::factory('AccountsTool')->findAll(array('toolId', Model_Tool::SUPERCLEAN));
                $slotsIdsNums = $this->_getSlotsIdsAndNumsRelation();
                
                $slotsNum = array();
                
                foreach ($accsSettings as $accSet)
                {
                        $slotsNum[$accSet->accountId] = $slotsIdsNums[$accSet->slotsId];
                }
                
                return $slotsNum;
        }
        
        private function _getSlotsIdsAndNumsRelation()
        {
                $rels = array();
                
                foreach (Repo::factory('Slots')->findAll(array('toolId', Model_Tool::SUPERCLEAN)) as $slots)
                {
                       $rels[$slots->id] = $slots->num;
                }
                
                return $rels;
        }

        public function testIncorrectRate($account, $slotNum)
        {
                // Если этот слот не пренадлежит этому аккаунту
                if(! $this->_isAllowedSlotByAccount($account, $slotNum))
                {
                        return FALSE;
                }
                
                // Грузим слот
                $_slot = $this->_getSlot($account->id, $slotNum);
                
                if(! $_slot || $_slot->status != Model_SuperClean::STATUS_COMPLETE)
                {
                        return FALSE;
                }
                
                return $this->testIncorrectBySlot($_slot);
        }

        public function testIncorrectBySlot($slot)
        {
                $tested = time();
                
                if($tested > $slot->tested + mt_rand(60, 3600))
                {
                        $minIncorrect = 0;
                        $maxIncorrect = 99;
                        
                        if($tested < $slot->tested + 600)
                        {
                                $minIncorrect = ($slot->incorrect < 10) ? 0 : $slot->incorrect - 10;
                                $maxIncorrect = ($slot->incorrect > 89) ? 99 : $slot->incorrect + 10;
                        }
                        
                        $slot->incorrect = $this->_getIncorrectRate($minIncorrect, $maxIncorrect);
                        $slot->tested = $tested;
                        $slot->save();
                }
                
                return $slot->incorrect;
        }

        private function _getActiveSlotsNumByAccountId($accountId)
        {
                $fields = array(
                    array('accountId', $accountId), 
                    array('toolId', Model_Tool::SUPERCLEAN)
                );
                $accountTool = Repo::factory('AccountsTool')->getOneByFields($fields);
                
                if(! $accountTool->loaded() || $accountTool->slotsId < 1)
                {
                        return NULL;
                }
                
                $slotsData = Repo::factory('Slots')->getOneById($accountTool->slotsId);
                
                return $slotsData->loaded() ? $slotsData->num : NULL;
        }

        public function getIncorrectRate()
        {
                return $this->_getIncorrectRate();
        }

        public function createHarmonizeImgByRaw($rawSrc, $cleanSrc)
        {
                @shell_exec('touch ' . $cleanSrc);
                
                $imgRaw = $this->_createHarmonizeImgByRaw($rawSrc);
                
                if (! $imgRaw || ! $imgRaw->save($cleanSrc))
                {
                        @shell_exec('rm ' . $cleanSrc);
                        return FALSE;
                }
                
                @shell_exec('chmod 0666 ' . $cleanSrc);                
                return TRUE;
        }

        public function renderHarmonizeImgByRaw($rawSrc, $type = 'jpg')
        {                
                $imgRaw = $this->_createHarmonizeImgByRaw($rawSrc);
                
                if(! $imgRaw)
                {
                        return FALSE;
                }
                
                return $imgRaw->render($type);
        }
        
        private function _getSlot($accountId, $slotNum)
        {
                $where = array(array('accountId', $accountId), array('slotNum', $slotNum));                
                $slot  = Repo::factory('SuperClean')->find($where);
                
                if(! $slot->loaded())
                {
                        return FALSE;
                }
                
                return $slot;
        }
        
        private function _createSlot($accountId, $slotNum, $rawFileType)
        {
                $tested   = time();
                $processed = $tested;
                $values    = compact('accountId', 'slotNum', 'rawFileType', 'tested', 'processed');
                
                return Repo::factory('SuperClean')->create($values);
        }
        
        private function _isAllowedSlotByAccount($account, $slotNum)
        {
                $fields = array(
                    array('accountId', $account->id), 
                    array('toolId', Model_Tool::SUPERCLEAN)
                );
                $tool = Repo::factory('AccountsTool')->getOneByFields($fields);
                
                if(! $tool->loaded() || $tool->slotsId < 1)
                {
                        return FALSE;
                }
                
                $accSlots = Repo::factory('Slots')->getOneById($tool->slotsId)->num;
                
                return ($slotNum > $accSlots) ? FALSE : TRUE;
        }
        
        private function _isValidUploadedImg($file)
        {
                $file['type'] = strtolower($file['type']);
                
                if(0 < $file['error'] || ! isset($this->_img_types[$file['type']]))
                {
                        return FALSE;
                }
                
                return TRUE;
        }
        
        private function _getIncorrectRate($min = 0, $max = 99)
        {
                if($min > $max)
                {
                        list($min, $max) = array($max, $min);
                }
                
                $min = ($min < 0) ? 0 : $min;
                $max = ($max > 99) ? 99 : $max;
                
                return mt_rand($min, $max);
        }

        private function _purgeImgsBySlot($slot)
        {
                $raw = $this-> _getImgRaw($slot);
                $img = $this-> _getImgSrc($slot, FALSE);
                
                if(file_exists($raw))
                {
                        @shell_exec('rm ' . $raw);
                }
                
                if(file_exists($img))
                {
                        @shell_exec('rm ' . $img);
                }
        }
        
        private function _getUploadedImgType($file)
        {
                $file['type'] = strtolower($file['type']);
                
                return isset($this->_img_types[$file['type']]) ? $this->_img_types[$file['type']] : 'jpg';
        }
        
        private function _saveUploadedImg($file, $slot)
        {
                // Создаем сразу же публичную папку юзера
                $this->_getImgSrc($slot, FALSE);
                
                return move_uploaded_file($file['tmp_name'], $this->_getImgRaw($slot));
        }

        private function _createHarmonizeImgByRaw($rawSrc)
        {
                if(!file_exists($rawSrc))
                {
                        return FALSE;
                }
                
                return $this->_getCleanImg($rawSrc);
        }
        
        private function _getCleanImg($rawSrc)
        {
                $rawImg = $this->_getRawImg($rawSrc);
                
                $cleanSrc = Conf::get_option('superclean_harmonize_img');
                $cleanImg = Image::factory($cleanSrc, 'Imagick');
                
                if($rawImg->width !== 1280 || $rawImg->height !== 720)
                {
                        $cleanImg->resize($rawImg->width, $rawImg->height);
                }
                
                return $cleanImg->over($rawImg, 0, 0, 0);
        }
        
        private function _getRawImg($rawSrc)
        {
                $rawImg = Image::factory($rawSrc, 'Imagick');
                
                if($rawImg->width <> 1280 || $rawImg->height <> 720)
                {
                        $rawImg->scale(1280, 720);
                }
                
                $created = date('d.m.Y H:i', time());
                $textCss = array(
                    'background'    => 'white', 
                    'color'         => 'red', 
                    'font-family'   => 'Carlito', 
                    'font-size'     => 42,
                    'font-weight'   => 600
                );
                $gravity = \Imagick::GRAVITY_SOUTHEAST;
                
                $rawImg->text($created, $textCss, 10, 10, $gravity);
                
                return $rawImg;
        }
        
        private function _getImgSrc($_slot, $public = TRUE)
        {
                $slot = is_array($_slot) ? $_slot : $_slot->as_array();
                $path = $public ? Conf::get_option('superclean_public_path') : Conf::get_option('superclean_dir');
                
                return $this->_getImgPath($slot, $path, 'jpg');
        }
        
        private function _getImgRaw($_slot)
        {
                $slot = is_array($_slot) ? $_slot : $_slot->as_array();
                $path = Conf::get_option('superclean_raw_dir');
                
                return $this->_getImgPath($slot, $path, $slot['rawFileType']);
        }
        
        private function _getImgPath($slot, $path, $ext)
        {
                $file = md5(Arr::get($slot, 'id') . ':=:=:' . Arr::get($slot, 'slotNum')) . '.' . $ext;
                
                return Service::factory('Dir')->createByAccount(Arr::get($slot, 'accountId'), $path) . $file;
        }
}
