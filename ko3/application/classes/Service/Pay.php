<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Pay extends Abstract_Service {
        
        public function acceptCryptocloud($data)
        {
                $payId = Arr::get($data, 'order_id');                
                $pay = $this->_getLogById($payId);
                $status = $this->_getCryptocloudInvoiceStatus(Arr::get($data, 'invoice_id'));
                
                if($pay === FALSE || $pay->type != Model_Payment::TYPE_CRYPTOCLOUD || $status != 'paid') 
                {
                        return FALSE;
                }
                
                if(is_null($pay->billId))
                {
                        // Пополняем баланс аккаунта
                        Service::factory('Account')->increaseBalance($pay->accountId, $pay->amount);
                }
                else
                {
                        // Акцептируем счет
                        Service::factory('Bill')->accept($pay->billId);
                }
                
                // Обновляем инфу о платеже в БД
                $this->_updateCryptocloudLog($pay, $data);
                
                return TRUE;
        }
        
        public function acceptManual($accountId, $payId, $amount)
        {
                $pay = Repo::factory('Payment')->getOneById($payId);                
                if (! $pay->loaded() || $pay->accountId != $accountId || 
                        $pay->amount != $amount || 
                        $pay->status != Model_Payment::NOT_ACCEPTED)
                {
                        return FALSE;
                }
                
                return $this->_acceptManual($pay);
        }
        
        public function acceptManualById($payId)
        {
                $pay = Repo::factory('Payment')->getOneById($payId);                
                if (! $pay->loaded() ||
                        $pay->status != Model_Payment::NOT_ACCEPTED)
                {
                        return FALSE;
                }
                
                return $this->_acceptManual($pay);
        }
        
        public function acceptProdamus($data)
        {
                $payId = Arr::get($data, 'order_num');                
                $pay = $this->_getLogById($payId);
                $status = trim(Arr::get($data, 'payment_status'));
                $amount = Arr::get($data, 'sum');
                
                if($pay === FALSE || $pay->type != Model_Payment::TYPE_PRODAMUS || $status != 'success') 
                {
                        return FALSE;
                }
                
                if(is_null($pay->billId))
                {
                        // Пополняем баланс аккаунта
                        Service::factory('Account')->increaseBalance($pay->accountId, $pay->amount);
                }
                else
                {
                        // Акцептируем счет
                        Service::factory('Bill')->accept($pay->billId);
                }
                
                // Обновляем инфу о платеже в БД
                $this->_updateProdamusLog($pay, $data);
                
                return TRUE;
        }
        
        public function acceptYandex($data)
        {                
                // Проверяем подпись запроса от Яшиного кошелька ;-D
                if (! $this->_confirmYandexHash($data))
                {
                        return FALSE;
                }
                
                // Параметры платежа
                $params = explode('::', $data['label']);
                if(count($params) < 3 || count($params) > 4)
                {
                    return FALSE;
                }
                
                $amount = intval($params[2]);
                $billId = isset($params[3]) ? intval($params[3]) : NULL;                
                $account = Repo::factory('Account')->getOneById(intval($params[1]), array('profile'));                
                if (! $account->loaded() || ! $this->_confirmYandexOperation($account, $data))
                {
                        return FALSE;
                }
                
                if(is_null($billId))
                {
                        // Пополняем баланс аккаунта
                        Service::factory('Account')->increaseBalance($account, $amount);
                }
                else
                {
                        // Акцептируем счет
                        Service::factory('Bill')->accept($billId);
                }
                
                // Записываем инфу о платеже в БД
                $this->_createYandexLog($account->id, $billId, $data);
                
                return TRUE;
        }
        
        public function cancelManual($accountId, $payId, $amount)
        {
                $pay = Repo::factory('Payment')->getOneById($payId);                
                if (! $pay->loaded() || $pay->accountId != $accountId || 
                        $pay->amount != $amount || 
                        $pay->status != Model_Payment::NOT_ACCEPTED)
                {
                        return FALSE;
                }
                
                return $this->_cancelManual($pay);
        }
        
        public function cancelManualById($payId)
        {                
                $pay = Repo::factory('Payment')->getOneById($payId);                
                if (! $pay->loaded() ||
                        $pay->status != Model_Payment::NOT_ACCEPTED)
                {
                        return FALSE;
                }
                
                return $this->_cancelManual($pay);
        }
        
        public function createCryptocloud($uid, $amount, $billId = NULL)
        {
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {                
                        $account = Repo::factory('Account')->getOneById($uid, array('profile'));

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                $accountId = $account->id;
                $status = Model_Payment::NOT_ACCEPTED;
                
                return $this->_createCryptocloudLog($accountId, $billId, $amount, $status);
        }
        
        public function createProdamus($uid, $amount, $method, $billId = NULL)
        {
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {                
                        $account = Repo::factory('Account')->getOneById($uid, array('profile'));

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                $accountId = $account->id;
                $status = Model_Payment::NOT_ACCEPTED;
                
                return $this->_createProdamusLog($accountId, $billId, $amount, $method, $status);
        }

        public function getCryptocloudLinkByParams($pay, $amount)
        {                
            $data = array(
                'amount'   => str_replace(',', '.', $this->_exchangeRubToUsd($amount)),
                'shop_id'  => Conf::get_option('ps.cryptocloud.shop_id'),
                'order_id' => $pay->id,
            );
            $ch = curl_init();
            $curl_opts = [
                CURLOPT_URL            => 'https://api.cryptocloud.plus/v1/invoice/create',
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => http_build_query($data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER         => false,
                CURLOPT_HTTPHEADER     => ['Authorization: Token ' . Conf::get_option('ps.cryptocloud.api_key')]
            ];
            curl_setopt_array($ch, $curl_opts);
            $output = curl_exec($ch);
            $result = json_decode($output, TRUE);
            curl_close($ch);

            if(! is_array($result) || ! isset($result['pay_url'])) die('Error');

            return $result['pay_url'];                
        }
        
        private function _getProdamusDataForLink($pay, $amount, $method)
        {
            $data = array(
                'do' => 'pay',
                'order_id' => $pay->id,
                'customer_email' => Identity::init()->getAccountInfo()->email,
                'payment_method' => $method,
                'urlReturn' => Conf::get_option('site_url') . 'payment/error',
                'urlSuccess' => Conf::get_option('site_url') . 'payment/ok',
            );            
            if(in_array($method, ['ACusd', 'ACusduk', 'ACUSDXP', 'Prodamus:xpay']))
            {
                $amount = $this->_exchangeRubToUsd($amount);
                $data['currency'] = 'usd';
            }
            else if(in_array($method, ['ACeuruk']))
            {
                $amount = $this->_exchangeRubToEur($amount);
                $data['currency'] = 'eur';
            }
            else if(in_array($method, ['ACkztjp']))
            {
                $amount = $this->_exchangeRubToKzt($amount);
                $data['currency'] = 'kzt';
            }
            $data['products'] = array($this->_getProdamusProductArr($amount, $pay->billId));
            $data['signature'] = Hmac::create($data, Conf::get_option('ps.prodamus.secret_key'));
            
            return $data;
        }

        public function getProdamusLinkByParams($pay, $amount, $method)
        {
                $data = $this->_getProdamusDataForLink($pay, $amount, $method);
                
                return sprintf('%s?%s', Conf::get_option('ps.prodamus.site'), http_build_query($data));
                
        }
        
        public function isValidParamsByHash($accountId, $payId, $amount, $hash)
        {
            $pepper = Conf::get_option('auth_pepper');
            return substr(md5($accountId.'::'.$payId.'::'.$amount.'::'.$pepper), 0, 8) == $hash;
        }
        
        public function isValidProdamusMethod($method)
        {
                return in_array($method, 
                        array('AC', 'ACeur', 'ACusd', 'Prodamus:xpay', 'ACUSDXP', 'ACkztjp', 'ACusduk', 'ACeuruk', 
                              'PC', 'sbol', 'GP', 'QW', 'WM'));
        }
        
        public function manual($uid, $amount, $method, $billId = NULL)
        {                
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {                
                        $account = Repo::factory('Account')->getOneById($uid, array('profile'));

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                // Пополняем баланс аккаунта
                Service::factory('Account')->increaseBalance($account, $amount);
                
                if ($method != 'free') {                
                        // Записываем инфу о платеже в БД
                        $this->_createManualLog($account->id, $billId, $amount, $method);
                }
                
                return TRUE;
        }
        
        public function manualConfirmation($uid, $amount, $provider, $billId = NULL)
        {
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {                
                        $account = Repo::factory('Account')->getOneById($uid, array('profile'));

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                $pay = $this->_createManualLog($account->id, $billId, $amount, $provider, Model_Payment::NOT_ACCEPTED);
                
                Service::factory('Account')->requestIncrease($account, $pay->id, $amount, $provider);
        }
        
        private function _acceptManual($pay)
        {
                $pay->status = Model_Payment::ACCEPTED;
                $pay->received = $pay->amount;
                $pay->save();
                
                if(is_null($pay->billId))
                {
                        // Пополняем баланс аккаунта
                        Service::factory('Account')->increaseBalance($pay->accountId, $pay->amount);
                }
                else
                {
                        // Акцептируем счет
                        Service::factory('Bill')->accept($pay->billId);
                }
                
                return TRUE;
        }
        
        private function _cancelManual($pay)
        {
                $pay->status = Model_Payment::CANCELED;
                $pay->received = 0;
                $pay->save();
                
                return TRUE;
        }
        
        private function _confirmYandexHash($data)
        {                
                $key = Conf::get_option('ps.yandex.key');
                
                $hash = $data['notification_type'] . '&' . 
                        $data['operation_id'] . '&' . 
                        $data['amount'] . '&643&' .  
                        $data['datetime'] . '&' . 
                        $data['sender'] . '&false&' . $key . '&' . 
                        $data['label'];
                
                return ($data['sha1_hash'] == sha1 ($hash)) ? TRUE : FALSE;
        }
        
        private function _confirmYandexOperation($account, $data)
        {
                $amount = $data['amount'] + 0;
                if ($amount < ($data['withdraw_amount'] + 0)) {
                        $amount = $data['withdraw_amount'] + 0;
                }
                
                $params = explode('::', $data['label'], 3);
                $value = intval($params[2]);
                
                if($amount < $value)
                {
                        return FALSE;
                }
                
                $yaPaymentTypes = array(Model_Payment::TYPE_YANDEX_MONEY, Model_Payment::TYPE_YANDEX_CARD);
                
                foreach($account->payments->find_all() as $payment)
                {
                        if(! in_array($payment->type, $yaPaymentTypes))
                        {
                                continue;
                        }
                        
                        $addInfo = unserialize($payment->additional);
                        if($addInfo['operation_id'] == $data['operation_id'])
                        {
                                return FALSE;
                        }
                }
                
                return TRUE;
        }
        
        private function _createCryptocloudLog($accountId, $billId, $amount, $status = Model_Payment::NOT_ACCEPTED)
        {
                $operation_id = 0;
                $datetime = gmdate('Y-m-d\TH:i:s\Z');                
                $type = Model_Payment::TYPE_CRYPTOCLOUD;

                // Доп. инфа о платеже
                $additional = serialize(compact('operation_id', 'datetime'));
                
                // Записываем инфу о платеже
                return $this->_createLog(compact('accountId', 'billId', 'type', 'amount', 'additional', 'status'));
        }
        
        private function _createLog($data)
        {
                $dataKeys = array('accountId', 'billId', 'type', 'amount', 'additional', 'status');
            
                // Создаем новую запись в БД
                $log = Repo::factory('Payment')->getNew();
                
                // Инфа о платеже
                $values = Arr::extract($data, $dataKeys);
                $values['received'] = Arr::get($data, 'received', 0);
                
                // Заносим инфу о платеже
                $log->values($values)->save();
                
                return $log;
        }
        
        private function _createManualLog($accountId, $billId, $amount, $sender, $status = Model_Payment::ACCEPTED)
        {
                $operation_id = 0;
                $datetime = gmdate('Y-m-d\TH:i:s\Z');
                $received = ($status == Model_Payment::ACCEPTED) ? $amount : 0;                
                $type = Model_Payment::TYPE_MANUAL_PAY;

                // Доп. инфа о платеже
                $additional = serialize(compact('operation_id', 'datetime', 'sender'));
                
                $values = compact('accountId', 'billId', 'type', 'amount', 'received', 'additional', 'status');
                
                // Записываем инфу о платеже
                return $this->_createLog($values);
        }
        
        private function _createProdamusLog($accountId, $billId, $amount, $method, $status = Model_Payment::NOT_ACCEPTED)
        {
                $operation_id = 0;
                $datetime = gmdate('Y-m-d\TH:i:s\Z');
                $sender = $this->_getProdamusMethodCode($method);
                
                $type = Model_Payment::TYPE_PRODAMUS;

                // Доп. инфа о платеже
                $additional = serialize(compact('operation_id', 'datetime', 'sender'));
                
                // Записываем инфу о платеже
                return $this->_createLog(compact('accountId', 'billId', 'type', 'amount', 'additional', 'status'));
        }
        
        private function _createYandexLog($accountId, $billId, array $data)
        {                     
                $type = $this->_getYandexPayType(Arr::get($data, 'notification_type'));
                $amount = Arr::get($data, 'withdraw_amount', 0);
                $received = Arr::get($data, 'amount', 0);
                $additional = serialize(Arr::extract($data, array('operation_id', 'datetime', 'sender')));
                $status = Model_Payment::ACCEPTED;
                
                $values = compact('accountId', 'billId', 'type', 'amount', 'received', 'additional', 'status');
                
                // Записываем инфу о платеже
                return $this->_createLog($values);
        }
        
        private function _exchangeRubToKzt($amount)
        {
                $rates = json_decode(file_get_contents(APPPATH . '/json/daily.json'));
                $rate = $rates->Valute->KZT->Value / 100;
                
                return round($amount / $rate, 2);
        }
        
        private function _exchangeRubToEur($amount)
        {
                $rates = json_decode(file_get_contents(APPPATH . '/json/daily.json'));
                $rate = $rates->Valute->EUR->Value;
                
                return round($amount / $rate, 2);
        }
        
        private function _exchangeRubToUsd($amount)
        {
                $rates = json_decode(file_get_contents(APPPATH . '/json/daily.json'));
                $rate = $rates->Valute->USD->Value;
                
                return round($amount / $rate, 2);
        }
        
        private function _getCryptocloudInvoiceStatus($invoiceId)
        {                
            $url = 'https://api.cryptocloud.plus/v1/invoice/info?' . 
                    http_build_query(['uuid' => $invoiceId]);
            $ch = curl_init();
            $curl_opts = [
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER         => false,
                CURLOPT_HTTPHEADER     => ['Authorization: Token ' . Conf::get_option('ps.cryptocloud.api_key')]
            ];
            curl_setopt_array($ch, $curl_opts);
            $output = curl_exec($ch);
            $result = json_decode($output, TRUE);
            curl_close($ch);

            if(! is_array($result) || $result['status'] != 'success') 
            {
                return FALSE;
            }

            return $result['status_invoice'];  
        }
        
        private function _getLogById($payId, $notCanceled = TRUE)
        {
                $pay = Repo::factory('Payment')->getOneById($payId);

                if (! $pay->loaded() || ($notCanceled && $pay->status == Model_Payment::CANCELED))
                {
                        return FALSE;
                }
                
                return $pay;
        }
        
        private function _getProdamusMethodCode($method)
        {
                $methods = array(
                    'AC'            => 'card', 
                    'ACeur'         => 'cardeur', 
                    'ACeuruk'       => 'cardeur', 
                    'ACusd'         => 'cardusd',
                    'ACusduk'       => 'cardusd',
                    'ACkztjp'       => 'cardusd',
                    'ACUSDXP'       => 'cardxpay',
                    'Prodamus:xpay' => 'cardxpay', 
                    'PC'            => 'yandexmoney', 
                    'sbol'          => 'sberonline', 
                    'GP'            => 'posterminal', 
                    'QW'            => 'qiwi', 
                    'WM'            => 'webmoney'
                );
                
                if(! isset($methods[$method]))
                {
                        die('Invalid payment method!');
                }
                
                return $methods[$method];
        }
        
        private function _getProdamusProductArr($amount, $billId = NULL)
        {
                if($billId)
                {
                    $name = 'Оплата счета #' . $billId;
                }
                else
                {
                    $name = 'MagicTools. Пополнение баланса аккаунта на ' . $amount . ' руб.';
                }
                return array(
                    'name' => $name,
                    'price' => intval($amount),
                    'quantity' => 1,
                    'paymentMethod' => 3,
                    'paymentObject' => 10
                );
        }
        
        private function _getYandexPayType($notify)
        {
                return ($notify == 'p2p-incoming') ? Model_Payment::TYPE_YANDEX_MONEY : Model_Payment::TYPE_YANDEX_CARD;
        }
        
        private function _updateCryptocloudLog($pay, $data, $status = Model_Payment::ACCEPTED)
        {
                // Доп. инфа о платеже
                $additional = unserialize($pay->additional);
                $additional['operation_id'] = Arr::get($data, 'order_id', 0);
                $additional['datetime'] = gmdate('Y-m-d\TH:i:s\Z');
                
                if($status == Model_Payment::ACCEPTED)
                {
                    $pay->received = $pay->amount;
                }
                $pay->additional = serialize($additional);
                $pay->status = $status;
                $pay->save();
                
                return TRUE;
        }
        
        private function _updateProdamusLog($pay, $data, $status = Model_Payment::ACCEPTED)
        {
                // Доп. инфа о платеже
                $additional = unserialize($pay->additional);
                $additional['operation_id'] = Arr::get($data, 'order_id', 0);
                $additional['datetime'] = gmdate('Y-m-d\TH:i:s\Z');
                
                if($status == Model_Payment::ACCEPTED)
                {
                    $sum = Arr::get($data, 'sum', 0);
                    $commission = Arr::get($data, 'commission_sum', 0);
                    $pay->received = intval($sum - $commission);
                }
                $pay->additional = serialize($additional);
                $pay->status = $status;
                $pay->save();
                
                return TRUE;
        }
}
