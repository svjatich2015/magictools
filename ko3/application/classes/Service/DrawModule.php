<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_DrawModule extends Abstract_Service {
        
        public function addCard($accountId, $moduleId, $data)
        {                
                try
                {
                        $module = $this->__getHandler($moduleId);
                        
                        return $module->createCard($accountId, $data);
                }
                catch (Exception_AutodrawModuleException $e)
                {
                        return FALSE;
                }
        }

        public function cardStatusChange($accountId, $cardId, $status)
        {
                $statuses = [
                    'activate' => Model_DrawModuleCard::STATUS_ENABLED,
                    'pause' => Model_DrawModuleCard::STATUS_DISABLED,
                    'delete' => Model_DrawModuleCard::STATUS_DELETED
                ];
                
                if(! isset($statuses[$status]))
                {
                        return FALSE;
                }
                
                $card = Repo::factory('DrawModuleCard')->find([
                    ['id', $cardId],
                    ['accountId', $accountId]
                ]);
                
                if(! $card->loaded())
                {
                        return FALSE;
                }
                
                return $card->set('status', $statuses[$status])->save();
        }

        public function getFormattedCardsAndOptionsData($accountId, $moduleId)
        {                
                try
                {
                        $module = $this->__getHandler($moduleId);
                        $cards = $module->getCardsByAccountId($accountId);
                        $fields = $module->getFields();
                        $data = $this->_generateFormattedCardsAndOptionsByFields($cards, $fields);
                        $data['options'] = $module->getOptions();
                        return $data;
                }
                catch (Exception_AutodrawModuleException $e)
                {
                        return FALSE;
                }
        }

        public function getAllActiveWithCardsCntByAccountId($accountId)
        {
                $where = ['status', Model_DrawModule::STATUS_DISABLED, '<'];
                $modules = Repo::factory('DrawModule')->findAll($where, [], ['orderNum', 'ASC']);
                $cards = Repo::factory('DrawModuleCard')->findAll([
                    ['accountId', $accountId],
                    ['status', Model_DrawModuleCard::STATUS_DELETED, '<']
                ]);
                
                $list = [];
                
                foreach ($modules as $module) 
                {
                        $list[$module->id] = [
                            'title' => $module->title,
                            'description' => $module->description,
                            'count' => 0
                        ];
                }
                
                foreach ($cards as $card) 
                {
                        if(! isset($list[$card->moduleId]))
                        {
                                continue;
                        }
                        
                        $list[$card->moduleId]['count']++;
                }
                
                return $list;
                
        }
        
        private function _generateFormattedCardsAndOptionsByFields($cards, $fields)
        {
                $result = ['cards' => [], 'length' => 0];
                
                foreach ($cards as $card) 
                {
                        $id = $card['id'];                        
                        $result['cards'][$id] = ['status' => $card['status'], 'data' => []];
                        
                        foreach ($fields as $field)
                        {
                               $result['cards'][$id]['data'][] = Arr::get($card['data'], $field);
                        }
                        
                        $result['length']++;
                }
                
                return $result;
        }


        private function __getHandler($moduleId)
        {
                $moduleId = intval($moduleId);
                $register = Model_DrawModule::REGISTER;
                
                if(! isset($register[$moduleId]))
                {
                        throw new Exception_AutodrawModuleHandlerException;
                }  
                
                return Autodraw_Module::factory($register[$moduleId]);
        }
}
