<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Bill extends Abstract_Service {
        
        public function accept($id, $sendmail = TRUE)
        {
            $bill = Repo::factory('Bill')->getOneById($id, ['items', 'items.accountTool']);            
            if(! $bill->loaded())
            {
                return FALSE;
            }  
            $items = [];
            foreach ($bill->items->find_all() as $item)
            {
                $items[] = Service::factory('Account')->prolongTool($item->accountTool, $item->value);
            }
            $bill->set('status', Model_Bill::STATUS_ACCEPTED);
            $bill->set('updated', time());
            $bill->save();
                
            if ($sendmail)
            {
                    // Отправляем поздравительное письмо
                    $this->_sendAcceptMail($bill, $items);
            }
            
            return TRUE;
        }
        
        public function create($accountId, $tools, $period)
        {
            $discount = (time() < 1677456000) ? '14%' : NULL;
            if(! $this->_isValidPeriod($period))
            {
                return FALSE;
            }
            $info = $this->_getInfoByToolsList($accountId, $tools);
            
            return $this->_create($accountId, $info, $period, $discount);
        }
        
        public function createAndAccept($accountId, $tools, $period, $sendmail = TRUE)
        {
            $discount = NULL;
            $accept = TRUE;
            if(! $this->_isValidPeriod($period))
            {
                return FALSE;
            }
            $info = $this->_getInfoByToolsList($accountId, $tools);
            $bill = $this->_create($accountId, $info, $period, $discount, $accept);
                
            if ($sendmail)
            {
                    // Отправляем поздравительное письмо
                    $this->_sendAutoAcceptMail($bill, $tools);
            }
            
            return $bill;
        }
        
        public function getById($id)
        {
            $bill = Repo::factory('Bill')->getOneById($id, ['items', 'items.accountTool']);            
            if(! $bill->loaded())
            {
                return FALSE;
            }
            
            return $bill;
        }
        
        public function updateStatus($id, $status)
        {
            $validStatuses = [Model_Bill::STATUS_NEW, Model_Bill::STATUS_ACCEPTED];
            $bill = Repo::factory('Bill')->getOneById($id);    
            
            if(! $bill->loaded() || ! in_array($status, $validStatuses));
            {
                return FALSE;
            }
            $bill->set('status', $status)->set('updated', time())->save();
            
            return TRUE;
        }
        
        private function _create($accountId, $tools, $period, $discount = NULL, $accept = FALSE)
        {
            if(count($tools['list']) < 1)
            {
                return FALSE;
            }
            $bill = $this->_createBasis($accountId, $tools, $period, $discount, $accept);
            $this->_createItems($bill, $tools, $period);
            
            return $bill;
        }
        
        private function _createBasis($accountId, $tools, $period, $discount = NULL, $accept = FALSE)
        {
            // Создаем новую запись в БД
            $bill = Repo::factory('Bill')->getNew();
            $bill->accountId = $accountId;
            $bill->amount = $tools['amount'] * $period;
            $bill->created = $bill->updated = time();
            if($discount)
            {
                $discountType = (substr($discount, -1) == '%') ? 'percent' : 'default';
                $deduction = intval($discount);
                if($discountType == 'percent')
                {
                    $deduction = ceil($bill->amount / 100 * intval($discount));
                }
                $bill->amount = $bill->amount - $deduction;
                $bill->discount = $deduction;
            }
            if($accept)
            {
                $bill->status = Model_Bill::STATUS_ACCEPTED;
            }
            $bill->save();
            
            return $bill;
        }
        
        private function _createItems($bill, $tools, $period)
        {
            foreach($tools['list'] as $tool)
            {
                // Создаем новую запись в БД
                $item = Repo::factory('BillItem')->getNew();
                $item->billId = $bill->id;
                $item->accountToolId = $tool['relId'];
                $item->value = $period;
                $item->amount = $tool['cost'] * $period;
                $item->save();
            }
        }
        
        private function _getFormattedListOfAccountToolsExpiriedByModel($model)
        {
            $tools = [];
            foreach($model as $item)
            {
                $toolId = intval($item->toolId);
                $tools[] = [
                    'title' => Model_Tool::$list[$toolId], 
                    'expiried' => Date::genetiveRusDateByUT($item->expiried)
                ];
            }
            
            return $tools;
        }
        
        private function _getFormattedListOfAccountToolsByListOfIds($accountId, $list)
        {
            $where = [['accountId', $accountId], ['toolId', $list, 'IN']]; 
            $model = Repo::factory('AccountsTool')->findAll($where);
            $tools = [];
            foreach($model as $item)
            {
                $accountId = intval($item->accountId);
                $toolId = intval($item->toolId);
                $tools[] = [
                    'title' => Model_Tool::$list[$toolId], 
                    'accountId' => $accountId, 
                    'expiried' => Date::genetiveRusDateByUT($item->expiried)
                ];
            }
            
            return $tools;
        }

        private function _getInfoByToolsList($accountId, $tools)
        {
            $info = array('amount' => 0, 'list' => array());
            $tools = $this->_transformArrayItemsToInt($tools);
            $toolsList = Service::factory('Account')->getTools($accountId)['list'];
            foreach ($toolsList as $code => $tool)
            {
                $toolId = intval($tool['id']);
                if(! in_array($toolId, $tools) || $tool['mode'] == 0)
                {
                    continue;
                }
                $info['list'][$code] = $tool;
                $info['amount'] += $tool['cost'];
            }
            
            return $info;
        }
        
        private function _isValidPeriod($period)
        {
            return in_array($period, array(1, 3, 6, 12));
        }
        
        private function _sendAcceptMail($bill, $items)
        {
            $account = Service::factory('Account')->getById($bill->accountId, TRUE);
            if(! $account)
            {
                return FALSE;
            }
            $name = $account->profile->name;
            $email = $account->email;
            $bill_id = $bill->id;
            $tools = $this->_getFormattedListOfAccountToolsExpiriedByModel($items);
            $message = View::factory('emails/txt/bill_accepted', compact('name', 'bill_id', 'tools'));
            Service::factory('Email')->send(
                [compact('name', 'email')],
                '[MagicTools] Счет оплачен!',
                $message->render()
            );
        }
        
        private function _sendAutoAcceptMail($bill, $items)
        {
            $account = Service::factory('Account')->getById($bill->accountId, TRUE);
            if(! $account)
            {
                return FALSE;
            }
            $name = $account->profile->name;
            $email = $account->email;
            $bill_id = $bill->id;
            $bill_amount = $bill->amount;
            $tools = $this->_getFormattedListOfAccountToolsByListOfIds($bill->accountId, $items);
            $message_vals = compact('name', 'bill_id', 'bill_amount', 'tools');
            $message = View::factory('emails/txt/bill_auto_accepted', $message_vals);
            Service::factory('Email')->send(
                [compact('name', 'email')],
                '[MagicTools] Счет автоматически создан и оплачен!',
                $message->render()
            );
        }
        
        private function _transformArrayItemsToInt($arr)
        {
            foreach ($arr as $k => $v)
            {
                $arr[$k] = intval($v);
            }
            
            return $arr;
        }
}
