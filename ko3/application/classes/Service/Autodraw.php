<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Autodraw extends Abstract_Service {

        public function create($title, $name, $type, array $themes = array(), $isBodies = NULL, $isScheme = NULL) 
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $type = in_array($type, array('fast', 'client', 'tpl')) ? $type : 'fast';
                $typeConst = constant('Model_Autodraw::TYPE_'.strtoupper($type));
                
                $this->__create($accountId, $title, $name, $typeConst, $themes, $isBodies, $isScheme);
        }

        public function createByClientScheduled($accountId, $clientId, $isMin = FALSE) 
        {
                $info = $this->getInfoByClient($clientId);
                        
                if(! $info)
                {
                        return FALSE;
                }
                
                extract($info);

                $info['title'] = Text::limit_chars($info['title'], 88);
                
                $this->__create($accountId, $title, $name, Model_Autodraw::TYPE_CLIENT, $themes, $isbodys, $isspheres, $isMin);
                
                return TRUE;
        }

        public function createByModuleCard($moduleName, $card, $script) 
        {
                extract(unserialize($card->data));
                
                $title = 'Модуль "' . $moduleName . '"';
                $type = Model_Autodraw::TYPE_MODULE;
                $themes = $script['themes'];
                $isbodys = $isspheres = $script['scheme'];
                $isMin = $script['minimal'];
                
                $this->__create($card->accountId, $title, $name, $type, $script['themes'], $isbodys, $isspheres, $isMin);
                
                return TRUE;
        }

        public function __create($accountId, $title, $name, $type, $themes = array(), $isBodies = NULL, $isScheme = NULL, $minimize = FALSE) 
        {
                $strainLevel = mt_rand(1, 9);
                $priority = $strainLevel + (Queue::DEFAULT_PRIORITY - 9);
                $processed = time();
                $status = Model_Autodraw::STATUS_WAITING;
                $delay = Queue::DEFAULT_DELAY;
                $ttr = Conf::get_option('autodraw.queue.ttr');
                
                $values = compact('accountId', 'title', 'name', 'type', 'isScheme', 'isBodies', 'strainLevel', 'processed', 'status');
                
                $autodraw = Repo::factory('Autodraw')->create($values);
                $autodrawId = $autodraw->id;
                
                $_autodraw = compact('autodrawId', 'name', 'strainLevel', 'delay', 'status', 'minimize');
                $_autodraw['themes'] = array();
                
                // Берем только первые 50 тем в работу
                $_themes = array_slice($themes, 0, 50);
                
                foreach (Service::factory('Draw')->getColorThemes($_themes, $isBodies, $isScheme) as $theme) {
                        $theme['autodrawId'] = $autodrawId;
                        $theme['title'] = $this->__themeFormat($theme['title']);
                        Repo::factory('AutodrawTheme')->create($theme);
                        
                        $timeout = rand(1, $strainLevel * 10);
                        
                        $_autodraw['themes'][] = array('timeout' => $timeout, 'color' => $theme['color']);                        
                        $_autodraw['delay'] = $_autodraw['delay'] + $timeout;
                }
                
                Queue::instance()->add(Model_Autodraw::QUEUE_NAME, $_autodraw, $priority, $delay, $ttr);
        }
        
        private function __themeFormat($theme)
        {
                $macro = new MacroReplace($theme);
                return Text::limit_chars($macro->render(), 88);
        }

        public function repeat($autodrawId, $accountId) 
        {
                $autodraw = Repo::factory('Autodraw')->getOneById($autodrawId);
                
                if (! $autodraw->loaded() || $autodraw->accountId != $accountId)
                {
                        return FALSE;
                }
                
                $autodraw->strainLevel = mt_rand(1, 9);
                $autodraw->status = Model_Autodraw::STATUS_WAITING;
                $autodraw->processed = time();
                $autodraw->save();
                
                extract(Arr::extract($autodraw->as_array(), array('name', 'strainLevel', 'status')));
                
                $priority = $strainLevel + (Queue::DEFAULT_PRIORITY - 9);
                $delay = Queue::DEFAULT_DELAY;
                $ttr = Conf::get_option('autodraw.queue.ttr');
                
                $_autodraw = compact('autodrawId', 'name', 'strainLevel', 'delay', 'status');
                $_autodraw['themes'] = array();
                $_autodraw['minimize'] = FALSE;
                
                foreach (Repo::factory('AutodrawTheme')->getAllByField('autodrawId', $autodraw->id) as $theme)
                {                        
                        $timeout = rand(1, $strainLevel * 10);
                        
                        $_autodraw['themes'][] = array('timeout' => $timeout, 'color' => $theme->color);                        
                        $_autodraw['delay'] = $_autodraw['delay'] + $timeout;
                }
                
                Queue::instance()->add(Model_Autodraw::QUEUE_NAME, $_autodraw, $priority, $delay, $ttr);
                
                return TRUE;
        }

        public function reanimate($autodraw) 
        {
                if ($autodraw->status != Model_Autodraw::STATUS_WAITING)
                {
                        return FALSE;
                }
                $autodraw->processed = time();
                $autodraw->save();
                
                extract(Arr::extract($autodraw->as_array(), array('name', 'strainLevel', 'status')));
                
                $priority = $strainLevel + (Queue::DEFAULT_PRIORITY - 9);
                $delay = Queue::DEFAULT_DELAY;
                $ttr = Conf::get_option('autodraw.queue.ttr');
                
                $_autodraw = compact('name', 'strainLevel', 'delay', 'status');
                $_autodraw['autodrawId'] = $autodraw->id;
                $_autodraw['themes'] = array();
                $_autodraw['minimize'] = FALSE;
                
                foreach (Repo::factory('AutodrawTheme')->getAllByField('autodrawId', $autodraw->id) as $theme)
                {                        
                        $timeout = rand(1, $strainLevel * 10);
                        
                        $_autodraw['themes'][] = array('timeout' => $timeout, 'color' => $theme->color);                        
                        $_autodraw['delay'] = $_autodraw['delay'] + $timeout;
                }
                
                Queue::instance()->add(Model_Autodraw::QUEUE_NAME, $_autodraw, $priority, $delay, $ttr);
                
                return TRUE;
        }

        public function getInfoByType($type, $entityId = NULL) 
        {       
                if (! in_array($type, array('tpl', 'client')))
                {
                        return array('title' => 'Отрисовка темы');  
                }
                
                if ($type == 'tpl') 
                {
                        $tpl = Service::factory('Draw')->getTemplate($entityId);
                        
                        if(! $tpl)
                        {                                
                                return FALSE;
                        }
                        
                        $info = array('title' => $tpl->title);
                }
                else
                {
                        $info = $this->getInfoByAccountClient($entityId);
                }

                $info['title'] = Text::limit_chars($info['title'], 88);
                
                return $info;
        }

        public function getInfoByClient($clientId) 
        {
                $info = Service::factory('Client')->getInfo($clientId);

                if(! $info)
                {                                
                        return FALSE;
                }

                return $this->__genReadyInfoByClient($info);
        }

        public function getInfoByAccountClient($clientId) 
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $info = Service::factory('Client')->getInfoByAccount($clientId, $accountId);

                if(! $info)
                {                                
                        return FALSE;
                }

                return $this->__genReadyInfoByClient($info);
        }

        public function __genReadyInfoByClient($info) 
        {
                $_themes = array();

                foreach($info['themes'] as $theme)
                {
                        $_themes[] = Text::limit_chars($theme['title'], 88);
                }

                $info['name'] = Text::limit_chars($info['name'], 42);
                $info['themes'] = $_themes;
                $info['isbodys']   = $info['isBodies'];
                $info['isspheres'] = $info['isScheme'];                                
                unset($info['isBodies']);
                unset($info['isScheme']);
                
                return $info;
        }

        public function getData($drawId, $accountId) 
        {
                $autodraw = $this->getOneByAccountWithThemes($drawId, $accountId);
                
                if (! $autodraw)
                {
                        return FALSE;
                }
                
                $autodraw['type'] = $this->__getStrType($autodraw['type']);
                $autodraw['src'] = $this->__getImgSrc($autodraw);
                
                return Arr::extract($autodraw, array('id', 'type', 'src', 'strainLevel', 'name', 'themes', 'title', 'template'));
        }
        
        public function getOneByAccount($drawId, $accountId)
        {
                $autodraw = Repo::factory('Autodraw')->getOneById($drawId);
                
                if (! $autodraw->loaded() || $autodraw->accountId != $accountId)
                {
                        return FALSE;
                }
                
                return $autodraw;
        }
        
        public function getByAccount($accountId, $complete = FALSE, $limit = NULL, $offset = NULL)
        {
                $where[] = array('accountId', $accountId);
                
                if($complete)
                {
                        $where[] = array('status', Model_Autodraw::STATUS_COMPLETE);
                }
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return Repo::factory('Autodraw')->findAll($where, array(), array('processed', 'DESC'), $params);
        }
        
        public function getOneByAccountWithThemes($drawId, $accountId)
        {
                $autodraw = Repo::factory('Autodraw')->getOneById($drawId);
                
                if (! $autodraw->loaded() || $autodraw->accountId != $accountId)
                {
                        return FALSE;
                }
                
                $_themes = array();
                
                foreach (Repo::factory('AutodrawTheme')->getAllByField('autodrawId', $autodraw->id) as $_theme)
                {
                        $_themes[] = Arr::extract($_theme->as_array(), array('title', 'color'));
                }
                
                $draw = $autodraw->as_array();
                $draw['themes'] = $_themes;
                
                return $draw;
        }

        public function deleteOneByAccount($drawId, $accountId) 
        {
                $autodraw = Repo::factory('Autodraw')->getOneById($drawId);
                
                if (! $autodraw->loaded() || $autodraw->accountId != $accountId)
                {
                        return FALSE;
                }
                
                foreach ($autodraw->themes->find_all() as $theme)
                {
                        $theme->delete();
                }
                
                $autodraw->delete();
                
                return TRUE;
        }

        public function deleteAllByPeriod($period, $accountId) 
        {
                $nowtime = time();
                $periods = array(
                    'all'   => $nowtime - 604800,
                    '5days' => $nowtime - 432000,
                    '3days' => $nowtime - 259200,
                    'day'   => $nowtime - 86400,
                    'hour'  => $nowtime - 3600
                );
                
                if(! isset($periods[$period]))
                {
                        return FALSE;
                }
                
                $where[] = array('accountId', $accountId);
                $where[] = array('processed', $periods[$period], '>');
                $where[] = array('status', Model_Autodraw::STATUS_COMPLETE);
                
                $delCnt = 0;
                
                foreach (Repo::factory('Autodraw')->findAll($where) as $autodraw)
                {
                        $this->__deleteWithThemes($autodraw);
                        $delCnt++;
                }
                
                return ($delCnt > 0);
        }
        
        private function __deleteWithThemes($autodraw)
        {                
                foreach ($autodraw->themes->find_all() as $theme)
                {
                        $theme->delete();
                }
                
                $autodraw->delete();                
        }
        
        public function countAllByAccount($accountId, $complete = FALSE)
        {
                $where[] = array('accountId', $accountId);
                
                if($complete)
                {
                        $where[] = array('status', Model_Autodraw::STATUS_COMPLETE);
                }
                
                return Repo::factory('Autodraw')->countAll($where);
        }
        
        public function updateScheduleByClient($accountId, $clientId, $periodStr, $isMin = FALSE, $duration = NULL)
        {               
                $client = Service::factory('Client')->getOneById($clientId);

                if(! $client || $client->accountId !== $accountId)
                {
                        return FALSE;
                }
                
                return Service::factory('Client')->createOrUpdateSchedule($accountId, $clientId, $periodStr, $isMin, $duration);                
        }
        
        public function getClientShedulePeriodsInfo()
        {
            return array(
                'null'       => array('period' => FALSE, 'isMin' => FALSE), 
                'hourly'     => array('period' => 'hourly', 'isMin' => FALSE), 
                'daily'      => array('period' => 'daily', 'isMin' => FALSE), 
                'weekly'     => array('period' => 'weekly', 'isMin' => FALSE)
            );
        }
        
        public function convertListObjectToArray($autodraw)
        {
                $_autodraw = array(); 
                foreach ($autodraw as $draw)
                {
                        $_draw = $draw->as_array();                
                        $_draw['date'] = date('d.m.Y - H:i:s', $_draw['processed']);
                        
                        if($_draw['type'] == Model_Autodraw::TYPE_CLIENT)
                        {
                                $_draw['title'] = 'Отрисовка клиента "' . $_draw['title'] . '"';
                        }
                        else if($_draw['type'] == Model_Autodraw::TYPE_TPL)
                        {
                                $_draw['title'] = 'Отрисовка шаблона "' . $_draw['title'] . '"';
                        }
                        
                        $_autodraw[] = Arr::extract($_draw, array('id', 'title', 'name', 'date', 'strainLevel', 'type', 'status'));
                }
                
                return $_autodraw;
        }
        
        private function __getStrType($type)
        {
                $types = array(
                    Model_Autodraw::TYPE_FAST   => 'fast', 
                    Model_Autodraw::TYPE_CLIENT => 'client',
                    Model_Autodraw::TYPE_TPL    => 'tpl'
                );
                
                return Arr::get($types, $type);
        }
        
        private function __getImgSrc($autodraw)
        {
                $worker = Arr::get($autodraw, 'worker');
                
                if(is_null($worker))
                {
                        $worker = 'default';
                }
                
                $path = Conf::get_option('autodraw.workers.' . $worker . '.public_path');
                $path .= date('Y/m/d', Arr::get($autodraw, 'processed')) . '/';
                $path .= md5(Arr::get($autodraw, 'id') . ':=:=:' . Arr::get($autodraw, 'name', '[Без имени]')) . '.png';
                
                return $path;
        }
}
