<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Draw extends Abstract_Service {
        
        private $_colors = array('blue', 'orange', 'peru', 'darkred', 'darkgreen', 'darkmagenta', 'crimson', 
                                 'darkviolet', 'darkorchid', 'indigo', 'slateblue', 'darkslateblue', 'navy', 
                                 'violet', 'royalblue', 'steelblue', 'cadetblue', 'darkolivegreen', 'olive', 
                                 'palevioletred', 'darkgoldenrod', 'purple', 'mediumorchid', 'springgreen',
                                 'darkkhaki', 'mediumvioletred', 'deeppink', 'goldenrod', 'sienna');
        
        private $_bodys = array('Физическое тело', 'Эфирное тело', 'Астральное тело', 'Ментальное тело',
                                'Каузальное тело', 'Будхиальное тело', 'Нирваническое тело', 'Восьмое тело',
                                'Девятое тело', 'Десятое тело', 'Одиннадцатое тело', 'Двенадцатое тело', 
                                'Тринадцатое тело');
        
        private $_spheres = array('Жизнь', 'Здоровье', 'Любовь/семья', 'Взаимоотношения с др.', 
                                  'Карьера/самореализация', 'Деньги/благосостояние');
        
        private $_themes_0_100 = array('Зачатие', '1-9 месяцы беременности', 'Роды', '1 год жизни', '2-5 годы жизни', 
                                       '6-10 годы жизни', '11-15 годы жизни', '16-20 годы жизни', '21-25 годы жизни',  
                                       '26-30 годы жизни', '31-35 годы жизни', '36-40 годы жизни', '41-45 годы жизни', 
                                       '46-50 годы жизни', '51-55 годы жизни', '56-60 годы жизни', '61-65 годы жизни', 
                                       '66-70 годы жизни', '71-75 годы жизни', '76-80 годы жизни', '81-85 годы жизни', 
                                       '86-90 годы жизни', '91-95 годы жизни', '96-100 годы жизни');


        public function getColorThemes($themes, $bodys = NULL, $spheres = NULL)
        {
                $draw = array();
                
                $colors = $this->_colors;
                shuffle($colors);
                
                if($spheres)
                {
                        foreach ($this->_spheres as $title)
                        {
                                $color = next($colors) ? current($colors) : reset($colors);
                                $draw[] = compact('color', 'title');
                        }
                }
                
                if($bodys)
                {
                        foreach ($this->_bodys as $title)
                        {
                                $color = next($colors) ? current($colors) : reset($colors);
                                $draw[] = compact('color', 'title');
                        }
                }
                
                foreach ($themes as $theme)
                {
                        $color = next($colors) ? current($colors) : reset($colors);
                        $title = is_object($theme) ? $theme->title : $theme;
                        $draw[] = compact('color', 'title');
                }
                
                return $draw;
        }
        
        public function getThemesArr0To100()
        {
                return $this->_themes_0_100;
        }
        
        public function getThemesArrByTpl($tpl)
        {
                $arr = array();
                
                foreach($tpl->themes->find_all() as $theme)
                {
                        $arr[] = $theme->title;
                }
                
                return $arr;
        }
        
        public function getTitlesFromColorThemes($themes)
        {
                $arr = array();
                
                foreach($themes as $theme)
                {
                        $arr[] = $theme['title'];
                }
                
                return $arr;
        }

        public function getTemplate($tplId, $deleted = FALSE)
        {                
                $tpl = Repo::factory('DrawTemplate')->getOneById($tplId);

                if(! $tpl->loaded() || (! $deleted && $tpl->status == Model_DrawTemplate::STATUS_DELETED))
                {
                        return FALSE;
                }

                return $tpl;
        }

        public function getTemplateInfo($tplId, $deleted = FALSE, $forceThemes100 = FALSE)
        {                              
                if(FALSE !== ($tpl = $this->getTemplate($tplId)))
                {
                        $info = array('title' => $tpl->title, 'type' => $tpl->type);    

                        if($tpl->type == Model_DrawTemplate::TYPE_SIMPLE)
                        {
                                $themes = $tpl->themes->find_all();

                                foreach($themes as $theme)
                                {
                                        $themeArr = $theme->as_array();
                                        $info['themes'][] = $themeArr;     
                                        $info['themes_list'][] = $themeArr['title'];                                
                                }
                        }
                        else if($forceThemes100 && Model_DrawTemplate::TYPE_0_100)
                        {
                                foreach($this->_themes_0_100 as $k => $v)
                                {
                                        $info['themes'][] = array(
                                            'id' => $k, 
                                            'title' => $v
                                        );   
                                        $info['themes_list'][] = $v;                             
                                }
                                $info['themes'] = $this->_themes_0_100;
                        }

                        return $info;
                }
                
                return FALSE;
        }
        
        public function createTemplate($title, $description, $categoryId, $type, $themes)
        {                
                $tpl = Repo::factory('DrawTemplate')->create(compact('title', 'description', 'categoryId', 'type'));
                
                if($type == Model_DrawTemplate::TYPE_SIMPLE && is_array($themes))
                {
                        $this->templateAddThemes($tpl, $themes);
                }
                
                return TRUE;
        }
        
        public function updateTemplate($tplId, $title, $description, $categoryId, $type, $themes)
        {                
                $tpl = Repo::factory('DrawTemplate')->getOneById($tplId);

                if(! $tpl->loaded())
                {
                        return FALSE;
                }
                
                if($type == Model_DrawTemplate::TYPE_0_100 && $tpl->type == Model_DrawTemplate::TYPE_SIMPLE)
                {
                        $tpl->remove('themes', NULL);
                }
                else if($type == Model_DrawTemplate::TYPE_SIMPLE && $tpl->type == Model_DrawTemplate::TYPE_0_100)
                {                        
                        $this->templateAddThemes($tpl, $themes);
                }
                else if($type == Model_DrawTemplate::TYPE_SIMPLE)
                {
                        $this->templateEditThemes($tpl, $themes);
                }
                
                $tpl->values(compact('title', 'description', 'categoryId', 'type'))->save();
                
                return TRUE;
        }
        
        public function deleteTemplate($tplId)
        {                
                $tpl = Repo::factory('DrawTemplate')->getOneById($tplId);

                if(! $tpl->loaded() || $tpl->status == Model_DrawTemplate::STATUS_DELETED)
                {
                        return FALSE;
                }
                
                $tpl->set('status', Model_DrawTemplate::STATUS_DELETED)->save();
                
                return TRUE;
        }
        
        public function templateAddThemes($tpl, array $themes)
        {
                foreach($themes as $title)
                {
                        $title = trim($title);
                        
                        if($title == '')
                        {
                                continue;
                        }
                        
                        $theme = Repo::factory('DrawTheme')->getOneByField('title', $title);

                        if(! $theme->loaded())
                        {
                                $theme = Repo::factory('DrawTheme')->create(array('title' => $title));
                        }
                        
                        $tpl->add('themes', $theme);
                }
                
                return TRUE;
        }
        
        public function templateEditThemes($tpl, array $_themes)
        {
                $themes = array_map('trim', $_themes);
                
                foreach($tpl->themes->find_all() as $theme)
                {
                        if(in_array($theme->title, $themes))
                        {
                                $key = array_search($theme->title, $themes);
                                unset($themes[$key]);
                        }
                        else
                        {
                                $tpl->remove('themes', $theme);
                        }
                }
                
                if(isset($themes))
                {
                        $this->templateAddThemes($tpl, $themes);
                }
                
                return TRUE;
        }
        
        public function templateOrderUp($tplId, $catId)
        { 
                if (! Repo::factory('DrawTemplate')->exist(array('id', $tplId)))
                {
                        return FALSE;
                }  
                
                $tpls = $this->getTemplatesByCat($catId)->as_array();
                
                if(current($tpls)->id == $tplId)
                {
                        return TRUE;
                }
                
                do
                {
                        $i = isset($i) ? $i + 1 : 1;
                        
                        $current = current($tpls);
                        
                        if($current->id == $tplId)
                        {
                                prev($tpls)->set('orderNum', $i)->save();
                                next($tpls)->set('orderNum', $i-1)->save();
                        }
                        else
                        {
                                $current->set('orderNum', $i)->save();
                        }
                }
                while(next($tpls) !== FALSE);
                
                return TRUE;
        }
        
        public function templateOrderDown($tplId, $catId)
        {
                if (! Repo::factory('DrawTemplate')->exist(array('id', $tplId)))
                {
                        return FALSE;
                } 
                
                $tpls = $this->getTemplatesByCat($catId)->as_array();
                
                if(end($tpls)->id == $tplId)
                {
                        return TRUE;
                }
                
                do
                {
                        $i = isset($i) ? $i - 1 : count($tpls);
                        
                        $current = current($tpls);
                        
                        if($current->id == $tplId)
                        {
                                next($tpls)->set('orderNum', $i)->save();
                                prev($tpls)->set('orderNum', $i+1)->save();
                        }
                        else
                        {
                                $current->set('orderNum', $i)->save();
                        }
                }
                while(prev($tpls) !== FALSE);
                
                return TRUE;
        }
        
        public function templateDisable($tplId)
        { 
                if (FALSE === ($tpl = $this->getTemplate($tplId)))
                {
                        return FALSE;
                }   
                
                $tpl->set('status', Model_DrawTemplate::STATUS_DISABLED)->save();
                
                return TRUE;
        }
        
        public function templateEnable($tplId)
        { 
                if (FALSE === ($tpl = $this->getTemplate($tplId)))
                {
                        return FALSE;
                }   
                
                $tpl->set('status', Model_DrawTemplate::STATUS_ENABLED)->save();
                
                return TRUE;
        }
        
        public function getTemplatesByCat($catId, $deleted = FALSE)
        {
                $fld[] = array('categoryId', $catId);
                $ordBy = array('orderNum', 'ASC');

                if(! $deleted)
                {
                        $fld[] = array('status', Model_DrawTemplate::STATUS_DELETED, '<');
                }

                return Repo::factory('DrawTemplate')->getAllByFields($fld, $ordBy);
        }

        public function createTemplatesCat($title, $description)
        {                
                // Удаляем лишние пробелы
                $title = trim($title);
                $description = trim($description);
                
                Repo::factory('DrawTemplatesCategory')->create(array('title' => $title, 'description' => $description));
                
                return TRUE;
        }
        
        public function getActiveTemplatesCat($catId = NULL)
        {                
                if($catId) 
                {
                        $cat = Repo::factory('DrawTemplatesCategory')->getOneById($catId);

                        if($cat->loaded() && $cat->status <= Model_DrawTemplatesCategory::STATUS_ENABLED)
                        {
                                return $cat;
                        }
                }
                else
                {
                        $fld = array('status', Model_DrawTemplatesCategory::STATUS_ENABLED, '<=');
                        $ordBy = array('orderNum', 'ASC');
                        
                        return Repo::factory('DrawTemplatesCategory')->getAllByFields($fld, $ordBy);
                }

                return FALSE;
        }
        
        public function getTemplatesCat($catId = NULL, $deleted = FALSE)
        {                
                if($catId) 
                {
                        $cat = Repo::factory('DrawTemplatesCategory')->getOneById($catId);

                        if($cat->loaded() && ($deleted || $cat->status != Model_DrawTemplatesCategory::STATUS_DELETED))
                        {
                                return $cat;
                        }
                }
                else
                {
                        $fld = array('status', Model_DrawTemplatesCategory::STATUS_DELETED, '<');
                        $ordBy = array('orderNum', 'ASC');
                        
                        if(! $deleted)
                        {
                                $cats = Repo::factory('DrawTemplatesCategory')->getAllByFields($fld, $ordBy);
                        }
                        else
                        {
                                $cats = Repo::factory('DrawTemplatesCategory')->getAll(array(), $ordBy);
                        }
                        
                        return $cats;
                }

                return FALSE;
        }
        
        public function deleteTemplatesCat($catId)
        {                
                $cat = Repo::factory('DrawTemplatesCategory')->getOneById($catId, array('templates'));

                if(! $cat->loaded() || $cat->status == Model_DrawTemplatesCategory::STATUS_DELETED)
                {
                        return FALSE;
                }
                
                foreach($cat->templates as $template)
                {
                        $template->set('status', Model_DrawTemplate::STATUS_DELETED)->save();
                }
                
                $cat->set('status', Model_DrawTemplatesCategory::STATUS_DELETED)->save();
                
                return TRUE;
        }
        
        public function templatesCatOrderUp($catId)
        { 
                if (! Repo::factory('DrawTemplatesCategory')->exist(array('id', $catId)))
                {
                        return FALSE;
                }   
                
                $cats = $this->getTemplatesCat(NULL)->as_array();
                
                if(current($cats)->id == $catId)
                {
                        return TRUE;
                }
                
                do
                {
                        $i = isset($i) ? $i + 1 : 1;
                        
                        $current = current($cats);
                        
                        if($current->id == $catId)
                        {
                                prev($cats)->set('orderNum', $i)->save();
                                next($cats)->set('orderNum', $i-1)->save();
                        }
                        else
                        {
                                $current->set('orderNum', $i)->save();
                        }
                }
                while(next($cats) !== FALSE);
                
                return TRUE;
        }
        
        public function templatesCatOrderDown($catId)
        { 
                if (! Repo::factory('DrawTemplatesCategory')->exist(array('id', $catId)))
                {
                        return FALSE;
                }   
                
                $cats = $this->getTemplatesCat(NULL)->as_array();
                
                if(end($cats)->id == $catId)
                {
                        return TRUE;
                }
                
                do
                {
                        $i = isset($i) ? $i - 1 : count($cats);
                        
                        $current = current($cats);
                        
                        if($current->id == $catId)
                        {
                                next($cats)->set('orderNum', $i)->save();
                                prev($cats)->set('orderNum', $i+1)->save();
                        }
                        else
                        {
                                $current->set('orderNum', $i)->save();
                        }
                }
                while(prev($cats) !== FALSE);
                
                return TRUE;
        }
        
        public function templatesCatDisable($catId)
        { 
                if (FALSE === ($cat = $this->getTemplatesCat($catId)))
                {
                        return FALSE;
                }   
                
                $cat->set('status', Model_DrawTemplatesCategory::STATUS_DISABLED)->save();
                
                return TRUE;
        }
        
        public function templatesCatEnable($catId)
        { 
                if (FALSE === ($cat = $this->getTemplatesCat($catId)))
                {
                        return FALSE;
                }   
                
                $cat->set('status', Model_DrawTemplatesCategory::STATUS_ENABLED)->save();
                
                return TRUE;
        }
        
        public function getTemplatesThemes($templateId = NULL)
        {                
                if(! is_null($templateId))
                {
                        return $this->getThemesByTemplateId($templateId);
                }
                
                return Repo::factory('DrawTheme')->getAll();
        }
        
        public function getThemesByTemplateId($templateId)
        {
                $template = Repo::factory('DrawTemplate')->getOneById($templateId);
                
                if(! $template->loaded())
                {
                        return array();
                }
                
                return $template->themes->find_all();
        }


        public function createTemplatesTheme($title, $description)
        {                
                // Удаляем лишние пробелы
                $title = trim($title);
                
                $theme = Repo::factory('DrawTheme')->getOneByField('title', $title);
                
                if ($theme->loaded())
                {
                        // Возвращаем код ошибки
                        return 'themeTitleExists';
                }
                
                Repo::factory('DrawTheme')->create(array('title' => $title, 'description' => $description));
                
                return TRUE;
        }
}
