<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Client extends Abstract_Service {

        public function getOneById($id) 
        {
                $client = Repo::factory('DrawClient')->getOneById($id);
                
                if (! $client->loaded())
                {
                        return FALSE;
                }
                
                return $client;
        }
        
        public function getByAccount($accountId, $active = FALSE, $limit = NULL, $offset = NULL)
        {
                $where[] = array('drawclient.accountId', $accountId);
                
                if($active)
                {
                        $where[] = array('status', Model_DrawClient::STATUS_ACTIVE);
                }
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return $this->_getAndFormatByParams($where, $params);
        }
        
        public function countAllByAccount($accountId, $active = FALSE)
        {
                $where[] = array('accountId', $accountId);
                
                if($active)
                {
                        $where[] = array('status', Model_DrawClient::STATUS_ACTIVE);
                }
                
                return Repo::factory('DrawClient')->countAll($where);
        }

        public function getInfo($id) 
        {
                $client = $this->getOneById($id);

                if(! $client || $client->status == Model_DrawClient::STATUS_DELETED)
                {
                        return FALSE;
                }
                
                return $this->__genReadyInfo($client);
        }

        public function getInfoByAccount($id, $accountId) 
        {
                $client = $this->getOneById($id);

                if(! $client || $client->accountId !== $accountId || 
                        $client->status == Model_DrawClient::STATUS_DELETED)
                {
                        return FALSE;
                }
                
                return $this->__genReadyInfo($client);
        }

        private function __genReadyInfo($entity) 
        {                
                $clientVars = array('title', 'name', 'isBodies', 'isScheme');
                $info = Arr::extract($entity->as_array(), $clientVars);
                $info['themes'] = $this->getThemesInArray($entity);  
                
                return $info;
        }

        public function create($title, $name, array $themes = array(), $isBodies = NULL, $isScheme = NULL) 
        {
                $accountId = Identity::init()->getAccountInfo()->id;

                $title = Text::limit_chars($title, 42);
                $name = Text::limit_chars($name, 42);
                
                $client = Repo::factory('DrawClient')->getNew();                
                $client->values(compact('accountId', 'title', 'name', 'isScheme', 'isBodies'))->save();
                
                if (! $client->loaded())
                {
                        return FALSE;
                }
                
                // Делаем дурацкое присвоение для краткости кода ;-)
                $clientId = $client->id;
                
                foreach($themes as $title)
                {
                        // Если пустое название темы - не учитываем его!
                        if(trim($title) == "")
                        {
                                continue;
                        }
                        
                        $title = Text::limit_chars($title, 42);
                        
                        $clientThemes = Repo::factory('DrawClientTheme')->getNew();
                        $clientThemes->values(compact('clientId', 'title'))->save();
                }
                
                return $client;
        }

        public function createByTemplate($tplId, $title, $name, array $themes = array(), $isBodies = NULL, $isScheme = NULL) 
        {
                $template = Service::factory('Draw')->getTemplateInfo($tplId, FALSE, TRUE);
                
                if(FALSE === $template)
                {
                        return FALSE;
                }
                
                if(! $title)
                {
                        $title = $template['title'];
                }
                
                
                foreach($themes as $k => $theme)
                {
                        if(! in_array($theme, $template['themes_list']))
                        {
                               unset($themes[$k]);
                        }
                }
                
                return $this->create($title, $name, $themes, $isBodies, $isScheme);
        }

        public function edit($id, $title, $name, array $themes = array(), $isBodies = NULL, $isScheme = NULL) 
        {
                $client = Repo::factory('DrawClient')->getOneById($id);
                
                if (! $client->loaded())
                {
                        return FALSE;
                }

                $title = Text::limit_chars($title, 42);
                $name = Text::limit_chars($name, 42);
                
                $client->values(compact('title', 'name', 'isScheme', 'isBodies'))->save();
                
                // Обновляем темы
                $this->_updateThemes($client->id, $client->themes->find_all(), $themes);
                
                return $client;
        }

        public function delete($id) 
        {
                $client = Repo::factory('DrawClient')->getOneById($id);
                
                if (! $client->loaded())
                {
                        return FALSE;
                }
                
                $client->set('status', Model_DrawClient::STATUS_DELETED)->save();
        }
        
        public function convertListObjectToArray($clients)
        {
                $_clients = array(); 
                foreach ($clients as $client)
                {
                        $_client = $client->as_array();                
                        $_client['themesCnt'] = $client->themes->count_all();
                        $_clients[] = Arr::extract($_client, 
                                array('id', 'title', 'name', 'isBodies', 'isScheme', 'themesCnt', 'schedule'));
                }
                
                return $_clients;
        }
        
        public function getThemesInArray($client)
        {
                return array_map(function($v){return $v->as_array();}, $client->themes->find_all()->as_array()); 
        }

        public function belongsToAccount($id, $accountId)
        {
                return Repo::factory('DrawClient')->exist(array(array('id', $id), array('accountId', $accountId)));
        }
        
        public function createOrUpdateSchedule($accountId, $clientId, $periodStr, $isMin = FALSE, $duration = NULL)
        {
                if(! in_array($periodStr, array(FALSE, 'hourly', 'daily', 'weekly')))
                {
                        return FALSE;
                }                
                $schedule = Repo::factory('AutodrawClientSchedule')->getOneByField('clientId', $clientId);
                if($periodStr === FALSE)
                {
                        return $schedule->loaded() ? $schedule->delete() : TRUE;
                }                
                $period = constant('Model_AutodrawClientSchedule::PERIOD_' . strtoupper($periodStr));
                $minimize = $isMin ? 1 : 0;
                $updated = time();       
                $start = $finish = NULL;
                $sheduleArr = compact('accountId', 'clientId', 'period', 'minimize', 'start', 'finish', 'updated');                
                if($duration)
                {
                        $sheduleArr['start'] = Arr::get($duration, 'start');
                        $sheduleArr['finish'] = Arr::get($duration, 'finish');
                }                
                if($schedule->loaded())
                {
                        return $schedule->values($sheduleArr)->update();
                }
                
                return Repo::factory('AutodrawClientSchedule')->create($sheduleArr);                
        }
        
        private function _getAndFormatByParams($where, $params)
        {
                $result = Repo::factory('DrawClient')->findAll($where, array('schedule'), array('drawclient.id', 'ASC'), $params);
                
                $clients = array(); 
                foreach ($result as $client)
                {
                        $_client = $client->as_array();  
                        $_client['schedule'] = $this->_getClientSchedule($_client['schedule']);              
                        $_client['themesCnt'] = $client->themes->count_all();
                        $clients[] = $_client;
                }
                
                return $clients;
        }
        
        private function _getClientSchedule($schedule)
        {
            $nowtime = time();
            $periods = [
                Model_AutodrawClientSchedule::PERIOD_HOURLY => 'daily',
                Model_AutodrawClientSchedule::PERIOD_DAILY  => 'daily',
                Model_AutodrawClientSchedule::PERIOD_WEEKLY => 'weekly',
            ];
            $start = date('Y-m-d', $nowtime);
            $finish = date('Y-m-d', strtotime('+1 month', $nowtime));
            $schedule['period'] = (is_null($schedule['period'])) ? 'null' : $periods[$schedule['period']];
            $switch = (is_null($schedule['start']) && is_null($schedule['finish'])) ? 'off' : 'on';  
            
            if($switch == 'on') {
                $start = date('Y-m-d', intval($schedule['start']));
                $finish = date('Y-m-d', intval($schedule['finish']));
            }
            
            $schedule['duration'] = compact('switch', 'start', 'finish');
            
            return $schedule;
        }
        
        private function _updateThemes($clientId, $dbThemes, array $themes = array())
        {
                // Избавляемся от "пустых" элементов
                $inputThemes = array_filter(array_map('trim', $themes));
                
                $themesCnt = count($inputThemes);
                $cnt = 1;                
                
                foreach ($dbThemes as $theme)
                {
                        if($cnt > $themesCnt)
                        {
                                $theme->delete();
                        }
                        else
                        {
                                $i = $cnt - 1;
                                $title = Text::limit_chars(array_shift($inputThemes), 42);
                                $theme->set('title', $title)->save();
                        }
                        
                        $cnt++;
                }
                
                foreach($inputThemes as $title)
                {
                        $title = Text::limit_chars($title, 42);
                        
                        $clientThemes = Repo::factory('DrawClientTheme')->getNew();
                        $clientThemes->values(compact('clientId', 'title'))->save();
                }    
        }
}
