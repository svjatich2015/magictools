<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Tool extends Abstract_Service {
        
        public function getAll($orderBy = "code")
        {
                if(! in_array($orderBy, ['id', 'code']))
                {
                    $orderBy = 'code';
                }
            
                $slots = array();                                
                $tools = array();                                
                $toolsInfo = $this->_getInfo();
                
                foreach (Repo::factory('Slots')->getAll() as $_slots)
                {
                        $slots[$_slots->toolId][$_slots->id] = array(
                            'num'  => $_slots->num,
                            'cost' => $_slots->monthlyCost
                        );
                }
                
                foreach (Repo::factory('Tool')->findAll(array('status', Model_Tool::STATUS_PAID)) as $tool)
                {
                        $tools[$tool->{$orderBy}] = $toolsInfo[$tool->code] + array(
                            'id'    => $tool->id,
                            'slots' => isset($slots[$tool->id]) ? $slots[$tool->id] : NULL,
                            'cost'  => $tool->monthlyCost
                        );
                }
                
                return $tools;
        }
        
        public function getArrTitlesSortById()
        {                                
                return [
                    Model_Tool::AUTODRAW => 'Рисовалка и авторисовалка',
                    Model_Tool::SUPERCLEAN => 'Суперчистка',
                    Model_Tool::ARCHECARDS => 'Архетип-карты',
                    Model_Tool::AUTOCOACH => 'Автокоуч'
                ];
        }
        
        /**
         * Возвращает массив ID аккаунтов, у которых кончились деньги на балансе
         * 
         * @return array
         */
        public function getAccountIdsByExpiriedList($toolId)
        {
                $where = array(array('expiried', time(), '<'), array('toolId', $toolId));
                $orderBy = array('expiried', 'DESC');    
                $ids = array();
                
                foreach (Repo::factory('AccountsTool')->findAll($where, array(), $orderBy) as $act)
                {
                        $ids[] = $act->accountId;
                }
                
                return $ids;
        }
        
        public function getSlotsByIdAndNum($id, $num)
        {                                
                $slotsObj = Repo::factory('Slots')->getOneByFields(
                        array(array('toolId', $id), array('num', $num)));
                if(! $slotsObj->loaded())
                {
                    return FALSE;
                }
                
                return $slotsObj;
        }
        
        private function _getInfo()
        {
                return array(
                    'archecards' => array(
                        'title'       => 'Архетип-карты',
                        'description' => 'Архетипические карты.'
                    ),
                    'autocoach' => array(
                        'title'       => 'Автокоуч',
                        'description' => 'Инструмент для достижения <u>неопределенных целей</u>, приводящих к <b>идеальному результату для вас</b>. ' .
                                         '<a href="' . URL::site('blog/post/3') . '" target="_blank">Подробнее...</a>'
                    ),
                    'draw' => array(
                        'title'       => 'Рисовалка и авторисовалка',
                        'description' => 'Услуга включает в себя инструменты "Рисовалка" и "Авторисовалка".'
                    ),
                    'superclean' => array(
                        'title'       => 'СуперЧистка',
                        'description' => 'Инструмент для информационной чистки людей и любых объектов. ' .
                                         '<a href="' . URL::site('pages/manual/5') . '" target="_blank">Подробнее...</a>'
                    )
                );
        }
}
