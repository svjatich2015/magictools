<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Account extends Abstract_Service {
        
        public function activateFreeUntilTool($accountId, $toolId, $periodDays = 7)
        {
                $accountTool = Repo::factory('AccountsTool')->getOneByFields([
                    ['accountId', $accountId],
                    ['toolId', $toolId]
                ]);
                if($accountTool->loaded())
                {
                    return FALSE;
                }
                $freeUntil = $expiried = strtotime('+' . ($periodDays + 1) . ' day midnight') - 1;
                $updated = time();
                $values = compact('accountId', 'toolId', 'freeUntil', 'expiried', 'updated');
                if($toolId == Model_Tool::SUPERCLEAN)
                {
                    $values['slotsId'] = 1;
                }
                Repo::factory('AccountsTool')->create($values);
                
                return $expiried;
        }

        /**
         * Подсчитывает количество аккаунтов
         * 
         * @param type $active
         * @return type
         */
        public function countAll($active = FALSE)
        {
                $where = array();
                
                if($active)
                {
                        $where[] = array('status', Model_Account::STATUS_BLOCKED, '<');
                }
                
                return Repo::factory('Account')->countAll($where);
        }
        
        /**
         * Возвращает модель аккаунта по ID
         * 
         * @param type $id
         * @param type $profile
         * @return boolean
         */
        public function getById($id, $profile = FALSE)
        {
                $rel = ($profile) ? array('profile') : array();
                
                $account = Repo::factory('Account')->getOneById($id, $rel);
                
                if (! $account->loaded())
                {
                        return FALSE;
                }
                
                return $account;
        }
        
        public function getByListIds(array $list) 
        {
            $accounts = array();
            foreach(Repo::factory('Account')->getAllByListId($list) as $account)
            {
                $accounts[$account->id] = $account;
            }
            return $accounts;
        }


        /**
         * Возвращает массив ID аккаунтов, у которых кончились деньги на балансе
         * 
         * @return array
         */
        public function getExpiriedIdsList()
        {
                $ids = array();
                
                foreach ($this->_getAllExpiried() as $account)
                {
                        $ids[] = $account->id;
                }
                
                return $ids;
        }
        
        /**
         * Возвращает список аккаунтов
         * 
         * @param type $active
         * @param type $profile
         * @param type $limit
         * @param type $offset
         * @return type
         */
        public function getList($active = FALSE, $profile = FALSE, $limit = NULL, $offset = NULL)
        {
                $where = array();
                $params = array();
                
                if($active)
                {
                        $where[] = array('status', Model_Account::STATUS_BLOCKED, '<');
                }
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return $this->_getArrayList($where, $params, $profile);
        }
        
        /**
         * Возвращает список аккаунтов, соответствующих фильтру
         * 
         * @param type $filter
         * @param type $active
         * @param type $profile
         * @return type
         */
        public function getListFilter($filter, $active = FALSE, $profile = FALSE)
        {
                $where = array();
                $params = array();
                
                if(is_array($filter) && isset($filter['name']) && isset($filter['value']))
                {
                        if($filter['name'] == 'name' || $filter['name'] == 'surname') {
                                return $this->_getListByProfileFilter($filter, $active);
                        }
                        
                        if($filter['name'] == 'id') 
                        {
                                $where[] = array('account.id', intval($filter['value']));
                        }
                        else if($filter['name'] == 'email') 
                        {
                                $where[] = array('email', '%' . $filter['value'] . '%', 'LIKE');
                        }
                }
                
                if($active)
                {
                        $where[] = array('status', Model_Account::STATUS_BLOCKED, '<');
                }
                
                return $this->_getArrayList($where, $params, $profile);
        }
        
        /**
         * Подсчитывает стоимость всех подключенных инструментов в месяц
         * 
         * @param type $tools
         * @return type
         */
        public function getMonthlyCost($tools)
        {
                $monthlyCost = 0;
                
                foreach($tools as $tool)
                {
                        $monthlyCost = $monthlyCost + $tool->monthlyCost;
                }
                
                return $monthlyCost;
        }

        /**
         * Логинит юзверя
         * 
         * @param Model_Account $uid
         * @param type $password
         * @return boolean
         */
        public function login($uid, $password = NULL) 
        {
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {
                        if (is_numeric($uid)) 
                        {
                                $account = Repo::factory('Account')->getOneById($uid);
                        } 
                        else 
                        {
                                $account = Repo::factory('Account')->getOneByField('email', strtolower($uid));
                        }

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                if(! is_null($password) && ! $this->_checkPassword($account, $password))
                {
                        return FALSE;
                }
                
                // Логинем
                Identity::init()->login($account->id);
                
                return TRUE;
        }

        /**
         * Разлогиневает юзверя
         * 
         * @return boolean
         */
        public function logout() 
        {                
                // Разлогиниваем
                Identity::init()->logout();
                
                return TRUE;
        }
        
        /**
         * Напоминалка пароля для забывчивых)))
         * Присылает на почту ссылку для сброса старого пароля
         * 
         * @param type $email
         * @return string
         */
        public function passwordRecovery($email)
        {
                // Переводим email в нижний регистр
                $email = strtolower($email);
                
                if (! $this->isEmailValid($email))
                {
                        // Возвращаем код ошибки
                        return 'emailInvalid';
                }
                
                $account = Repo::factory('Account')->getOneByFields(array('email', $email), array('profile'));
                
                if (! $account->loaded())
                {
                        // Возвращаем код ошибки
                        return 'emailNotExists';
                }
                
                if ($this->isDdosRecovery($account->recoveries->find_all()))
                {
                        // Возвращаем код ошибки
                        return 'ddosRecovery';
                }
                
                // Генерируем hash-код восстановления пароля
                $recovery = $this->_generateRecoveryHash($account->id);
                
                // Отправляем письмо с кодом восстановления пароля
                $this->_sendRecoveryMail($account->email, $account->profile->name, $recovery->hash, $recovery->expiried);
                
                return NULL;                
        }
        
        public function prolongTool(Model_AccountsTool $accountTool, $periodMonths)
        {
                $updated = time();
                $refPoint = ($accountTool->expiried > $updated) ? 
                        $accountTool->expiried : $updated; 
                $periodMonths = intval($periodMonths);
                switch ($periodMonths) {
                    case 12:
                        $strtotime = '+13 months';
                        break;
                    case 6:
                        $strtotime = '+6 months 10 days';
                        break;
                    case 3:
                        $strtotime = '+3 months 3 days';
                        break;
                    default:
                        $strtotime = '+1 month';
                        break;
                }
                $expiried = strtotime($strtotime, $refPoint);
                $accountTool->values(compact('updated', 'expiried'))->save();     
                
                return $accountTool;
        }
        
        /**
         * Сбрасывает старый пароль и присылает на почту новый
         * 
         * @param type $email
         * @param type $hash
         * @return string
         */
        public function resetPassword($email, $hash)
        {      
                // Переводим email в нижний регистр
                $email = strtolower($email);
                
                $recovery = Repo::factory('PasswordRecovery')->getOneByField('hash', $hash);
                
                if ($error = $this->_getRecoveryError($recovery))
                {
                        // Возвращаем код ошибки
                        return $error;
                }
                
                $account = Repo::factory('Account')->getOneById($recovery->accountId, array('profile'));
                
                if (! $account->loaded() || $account->email != $email)
                {
                        // Возвращаем код ошибки
                        return 'recoveryInvalid';
                }
                
                // Деактивируем использованый хеш для восстановления пароля
                $recovery->set('status', Model_PasswordRecovery::STATUS_USED)->save();
                
                // Отправляем письмо с новым паролем
                $this->_sendNewPasswordMail($account->email, $account->profile->name, $this->_getNewPassword($account));
                
                return NULL;                
        }
        
        /**
         * Регистрирует нового юзверя
         * 
         * @param type $data
         */
        public function registration($data) 
        {
                $_str = md5(date('D, d M Y H:i:s'));
                
                $accountData = Arr::extract($data, array('email', 'name', 'surname'));
                
                // Переводим email в нижний регистр
                $accountData['email'] = strtolower($accountData['email']);
                
                // Формируем пароль и "соль"
                $accountData['password'] = substr($_str, 0, 7);
                $accountData['passwordSalt'] = substr($_str, 7, 8);
                
                // Создаем новый аккаунт
                $account = $this->_create($accountData);
                
                // Деактивируем инвайт
                $this->_useInvite($account, $data['invite']);
                
                // Подключаем новому аккаунту сервисы
                $this->_activateFreeUntilTools($account->id);
                
                // Отправляем приветственное письмо
                $this->_sendWelcomeMail($accountData['email'], $accountData['name'], $accountData['password']);
        }
        
        private function _activateFreeUntilTools($accountId, $periodDays = 7)
        {
                $services = array(
                //  id => slotsId
                    1 => NULL,
                    2 => 1,
                    3 => NULL,
                    4 => NULL
                );
                $freeUntil = $expiried = strtotime('+' . ($periodDays + 1) . ' day midnight') - 1;
                $updated = time();
                
                foreach($services as $toolId => $slotsId)
                {
                    // Подключаем новому аккаунту сервис
                    $values = compact('accountId', 'toolId', 'freeUntil', 'expiried', 'updated');
                    if($slotsId)
                    {
                        $values['slotsId'] = $slotsId;
                    }
                    Repo::factory('AccountsTool')->create($values);
                }
        }


        /**
         * Возвращает список всех инструментов аккаунта (включенных и отключенных)
         * 
         * @param type $uid
         * @return boolean|int
         */
        public function getTools($uid)
        {
                $account = $this->_getModel($uid);

                if (! $account)
                {
                        return FALSE;
                }
                
                $accTools = array();
                $delTools = array();
                
                foreach (Repo::factory('AccountsTool')->findAll(array('accountId', $account->id)) as $accTool)
                {
                        $accTools[$accTool->toolId] = $accTool;
                }
                
                foreach ($account->toolsRemove->find_all() as $tool)
                {
                        $delTools[$tool->id] = $tool->id;
                }
                
                return $this->_getToolsArrayList($account, $accTools, $delTools);
        }
        
        public function activateTool($account, $tool, $slotsNum = NULL)
        {
                Bag::set('accountBalance', $account->balance);
                
                $this->_activateTool($account, $tool, $slotsNum);
                
                if($account->balance !== Bag::get('accountBalance'))
                {
                        $mCostArr = $this->_getMonthlyCostByTools($account);
                        $expiried = $this->_expiredCalculate(Bag::get('accountBalance'), $mCostArr, $account->expiried);
                        
                        // Обновляем срок жизни и баланс
                        $account->set('expiried', $expiried)->set('balance', Bag::get('accountBalance'))->save();
                }
        }
        
        public function switchToolAutopay($accountId, $toolId, $cond)
        {
                $cond = intval($cond);
                if(! in_array($cond, [Model_AccountsTool::AUTOPAY_ON, Model_AccountsTool::AUTOPAY_OFF]))
                {
                    return FALSE;
                }
                $accountTool = Repo::factory('AccountsTool')->getOneByFields([
                    ['accountId', $accountId],
                    ['toolId', $toolId]
                ]);
                if(! $accountTool->loaded())
                {
                    return FALSE;
                }
                
                return $accountTool->set('autopay', $cond)->save();
        }
        
        private function _generateToolInfo($account, $tool, $accTools, $delTools)
        {
                $utime = time();                
                $toolId = $tool['id'];
                $cost = $tool['cost'];                        
                if($toolId == Model_Tool::SUPERCLEAN && isset($accTools[$toolId]))
                {
                        $slotsId = $accTools[$toolId]->slotsId;                                
                        $tool['slots'][$slotsId]['active'] = TRUE; 
                        $tool['cost'] = $tool['slots'][$slotsId]['cost'];
                } 
                $tool['expiried'] = isset($accTools[$toolId]) ? $accTools[$toolId]->expiried : 0;
                $tool['freeUntil'] = isset($accTools[$toolId]) ? $accTools[$toolId]->freeUntil : 0;                        
                $tool['mode'] = Model_AccountsTool::MODE_NOT_ACTIVE;                      
                $tool['autopay'] = isset($accTools[$toolId]) ? $accTools[$toolId]->autopay : Model_AccountsTool::AUTOPAY_OFF;
                if(isset($accTools[$toolId]))
                { 
                        $tool['mode'] = ($utime < $tool['expiried'] || $utime < $tool['freeUntil']) ? 
                                Model_AccountsTool::MODE_ACTIVE : Model_AccountsTool::MODE_EXPIRIED;
                        $tool['relId'] = $accTools[$toolId]->id;
                }
                
                return $tool;
        }
        
        private function _getToolsArrayList($account, $accTools, $delTools)
        {                
                $tools = array('list' => array(), 'totalCost' => 0);
                foreach (Service::factory('Tool')->getAll() as $code => $_tool)
                {
                        $tool = $this->_generateToolInfo($account, $_tool, $accTools, $delTools);
                        $toolId = $tool['id'];
                        if($toolId == Model_Tool::SUPERCLEAN && ! isset($accTools[$toolId]))
                        {
                            $tool['cost'] = $tool['slots'][1]['cost'];
                        }
                        $tools['list'][$code] = $tool;
                        $tools['totalCost'] += ($tool['mode'] > 0) ? $tool['cost'] : 0;
                }
                
                return $tools;
        }
        
        private function _getAutocoachSlots($accountId)
        {                
                $slotsCnt = Repo::factory('Autocoach')->countAll([
                    ['accountId', $accountId], 
                    ['status', Model_Autocoach::STATUS_ACTIVE]
                ]);   
                $info = [
                    'cnt' => $slotsCnt, 
                    'active' => TRUE, 
                    'cost' => $slotsCnt * Model_Autocoach::TOOL_INSTANCE_FEE
                ];                                
                                
                return $info;
        }
        
        public function updateFreeToolSlotsById($accountId, $toolId, $slotsNum)
        {
                $nowTime = time();
                $accountTool = Repo::factory('AccountsTool')->getOneByFields([
                    ['accountId', $accountId],
                    ['toolId', $toolId]
                ]);                
                if (! $accountTool->loaded() || 
                        ($accountTool->freeUntil < $nowTime && $accountTool->expiried > $nowTime))
                {
                    return FALSE;
                }
                
                return $this->updateToolSlots($accountTool, $slotsNum);
        }
        
        public function recalculateToolExpiriedBySlotsId($accountId, $toolId, $slotsNum)
        {
                $nowTime = time();
                $accountTool = Repo::factory('AccountsTool')->getOneByFields([
                    ['accountId', $accountId],
                    ['toolId', $toolId]
                ]);                
                if (! $accountTool->loaded())
                {
                    return FALSE;
                }
                                
                return $this->updateToolSlots($accountTool, $slotsNum, TRUE);
        }
        
        /**
         * @param type $accountTool
         * @param type $action
         * @param type $value
         */
        public function updateTool(Model_AccountsTool $accountTool, $action, $value)
        {
                $action = intval($action);
                switch ($action)
                {
                    case Model_AccountsTool::ACTION_PROLONG:
                        $this->prolongTool($accountTool, $value);
                        break;
                    case Model_AccountsTool::ACTION_UPDATE_SLOTS:
                        $this->updateToolSlots($accountTool, $value);
                        break;
                }
        }
        
        public function updateToolSlots(Model_AccountsTool $accountTool, $slots, $withExpiried = FALSE)
        {
                $updated = time();
                $expiried = $accountTool->expiried;
                $toolId = $accountTool->toolId;
                $ratherSlotsObj = Service::factory('Tool')->getSlotsByIdAndNum($toolId, $slots);
                if(! $ratherSlotsObj)
                {
                    return FALSE;
                }
                if($withExpiried && $updated > $accountTool->freeUntil && $updated < $accountTool->expiried)
                {
                    $currentSlotsObj = Repo::factory('Slots')->getOneById($accountTool->slotsId);
                    $expiried = $this->_recalculateExpiriedByChangeSlotsNum(
                            $accountTool->expiried, $currentSlotsObj, $ratherSlotsObj);
                }
                $slotsId = $ratherSlotsObj->id;
                $accountTool->values(compact('updated', 'slotsId', 'expiried'))->save();
                
                return $accountTool;
        }
        
        private function _recalculateExpiriedByChangeSlotsNum(
                $expiried, 
                Model_Slots $currentSlotsObj, 
                Model_Slots $ratherSlotsObj)
        {
                $nowTime = time();
                if($nowTime > $expiried)
                {
                    return $expiried;
                }
                $difference = $expiried - $nowTime;
                return $nowTime + ceil($difference / $ratherSlotsObj->monthlyCost * $currentSlotsObj->monthlyCost);
        }

        /**
         * Обновляет список инструментов для аккаунта
         * 
         * @param Model_Account $uid
         * @param type $tools
         * @param type $slots
         * @return boolean
         */
        public function updateTools($uid, $tools, $slots)
        {                
                if(FALSE === ($account = ($uid instanceof Model_Account) ?  $uid : $this->getById($uid)))
                {
                        return FALSE;
                }
                
                $this->_updateTools($account, $tools, $slots);
                                
                return $this->getTools($account);
        }
        
        public function increaseBalance($uid, $amount, $sendmail = TRUE)
        {
                // Временный впрыск кода на время двухдневной акции
                $nowtime = time();
                $clearAmount = $amount;
                if($nowtime >= 1637960395 && $nowtime <= 1638133195)
                {
                        $amount = $clearAmount * 1.3;
                }
                
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {                
                        $account = Repo::factory('Account')->getOneById($uid, array('profile'));

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                // Сумма в копейках
                $amount = floor($amount * 100);
                
                // Обновляем баланс и сохраняемся
                $account->set('balance', $account->balance + $amount)->save();
                
                if ($sendmail)
                {
                        // Отправляем поздравительное письмо
                        $this->_sendIncreaseBalanceMail($account->email, $account->profile->name, $amount, $clearAmount);
                }
        }
        
        public function deductBalance($uid, $amount)
        {
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {                
                        $account = Repo::factory('Account')->getOneById($uid, array('profile'));

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                // Сумма в копейках
                $amount = floor($amount * 100);
                
                // Обновляем баланс и сохраняемся
                $account->set('balance', $account->balance - $amount)->save();
        }

        public function changeData($uid, $email, $password, $currentPassword = NULL)
        {
                if(FALSE === ($account = ($uid instanceof Model_Account) ?  $uid : $this->getById($uid)))
                {
                        return FALSE;
                }
                
                $currentEmail = $account->email;
                $email = strtolower($email);
                $password = trim($password);
                
                if($currentPassword !== NULL && ! $this->_checkPassword($account, $currentPassword))
                {
                        // Возвращаем код ошибки
                        return 'currentPasswordInvalid';
                }
                
                // Переводим email-адрес из БД в нижний регистр
                if(strtolower($account->email) != $email && TRUE !== ($result = $this->_changeEmail($account, $email)))
                {
                        // Возвращаем код ошибки
                        return $result;
                }
                
                if($password != "" && TRUE !== ($result = $this->_changePassword($account, $password)))
                {
                        if($email != $currentEmail)
                        {
                                $account->set('email', $currentEmail)->save();
                        }

                        // Возвращаем код ошибки
                        return $result;
                }                
                
                $this->_sendProfileUpdateMail($email, $account->profile->name, $password);
                
                return TRUE;
        }

        /**
         * Отправка запроса на получение инвайта
         * 
         * @param array $data
         * @return array
         */
        public function requestInvite($email, $name, $surname, $text)
        {
                $this->_sendRequestInviteMail($email, $name, $surname, $text);
        }
        
        /**
         * Отправка инвайта
         * 
         * @param array $data
         * @return array
         */
        public function sendInvite($email, $name, $surname)
        {
                // Переводим email в нижний регистр
                $email = strtolower($email);
                
                $invite = Repo::factory('Invite')->getOneByField('status', Model_Invite::STATUS_NEW); 
                
                if (! $invite->loaded())
                {
                        return FALSE;
                }
                
                $invite->status = Model_Invite::STATUS_SEND;
                $invite->save();                
                
                $this->_sendInviteMail($email, $name, $surname, $invite->code);
        }

        /**
         * Проверка правильности заполнения регистрационной формы
         * 
         * @param array $data
         * @return array
         */
        public function registrationValidateData($data)
        {
                if (Arr::get($data, 'agree') != 1)
                {
                        // Присваиваем код ошибки
                        $errors[] = 'noСonsent';
                }
                if (! $this->isEmailValid(Arr::get($data, 'email')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'emailInvalid';
                }
                if ($this->isEmailExists(Arr::get($data, 'email')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'emailExists';
                }
                if (! $this->isCharsStr(Arr::get($data, 'name')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'nameInvalid';
                }
                if (! $this->isCharsStr(Arr::get($data, 'surname')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'surnameInvalid';
                }
                if ($error = $this->isInvalidInvite(Arr::get($data, 'invite')))
                {
                        // Присваиваем код ошибки
                        $errors[] = $error;
                }
                
                return isset($errors) ? $errors : array();
        }

        /**
         * Проверка правильности заполнения формы запроса инвайта
         * 
         * @param array $data
         * @return array
         */
        public function inviteValidateData($data)
        {
                if (! $this->isEmailValid(Arr::get($data, 'email')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'emailInvalid';
                }
                if ($this->isEmailExists(Arr::get($data, 'email')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'emailExists';
                }
                if (! $this->isCharsStr(Arr::get($data, 'name')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'nameInvalid';
                }
                if (! $this->isCharsStr(Arr::get($data, 'surname')))
                {
                        // Присваиваем код ошибки
                        $errors[] = 'surnameInvalid';
                }
                if (trim(Arr::get($data, 'text')) == "")
                {
                        // Присваиваем код ошибки
                        $errors[] = 'inviteReqNotNull';
                }
                
                return isset($errors) ? $errors : array();
        }
        
        public function isEmailExists($email, $accountIdIgnore = NULL) 
        {
                // Переводим email в нижний регистр
                $email = strtolower($email);
                
                $account = Repo::factory('Account')->getOneByField('email', $email);
                
                if ($account->loaded())
                {
                        return (! is_null($accountIdIgnore) && $accountIdIgnore == $account->id) ? FALSE : TRUE;
                }
                
                return FALSE;
        }
        
        public function isEmailValid($email) 
        {
                return filter_var($email, FILTER_VALIDATE_EMAIL);
        }
        
        public function isPasswordValid($password) 
        {
                return preg_match('/[\w\W]{6,}/', $password);
        }
        
        public function isCharsStr($str) 
        {
                return ! preg_match("/[^a-zа-яё]/iu", $str);
        }

        public function isInvalidInvite($code) 
        {
                $invite = Repo::factory('Invite')->getOneByField('code', $code); 
                
                if (! $invite->loaded())
                {
                        return 'inviteInvaid';
                }
                
                $validInviteStatuses = array(Model_Invite::STATUS_NEW, Model_Invite::STATUS_SEND);
                
                if(! in_array($invite->status, $validInviteStatuses))
                {
                        return 'inviteUsed';
                }
                
                return FALSE;
        }
        
        public function isDdosRecovery($recoveries)
        {
                $recovery = array();
                
                foreach ($recoveries as $r) 
                {
                        if(time() < $r->expiried - 82800) // 82800 = 60*60*23
                        {
                                $recovery[] = $r;
                        }
                }
                
                if(count($recovery) < 3)
                {
                        return FALSE;
                }
                
                return TRUE;
        }
        
        public function createNotificationsOfExpiration()
        {
                // Все аккаунты, заканчивающиеся в течении 5 дней
                $accounts = $this->_getAllExpiring();
                
                foreach ($accounts as $account)
                {
                        // Шлем письма только тем, у кого аккаунт заканчивается через нечетное количество дней...
                        if((intval(Date::span(time(), $account->expiried + 1, 'days')) % 2) != 0)
                        {
                                $this->_sendExpireMail($account->email, $account->expiried);
                        }                        
                }
        }
        
        public function requestIncrease($account, $payId, $amount, $provider)
        {                
                $this->_sendRequestIncreaseMail($account, $payId, $amount, $provider);
        }
        
        private function _getModel($uid)
        {
                if ($uid instanceof Model_Account) 
                {
                        $account = $uid;
                }
                else
                {                
                        $account = Repo::factory('Account')->getOneById($uid);

                        if (! $account->loaded())
                        {
                                return FALSE;
                        }
                }
                
                return $account;
        }

        private function _create($data)
        {                
                $account = Repo::factory('Account')->getNew();
                
                // Наполняем аккаунт инфой
                $account->email = $data['email'];
                $account->passwordSalt = $data['passwordSalt'];
                $account->passwordHash = $this->_getPasswordHash($data['password'], $data['passwordSalt']);
                $account->expiried = strtotime('+8 day midnight') - 1;
                $account->save(); 
                
                $profile = Repo::factory('Profile')->getNew();
                
                // Наполняем профиль инфой
                $profile->accountId = $account->id;
                $profile->name = $data['name'];
                $profile->surname = $data['surname'];
                $profile->save();
                
                return $account;
        }
        
        private function _getListByProfileFilter($filter, $active)
        {
                $where = array();
                $return = array();
                $relations = array('account');
                
                if(! is_array($filter) || ! isset($filter['name']) || ! isset($filter['value']) || 
                   ! in_array($filter['name'], array('name', 'surname')))
                {
                        return $return;
                }
                
                $where[] = array($filter['name'], '%' . $filter['value'] . '%', 'LIKE');
                
                foreach(Repo::factory('Profile')->findAll($where, $relations, array(), array()) as $profile) 
                {
                        $account = array();
                        $acc = $profile->account;
                        
                        if($active && $acc->status >= Model_Account::STATUS_BLOCKED) 
                        {
                                continue;
                        }
                        
                        $account['id'] = $acc->id;
                        $account['email'] = $acc->email;
                        $account['balance'] = $acc->balance;
                        $account['expiried'] = $acc->expiried;
                        $account['status'] = $acc->status;
                        $account['profile'] = $profile->as_array();
                        
                        unset($account['profile']['account']);
                        
                        $return[] = $account;
                }
                
                return $return;
        }
        
        private function _getArrayList($where, $params, $profile)
        {
                $return = array();
                $relations = $profile ? array('profile') : array();
                
                foreach(Repo::factory('Account')->findAll($where, $relations, array(), $params) as $acc) 
                {
                        $account = array();
                        
                        $account['id'] = $acc->id;
                        $account['email'] = $acc->email;
                        $account['balance'] = $acc->balance;
                        $account['expiried'] = $acc->expiried;
                        $account['status'] = $acc->status;
                        
                        if($profile) 
                        {
                                $account['profile'] = $acc->profile->as_array();
                        }
                        
                        $return[] = $account;
                }
                
                return $return;
        }
        
        private function _updateTools($account, $tools, $slots)
        {                
                $active  = array();
                $deleted = array();
                
                Bag::set('accountBalance', $account->balance);
                
                foreach($account->tools->find_all() as $tool)
                {
                        $active[$tool->id] = $tool->id;
                }
                
                foreach ($account->toolsRemove->find_all() as $tool)
                {
                        $deleted[$tool->id] = $tool->id;
                }
                
                foreach (Repo::factory('Tool')->findAll(array('status', Model_Tool::STATUS_PAID)) as $tool)
                {
                        if (! isset($tools[$tool->id])
                                || ($tools[$tool->id] < 1 && isset($deleted[$tool->id]))
                                || ($tools[$tool->id] < 1 && ! isset($active[$tool->id])))
                        {
                                continue;
                        }
                        
                        $this->_updateTool($account, $tool, $tools[$tool->id], $active, $deleted, $slots);
                }
                
                if($account->balance !== Bag::get('accountBalance'))
                {
                        $mCostArr = $this->_getMonthlyCostByTools($account);
                        $expiried = $this->_expiredCalculate(Bag::get('accountBalance'), $mCostArr, $account->expiried);
                        
                        // Обновляем срок жизни и баланс
                        $account->set('expiried', $expiried)->set('balance', Bag::get('accountBalance'))->save();
                }
        }
        
        private function _updateTool($account, $tool, $isActive, $activated, $deleted, $slots)
        {      
                $isActivated = in_array($tool->id, $activated);                        
                $isDeleted   = in_array($tool->id, $deleted);
                
                // Если это не "СуперЧистка" - слоты не нужны!
                if($tool->code !== 'superclean')
                {
                        $slots = NULL;
                }
                
                // Если активируется инструмент, который сегодня был отключен
                if ($isActive && $isActivated && $isDeleted)
                {
                        $this->_reactivateTool($account, $tool, $slots); 
                        return;
                }
                
                // Если активируется новый инструмент или изменяется количество слотов
                if ($isActive)
                {
                        $this->_activateTool($account, $tool, $slots);                       
                        return;
                }
                
                $this->_removeTool($account, $tool);
        }
        
        private function _activateTool($account, $tool, $slotsNum = NULL)
        {
                $slots = NULL;
                $updated = time();
                $accTool = Repo::factory('AccountsTool')->getOneByFields(
                        array(array('accountId', $account->id), array('toolId', $tool->id)));
                
                if(! is_null($slotsNum))
                {
                        $slots = $this->_getToolSlotsByNum($tool, $slotsNum);
                }
                
                if(! $accTool->loaded())
                {       
                        $accTool = $this->_activateNewTool($account, $tool, $slots, $updated);
                }
                else if($slots && $accTool->slotsId !== $slots->id)
                {
                        $this->_recalculateSlotsFee($accTool, $slots, $updated);
                        
                        $accTool->set('slotsId', $slots->id)->set('updated', $updated)->save();
                }
                
                return $accTool;
        }
        
        private function _activateNewTool($account, $tool, $slots, $updated)
        {      
                $values['accountId'] = $account->id;
                $values['toolId']    = $tool->id;
                $values['updated']   = $updated;

                if($slots)
                {
                        $values['slotsId'] = $slots->id;
                }

                if($tool->freeUntil > $updated)
                {
                        $values['freeUntil'] = $tool->freeUntil;
                        
                        return Repo::factory('AccountsTool')->create($values);
                }

                $accTool = Repo::factory('AccountsTool')->create($values);

                $this->_takeToolFee($accTool, $tool, $slots);
                
                return $accTool;
        }
        
        public function _reactivateTool($account, $tool, $slotsNum = NULL)
        {                
                DB::delete('AccountsToolsRemove')
                        ->where('accountId', '=', $account->id)
                        ->where('toolId', '=', $tool->id)
                        ->execute();
                
                if($slotsNum)
                {
                        $this->_activateTool($account, $tool, $slotsNum);
                }
        }
        
        public function _removeTool($account, $tool)
        {
                $accountId = $account->id;
                $toolId = $tool->id;

                Repo::factory('AccountsToolsRemove')->create(compact('accountId', 'toolId'));
        }
        
        private function _getToolSlotsByNum($tool, $slotsNum)
        {
                $slots = NULL;
                $slotsNumArr = array();
                
                foreach(Repo::factory('Slots')->findAll(array(array('toolId', $tool->id))) as $_slots)
                {
                        if(count($slotsNumArr) < 1)
                        {
                                $slotsNumDefault = $_slots;
                        }
                        
                        if($slotsNum == $_slots->num)
                        {
                                $slots = $_slots;
                        }
                        
                        $slotsNumArr[$_slots->num] = $_slots->id;
                }
                
                if(is_null($slots))
                {
                        $slots = $slotsNumDefault;
                }
                
                return $slots;
        }
        
        private function _takeToolFee($accTool, $tool, $slots)
        {
                if($accTool->freeUntil > $accTool->updated)
                {
                        return;
                }
                
                $balance = Bag::get('accountBalance');
                $toolFee = $tool->monthlyCost * 100;
                
                if($slots && ! is_null($accTool->slotsId) && $slots->id == $accTool->slotsId)
                {
                        $toolFee += $slots->monthlyCost * 100;
                }
                
                $expiried = strtotime('+1 day midnight') - ($accTool->updated + 1);

                // Вычитаем стоимость аренды до полуночи сегодняшнего дня
                $balance -= floor($toolFee / date("t") * $expiried / 86400);
                
                Bag::set('accountBalance', $balance);
        }

        private function _recalculateSlotsFee($accTool, $slots, $updated)
        {
                if($accTool->freeUntil > $updated)
                {
                        return;
                }
                        
                $balance  = Bag::get('accountBalance');
                $oldSlots = Repo::factory('Slots')->getOneById($accTool->slotsId);                
                $slotsFee = $oldSlots->monthlyCost * 100;
                $expiried = strtotime('+1 day midnight') - ($updated + 1);

                // Возвращаем стоимость аренды  старых слотов до полуночи сегодняшнего дня
                $balance += floor($slotsFee / date("t") * $expiried / 86400);
                
                // Получаем месячную стоимость аренды новых слотов
                $slotsFee = $slots->monthlyCost * 100;

                // Вычитаем стоимость аренды  новых слотов до полуночи сегодняшнего дня
                $balance -= floor($slotsFee / date("t") * $expiried / 86400);
                
                Bag::set('accountBalance', $balance);
        }

        private function _balanceCalculate($balance, $amount, $mCostArr, $expiried)
        {
                $amount = floor($amount * 100);
                $utNow  = time();
                
                if($utNow > $expiried)
                {                
                        $expiried  = strtotime('+1 day midnight') - ($utNow + 1);
                        $monthly   = $this->_getMonthlyCostByUTS($mCostArr, strtotime('midnight') - 1);
                
                        // Вычисляем стоимость аренды до полуночи сегодняшнего дня
                        $dailyCost = floor($monthly / date("t") * $expiried / 86400);
                        
                        $amount -= $dailyCost;
                }
                
                return ($amount > 0) ? $balance + $amount : $balance;
        }
        
        private function _expiredCalculate($balance, $mCostArr, $expiried) 
        {                
                $daily = floor($this->_getMonthlyCostByUTS($mCostArr, strtotime('midnight') - 1) / date("t"));
                
                if($balance < 1 || $balance < $daily)
                {
                        return $expiried;
                }
                
                $expiried = strtotime('+1 day midnight') - 1;
                
                if(count($mCostArr) < 1)
                {
                        return strtotime('+10 year', $expiried);
                }
                
                while ($balance > 0)
                {
                        $daily    = floor($this->_getMonthlyCostByUTS($mCostArr, $expiried) / date("t", $expiried));
                        $balance  = $balance - $daily;
                        $expiried = strtotime('+1 day', $expiried);
                }
                
                return $expiried;                
        }
        
        private function _getMonthlyCostByTools($account)
        {
                $mcArr = array();
                $dtArr = array();
                $tools = array();
                $slots = array();
                
                foreach (Repo::factory('Tool')->getAll() as $tool)
                {
                        $tools[$tool->id] = $tool;
                }
                
                foreach (Repo::factory('Slots')->getAll() as $_slots)
                {
                        $slots[$_slots->id] = $_slots;
                }
                
                foreach (Repo::factory('AccountsToolsRemove')->findAll(array(array('accountId', $account->id))) as $tool)
                {
                        $dtArr[$tool->id] = $tool->id;
                }
                
                foreach(Repo::factory('AccountsTool')->findAll(array(array('accountId', $account->id))) as $accTool)
                {
                        if(in_array($accTool->toolId, $dtArr))
                        {
                                continue;
                        }
                        
                        $cost = $tools[$accTool->toolId]->monthlyCost;
                        
                        if(! is_null($accTool->slotsId) && isset($slots[$accTool->slotsId]) 
                                && $slots[$accTool->slotsId]->toolId == $accTool->toolId)
                        {
                                $cost += $slots[$accTool->slotsId]->monthlyCost;
                        }
                        
                        $mcArr[$accTool->toolId] = array('cost' => $cost, 'freeUntil' => $accTool->freeUntil);
                }
                
                return $mcArr;
        }
        
        private function _getMonthlyCostByUTS($mCostArr, $utNow)
        {
                $monthlyCost = 0;
                        
                foreach ($mCostArr as $toolMCost)
                {
                        if($toolMCost['freeUntil'] < $utNow)
                        {
                                $monthlyCost += $toolMCost['cost'] * 100;
                        }
                }
                
                return $monthlyCost;
        }
        
        private function _getAllExpiring()
        {
                // Условия для поиска аккаунтов, заканчивающихся в течении 5 дней
                $where = array(array('expiried', time(), '>'), array('expiried', strtotime("+6 days") - 1, '<'));
                
                return Repo::factory('Account')->findAll($where, array(), array('expiried', 'DESC'));
        }
        
        private function _getAllExpiried()
        {
                // Условия для поиска аккаунтов, заканчивающихся в течении 5 дней
                $where = array(array('expiried', time(), '<'));
                
                return Repo::factory('Account')->findAll($where, array(), array('expiried', 'DESC'));
        }
        
        private function _useInvite($account, $code)
        {  
                $invite = Repo::factory('Invite')->getOneByField('code', $code); 
                
                $validInviteStatuses = array(Model_Invite::STATUS_NEW, Model_Invite::STATUS_SEND);
                
                if (! $invite->loaded() || ! in_array($invite->status, $validInviteStatuses))
                {
                        return FALSE;
                }
                
                $invite->accountId = $account->id;
                $invite->status = Model_Invite::STATUS_USED;
                $invite->save();
                
                return $invite;
        }
        
        private function _getRecoveryError($recovery)
        {
                if(! $recovery->loaded()) 
                {
                        // Возвращаем код ошибки
                        return 'recoveryInvalid';                        
                } 
                elseif ($recovery->expiried < time()) 
                {
                        // Возвращаем код ошибки
                        return 'recoveryExpiried'; 
                } 
                elseif ($recovery->status == Model_PasswordRecovery::STATUS_USED) 
                {
                        // Возвращаем код ошибки
                        return 'recoveryUsed';      
                }
                
                return FALSE;
        }
        
        private function _changeEmail($account, $email)
        {                
                if (! $this->isEmailValid($email))
                {
                        // Возвращаем код ошибки
                        return 'emailInvalid';
                }

                if ($this->isEmailExists($email, $account->id))
                {
                        // Возвращаем код ошибки
                        return 'emailExistsAnOtherUser';
                }
                
                $account->set('email', strtolower($email))->save();
                
                return TRUE;
        }

        private function _changePassword($account, $password)
        {
                if (! $this->isPasswordValid($password))
                {
                        // Возвращаем код ошибки
                        return 'newPasswordInvalid';
                }
                
                if ($this->_checkPassword($account, $password))
                {
                        // Возвращаем код ошибки
                        return 'passwordNotChanged';
                }

                $this->_getNewPassword($account, $password);
                
                return TRUE;
        }
        
        private function _checkPassword($account, $password)
        {
                $pepperoni  = Conf::get_option('auth_pepper');
                
                $passwordHash = $this->_getPasswordHash($password, $account->passwordSalt);
                
                return ($passwordHash == $account->passwordHash);
        }
        
        private function _getNewPassword($account, $password = NULL)
        {
                // Формируем новый пароль (если он не был указан при вызове функции...)
                $password = is_null($password) ? substr(md5(date('D, d M Y H:i:s')), 0, 7) : $password;
                
                $account->passwordHash = $this->_getPasswordHash($password, $account->passwordSalt);
                $account->save();
                
                return $password;
        }
        
        private function _getPasswordHash($password, $salt)
        {
                return md5(serialize(array($password, $salt, Conf::get_option('auth_pepper'))));
        }
        
        private function _generateRecoveryHash($accountId)
        {
                return Repo::factory('PasswordRecovery')->create(array(
                    'accountId' => $accountId, 
                    'expiried'  => time() + 86400, //86400 = 60*60*24
                    'hash'      => substr(md5(date('D, d M Y H:i:s')), 16, 8),
                ));
        }
        
        private function _getNewEmailRawFilePath($emailRawDir = 'raw')
        {                
                do
                {
                        $taskPath = APPPATH . '/emails/' . $emailRawDir . '/' . microtime(true);
                }
                while(file_exists($taskPath));
                
                return $taskPath;
        }

        private function _sendRecoveryMail($email, $name, $hash, $expiried)
        {                
                $message = View::factory('emails/txt/recovery', compact('name', 'email', 'hash', 'expiried'));

                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => 'Восстановление пароля в Magic Tools',
                    'to' => array(
                        array('name' => $name, 'email' => $email)
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }

        private function _sendWelcomeMail($email, $name, $password)
        {                
                $message = View::factory('emails/txt/registration', compact('name', 'email', 'password'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => 'Регистрация в Magic Tools',
                    'to' => array(
                        array('name' => $name, 'email' => $email)
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
        
        private function _sendNewPasswordMail($email, $name, $password)
        {                
                $message = View::factory('emails/txt/reset', compact('name', 'email', 'password'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => 'Новый пароль в Magic Tools',
                    'to' => array(
                        array('name' => $name, 'email' => $email)
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
        
        private function _sendProfileUpdateMail($email, $name, $password)
        {                
                $message = View::factory('emails/txt/profile_update', compact('name', 'email', 'password'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => 'Обновление профиля в Magic Tools',
                    'to' => array(
                        array('name' => $name, 'email' => $email)
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
        
        private function _sendExpireMail($email, $expire)
        {                
                $message = View::factory('emails/txt/notify', compact('email', 'expire'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    //'subject' => 'Ваш аккаунт в Magic Tools',
                    'subject' => '[Magic Tools] Напоминание об оплате аккаунта',
                    'to' => array(
                        array('name' => 'Пользователь', 'email' => $email)
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath('raw_notify'), serialize($letter));
        }
        
        private function _sendRequestInviteMail($email, $name, $surname, $message)
        {                
                $message = View::factory('emails/txt/request_invite', compact('email', 'name', 'surname', 'message'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => '[MagicTools] Новый запрос инвайта',
                    'to' => array(
                        array('name' => 'Святыч', 'email' => Conf::get_option('admin_email'))
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
        
        private function _sendInviteMail($email, $name, $surname, $invite)
        {                
                $message = View::factory('emails/txt/invite', compact('email', 'name', 'surname', 'invite'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => 'Ваш инвайт в MagicTools',
                    'to' => array(
                        compact('name', 'email')
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
        
        private function _sendRequestIncreaseMail($account, $payId, $amount, $provider)
        {                
                $vars = compact('account', 'payId', 'amount', 'provider');
                $message = View::factory('emails/txt/pay_request', $vars);
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => '[MagicTools] Запрос на продление аккаунта!',
                    'to' => array(
                        array('name' => 'Админ', 'email' => Conf::get_option('service_email'))
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
        
        private function _sendIncreaseBalanceMail($email, $name, $amount, $clearAmount = NULL)
        {
                // Временный впрыск кода на время двухдневной акции
                $nowtime = time();
                if($nowtime >= 1637960395 && $nowtime <= 1638133195)
                {
                        $message = View::factory('emails/txt/pay_accepted_light_sat', 
                                compact('name', 'amount', 'clearAmount'));
                }
                else
                {
                        $message = View::factory('emails/txt/pay_accepted', compact('name', 'amount'));
                }
                
                //$message = View::factory('emails/txt/pay_accepted', compact('name', 'amount'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => '[MagicTools] Баланс вашего аккаунта пополнен!',
                    'to' => array(
                        compact('name', 'email')
                    )
                );
                
                file_put_contents($this->_getNewEmailRawFilePath(), serialize($letter));
        }
}
