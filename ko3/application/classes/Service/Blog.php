<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Blog extends Abstract_Service {
        
        public function addCat($title)
        {
                // Создаем новую запись в БД
                $cat = Repo::factory('BlogCat')->getNew();
                
                // Заносим инфу
                $cat->values(compact('title'))->save();
                
                return $cat;
        }
        
        public function addComment($postId, $accountId, $profileId, $text)
        {
                $comment = Repo::factory('BlogComment')->getNew();
                $comment->accountId = $accountId;
                $comment->profileId = $profileId;
                $comment->blogPostId = $postId;
                $comment->text = $text;
                $comment->timestamp = time();
                $comment->save();
                
                return $comment;
        }
        
        public function countAllActivePostsByCategoryId($id)
        {
                $where = array();
                
                $where[] = array('blogCatId', $id);
                $where[] = array('status', Model_BlogPost::STATUS_ACTIVE);
                
                return Repo::factory('BlogPost')->countAll($where);
        }
        
        public function countAllActivePostsByTag($tag)
        {
                return $tag->posts
                        ->where('status', '=', Model_BlogPost::STATUS_ACTIVE)
                        ->count_all();
                
                return Repo::factory('BlogPost')->countAll($where);
        }
        
        public function countAllCats()
        {
                return Repo::factory('BlogPost')->countAll();
        }
        
        public function countAllPosts($status = 'active')
        {
                $where = array();
                
                if($status == 'active')
                {
                        $where[] = array('status', Model_BlogPost::STATUS_ACTIVE);
                } 
                elseif($status == 'active&draft')
                {
                        $where[] = array('status', Model_BlogPost::STATUS_TRASH, '<');
                }
                
                return Repo::factory('BlogPost')->countAll($where);
        }
        
        public function createPost($title, $content, $blogCatId, $tags = NULL)
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $timestamp = time();
                
                // Создаем новую запись в БД
                $post = Repo::factory('BlogPost')->getNew();
                
                // Заносим инфу
                $post->values(compact('accountId', 'title', 'content', 'blogCatId', 'timestamp'))->save();
                
                return $post;
        }
        
        public function editPost($id, $title, $body, $blogCatId, $tags = NULL)
        {
                $post = Repo::factory('BlogPost')->getOneById($id);
                
                if (! $post->loaded())
                {
                        return FALSE;
                }
                
                // Заносим инфу
                $post->values(compact('title', 'body', 'blogCatId'))->save();
                
                return $post;
        }
        
        public function editStatusPost($id, $isPublished)
        {
                $post = Repo::factory('BlogPost')->getOneById($id);
                
                if (! $post->loaded())
                {
                        return FALSE;
                }
                
                $status = $isPublished ? Model_BlogPost::STATUS_ACTIVE : Model_BlogPost::STATUS_DRAFT;
                
                // Заносим инфу
                $post->set('status', $status)->save();
                
                return $post;
        }
        
        public function getActiveCommentsByPostId($postId, $limit = NULL, $offset = NULL)
        {
                $where = array();
                $params = array();
                $rels = array('account', 'profile');
                
                $where[] = array('blogcomment.blogPostId', $postId);
                $where[] = array('blogcomment.status', Model_BlogComment::STATUS_ACTIVE);
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return Repo::factory('BlogComment')->findAll($where, $rels, array('timestamp', 'DESC'), $params);
        }

        public function getActivePostById($id, $withComments = FALSE) 
        {
                $where = array();
                $rels = $withComments ? array('comments', 'tags') : array('tags');
                
                $where[] = array('id', $id);
                $where[] = array('status', Model_BlogPost::STATUS_ACTIVE);
                
                $post = Repo::factory('BlogPost')->getOneByFields($where, $rels);
                
                if (! $post->loaded())
                {
                        return FALSE;
                }
                
                return $this->_combinePostWithTags($post);
        }
        
        public function getActivePostsByCategory($catId, $limit = NULL, $offset = NULL, $relations = array())
        {
                $where = array();
                $params = array();
                
                $where[] = array('blogpost.blogCatId', $catId);
                $where[] = array('blogpost.status', Model_BlogPost::STATUS_ACTIVE);
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return Repo::factory('BlogPost')->findAll($where, $relations, array('timestamp', 'DESC'), $params);
        }
        
        public function getActivePostsByTag($tag, $limit = NULL, $offset = NULL, $relations = array())
        {
                $where = array();
                $params = array();
                $order = array('timestamp', 'DESC');
                $entity = $tag->posts;
                
                $where[] = array('blogpost.status', Model_BlogPost::STATUS_ACTIVE);
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return Repo::factory('BlogPost')->findAllByEntity($entity, $where, $relations, $order, $params);
        }

        public function getCategoryById($id)
        {
                $category = Repo::factory('BlogCat')->getOneById($id);
                
                if (! $category->loaded())
                {
                        return FALSE;
                }
                
                return $category;
        }

        public function getCats($limit = NULL, $offset = NULL, $relations = array())
        {
                $where = array();
                $params = array();
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return Repo::factory('BlogCat')->findAll($where, $relations, array(), $params);
        }
        
        public function getExcerpts($status = 'active', $limit = NULL, $offset = NULL)
        {       
                $rels = array('tags', 'cat', 'comments');
                $posts = $this->getPosts($status, $limit, $offset, $rels);
                $excerpts = array();
                
                foreach ($posts as $post) {
                        $excerpts[] = $this->_combinePostWithTags($post);
                }
                
                return $excerpts;
        }
        
        public function getExcerptsByCategoryId($catId, $limit = NULL, $offset = NULL)
        {       
                $rels = array('tags', 'cat', 'comments');
                $posts = $this->getActivePostsByCategory($catId, $limit, $offset, $rels);
                $excerpts = array();
                
                foreach ($posts as $post) {
                        $excerpts[] = $this->_combinePostWithTags($post);
                }
                
                return $excerpts;
        }
        
        public function getExcerptsByTag($tag, $limit = NULL, $offset = NULL)
        {       
                $rels = array('tags', 'cat', 'comments');
                $posts = $this->getActivePostsByTag($tag, $limit, $offset, $rels);
                $excerpts = array();
                
                foreach ($posts as $post) {
                        $excerpts[] = $this->_combinePostWithTags($post);
                }
                
                return $excerpts;
        }

        public function getPostById($id, $withComments = FALSE) 
        {
                $rels = array('comments', 'tags');
                $post = Repo::factory('BlogPost')->getOneById($id, $rels);
                
                if (! $post->loaded())
                {
                        return FALSE;
                }
                
                return $this->_combinePostWithTags($post);
        }
        
        public function getPosts($status = 'active', $limit = NULL, $offset = NULL, $relations = array())
        {
                $where = array();
                
                if($status == 'active')
                {
                        $where[] = array('blogpost.status', Model_BlogPost::STATUS_ACTIVE);
                } 
                elseif ($status == 'active&draft')
                {
                        $where[] = array('blogpost.status', Model_BlogPost::STATUS_TRASH, '<');
                }
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return Repo::factory('BlogPost')->findAll($where, $relations, array('timestamp', 'DESC'), $params);
        }

        public function getTagById($id)
        {
                $tag = Repo::factory('BlogTag')->getOneById($id);
                
                if (! $tag->loaded())
                {
                        return FALSE;
                }
                
                return $tag;
        }
        
        public function getTags()
        {
                return Repo::factory('BlogTag')->getAll();
        }
        
        public function trashCommentByAccountId($commentId, $accountId)
        {
                $comment = Repo::factory('BlogComment')->getOneById($commentId);
                
                if (! $comment->loaded() || $comment->accountId != $accountId)
                {
                        return FALSE;
                }
                
                $comment->set('status', Model_BlogComment::STATUS_DELETED)->save();
                
                return TRUE;
        }
        
        public function trashPost($id)
        {
                $post = Repo::factory('BlogPost')->getOneById($id);
                
                if (! $post->loaded())
                {
                        return FALSE;
                }
                
                // Заносим инфу
                $post->set('status', Model_BlogPost::STATUS_TRASH)->save();
                
                return $post;
        }
        
        private function _combinePostWithTags($postModel)
        {
                $excerpt = strstr($postModel->content, '<!--more-->', TRUE);
                $content = str_replace('<!--more-->', '<span id="continue-reading"></span>', $postModel->content);
                $commentsCnt = $postModel->comments->where('status', '=', Model_BlogComment::STATUS_ACTIVE)->count_all();
                
                $post = new stdClass();
                $post->id = $postModel->id;
                $post->title = $postModel->title;
                $post->excerpt = $excerpt ? $excerpt : $postModel->content;
                $post->content = $content;
                $post->cat = $postModel->cat;
                $post->timestamp = $postModel->timestamp;
                $post->tags = $postModel->tags->find_all();
                $post->commentsCnt = $commentsCnt;
                
                return $post;
        }
}
