<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Payment extends Abstract_Service {
        
        public function getList($limit = NULL, $offset = NULL)
        {
                $where = array();
                $params = array();
                
                if($limit)
                {
                        $params[] = array('limit', $limit);
                }
                
                if($offset)
                {
                        $params[] = array('offset', $offset);
                }
                
                return $this->_getArrayList($where, $params);
        }
        
        private function _getAdditionalFromPay($pay)
        {
            try
            {
                return Arr::extract(unserialize($pay->additional), array('sender', 'datetime'));
            } 
            catch (Exception $e) {
                return NULL;
            }
        }
        
        private function _getArrayList($where, $params)
        {
                $return = array();
                
                foreach(Repo::factory('Payment')->findAll($where, array(), array('id', 'DESC'), $params) as $pay) 
                {
                        $payment = array();
                        $ddtnl = $this->_getAdditionalFromPay($pay);
                        
                        $payment['id'] = $pay->id;
                        $payment['accountId'] = $pay->accountId;
                        $payment['billId'] = $pay->billId;
                        $payment['amount'] = $pay->amount;
                        $payment['datetime'] = strtotime($ddtnl['datetime']);
                        $payment['type'] = $pay->type;
                        $payment['status'] = $pay->status;
                        $payment['acceptLink'] = $this->_createAcceptLink($pay->accountId, $pay->id, $pay->amount);
                        
                        $return[] = $payment;
                }
                
                return $return;
        }
        
        private function _createAcceptLink($accountId, $payId, $amount)
        {
                $pepper = Conf::get_option('auth_pepper');
                $query = array(
                    'sha'     => substr(md5($accountId.'::'.$payId.'::'.$amount.'::'.$pepper), 0, 8),
                    'account' => $accountId,
                    'pay'     => $payId,
                    'amount'  => $amount
                );
                
                return Conf::get_option('site_url') . 'payment/confirm/?' . http_build_query($query);
        }
}
