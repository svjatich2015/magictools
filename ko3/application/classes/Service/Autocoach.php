<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_Autocoach extends Abstract_Service {

        public function create($accountId, $target, $theme) 
        {
                $theme = Text::limit_chars($theme, 500);
                $values = compact('accountId', 'target', 'theme');
                
                Repo::factory('Autocoach')->create($values);
                
                return TRUE;
        }

        public function getData($drawId, $accountId) 
        {
                $autodraw = $this->getOneByAccountWithThemes($drawId, $accountId);
                
                if (! $autodraw)
                {
                        return FALSE;
                }
                
                $autodraw['type'] = $this->__getStrType($autodraw['type']);
                $autodraw['src'] = $this->__getImgSrc($autodraw);
                
                return Arr::extract($autodraw, array('id', 'type', 'src', 'strainLevel', 'name', 'themes', 'title', 'template'));
        }
        
        public function getOneByAccount($autocoachId, $accountId)
        {
                $autocoach = Repo::factory('Autocoach')->getOneById($autocoachId);
                
                if (! $autocoach->loaded() || $autocoach->accountId != $accountId)
                {
                        return FALSE;
                }
                
                return $autocoach;
        }
        
        public function getByAccount($accountId)
        {
                $where[] = array('accountId', $accountId);
                
                return Repo::factory('Autocoach')->findAll($where);
        }

        public function activateOneByAccount($autocoachId, $accountId) 
        {
                $autocoach = Repo::factory('Autocoach')->getOneById($autocoachId);
                
                if (! $autocoach->loaded() || $autocoach->accountId != $accountId)
                {
                        return FALSE;
                }
                
                $autocoach->set('status', Model_Autocoach::STATUS_ACTIVE)->save();
                
                return TRUE;
        }

        public function pauseOneByAccount($autocoachId, $accountId) 
        {
                $autocoach = Repo::factory('Autocoach')->getOneById($autocoachId);
                
                if (! $autocoach->loaded() || $autocoach->accountId != $accountId)
                {
                        return FALSE;
                }
                
                $autocoach->set('status', Model_Autocoach::STATUS_PAUSED)->save();
                
                return TRUE;
        }

        public function editOneByAccount($autocoachId, $accountId, $target, $theme) 
        {
                $autocoach = Repo::factory('Autocoach')->getOneById($autocoachId);
                
                if (! $autocoach->loaded() || $autocoach->accountId != $accountId)
                {
                        return FALSE;
                }
                
                $autocoach->set('theme', $theme)->set('target', $target)->save();
                
                return TRUE;
        }

        public function deleteOneByAccount($autocoachId, $accountId) 
        {
                $autocoach = Repo::factory('Autocoach')->getOneById($autocoachId);
                
                if (! $autocoach->loaded() || $autocoach->accountId != $accountId)
                {
                        return FALSE;
                }
                
                $autocoach->delete();
                
                return TRUE;
        }
}
