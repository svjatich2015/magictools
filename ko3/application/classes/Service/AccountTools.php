<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Service
 */
class Service_AccountTools extends Abstract_Service {
    
        public function applyGift($accountId, $giftId)
        {
            $account = Service::factory('Account')->getById($accountId, TRUE);
            if(! $account)
            {
                return FALSE;
            }
            $gift = Repo::factory('Gift')->getOneById($giftId, ['items']);
            if(! $gift->loaded())
            {
                return FALSE;
            }
            return $this->_applyGift($account, $gift);
        }

        public function create($accountId, $toolId)
        {
            $accountTool = $this->_getNew($accountId, $toolId);
            
            return $accountTool->save();
        }

        public function createNotificationsOfExpiration()
        {
            $this->_createNotifiesOfExpiring();
            $this->_createNotifiesOfExpiried();
        }
        
        public function getAllExpiried()
        {
                $expiried = [];
                $tools = Service::factory('Tool')->getArrTitlesSortById();
                // Условия для поиска аккаунтов,  которые закончились в течении 5 дней
                $where = array(array('expiried', time(), '<'), array('expiried', strtotime("-6 days") - 1, '>'));                
                foreach(Repo::factory('AccountsTool')->findAll($where, array(), array('expiried', 'DESC')) as $at) 
                {
                    if(! isset($expiried[$at->accountId]))
                    {
                        $expiried[$at->accountId] = [];
                    }
                    $expiried[$at->accountId][] = [
                        'title' => $tools[$at->toolId],
                        'expiried' => date('d.m.Y', $at->expiried)
                    ];
                }
                return $expiried;
        }
        
        public function getAllExpiring()
        {
                $nowTime = time();
                $expiring = [];
                $tools = Service::factory('Tool')->getArrTitlesSortById();
                // Условия для поиска аккаунтов,  которые заканчиваются в течении 3 дней
                $where = array(array('expiried', time(), '>'), array('expiried', strtotime("+4 days") - 1, '<'));              
                foreach(Repo::factory('AccountsTool')->findAll($where, array(), array('expiried', 'DESC')) as $at) 
                {
                    if((intval(Date::span($nowTime, $at->expiried + 1, 'days')) % 2) == 0)
                    {
                        continue;
                    }
                    if(! isset($expiring[$at->accountId]))
                    {
                        $expiring[$at->accountId] = [];
                    }
                    $expiring[$at->accountId][] = [
                        'title' => $tools[$at->toolId],
                        'expiried' => date('d.m.Y', $at->expiried)
                    ];
                }
                return $expiring;
        }
        
        private function _applyGift($account, $gift) 
        {
            foreach($gift->items->find_all() as $giftItem)
            {
                $this->_applyGiftItem($account, $giftItem);
            }
            Service::factory('Email')->send(
                [
                    [
                        'name' => $account->profile->name, 
                        'email' => $account->email
                    ]
                ],
                $gift->emailSubj,
                [
                    'text' => $gift->emailMsg, 
                    'replace' => ['name' => $account->profile->name]
                ]
            );
        }
        
        private function _applyGiftItem($account, $giftItem) 
        {
            $nowTime = time();
            $accountTool = Repo::factory('AccountsTool')->getOneByFields([
                ['accountId', $account->id],
                ['toolId', $giftItem->toolId]
            ]); 
            if(! $accountTool->loaded())
            {
                $accountTool = $this->_getNew($account->id, $giftItem->toolId);
            }
            $expiried = ($accountTool->expiried > $nowTime) ? $accountTool->expiried : $nowTime;
            $accountTool->expiried = strtotime('+' . $giftItem->periodMonths . ' months', $expiried);
            $accountTool->updated = $nowTime;
            $accountTool->save();
        }
        
        private function _createNotifiesOfExpiried() 
        {
                // Все аккаунты, закончившиеся в течении 5 дней
                $expiried = $this->getAllExpiried();
                if(! $expiried || count($expiried) < 1)
                {
                    return;
                }
                $accounts = Service::factory('Account')->getByListIds(array_keys($expiried));             
                foreach ($expiried as $id => $tools)
                {
                        if(! isset($accounts[$id]))
                        {
                            continue;
                        }
                        $this->_sendExpireMail($accounts[$id], $tools);
                }
        }
        
        private function _createNotifiesOfExpiring() 
        {
                // Все аккаунты, закончившиеся в течении 5 дней
                $expiring = $this->getAllExpiring();
                if(! $expiring || count($expiring) < 1)
                {
                    return;
                }
                $accounts = Service::factory('Account')->getByListIds(array_keys($expiring));             
                foreach ($expiring as $id => $tools)
                {
                        if(! isset($accounts[$id]))
                        {
                            continue;
                        }
                        $this->_sendExpiringMail($accounts[$id], $tools);
                }
        }

        private function _getNew($accountId, $toolId, $slotsId = 1)
        {
            $nowTime = time();
            $accountTool = Repo::factory('AccountsTool')->getNew();
            $accountTool->accountId = $accountId;
            $accountTool->toolId = $toolId;
            if($toolId == Model_Tool::SUPERCLEAN)
            {
                $accountTool->slotsId = $slotsId;
            }
            $accountTool->freeUntil = 0;
            $accountTool->expiried = $nowTime;
            $accountTool->updated = $nowTime;
            
            return $accountTool;
        }
        
        private function _sendExpireMail($account, $tools)
        {
                $email = $account->email;
                $name = $account->profile->name;
                $message = View::factory('emails/txt/tools_expiried', compact('name', 'tools'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => '[Magic Tools] Срок действия инструментов закончился',
                    'to' => array(compact('email', 'name')
                    )
                );
                
                Service::factory('Notify')->createRaw($letter);
        }
        
        private function _sendExpiringMail($account, $tools)
        {
                $email = $account->email;
                $name = $account->profile->name;
                $message = View::factory('emails/txt/tools_expiring', compact('name', 'tools'));
                
                $letter = array(
                    'text' => $message->render(),
                    'encoding' => 'UTF-8',
                    'subject' => '[Magic Tools] Срок действия инструментов заканчивается',
                    'to' => array(compact('email', 'name')
                    )
                );
                
                Service::factory('Notify')->createRaw($letter);
        }
}
