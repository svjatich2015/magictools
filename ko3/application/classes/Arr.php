<?php defined('SYSPATH') OR die('No direct script access.');

class Arr extends Kohana_Arr {

    /**
     * "Выталкивает" из ассоциативного массива элемент с определенным ключом.
     * 
     * Arr::pop_assoc() извлекает и возвращает $array[$key], уменьшая размер
     * массива на один элемент. Если $array пуст, не является массивом или 
     * не содержит элемент с индексом $key, будет возвращено значение $default.
     *
     *     // Returns 'John'
     *     Arr::pop_assoc(array('name' => 'John', 'surname' => 'Doe'), 'name');
     *
     *     // Returns NULL
     *     Arr::pop_assoc(array('foo'), 'foo');
     *
     *     // Returns FALSE
     *     Arr::pop_assoc(Database::instance(), 'foo', FALSE);
     * 
     * Функция по своему действию напоминает array_shift() или array_pop(). 
     *
     * @param   array   $array  array to check
     * @return  boolean
     */
    public static function pop_assoc(array & $array, $key, $default = NULL)
    {
        if ( ! Arr::is_array($array) || ! Arr::get($array, $key, FALSE))
        {
                // This is not an array!
                return $default;
        }
        $found = $array[$key];
        unset($array[$key]);
        
        return $found;
    }
    
    /**
     * Рекурсивная версия функции array_diff()
     * 
     * @link   http://www.php.net/manual/ru/function.array-diff.php 
     * @param  array  $array1  Исходный массив
     * @param  array  $array2  Массив, с которым идет сравнение
     * @return array
     */
    public static function diff_assoc(array $array1, array $array2)
    {
        $diff = $array1;
        
        foreach($array1 as $k => $v)
        {            
            if(array_key_exists($k, $array2))
            {
                if(is_array($array1[$k]))
                {
                    if(is_array($array2[$k]))
                    {
                        $diff[$k] = Arr::diff_assoc($array1[$k], $array2[$k]);
                        if(count($diff[$k]) > 0)
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    if(is_array($array2[$k]) || $array1[$k] !== $array2[$k])
                    {
                        continue;
                    }
                }
            }
            
            unset($diff[$k]);
        }
        
        return isset($diff) ? $diff : array();
    }
    
    /**
     * Превращает многомерный массив в одномерный, присваивая подмассивам ключи
     * в стиле оформления html-формы.
     * 
     *      // Вернет array('one[two]' => 'three', 'four' => 'five')
     *      Arr::equalize(array('one' => array('two' => 'three'), 'four' => 'five'));
     * 
     * @param  array  $array
     * @param  type   $first_level
     * @return type
     */
    public static function equalize(array $array, $first_level = TRUE)
    {
        $equalize = array();
        
        foreach ($array as $key => $value)
        {
            $_key = ($first_level) ? $key : '[' . $key . ']';
            if(!is_array($value))
            {
                $equalize[$_key] = $value;
                continue;                
            }  
            $_equalize = Arr::equalize($value, FALSE);
            foreach($_equalize as $k => $v) 
            {
                $_k = $_key . $k;
                $equalize[$_k] = $v;
            }
        }
        
        return $equalize;
    }

    /**
     * Создает и возвращает экземпляр класса ArrayObject
     *
     *     // Создание экземпляра ArrayObject без публичных свойств
     *     $arrObj = Arr::obj();
     *
     *     // Свойства можно добавлять так: 
     *     $arrObj->foo = 'bar';
     *
     *     // Создание экземпляра ArrayObject из массива
     *     $arrObj = Arr::obj(array('foo' => 'bar'));
     * 
     * @param   mixed        $input           Массив значений для добавления в качестве свойств
     * @param   integer      $flags           Доп. опции ArrayObject
     * @param   string       $iterator_class  Имя класса итератора для ArrayObject
     * @return  ArrayObject
     */
    public static function obj($input = array(), $flags = ArrayObject::ARRAY_AS_PROPS)
    {
        return new ArrayObject($input, $flags);
    }
    
    /**
     * Аналог функции array_key_exists(), но проверяет наличие нескольких ключей
     * 
     * @link   http://www.php.net/manual/ru/function.array-key-exists.php
     * @param  array   $keys    Проверяемые значения
     * @param  array   $search  Массив с проверяемыми ключами
     * @return boolean
     */
    public static function keys_exists(array $keys, array $search)
    {
        foreach ($keys as $key)
        {
            if(!array_key_exists($key, $search))
            {
                return FALSE;
            }
        }
        
        return TRUE;
    }

    /**
     * Рекурсивная версия Arr::extract
     *
     * @param   array  $array    array to extract paths from
     * @param   array  $paths    list of path
     * @param   mixed  $default  default value
     * @return  array
     */
    public static function extract_recursive($array, array $paths, $default = NULL)
    {
        $found = array();
        foreach ($paths as $key => $path)
        {
            if(is_array($path))
            {
                $_array = isset($array[$key]) ? $array[$key] : array();
                $found[$key] = Arr::extract_recursive($_array, $path, $default);
            }
            else
            {
                $found[$path] = Arr::get($array, $path, $default);  
            }
        }

        return $found;
    }

}
