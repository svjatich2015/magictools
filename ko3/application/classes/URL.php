<?php defined('SYSPATH') OR die('No direct script access.');

class URL extends Kohana_URL {

	/**
	 * Test if given $host should be trusted.
	 *
	 * Tests against given $trusted_hosts
	 * or looks for key `trusted_hosts` in `url` config
	 *
	 * @param string $host
	 * @param array $trusted_hosts
	 * @return boolean TRUE if $host is trustworthy
	 */
	public static function is_trusted_host($host, array $trusted_hosts = NULL)
	{
		// If list of trusted hosts is not directly provided read from config
		if (empty($trusted_hosts))
		{
			$trusted_hosts = (array) Conf::get_option('trusted_hosts', array());
		}

		return parent::is_trusted_host($host, $trusted_hosts);

	}
        
}
