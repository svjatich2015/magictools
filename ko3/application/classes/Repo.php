<?php defined('SYSPATH') or die('No direct script access.');

 /**
 * Базовый класс Repo.
 *
 * @category  Repository
 * @authors   WebRacer & Svjat Maslov
 */
class Repo {

    /**
     * @var  array  Контейнер для хранения инстансов 
     */
    protected static $_instance = array();

    /**
     * Обертка для вызова методов Repo через единый интерфейс
     *
     *     $repo = Repo::factory($name);
     * 
     * @param  string   $name       Имя Repo-класса
     * @param  mixed    $param1..N  Параметры, передаваемые в конструктор класса
     * @return Repo
     */
    public static function factory()
    {
        $class = 'Repo_' . func_get_arg(0);
        
        if(func_num_args() > 1)
        {
            $args_list = func_get_args();            
            array_shift($args_list);
            
            $ref_class  = new ReflectionClass($class);            
            $repo_class = $ref_class->newInstanceArgs($args_list);
        }
        else
        {
            $repo_class = new $class();
        }
        
        return $repo_class;
    }

    /**
     * Обертка для вызова Repo, как синглтон.
     * 
     * @param  string   $name      Имя метода Repo
     * @return instance
     */
    public static function instance($name)
    {
        $class = 'Repo_' . $name;

        if (!isset($_instance[$name]))
        {
            $_instance[$name] = new $class();
        }

        return $_instance[$name];
    }

}
