<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Payments extends Controller_Admin_Base {

	public function action_index() 
        {
                $paymentsCnt = Repo::factory('Payment')->countAll();
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;
                
                if($pageNum < 0 || $offset >= $paymentsCnt)
                {
                        // Редиректим на страничку со списком клиентов
                        HTTP::redirect('admin/payments');
                }
                
                $payments = Service::factory('Payment')->getList($limit, $offset);
                
                $contentData = compact('payments', 'paymentsCnt', 'limit', 'pageNum');
                
                $this->template->content = View::factory('pages/admin/payments', $contentData);
                //$this->template->jsCode = View::factory('javascripts/admin/payments');
        }
}
