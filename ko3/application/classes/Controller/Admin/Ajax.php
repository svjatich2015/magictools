<?php

 class Controller_Admin_Ajax extends Controller_Admin_Base {

	/**
	 * @var  boolean  auto render template
	 **/
	public $auto_render = FALSE;
     
        public function before()
        {        
                if (!Request::initial()->is_ajax())
                {
                    throw new HTTP_Exception_404('The requested URL :uri was not found on this server.', 
                            array(':uri' => Request::detect_uri()));
                }

                parent::before();
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/
        
        public function post_action_blog_post_trash()
        {     
                if(isset($_POST['id']))
                {
                        if(Service::factory('Blog')->trashPost(intval($_POST['id'])))
                        {
                                $this->response->body(json_encode(array('status' => 'ok')));  
                                return;
                        }
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_blog_post_edit_status()
        {     
                if(isset($_POST['id']) && isset($_POST['published']))
                {
                        if(Service::factory('Blog')->editStatusPost(intval($_POST['id']), intval($_POST['published'])))
                        {
                                $this->response->body(json_encode(array('status' => 'ok')));  
                                return;
                        }
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_user_increase()
        {     
                if(Arr::keys_exists(array('id', 'amount', 'method'), $_POST))
                {
                        $methods = array('free', 'sbrf', 'paypal', 'yamoney', 'cash', 'custom');
                        
                        extract(Arr::extract($_POST, array('id', 'amount', 'method')));
                        
                        if(in_array($method, $methods) && is_numeric($amount))
                        {
                                if(Service::factory('Pay')->manual($id, $amount, $method))
                                {
                                        $this->response->body(json_encode(array('status' => 'ok')));  
                                        return;
                                }
                        }
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_user_pseudologin()
        {   
                $accountId = Arr::get($_POST, 'id');
                if($accountId)
                {
                    Identity::init()->pseudoLogin($accountId);
                    $this->response->body(json_encode(array('status' => 'ok'))); 
                    return;
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
}