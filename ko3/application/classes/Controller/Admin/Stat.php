<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Stat extends Controller_Admin_Base {

	public function action_index() 
        {
                $period = intval(Arr::get($_GET, 'period', 'month'));
                $period = in_array($period, ['month', 'months', 'years']) ? $period : 'month';                
                $value = intval(Arr::get($_GET, 'value'));
                
                $stat = Service::factory('AdminStat')->getByParams($period, $value);
                
                $this->template->content = View::factory('pages/admin/stat/index', compact('period', 'stat'));
        }
}
