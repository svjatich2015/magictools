<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Draw extends Controller_Admin_Base {

	public function action_index() 
        {
                HTTP::redirect('admin/draw/templates');
        }

        public function action_templates() 
        {
                $action = $this->request->param('param1', 'index');
                
                if($action == 'index')
                {
                        $tplsRepo = Repo::factory('DrawTemplate');                        
                        
                        if($tplsRepo->countAll(array('status', Model_DrawTemplate::STATUS_ENABLED)) < 1)
                        {
                                // Редиректим на страничку добавления темы для шаблонов
                                //HTTP::redirect('admin/draw/templates/add');
                        }
                }
                else if(! in_array($action, array('add', 'edit', 'delete', 'up', 'down', 'disable', 'enable')))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('admin/draw/templates');
                }
                
                $this->{'templates_' . $action}();
        }

        public function action_templates_cat() 
        {
                $action = $this->request->param('param1');
                
                if(is_numeric($action))
                {
                       $action = 'index'; 
                }
                else if(! in_array($action, array('add', 'edit', 'delete', 'up', 'down', 'disable', 'enable')))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('admin/draw/templates');
                }
                
                $this->{'templates_cat_' . $action}();
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           Шаблоны рисовалки                                                *
         *                                                                                                            *
         **************************************************************************************************************/

        protected function templates_index() 
        {
                $cats = Service::factory('Draw')->getTemplatesCat();
                $this->template->content = View::factory('pages/admin/draw/templates', compact('cats'));
                $this->template->jsCode  = View::factory('javascripts/admin/draw/templates');
        }

        protected function templates_add() 
        {
                if(Arr::keys_exists(array('title', 'description', 'cat', 'type'), $_POST))
                {
                        extract(Arr::extract($_POST, array('title', 'description', 'cat', 'type', 'what')));
                        
                        Service::factory('Draw')->createTemplate($title, $description, $cat, $type, $what);
                        
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('admin/draw/templates_cat/' . Arr::get($_POST, 'cat'));
                }
                
                $themes = Service::factory('Draw')->getTemplatesThemes();
                $cats = Service::factory('Draw')->getTemplatesCat();
                
                $this->template->content = View::factory('pages/admin/draw/templates/add', compact('cats'));
                
                $this->template->styles  = array('assets/jquery/ui/1.12.1/themes/base/jquery-ui.css');
                $this->template->scripts = array('assets/jquery/ui/1.12.1/jquery-ui.js');
                $this->template->jsCode  = View::factory('javascripts/admin/draw/templates/add', array('themes' => $themes));
        }

        protected function templates_edit() 
        {
                $tplId = $this->request->param('param2', FALSE);
                
                if(Arr::keys_exists(array('title', 'description', 'cat', 'type'), $_POST))
                {
                        extract(Arr::extract($_POST, array('title', 'description', 'cat', 'type', 'what')));
                        
                        if(Service::factory('Draw')->updateTemplate($tplId, $title, $description, $cat, $type, $what))
                        {
                                // Редиректим на страничку шаблонов
                                HTTP::redirect('admin/draw/templates_cat/' . Arr::get($_POST, 'cat'));
                        }
                }
                
                if(! is_numeric($tplId) || FALSE == ($tpl = Service::factory('Draw')->getTemplate($tplId)))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('admin/draw/templates');   
                }
                
                $themes = Service::factory('Draw')->getTemplatesThemes();
                $cats = Service::factory('Draw')->getTemplatesCat();
                
                $this->template->content = View::factory('pages/admin/draw/templates/edit', compact('tpl', 'cats'));
                
                $this->template->styles  = array('assets/jquery/ui/1.12.1/themes/base/jquery-ui.css');
                $this->template->scripts = array('assets/jquery/ui/1.12.1/jquery-ui.js');
                $this->template->jsCode  = View::factory('javascripts/admin/draw/templates/add', array('themes' => $themes));
        }

        protected function templates_delete() 
        {
                $catId = Arr::get($_POST, 'cat', FALSE);    
                $tplId = Arr::get($_POST, 'tpl', FALSE);
                
                if($tplId)
                {
                        Service::factory('Draw')->deleteTemplate($tplId);
                }
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat/' . $catId);
        }

        protected function templates_up() 
        {
                $catId = $this->request->param('param2', FALSE);    
                $tplId = $this->request->param('param3', FALSE);      
                
                if(! is_numeric($catId) || ! is_numeric($tplId))
                {
                    // Редиректим на страничку шаблонов
                    HTTP::redirect('admin/draw/templates');
                }
                
                Service::factory('Draw')->templateOrderUp($tplId, $catId);
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat/' . $catId);
        }

        protected function templates_down() 
        {
                $catId = $this->request->param('param2', FALSE);    
                $tplId = $this->request->param('param3', FALSE);      
                
                if(! is_numeric($catId) || ! is_numeric($tplId))
                {
                    // Редиректим на страничку шаблонов
                    HTTP::redirect('admin/draw/templates');
                }
                
                Service::factory('Draw')->templateOrderDown($tplId, $catId);
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat/' . $catId);
        }

        protected function templates_disable() 
        {
                $catId = $this->request->param('param2', FALSE);    
                $tplId = $this->request->param('param3', FALSE);      
                
                if(! is_numeric($catId) || ! is_numeric($tplId))
                {
                    // Редиректим на страничку шаблонов
                    HTTP::redirect('admin/draw/templates');
                }
                
                Service::factory('Draw')->templateDisable($tplId);
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat/' . $catId);
        }

        protected function templates_enable() 
        {
                $catId = $this->request->param('param2', FALSE);    
                $tplId = $this->request->param('param3', FALSE);      
                
                if(! is_numeric($catId) || ! is_numeric($tplId))
                {
                    // Редиректим на страничку шаблонов
                    HTTP::redirect('admin/draw/templates');
                }
                
                Service::factory('Draw')->templateEnable($tplId);
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat/' . $catId);
        }

        protected function templates_cat_index() 
        {
                $catId = $this->request->param('param1', FALSE);
                
                if(! is_numeric($catId) || FALSE == ($cat = Service::factory('Draw')->getTemplatesCat($catId)))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('admin/draw/templates_cat');   
                }
                
                $tpls = Service::factory('Draw')->getTemplatesByCat($catId);
                
                $this->template->content = View::factory('pages/admin/draw/templates/cat/index', compact('cat', 'tpls'));
                $this->template->jsCode  = View::factory('javascripts/admin/draw/templates/cat/index');
        }

        protected function templates_cat_add() 
        {       
                // Проверяем наличие POST-переменных
                if(strlen(Arr::get($_POST, 'description')) > 0 && strlen(Arr::get($_POST, 'title')) > 0)
                {
                        extract(Arr::extract($_POST, array('title', 'description')));
                        
                        if(Service::factory('Draw')->createTemplatesCat($title, $description))
                        {
                                HTTP::redirect('admin/draw/templates_cat');
                        }
                }
                
                $this->template->content = View::factory('pages/admin/draw/templates/cat/add');
                $this->template->jsCode  = View::factory('javascripts/admin/draw/templates/cat/add');
        }

        protected function templates_cat_edit() 
        {
                $catId = $this->request->param('param2', FALSE);
                
                if(! is_numeric($catId) || FALSE == ($category = Service::factory('Draw')->getTemplatesCat($catId)))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('admin/draw/templates_cat');   
                }

                // Проверяем наличие POST-переменных
                if(strlen(Arr::get($_POST, 'description')) > 0 && strlen(Arr::get($_POST, 'title')) > 0)
                {
                        extract(Arr::extract($_POST, array('title', 'description')));
                        
                        $category->set('title', $title)->set('description', $description)->save();
                        
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('admin/draw/templates_cat');
                }
                
                $this->template->content = View::factory('pages/admin/draw/templates/cat/edit', compact('category'));
                $this->template->jsCode  = View::factory('javascripts/admin/draw/templates/cat/add');
        }

        protected function templates_cat_delete() 
        {
                if($catId = Arr::get($_POST, 'cat', FALSE))
                {
                        Service::factory('Draw')->deleteTemplatesCat($catId);
                }
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat');
        }

        protected function templates_cat_up() 
        {
                $catId = $this->request->param('param2', FALSE);      
                
                if(is_numeric($catId))
                {
                    Service::factory('Draw')->templatesCatOrderUp($catId);
                }
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat');
        }

        protected function templates_cat_down() 
        {
                $catId = $this->request->param('param2', FALSE);
                
                if(is_numeric($catId))
                {
                    Service::factory('Draw')->templatesCatOrderDown($catId);
                }
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat');
        }

        protected function templates_cat_disable() 
        {
                $catId = $this->request->param('param2', FALSE);
                
                if(is_numeric($catId))
                {
                    Service::factory('Draw')->templatesCatDisable($catId);
                }
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat');
        }

        protected function templates_cat_enable() 
        {
                $catId = $this->request->param('param2', FALSE);
                
                if(is_numeric($catId))
                {
                    Service::factory('Draw')->templatesCatEnable($catId);
                }
                
                // Редиректим на страничку шаблонов
                HTTP::redirect('admin/draw/templates_cat');
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_example() 
        {
                $this->action_example();
        }

} // End Welcome
