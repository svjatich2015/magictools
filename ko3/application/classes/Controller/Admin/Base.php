<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Base extends Controller_Base {
    
        /**
         * @var  View  Базовый шаблон всех страниц
         */
        public $template = 'templates/admin';

        public function before()
        {
                parent::before();
                
                // Проверка на наличие статуса админа
                if (! Identity::init()->getStaffInfo())
                {
                        // Редиректим на главную страничку
                        HTTP::redirect('');
                }
                
                // Проверка на наличие права доступа
                if (! Identity::init()->isStaffAllowed(get_class($this), $this->request->action()))
                {
                        // Редиректим на главную страничку админки
                        HTTP::redirect('admin');
                }
                
                // Дефолтные переменные для рендеринга страницы
                if ($this->auto_render)
                {
                        $this->template->title = '[MagicTools Admin]';
                }
        }
}
