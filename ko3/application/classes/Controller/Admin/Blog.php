<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Blog extends Controller_Admin_Base {

	public function action_index() 
        {
                $postsCnt = Service::factory('Blog')->countAllPosts('active&draft');
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;
                
                $rels = array('account.profile', 'cat', 'comments');
                
                if($postsCnt < 1)
                {
                        // Редиректим на страничку создания первого поста
                        HTTP::redirect('admin/blog/create');
                }
                
                if($pageNum < 0 || $offset >= $postsCnt)
                {
                        // Редиректим на страничку создания первого поста
                        HTTP::redirect('admin/blog');
                }
                
                $posts = Service::factory('Blog')->getPosts('active&draft', $limit, $offset, $rels);
                
                $this->template->content = View::factory('pages/admin/blog/index', compact('posts', 'postsCnt', 'limit', 'pageNum'));
                $this->template->jsCode = View::factory('javascripts/admin/blog/index');
        }

	public function action_create() 
        {
                $cats = Service::factory('Blog')->getCats(TRUE);
                $tags = Service::factory('Blog')->getTags(TRUE);
                
                if(count($cats) < 1) {
                        // Редиректим на страничку добавления категории
                        HTTP::redirect('admin/blog/cat_add');
                }
                
                $this->template->scripts[] = '/assets/ckeditor/ckeditor.js';
                $this->template->content = View::factory('pages/admin/blog/create_edit', compact('cats', 'tags'));
                $this->template->jsCode = View::factory('javascripts/admin/blog/create_edit');
        }

	public function action_edit() 
        {
                $post = Service::factory('Blog')->getPostById(intval($this->request->param('param1')));
                $cats = Service::factory('Blog')->getCats(TRUE);
                $tags = Service::factory('Blog')->getTags(TRUE);
                
                if(count($cats) < 1) {
                        // Редиректим на страничку добавления категории
                        HTTP::redirect('admin/blog/cat_add');
                }
                
                $this->template->scripts[] = '/assets/ckeditor/ckeditor.js';
                $this->template->content = View::factory('pages/admin/blog/create_edit', compact('cats', 'tags', 'post'));
                $this->template->jsCode = View::factory('javascripts/admin/blog/create_edit');
        }

	public function action_cats() 
        {
                $catsCnt = Service::factory('Blog')->countAllCats(TRUE);
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;
                
                if($catsCnt < 1)
                {
                        // Редиректим на страничку добавления категории
                        HTTP::redirect('admin/blog/cat_add');
                }
                
                if($pageNum < 0 || $offset >= $catsCnt)
                {
                        // Редиректим на страничку создания первого поста
                        HTTP::redirect('admin/blog/cats');
                }
                
                $cats = Service::factory('Blog')->getCats($limit, $offset, array('posts'));
                
                $this->template->content = View::factory('pages/admin/blog/cats', compact('cats', 'catsCnt', 'limit', 'pageNum'));
                //$this->template->jsCode = View::factory('javascripts/admin/blog/create');
        }

	public function action_cat_add() 
        {
                $this->template->content = View::factory('pages/admin/blog/cat_add');
                //$this->template->jsCode = View::factory('javascripts/admin/blog/create');
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_create() 
        {
                if(!isset($_POST['title']) || !isset($_POST['cat']) || !isset($_POST['html']) || !isset($_POST['tags']))
                {
                        return $this->action_create();
                }
                
                $filter = Arr::extract($_POST, array('title', 'cat', 'html', 'tags'));
                
                extract($filter);
                
                Service::factory('Blog')->createPost($title, $html, $cat, is_array($tags) ? $tags : NULL);
                
                HTTP::redirect('admin/blog');
        }

        public function post_action_edit() 
        {
                if(!isset($_POST['id']) || !isset($_POST['title']) || !isset($_POST['cat']) || 
                   !isset($_POST['html']) || !isset($_POST['tags']))
                {
                        return $this->action_create();
                }
                
                $filter = Arr::extract($_POST, array('id', 'title', 'cat', 'html', 'tags'));
                
                extract($filter);
                
                Service::factory('Blog')->editPost($id, $title, $html, $cat, is_array($tags) ? $tags : NULL);
                
                HTTP::redirect('admin/blog');
        }

        public function post_action_cat_add() 
        {
                if(! isset($_POST['title']))
                {
                        return $this->action_cat_add();
                }
                
                Service::factory('Blog')->addCat($_POST['title']);
                
                HTTP::redirect('admin/blog/cats');
        }
}
