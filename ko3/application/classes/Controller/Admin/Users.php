<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Users extends Controller_Admin_Base {

	public function action_index() 
        {
                $accountsCnt = Service::factory('Account')->countAll();
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;
                
                if($pageNum < 0 || $offset >= $accountsCnt)
                {
                        // Редиректим на страничку со списком клиентов
                        HTTP::redirect('admin/users');
                }
                
                $accounts = Service::factory('Account')->getList(FALSE, TRUE, $limit, $offset);
                
                $contentData = compact('accounts', 'accountsCnt', 'limit', 'pageNum');
                
                $this->template->content = View::factory('pages/admin/users', $contentData);
                $this->template->jsCode = View::factory('javascripts/admin/users');
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_index() 
        {
                if(!isset($_POST['name']) || !isset($_POST['value']))
                {
                        return $this->action_index();
                }
                
                $filter = Arr::extract($_POST, array('name', 'value'));
                
                $accountsCnt = 10;                
                $limit = 100;
                $pageNum = 0;
                
                $accounts = Service::factory('Account')->getListFilter($filter, FALSE, TRUE);
                
                $contentData = compact('accounts', 'accountsCnt', 'limit', 'pageNum');
                
                $this->template->content = View::factory('pages/admin/users', $contentData);
                $this->template->jsCode = View::factory('javascripts/admin/users');
        }
}
