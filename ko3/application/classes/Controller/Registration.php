<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Registration extends Controller_Base {
    
        /**
         * @var  View  Базовый шаблон всех страниц
         */
        public $template = 'templates/registration';

        /**
         * Страница регистрации
         */
        public function action_index()
	{
                $this->template->content = View::factory('pages/registration/index');
                
                // Ошибки формы регистрации
                $this->template->content->errors = array();
	}

        /**
         * Страница успешной регистрации
         */
        public function action_complete()
	{
                $this->template->content = View::factory('pages/registration/complete');
	}

        
        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        /**
         * Обработчик формы регистрации
         */
        public function post_action_index()
	{
                $this->action_index();
                
                if (Arr::keys_exists(array('name', 'surname', 'email', 'invite', 'agree'), $_POST)) {
                        
                        // Проверяем правильность заполнения формы
                        $this->template->content->errors = Service::factory('Account')->registrationValidateData($_POST);
                        
                        if(count($this->template->content->errors) < 1)
                        {
                                // Регистрируем аккаунт
                                Service::factory('Account')->registration($_POST);

                                // Редиректим на страничку успешной регистрации
                                HTTP::redirect('registration/complete');
                        }
                }
	}

}
