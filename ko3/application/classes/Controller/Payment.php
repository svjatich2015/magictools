<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Payment extends Controller_Base {

	public function action_index()
	{
		HTTP::redirect('pay');
	}

	public function action_ok()
	{
		die('Оплата прошла успешно! Деньги зачислятся в течение пары минут.');
	}

	public function action_error()
	{
		die('Оплата не удалась! :( Что-то пошло не так...');
	}

	public function action_confirm()
	{               
                if (Arr::keys_exists(array('sha', 'account', 'pay', 'amount'), $_GET)) 
                {
                        extract(Arr::extract($_GET, array('sha', 'account', 'pay', 'amount')));
                        
                        if(Service::factory('Pay')->isValidParamsByHash($account, $pay, $amount, $sha))
                        {
                                // Продлеваем аккаунт
                                if(Service::factory('Pay')->acceptManualById($pay))
                                {
                                        die('Платеж зачислен!');
                                }
                                
                                die('Ошибка! Запрос был ранее аннулирован!');
                        }
                }
                
                die('Ошибка продления аккаунта');
	}

	public function action_cancel()
	{               
                if (Arr::keys_exists(array('sha', 'account', 'pay', 'amount'), $_GET)) 
                {
                        extract(Arr::extract($_GET, array('sha', 'account', 'pay', 'amount')));
                        
                        if(Service::factory('Pay')->isValidParamsByHash($account, $pay, $amount, $sha))
                        {
                                // Отменяем платеж
                                if(Service::factory('Pay')->cancelManualById($pay))
                                {
                                        die('Запрос аннулирован!');
                                }
                                
                                die('Ошибка! Запрос был аннулирован ранее!');
                        }
                }
                
                die('Ошибка аннулирования запроса!');
	}

}
