<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Archecards extends Controller_BaseArchecards {

        public function action_index()
	{
                $this->template->content = View::factory('pages/archecards/index');
                $this->template->jsCode = View::factory('javascripts/archecards/index');
                $this->template->styles[] = '/assets/magiccards.css?v=1.1';
	}

}
