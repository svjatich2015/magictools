<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Suspended extends Controller_Base {

	/**
	 * @var  boolean  auto render template
	 **/
	//public $auto_render = FALSE;

	public function before()
        {
                parent::before();
                
                if (Identity::init()->getAccountInfo()->expiried > time())
                {
                        // Редиректим на главную
                        HTTP::redirect('');
                }
        }

        public function action_index()
	{
                $this->template->content = View::factory('pages/suspended/index');
	}

}
