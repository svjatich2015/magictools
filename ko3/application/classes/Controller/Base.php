<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Базовый контроллер.
 * 
 * От него наследуют все остальные контроллеры
 * 
 * @category  Controller
 * @author    Svjat Maslov
 */
class Controller_Base extends Controller_Template {
    
        /**
         * @var  array  Контроллеры, не требующие входа в аккаунт
         */
        public $no_login_controllers = array('Login', 'Registration', 'Invite', 'Payment');
    
        /**
         * @var  array  Контроллеры, не требующие оплаченного аккаунта
         */
        public $no_account_expire_controllers = array(
            'Admin', 'Community', 'Page', 'Pay', 'Settings', 'Support', 'Suspended', 'Blog'
        );

        /**
         * Инициализирует переменные перед запуском методов контроллеров (экшенов),
         * поэтому они доступны в этих экшенах.
         */
        public function before()
        {
                parent::before();
                
                // Проверка на залогиненность юзверя
                if (! in_array($this->request->controller(), $this->no_login_controllers))
                {
                        if (! Identity::init()->isLoggedIn())
                        {
                                // Редиректим на страничку авторизации
                                HTTP::redirect('login/?path=' . rawurlencode($_SERVER['REQUEST_URI']));
                        }
                }
        }
}
