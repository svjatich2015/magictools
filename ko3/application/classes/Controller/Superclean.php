<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Superclean extends Controller_BaseSuperclean {

        public function action_index()
	{
                $account = Identity::init()->getAccountInfo();
                $slots   = Service::factory('Superclean')->getSlotsList($account);
                
                $this->template->content = View::factory('pages/superclean/index', compact('slots'));
                $this->template->jsCode  = View::factory('javascripts/superclean/index');
	}

}
