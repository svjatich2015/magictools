<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Clients extends Controller_Base {

	public function action_index() 
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $clientsCnt = Service::factory('Client')->countAllByAccount($accountId, TRUE);
                
                if($clientsCnt < 1)
                {
                        // Редиректим на страничку добавления клиента
                        HTTP::redirect('clients/add');
                }
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;
                
                if($pageNum < 0 || $offset >= $clientsCnt)
                {
                        // Редиректим на страничку со списком клиентов
                        HTTP::redirect('clients');
                }
                
                $clients = Service::factory('Client')->getByAccount($accountId, TRUE, $limit, $offset);
                
                $contentData = compact('clients', 'clientsCnt', 'limit', 'pageNum');
                
                $this->template->content = View::factory('pages/clients/index', $contentData);
                $this->template->jsCode = View::factory('javascripts/clients/index');
        }

	public function action_add() 
        {
                $this->template->content = View::factory('pages/clients/add');
                $this->template->jsCode = View::factory('javascripts/clients/add_edit');
        }

	public function action_edit() 
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $client = Service::factory('Client')->getOneById(intval($this->request->param('param1')));
                $themes = $client->themes->find_all();
                
                if(! $client || $client->accountId !== $accountId)
                {
                        // Редиректим на страничку со списком клиентов
                        HTTP::redirect('clients');
                }
                
                $this->template->content = View::factory('pages/clients/edit', compact('client', 'themes'));
                $this->template->jsCode = View::factory('javascripts/clients/add_edit', compact('themes'));
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_add() 
        {
                $this->action_add();
                
                if(Arr::keys_exists(array('isbodys', 'isspheres'), $_POST) && is_array(Arr::get($_POST, 'what'))
                        && strlen(Arr::get($_POST, 'title')) > 0 && strlen(Arr::get($_POST, 'who')) > 0)
                {                        
                        extract(Arr::extract($_POST, array('title', 'who', 'what', 'isbodys', 'isspheres')));

                        // Создаем клиента в БД
                        $client = Service::factory('Client')->create($title, $who, $what, $isbodys, $isspheres);
                        
                        if($client)
                        {
                                // Редиректим на страничку со списком клиентов
                                HTTP::redirect('clients');
                        }
                }
        }

        public function post_action_edit() 
        {
                $this->action_edit();
                
                $clientId = $this->request->param('param1');
                $accountId = Identity::init()->getAccountInfo()->id;
                
                // Проверяем POST-переменные на валидность
                if(Arr::keys_exists(array('isbodys', 'isspheres'), $_POST) && is_array(Arr::get($_POST, 'what')) &&
                        trim(Arr::get($_POST, 'title')) !== "" && trim(Arr::get($_POST, 'who')) !== "")
                {
                        $validInputs = TRUE;
                }
                else
                {
                        $validInputs = FALSE;
                }
                
                if($clientId && $validInputs && Service::factory('Client')->belongsToAccount($clientId, $accountId))
                {                        
                        extract(Arr::extract($_POST, array('title', 'who', 'what', 'isbodys', 'isspheres')));

                        // Редактируем клиента
                        $client = Service::factory('Client')->edit($clientId, $title, $who, $what, $isbodys, $isspheres);
                        
                        if($client)
                        {
                                // Редиректим на страничку со списком клиентов
                                HTTP::redirect('clients');
                        }
                }
        }

        public function post_action_delete() 
        {                
                $clientId = Arr::get($_POST, 'client');
                $accountId = Identity::init()->getAccountInfo()->id;
                
                if($clientId && Service::factory('Client')->belongsToAccount($clientId, $accountId))
                {       
                        Service::factory('Client')->delete(Arr::get($_POST, 'client'));
                }
                
                // Редиректим на страничку со списком клиентов
                HTTP::redirect('clients');
        }

}
