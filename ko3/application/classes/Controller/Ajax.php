<?php

 class Controller_Ajax extends Controller_Base {

	/**
	 * @var  boolean  auto render template
	 **/
	public $auto_render = FALSE;
     
        public function before()
        {        
                if (!Request::initial()->is_ajax())
                {
                    throw new HTTP_Exception_404('The requested URL :uri was not found on this server.', 
                            array(':uri' => Request::detect_uri()));
                }

                parent::before();
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/
        
        public function post_action_activate_free_until()
        {
                if(isset($_POST['tool']))
                {
                        $accountId = Identity::init()->getAccountInfo()->id;
                        $toolId = $_POST['tool'];
                        $expiried = Service::factory('Account')->activateFreeUntilTool($accountId, $toolId);
                        
                        if($expiried)
                        {
                                $this->response->body(json_encode(array(
                                    'status' => 'ok',
                                    'expiried' => $expiried,
                                    'expiriedRusStr' => Date::genetiveRusDateByUT($expiried)
                                )));
                                return;
                        }
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_autodraw()
        {                
                $keys = array('type', 'clientId', 'tplId', 'name', 'themes', 'isbodys', 'isspheres');
                
                if(Arr::keys_exists($keys, $_POST))
                {
                        extract(Arr::extract($_POST, $keys));
                        
                        $entityId = NULL;
                        
                        if ($type == 'tpl') 
                        {
                                $entityId = intval($tplId);

                        }
                        else if ($type == 'client')
                        {
                                $entityId = intval($clientId);
                        }
                        
                        $info = Service::factory('Autodraw')->getInfoByType($type, $entityId);
                        
                        if(! $info)
                        {
                                $this->response->body(json_encode(array('status' => 'error1')));
                                return;
                        }
                        
                        extract($info);
                        
                        if((count($themes) < 1 && ! (bool) $isbodys && ! (bool) $isspheres) || 
                                strlen(Arr::get($_POST, 'name')) < 1)
                        {
                                $this->response->body(json_encode(array('status' => 'error2')));
                                return;
                        }
                        
                        $task = Service::factory('Autodraw')->create($title, $name, $type, $themes, $isbodys, $isspheres);

                        $this->response->body(json_encode(array('status' => 'ok'/*, 'task' => $task->id*/)));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_autodraw_delete()
        {                
                if(Arr::get($_POST, 'drawId'))
                {
                        $accountId = Identity::init()->getAccountInfo()->id;

                        extract(Arr::extract($_POST, array('drawId')));
                        
                        if (Service::factory('Autodraw')->deleteOneByAccount($drawId, $accountId))
                        {
                                $this->response->body(json_encode(array('status' => 'ok')));
                        }
                        else 
                        {
                                $this->response->body(json_encode(array('status' => 'error1')));
                        }
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_autodraw_get()
        {                
                $drawId = Arr::get($_POST, 'id');
                
                if($drawId)
                {                        
                        $accountId = Identity::init()->getAccountInfo()->id;                        
                        $autodraw = Service::factory('Autodraw')->getData($drawId, $accountId); 
                        
                        if(! $autodraw)
                        {
                                $this->response->body(json_encode(array('status' => 'error1')));
                                return;
                        }

                        $this->response->body(json_encode(array('status' => 'ok', 'data' => $autodraw)));
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_autodraw_get_log10()
        {                
                if(Arr::get($_POST, 'page'))
                {
                        extract(Arr::extract($_POST, array('page')));
                        
                        $accountId = Identity::init()->getAccountInfo()->id;
                        $autodrawCnt = Service::factory('Autodraw')->countAllByAccount($accountId);
                        $maxPage = ($autodrawCnt < 10) ? 1 : ceil($autodrawCnt / 10);
                        $page = ($page < 1) ? 1 : (($maxPage < $page) ? $maxPage : $page);                        
                        $offset = ($page - 1) * 10;
                        
                        $autodraw = Service::factory('Autodraw')->getByAccount($accountId, FALSE, 10, $offset);
                        $list = Service::factory('Autodraw')->convertListObjectToArray($autodraw);
                        $data = array('status' => 'ok', 'counter' => $autodrawCnt, 'page' => $page, 'list' => $list);
                                
                        $this->response->body(json_encode($data));
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_autodraw_group_delete()
        {                
                if(Arr::get($_POST, 'period'))
                {
                        $accountId = Identity::init()->getAccountInfo()->id;

                        extract(Arr::extract($_POST, array('period')));
                        
                        if (Service::factory('Autodraw')->deleteAllByPeriod($period, $accountId))
                        {
                                $this->response->body(json_encode(array('status' => 'ok')));
                        }
                        else 
                        {
                                $this->response->body(json_encode(array('status' => 'error1')));
                        }
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_autodraw_module_card_create()
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $moduleId = Arr::get($_POST, 'module', FALSE);
                
                if($moduleId && 
                        Service::factory('DrawModule')->addCard($accountId, $moduleId, $_POST))
                {
                        $status = 'ok';
                }
                else
                {
                        $status = 'error';
                }
                
                $this->response->body(json_encode(compact('status')));
        }
        
        public function post_action_autodraw_module_card_status_change()
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $cardId = Arr::get($_POST, 'card', FALSE);
                $status = Arr::get($_POST, 'status', FALSE);                
                
                if(Service::factory('DrawModule')->cardStatusChange($accountId, $cardId, $status))
                {
                        $status = 'ok';
                }
                else
                {
                        $status = 'error';
                }
                
                $this->response->body(json_encode(compact('status')));
        }
        
        public function post_action_autodraw_module_get_cards()
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $moduleId = Arr::get($_POST, 'module', FALSE);                
                $info = ($moduleId) ? 
                        Service::factory('DrawModule')->getFormattedCardsAndOptionsData($accountId, $moduleId) : FALSE;
                
                if($info)
                {
                        $this->response->body(json_encode(array('status' => 'ok', 'info' => $info)));
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_autodraw_re()
        {                
                $drawId = Arr::get($_POST, 'drawId');
                
                if($drawId)
                {                        
                        if(! Service::factory('Autodraw')->repeat($drawId, Identity::init()->getAccountInfo()->id))
                        {
                                $this->response->body(json_encode(array('status' => 'error1')));
                                return;
                        }
                        
                        $this->response->body(json_encode(array('status' => 'ok'/*, 'task' => $task->id*/)));
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_autodraw_status_update()
        {                
                if(Arr::get($_POST, 'drawId'))
                {
                        extract(Arr::extract($_POST, array('drawId')));
                        
                        $accountId = Identity::init()->getAccountInfo()->id;                        
                        $autodraw = Service::factory('Autodraw')->getOneByAccount($drawId, $accountId);

                        if ($autodraw)
                        {
                                $data = Arr::extract($autodraw->as_array(), array('strainLevel', 'status'));
                                $data['date'] = date('d.m.Y - H:i:s', $autodraw->processed);
                                
                                $this->response->body(json_encode(array('status' => 'ok', 'autodraw' => $data)));
                        }
                        else 
                        {
                                $this->response->body(json_encode(array('status' => 'error1')));
                        }
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_autodraw_client_schedule_update()
        {
                $clientId = Arr::get($_POST, 'clientId', FALSE);
                $period = Arr::get($_POST, 'period', FALSE);
            
                if(is_numeric($clientId) && in_array($period, ['hourly', 'daily', 'weekly']))
                {
                        $accountId = Identity::init()->getAccountInfo()->id;
                        $isMin = boolval(Arr::get($_POST, 'isMin'));
                        $duration = boolval(Arr::get($_POST, 'isDuration'));
                        if($duration)
                        {
                            $durationStart = Arr::get($_POST, 'durationStart');
                            $durationFinish = Arr::get($_POST, 'durationFinish');
                            $duration = [
                                'start' => $durationStart ? strtotime($durationStart) : NULL,
                                'finish' => $durationFinish ? strtotime($durationFinish) : NULL
                            ];
                        } 
                        
                        if(! Service::factory('Autodraw')->updateScheduleByClient($accountId, $clientId, $period, $isMin, $duration))
                        {
                                $this->response->body(json_encode(array('status' => 'error1')));
                        }
                        else 
                        {
                                $this->response->body(json_encode(array('status' => 'ok')));
                        }
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_bill_create()
        {
                $period = intval(Arr::get($_POST, 'period'));
                $tools = Arr::get($_POST, 'tools', FALSE);
                
                if(in_array($period, array(1, 3, 6, 12)) && is_array($tools))
                {
                        $accountId = Identity::init()->getAccountInfo()->id;
                        $bill = Service::factory('Bill')->create($accountId, $tools, $period);
                
                        if($bill instanceof Model_Bill)
                        {
                                $this->response->body(json_encode(array(
                                    'status' => 'ok',
                                    'billId' => $bill->id
                                )));
                                return;
                        }
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_get_draw_clients_list()
        {                
                if(is_numeric(Arr::get($_POST, 'page', FALSE)))
                {
                        $accountId = Identity::init()->getAccountInfo()->id;
                        $clientsCnt = Service::factory('Client')->countAllByAccount($accountId, TRUE);
                        
                        extract(Arr::extract($_POST, array('page')));
                        
                        $offset = ($page - 1) * 10;
                
                        if($page < 0 || $offset >= $clientsCnt)
                        {
                                $list = array();
                        }  
                        else 
                        {
                                $list = Service::factory('Client')->getByAccount($accountId, TRUE, 10, $offset);
                                //$list = Service::factory('Client')->convertListObjectToArray($clients);
                        }

                        $this->response->body(json_encode(array('status' => 'ok', 'list' => $list)));
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_get_client_with_themes()
        {                
                if(is_numeric(Arr::get($_POST, 'client', FALSE)))
                {                        
                        extract(Arr::extract($_POST, array('client')));
                        
                        $accountId = Identity::init()->getAccountInfo()->id;
                        $client = Service::factory('Client')->getOneById(intval($client));

                        if(! $client || $client->accountId !== $accountId)
                        {
                                $this->response->body(json_encode(array('status' => 'error')));
                        }
                        else
                        {                                
                                $clientVars = array('id', 'title', 'name', 'isBodies', 'isScheme', 'themesCnt');
                                $info = Arr::extract($client->as_array(), $clientVars);      
                                
                                // Темы для отрисовки
                                $info['themes'] = Service::factory('Client')->getThemesInArray($client);        

                                // Сохраняем время последней отработки
                                $client->set('processed', time())->save();
                                
                                $this->response->body(json_encode(array('status' => 'ok', 'info' => $info)));
                        }
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_get_category_templates()
        {                
                if(is_numeric(Arr::get($_POST, 'cat', FALSE)))
                {                        
                        $info = array();
                        
                        extract(Arr::extract($_POST, array('cat')));
                        
                        //$templates = Repo::factory('DrawTemplate')->findAll(array('categoryId', intval($cat)));
                        $templates = Service::factory('Draw')->getTemplatesByCat(intval($cat));

                        foreach($templates as $template)
                        {
                                $info[] = $template->as_array();                                
                        }
                        
                        $this->response->body(json_encode(array('status' => 'ok', 'info' => $info)));
                }
                else
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                }
        }
        
        public function post_action_get_template_form()
        {     
                $tplId = Arr::get($_POST, 'template', FALSE);
        
                if(is_numeric($tplId) && FALSE !== ($info = Service::factory('Draw')->getTemplateInfo($tplId)))
                {                              
                        $this->response->body(json_encode(array('status' => 'ok', 'info' => $info)));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_create_client_by_template()
        {
                if(Arr::keys_exists(array('tplId', 'isbodys', 'isspheres'), $_POST) 
                        && is_array(Arr::get($_POST, 'themes')) && strlen(Arr::get($_POST, 'name')) > 0)
                {                        
                        extract(Arr::extract($_POST, array('tplId', 'name', 'themes', 'isbodys', 'isspheres')));
                        
                        $title = Arr::get($_POST, 'title', NULL);
        
                        $client = Service::factory('Client')
                                ->createByTemplate($tplId, $title, $name, $themes, $isbodys, $isspheres);
                        
                        if($client)
                        {                              
                                $this->response->body(json_encode(array('status' => 'ok')));
                                return;
                        }

                        $this->response->body(json_encode(array('status' => 'error1')));
                }
                
                $this->response->body(json_encode(array('status' => 'error', 'data' => $_POST)));
        }
        
        public function post_action_service_autopay_switch()
        {
                $tools = [Model_Tool::AUTODRAW, Model_Tool::SUPERCLEAN, Model_Tool::ARCHECARDS, Model_Tool::AUTOCOACH];
                
                $accountId = Identity::init()->getAccountInfo()->id;
                $toolId = intval(Arr::get($_POST, 'tool'));
                $cond = intval(Arr::get($_POST, 'cond'));
                
                if(! in_array($toolId, $tools) || ! in_array($cond, [1, 2]))
                { 
                        $this->response->body(json_encode(array('status' => 'error1')));
                        return;
                }                       
        
                if(Service::factory('Account')->switchToolAutopay($accountId, $toolId, $cond))
                {                              
                        $this->response->body(json_encode(array('status' => 'ok')));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_superclean_change_active_slots()
        {
                $slotsNum = intval(Arr::get($_POST, 'slotsNum'));
                if(! in_array($slotsNum, array(1, 3, 5, 10, 25, 50, 100)))
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                        return;
                }                
                $account = Identity::init()->getAccountInfo();
                $toolId = Model_Tool::SUPERCLEAN;
                if(Service::factory('Account')->updateFreeToolSlotsById($account->id, $toolId, $slotsNum))
                {
                        $this->response->body(json_encode(array('status' => 'ok')));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_superclean_slot_upload()
        {
                if(isset($_POST['slot']) && isset($_FILES['file']))
                {
                        $account = Identity::init()->getAccountInfo();
                        $slot = $_POST['slot'];
                        $file = $_FILES['file'];
                        
                        if(Service::factory('Superclean')->uploadImgBySlot($account, $slot, $file))
                        {
                                $this->response->body(json_encode(array('status' => 'ok', 'slot' => $slot)));
                                return;
                        }

                        $this->response->body(json_encode(array('status' => 'error1', 'slot' => $slot)));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error', 'slot' => 0)));
        }
        
        public function post_action_superclean_slot_clear()
        {
                if(isset($_POST['slot']))
                {
                        $account = Identity::init()->getAccountInfo();
                        $slot = $_POST['slot'];
                        
                        if(Service::factory('Superclean')->clearSlot($account, $slot))
                        {
                                $status = 'ok';
                                
                                $this->response->body(json_encode(array('status' => 'ok', 'slot' => $slot)));
                                return;
                        }

                        $this->response->body(json_encode(array('status' => 'error1', 'slot' => $slot)));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error', 'slot' => 0)));
        }
        
        public function post_action_superclean_slot_info()
        {
                if(isset($_POST['slot']))
                {
                        $account = Identity::init()->getAccountInfo();
                        $slot = $_POST['slot'];
                        
                        $info = Service::factory('Superclean')->getSlotInfo($account, $slot);
                        
                        if($info)
                        {
                                $status = 'ok';
                                
                                $this->response->body(json_encode(compact('status', 'slot', 'info')));
                                return;
                        }

                        $this->response->body(json_encode(array('status' => 'error1', 'slot' => $slot)));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error', 'slot' => 0)));
        }
        
        public function post_action_superclean_recalculate_expiried()
        {
                $slotsNum = intval(Arr::get($_POST, 'slotsNum'));
                if(! in_array($slotsNum, array(1, 3, 5, 10, 25, 50, 100)))
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                        return;
                }                
                $account = Identity::init()->getAccountInfo();
                $toolId = Model_Tool::SUPERCLEAN;
                $accountTool = Service::factory('Account')->recalculateToolExpiriedBySlotsId($account->id, $toolId, $slotsNum);
                if($accountTool instanceof Model_AccountsTool)
                {
                        $this->response->body(
                            json_encode(
                                array(
                                    'status' => 'ok',
                                    'expiriedRusStr' => Date::genetiveRusDateByUT($accountTool->expiried)
                                )
                            )
                        );
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_pay_confirm()
        {     
                if(! Arr::keys_exists(array('method', 'cash'), $_POST))
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                        return;
                }
                
                $account = Identity::init()->getAccountInfo();
                $methods = array('paypal', 'sbrf');

                extract(Arr::extract($_POST, array('method', 'cash', 'bill')));

                if(in_array($method, $methods))
                {
                        if(Service::factory('Pay')->manualConfirmation($account, $cash, $method, $bill))
                        {
                                $this->response->body(json_encode(array('status' => 'ok')));  
                                return;
                        }
                }
                
                $this->response->body(json_encode(array('status' => 'error')));
        }
        
        public function post_action_blog_delete_comment()
        {     
                if(! isset($_POST['commentId']))
                {
                        $this->response->body(json_encode(array('status' => 'error')));
                        return;
                }
                
                $accountId = Identity::init()->getAccountInfo()->id;
                $commentId = $_POST['commentId'];
                
                if(! Service::factory('Blog')->trashCommentByAccountId($commentId, $accountId))
                {
                        $this->response->body(json_encode(array('status' => 'error2')));
                        return;
                }
                
                $this->response->body(json_encode(array('status' => 'ok'))); 
        }
}