<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Page extends Controller_Base {

	public function action_index()
	{        
                $page = $this->request->param('page', FALSE);
                
                if (! $page || ! file_exists(APPPATH . 'views/pages/page/' . $page . '.php'))
                {
                    throw new HTTP_Exception_404(APPPATH . 'views/pages/page/' . $page . '.php', 
                            array(':uri' => Request::detect_uri()));
                }
                
                $this->template->content = View::factory('pages/page/' . $page);
	}

} // End Welcome
