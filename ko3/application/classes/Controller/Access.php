<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Access extends Controller_Base {

        public function action_archecards()
	{
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'archecards');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect('archecards');
                        return;
                }                
                $tool = Repo::factory('Tool')->find(array('code', 'archecards'));
                
                $this->template->content = View::factory('pages/access/archecards', compact('service', 'tool'));
	}

        public function action_autocoach()
	{
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'autocoach');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect('autocoach');
                        return;
                }
                $tool = Repo::factory('Tool')->find(array('code', 'autocoach'));
                
                $this->template->content = View::factory('pages/access/autocoach', compact('service', 'tool'));
	}

        public function action_draw()
	{
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'draw');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect('draw');
                        return;
                }
                $tool = Repo::factory('Tool')->find(array('code', 'draw'));
                
                $this->template->content = View::factory('pages/access/draw', compact('service', 'tool'));
	}

        public function action_superclean()
	{
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'superclean');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect('superclean');
                        return;
                }
                $tool = Repo::factory('Tool')->find(array('code', 'superclean'));
                
                $this->template->content = View::factory('pages/access/superclean', compact('service', 'tool'));
	}

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_draw() 
        {
                $path = isset($_GET['ret_path']) ? rawurldecode($_GET['ret_path']) : 'draw';                
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'draw');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect($path);
                        return;
                }
                
                if(Arr::keys_exists(array('accept'), $_POST))
                {
                        $account = Identity::init()->getAccountInfo();
                        $tool    = Repo::factory('Tool')->find(array('code', 'draw'));
                        
                        Service::factory('Account')->activateTool($account, $tool);

                        HTTP::redirect($path);
                }
                
                $this->action_draw();
        }

        public function post_action_autocoach() 
        {
                $path = isset($_GET['ret_path']) ? rawurldecode($_GET['ret_path']) : 'autocoach';
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'autocoach');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect($path);
                        return;
                }
                
                if(Arr::keys_exists(array('accept'), $_POST))
                {
                        $account = Identity::init()->getAccountInfo();
                        $tool    = Repo::factory('Tool')->find(array('code', 'autocoach'));
                        
                        Service::factory('Account')->activateTool($account, $tool);

                        HTTP::redirect($path);
                }
                
                $this->action_autocoach();
        }

        public function post_action_archecards() 
        {
                $path = isset($_GET['ret_path']) ? rawurldecode($_GET['ret_path']) : 'archecards';                
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'archecards');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect($path);
                        return;
                }
                
                if(Arr::keys_exists(array('accept'), $_POST))
                {
                        $account = Identity::init()->getAccountInfo();
                        $tool    = Repo::factory('Tool')->find(array('code', 'archecards'));
                        
                        Service::factory('Account')->activateTool($account, $tool);

                        HTTP::redirect($path);
                }
                
                $this->action_archecards();
        }

        public function post_action_superclean() 
        {
                $path = isset($_GET['ret_path']) ? rawurldecode($_GET['ret_path']) : 'superclean';               
                $service = Arr::get(Identity::init()->getAccountServicesList(), 'superclean');
                if ($service && ($service->freeUntil > time() || $service->expiried > time()))
                {
                        HTTP::redirect($path);
                        return;
                }
                
                if(Arr::keys_exists(array('accept', 'slots'), $_POST))
                {
                        $account = Identity::init()->getAccountInfo();
                        $tool    = Repo::factory('Tool')->find(array('code', 'superclean'));
                        
                        extract(Arr::extract($_POST, array('slots')));
                        
                        Service::factory('Account')->activateTool($account, $tool, $slots);

                        HTTP::redirect($path);
                        return;
                }
                
                $this->action_superclean();
        }
}
