<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Pay extends Controller_Base {

	public function action_index()
	{
                HTTP::redirect('pay/' . Conf::get_option('ps_page'));
	}

	public function action_default()
	{
		$this->template->content = View::factory('pages/pay/index');
		$this->template->jsCode  = View::factory('javascripts/pay/index');
	}

	public function action_prodamus()
	{
		$this->template->content = View::factory('pages/pay/test_prodamus');
		$this->template->jsCode  = View::factory('javascripts/pay/prodamus');
	}

	public function action_sbrf()
	{
		HTTP::redirect('pay');
	}

	public function action_paypal()
	{
		HTTP::redirect('pay');
	}

	public function action_cryptocloud()
	{
		HTTP::redirect('pay');
	}
        
        
        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/
        
        public function post_action_paypal()
        {  
                extract(Arr::extract($_POST, array('cash')));
                
		$this->template->content = View::factory('pages/pay/paypal', compact('cash'));
		$this->template->jsCode  = View::factory('javascripts/pay/manual');
        }  
        
        public function post_action_sbrf()
        {       
                extract(Arr::extract($_POST, array('cash', 'bill')));
                
		$this->template->content = View::factory('pages/pay/sbrf', compact('cash', 'bill'));
		$this->template->jsCode  = View::factory('javascripts/pay/manual');
        } 
        
        public function post_action_prodamus()
        {       
                extract(Arr::extract($_POST, array('cash', 'method', 'bill')));
                
                if(! is_numeric($cash) || ! Service::factory('Pay')->isValidProdamusMethod($method))
                {
                        die('Invalid request!');
                }
                
                $account = Identity::init()->getAccountInfo();
                $pay = Service::factory('Pay')->createProdamus($account, $cash, $method, $bill);
                $link = Service::factory('Pay')->getProdamusLinkByParams($pay, $cash, $method);
                
		HTTP::redirect($link);
        } 
        
        public function post_action_cryptocloud()
        {       
                extract(Arr::extract($_POST, array('cash', 'bill')));
                
                if(! is_numeric($cash))
                {
                        die('Invalid request!');
                }
                
                $account = Identity::init()->getAccountInfo();
                $pay = Service::factory('Pay')->createCryptocloud($account, $cash, $bill);
                $link = Service::factory('Pay')->getCryptocloudLinkByParams($pay, $cash);
                
		HTTP::redirect($link);
        }

} // End Welcome
