<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Services extends Controller_Base {

	public function action_index() 
        {
                $tools = Service::factory('Account')->getTools(Identity::init()->getAccountInfo());
                
                $this->template->content = View::factory('pages/services/index', compact('tools'));
                $this->template->jsCode = View::factory('javascripts/services/index', compact('tools'));
                $this->template->styles[] = '/assets/materialswitch.css';
        }

	public function action_bills() 
        {
                $bills = Repo::factory('Bill')->findAll(array('accountId', Identity::init()->getAccountInfo()->id));
                
                $this->template->content = View::factory('pages/services/bills', compact('bills'));
                //$this->template->jsCode = View::factory('javascripts/services/bills', compact('tools'));
        }
}
