<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Базовый контроллер "автокоуча".
 * 
 * @category  Controller
 * @author    Svjat Maslov
 */
class Controller_BaseAutocoach extends Controller_Base {
    
        /**
         * @var
         */
        public $tool_id = Model_Tool::AUTOCOACH;

        /**
         * Инициализирует переменные перед запуском методов контроллеров (экшенов),
         * поэтому они доступны в этих экшенах.
         */
        public function before()
        {
                parent::before();
                
                $service = Arr::get(Identity::init()->getAccountServicesList(), $this->tool_id);
                // Проверка на доступ к сервису
                if (! $service || ($service->freeUntil < time() && $service->expiried < time()))
                {
                        // Редиректим на страничку авторизации
                        HTTP::redirect('access/autocoach/?ret_path=' . rawurlencode($_SERVER['REQUEST_URI']));
                }
                
                $this->template->menu = array('controller' => 'tools', 'action' => 'autocoach');
        }
}
