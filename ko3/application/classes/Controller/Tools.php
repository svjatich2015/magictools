<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Tools extends Controller_Base {

	/**
	 * @var  boolean  auto render template
	 **/
	//public $auto_render = FALSE;

	public function action_index()
	{
                // Редиректим на страничку "С чего начать?"
                //HTTP::redirect('pages/manual/index');
                
		$this->template->content = View::factory('pages/tools/index');
                //echo 'Привет, '. Identity::init()->getAccountInfo()->profile->name . '!';
	}

} // End Welcome
