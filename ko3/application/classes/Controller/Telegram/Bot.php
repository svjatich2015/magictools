<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Контроллер TG SDK
 * 
 * @category  Controller
 * @author    Svjat Maslov
 */
class Controller_Telegram_Bot extends Controller {

        public function action_index()
        {
                $token = Conf::get_option('telegram_bot');
                $bot = new \TelegramBot\Api\Client($token); //Устанавливаем токен, полученный у BotFather
                
                $bot->command('start', function ($message) use ($bot)
                {
                        // Пишем в лог ошибку
                        Kohana::$log->add(Log::DEBUG, serialize($message));
                        Kohana::$log->write();
                        $answer = 'Howdy! Welcome to the stopwatch. Use bot commands or keyboard to control your time.';
                        $bot->sendMessage($message->getChat()->getId(), $answer);
                });
                
                $bot->run();
        }
}
