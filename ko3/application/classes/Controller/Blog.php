<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Blog extends Controller_Base {

	/**
	 * @var  boolean  auto render template
	 **/
	//public $auto_render = FALSE;

	public function action_index()
	{
                $postsCnt = Service::factory('Blog')->countAllPosts('active');
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;
                
                $posts = Service::factory('Blog')->getExcerpts('active', $limit, $offset);
                
                $this->template->content = View::factory('pages/blog/index', compact('posts', 'postsCnt', 'limit', 'pageNum'));
                $this->template->styles[] = '/assets/blog.css?v=1.0';
	}

	public function action_post()
	{        
                $postId = $this->request->param('param1', FALSE);                
                $post = Service::factory('Blog')->getActivePostById($postId, TRUE);
                
                if(! $post) 
                {
                    throw new HTTP_Exception_404('Post #:id Not Found!', 
                            array(':id' => $postId));
                }  
                
                $comments = Service::factory('Blog')->getActiveCommentsByPostId($postId);
                
                $this->template->content = View::factory('pages/blog/post', compact('post', 'comments'));
		$this->template->jsCode  = View::factory('javascripts/blog/post');
                $this->template->styles[] = '/assets/blog.css?v=1.0';
	}

	public function action_category()
	{
                $catId = $this->request->param('param1', FALSE);  
                $category = Service::factory('Blog')->getCategoryById($catId, TRUE);
                
                if(! $category) 
                {
                    throw new HTTP_Exception_404('Category #:id Not Found!', 
                            array(':id' => $catId));
                } 
                
                $postsCnt = Service::factory('Blog')->countAllActivePostsByCategoryId($catId);
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;                             
                $posts = Service::factory('Blog')->getExcerptsByCategoryId($catId);     
                
                $this->template->content = View::factory('pages/blog/category', 
                        compact('category', 'posts', 'postsCnt', 'limit', 'pageNum'));
                $this->template->styles[] = '/assets/blog.css?v=1.0';
	}

	public function action_tag()
	{
                $tagId = $this->request->param('param1', FALSE);  
                $tag = Service::factory('Blog')->getTagById($tagId, TRUE);
                
                if(! $tag) 
                {
                    throw new HTTP_Exception_404('Tag #:id Not Found!', 
                            array(':id' => $tagId));
                } 
                
                $postsCnt = Service::factory('Blog')->countAllActivePostsByTag($tag);
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;                             
                $posts = Service::factory('Blog')->getExcerptsByTag($tag, TRUE);     
                
                $this->template->content = View::factory('pages/blog/tag', 
                        compact('tag', 'posts', 'postsCnt', 'limit', 'pageNum'));
                $this->template->styles[] = '/assets/blog.css?v=1.0';
	}

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_post() 
        {
                $postId = $this->request->param('param1', FALSE); 
                $accountId = Identity::init()->getAccountInfo()->id;
                $profileId = Identity::init()->getAccountInfo()->profile->id;
                $comment = trim(Arr::get($_POST, 'comment', ''));
                
                if($postId && strlen($comment) > 0)
                {                        
                        Service::factory('Blog')->addComment($postId, $accountId, $profileId, $comment);
                        
                        // Редиректим на страничку со списком клиентов
                        HTTP::redirect('blog/post/' . $postId . '#comments');
                }
                else
                {
                        $this->action_post();
                }
        }

} // End Welcome
