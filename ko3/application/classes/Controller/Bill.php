<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Bill extends Controller_Base {

	public function action_index()
	{
            $billId = $this->request->param('id', FALSE);
            if(! $billId)
            {
                HTTP::redirect('services');
            }
            $account = Identity::init()->getAccountInfo();
            $bill = Service::factory('Bill')->getById($billId);
            if($account->id != $bill->accountId)
            {
                HTTP::redirect('services');
            }
            else
            {
                $this->template->content = View::factory('pages/bill/index', compact('bill'));
		$this->template->jsCode  = View::factory('javascripts/bill/index', compact('bill'));
            }
            
	}

	public function action_pay_from_balance()
	{
            $billId = $this->request->param('id', FALSE);
            if(! $billId)
            {
                HTTP::redirect('payment/error');
            }
            $account = Identity::init()->getAccountInfo();
            $balance = Identity::init()->getAccountBalance();
            $bill = Service::factory('Bill')->getById($billId);
            if($bill->status == Model_Bill::STATUS_ACCEPTED || 
                    $account->id != $bill->accountId || $balance < $bill->amount)
            {
                HTTP::redirect('payment/error');
            }
            if(Service::factory('Bill')->accept($billId))
            {
                Service::factory('Account')->deductBalance($account, $bill->amount);
                HTTP::redirect('payment/ok');
            }
            else
            {
                HTTP::redirect('payment/error');
            }
            
	}
        
        
        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/      
        
        public function post_action_pay_from_balance()
        {  
                return $this->action_pay_from_balance();
        }  
}
