<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller_Base {
    
        /**
         * @var  View  Базовый шаблон всех страниц
         */
        public $template = 'templates/login';

        public function action_index()
	{
                $this->template->content = View::factory('pages/login/index');
	}

        public function action_recovery()
	{
                $this->template->content = View::factory('pages/login/recovery');
                
                // Ошибка формы регистрации
                $this->template->content->error = NULL;
	}

        public function action_restore()
	{
                $this->template->content = View::factory('pages/login/restore');
	}

        public function action_reset()
	{
                // Ошибка, если не переданы обязательные параметры
                $error = 'recoveryInvalid';            
                
                if(Arr::keys_exists(array('email', 'hash'), $_GET))
                {
                        $error = Service::factory('Account')->resetPassword($_GET['email'], $_GET['hash']);
                }
                
                $this->template->content = View::factory('pages/login/reset', compact('error'));
	}

        public function action_logout()
	{
                Service::factory('Account')->logout();
                HTTP::redirect('');
	}
        
        
        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_index()
	{
                $this->action_index();
                
                if(Arr::keys_exists(array('email', 'password'), $_POST))
                {
                        if(Service::factory('Account')->login($_POST['email'], $_POST['password']))
                        {
                                // Определяем URI последней посещенной страницы
                                $path = isset($_GET['path']) ? rawurldecode($_GET['path']) : '';
                                
                                HTTP::redirect($path);
                        }
                }
	}

        public function post_action_recovery()
	{
                $this->action_recovery();
                
                if($email = Arr::get($_POST, 'email', FALSE))
                {                        
                        // Проверяем правильность заполнения формы
                        $this->template->content->error = Service::factory('Account')->passwordRecovery($email);
                        
                        if(is_null($this->template->content->error))
                        {
                                $redirectPath = URL::site(Route::get('default')->uri(array(
                                                    'controller' => 'login',
                                                    'action' => 'restore'
                                                )), TRUE);

                                // Редиректим на главную страницу кабинета пользователя
                                HTTP::redirect($redirectPath);
                        }
                }
	}

}
