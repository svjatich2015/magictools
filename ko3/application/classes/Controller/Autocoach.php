<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Autocoach extends Controller_BaseAutocoach {

        public function action_index()
	{
                $accountId = Identity::init()->getAccountInfo()->id;
                $autocoach = Service::factory('Autocoach')->getByAccount($accountId);
                
                $contentData = compact('autocoach');
                
                $this->template->content = View::factory('pages/autocoach/index', $contentData);
                $this->template->jsCode = View::factory('javascripts/autocoach/index');
	}

        public function action_add()
	{                
                $this->template->content = View::factory('pages/autocoach/add');
                $this->template->jsCode = View::factory('javascripts/autocoach/add');
	}

        public function action_edit()
	{ 
                $accountId = Identity::init()->getAccountInfo()->id;
                $themeId = $this->request->param('param1');
                $autocoach = Service::factory('Autocoach')->getOneByAccount($themeId, $accountId);
                
                $contentData = compact('autocoach');
                
                $this->template->content = View::factory('pages/autocoach/edit', $contentData);
                $this->template->jsCode = View::factory('javascripts/autocoach/edit');
	}

        public function action_activate() 
        {                
                $themeId = $this->request->param('param1');
                $accountId = Identity::init()->getAccountInfo()->id;
                
                if($themeId && Service::factory('Autocoach')->getOneByAccount($themeId, $accountId))
                {       
                        Service::factory('Autocoach')->activateOneByAccount($themeId, $accountId);
                }
                
                // Редиректим на страничку со списком клиентов
                HTTP::redirect('autocoach');
        }

        public function action_pause() 
        {                
                $themeId = $this->request->param('param1');
                $accountId = Identity::init()->getAccountInfo()->id;
                
                if(Service::factory('Autocoach')->getOneByAccount($themeId, $accountId))
                {       
                        Service::factory('Autocoach')->pauseOneByAccount($themeId, $accountId);
                }
                
                // Редиректим на страничку со списком клиентов
                HTTP::redirect('autocoach');
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_add() 
        {
                $this->action_add();
                
                $accountId = Identity::init()->getAccountInfo()->id;
                
                if(Arr::keys_exists(array('who', 'theme'), $_POST))
                {                        
                        extract(Arr::extract($_POST, array('who', 'theme')));

                        // Создаем тему в БД
                        $theme = Service::factory('Autocoach')->create($accountId, $who, $theme);
                        
                        if($theme)
                        {
                                // Редиректим на страничку со списком клиентов
                                HTTP::redirect('autocoach');
                        }
                }
        }

        public function post_action_edit() 
        {
                $this->action_edit();
                
                $themeId = $this->request->param('param1');
                $accountId = Identity::init()->getAccountInfo()->id;
                
                // Проверяем POST-переменные на валидность
                if(Arr::keys_exists(array('who', 'theme'), $_POST))
                {
                        $validInputs = TRUE;
                }
                else
                {
                        $validInputs = FALSE;
                }
                
                if($themeId && $validInputs && Service::factory('Autocoach')->getOneByAccount($themeId, $accountId))
                {                        
                        extract(Arr::extract($_POST, array('who', 'theme')));

                        // Редактируем тему
                        if(Service::factory('Autocoach')->editOneByAccount($themeId, $accountId, $who, $theme))
                        {
                                // Редиректим на страничку со списком клиентов
                                HTTP::redirect('autocoach');
                        }
                }
        }

        public function post_action_delete() 
        {                
                $themeId = Arr::get($_POST, 'theme');
                $accountId = Identity::init()->getAccountInfo()->id;
                
                if($themeId && Service::factory('Autocoach')->getOneByAccount($themeId, $accountId))
                {       
                        Service::factory('Autocoach')->deleteOneByAccount($themeId, $accountId);
                }
                
                // Редиректим на страничку со списком клиентов
                HTTP::redirect('autocoach');
        }

}
