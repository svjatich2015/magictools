<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Invite extends Controller_Base {
    
        /**
         * @var  View  Базовый шаблон всех страниц
         */
        public $template = 'templates/invite';

        /**
         * Страница запроса инвайта
         */
        public function action_index()
	{
                $this->template->content = View::factory('pages/invite/index');
                
                // Ошибки формы запроса инвайта
                $this->template->content->errors = array();
	}

        /**
         * Страница успешно отправленного запроса
         */
        public function action_complete()
	{
                $this->template->content = View::factory('pages/invite/complete');
	}

        /**
         * Страница успешно отправленного запроса
         */
        public function action_approve()
	{                
                if (Arr::keys_exists(array('name', 'surname', 'email', 'hash'), $_GET)) 
                {
                        extract(Arr::extract($_GET, array('name', 'surname', 'email', 'hash')));
                        
                        if(md5($name.$surname.$email.Conf::get_option('auth_pepper')) == $hash)
                        {
                                // Отправляем инвайт
                                Service::factory('Account')->sendInvite($email, $name, $surname);
                                
                                die('Инвайт отправлен');
                        }
                }
                
                die('Ошибка отправки инвайта');
	}
        
        
        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        /**
         * Обработчик формы регистрации
         */
        public function post_action_index()
	{
                $this->action_index();
                
                if (Arr::keys_exists(array('name', 'surname', 'email', 'text'), $_POST)) {
                        
                        // Проверяем правильность заполнения формы
                        $this->template->content->errors = Service::factory('Account')->inviteValidateData($_POST);
                        
                        if(count($this->template->content->errors) < 1)
                        {
                                extract(Arr::extract($_POST, array('name', 'surname', 'email', 'text')));

                                // Отправляем запрос на инвайт
                                Service::factory('Account')->requestInvite($email, $name, $surname, $text);

                                // Редиректим на страничку успешного запроса
                                HTTP::redirect('invite/complete');
                        }
                }
	}

}
