<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Draw extends Controller_BaseDraw {
        
        public function before() 
        {
                parent::before();
                
                if($this->request->action() !== 'index')
                {
                        HTTP::redirect('draw');
                }
        }

        public function action_index() 
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $clientsCnt = Service::factory('Client')->countAllByAccount($accountId, TRUE);
                
                $clients = Service::factory('Client')->getByAccount($accountId, TRUE, 10, 0);
                $cats = Service::factory('Draw')->getActiveTemplatesCat();
                
                $contentData = compact('cats', 'clients', 'clientsCnt', 'limit', 'pageNum');
                
                $this->template->content = View::factory('pages/draw/index', $contentData);
                $this->template->jsCode = View::factory('javascripts/draw/index');
                $this->template->styles[] = '/assets/draw.css?v=1.2';
                $this->template->scripts[] = '/assets/draw.min.js?v=1.2';
        }

	public function action_index_() 
        {
                $this->template->content = View::factory('pages/draw/index_');
                $this->template->jsCode = View::factory('javascripts/draw/index_');
        }

	public function action_clients() 
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $clientsCnt = Service::factory('Client')->countAllByAccount($accountId, TRUE);
                
                if($clientsCnt < 1)
                {
                        // Редиректим на страничку добавления клиента
                        HTTP::redirect('clients/add');
                }
                
                $limit = intval(Arr::get($_GET, 'rows', 10));                
                $limit = in_array($limit, array(10, 20, 50, 100)) ? $limit : 10;
                        
                $pageNum = intval(Arr::get($_GET, 'page', 1));
                $pageNum = is_int($pageNum) ? $pageNum : 1;
                
                $offset = ($pageNum - 1) * $limit;
                
                if($pageNum < 0 || $offset >= $clientsCnt)
                {
                        // Редиректим на страничку со списком клиентов
                        HTTP::redirect('clients');
                }
                
                $clients = Service::factory('Client')->getByAccount($accountId, TRUE, $limit, $offset);
                
                $contentData = compact('clients', 'clientsCnt', 'limit', 'pageNum');
                
                $this->template->content = View::factory('pages/draw/clients', $contentData);
                $this->template->jsCode = View::factory('javascripts/clients/index');
        }

	public function action_client() 
        {
                $client = Service::factory('Client')->getOneById(intval($this->request->param('param1')));
                
                if(! $client || $client->accountId !== Identity::init()->getAccountInfo()->id)
                {
                        // Редиректим на страничку со списком клиентов
                        HTTP::redirect('draw/clients');
                }
                
                // Темы для отрисовки
                $what = $client->themes->find_all();
                
                $info = array('title' => $client->name);            
                $themes = Service::factory('Draw')->getColorThemes($what, $client->isBodies, $client->isScheme);
                
                // Сохраняем время последней отработки
                $client->set('processed', time())->save();
                        
                $this->draw($themes, $info, $client);
        }

	public function action_template() 
        {
                $tplId = $this->request->param('param1', FALSE);
                
                if(! is_numeric($tplId) || FALSE == ($tpl = Service::factory('Draw')->getTemplate($tplId)))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('draw/templates');   
                }
                
                if($tpl->type == Model_DrawTemplate::TYPE_SIMPLE)
                {
                        $this->template->content = View::factory('pages/draw/template/simple', compact('tpl'));
                        $this->template->jsCode = View::factory('javascripts/draw/template/simple');
                }
                else if($tpl->type == Model_DrawTemplate::TYPE_USER_0_100)
                {
                        $this->template->content = View::factory('pages/draw/template/user_0_100', compact('tpl'));
                }
                else 
                {
                        $this->template->content = View::factory('pages/draw/template/0_100', compact('tpl'));
                }
        }

	public function action_templates() 
        {    
                $cats = Service::factory('Draw')->getTemplatesCat();
                $this->template->content = View::factory('pages/draw/templates', compact('cats'));  
        }

	public function action_templates_cat() 
        {
                $catId = $this->request->param('param1', FALSE);
                
                if(! is_numeric($catId) || FALSE == ($cat = Service::factory('Draw')->getTemplatesCat($catId)))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('draw/templates');   
                }
                
                $tpls = Service::factory('Draw')->getTemplatesByCat($catId);
                
                $this->template->content = View::factory('pages/draw/templates_cat', compact('cat', 'tpls'));
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_index() 
        {
                $this->action_index();
                
                if(Arr::keys_exists(array('what', 'isbodys', 'isspheres'), $_POST) && trim(Arr::get($_POST, 'who')) !== '')
                {
                        extract(Arr::extract($_POST, array('what', 'isbodys', 'isspheres')));
                        
                        $info = array('title' => Arr::get($_POST, 'who'));
                        $themes = Service::factory('Draw')->getColorThemes($what, $isbodys, $isspheres);
                        
                        $this->draw($themes, $info);
                }
        }

        public function post_action_template() 
        {
                $tplId = $this->request->param('param1', FALSE);
                
                if(! is_numeric($tplId) || FALSE == ($tpl = Service::factory('Draw')->getTemplate($tplId)))
                {
                        // Редиректим на страничку шаблонов
                        HTTP::redirect('draw/templates');   
                }
                
                if(Arr::keys_exists(array('isbodys', 'isspheres'), $_POST) && trim(Arr::get($_POST, 'who')) !== '')
                {
                        extract(Arr::extract($_POST, array('isbodys', 'isspheres')));
                        
                        $info = array('title' => Arr::get($_POST, 'who'));
                        
                        if($tpl->type == Model_DrawTemplate::TYPE_SIMPLE)
                        {
                                $what = (Arr::get($_POST, 'mode') == 'special') ? Arr::get($_POST, 'what') : FALSE;
                                
                                if(! is_array($what))
                                {
                                        $what = Service::factory('Draw')->getThemesArrByTpl($tpl);
                                }
                        }
                        else if($tpl->type == Model_DrawTemplate::TYPE_USER_0_100)
                        {
                                $what = Service::factory('Draw')->getThemesArr0To100();
                                $info['theme'] = Arr::get($_POST, 'what');
                        }
                        
                        $themes = Service::factory('Draw')->getColorThemes($what, $isbodys, $isspheres);
                        
                        $this->draw($themes, $info, NULL, $tpl);
                }
                else
                {
                        $this->action_template();
                }
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                               РИСОВАЛКА                                                    *
         *                                                                                                            *
         **************************************************************************************************************/
        
        protected function draw(array $themes, $info, $client = NULL, $tpl = NULL)
        {
                $title = (is_array($info) && Arr::get($info, 'title')) ? Arr::get($info, 'title') : $info;
                
                $this->template->content = View::factory('pages/draw/draw', compact('themes', 'info', 'client', 'tpl'));
                $this->template->jsCode = View::factory('javascripts/draw/draw', compact('themes', 'title'));
                $this->template->styles[] = '/assets/draw.css?v=1.1';
                $this->template->scripts[] = '/assets/draw.min.js?v=1.1';
        }

} // End Welcome
