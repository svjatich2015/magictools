<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Settings extends Controller_Base {

	public function action_index() 
        {
                $this->template->content = View::factory('pages/settings/index');
                $this->template->jsCode = View::factory('javascripts/settings/index');
        }

	public function action_services() 
        {
                $tools = Service::factory('Account')->getTools(Identity::init()->getAccountInfo());
                
                $this->template->content = View::factory('pages/settings/services', compact('tools'));
                $this->template->jsCode = View::factory('javascripts/settings/services', compact('tools'));
        }

        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/

        public function post_action_index() 
        {
                $this->action_index();
                
                if(Arr::keys_exists(array('email', 'currentpassword', 'password'), $_POST))
                {                        
                        $account = Identity::init()->getAccountInfo();
                        
                        extract(Arr::extract($_POST, array('email', 'password', 'currentpassword')));

                        // Изменяем профиль клиента
                        $result = Service::factory('Account')->changeData($account, $email, $password, $currentpassword);
                        
                        $this->template->content = View::factory('pages/settings/index', array('result' => $result));
                }
        }

        public function post_action_services() 
        {
                if(! Arr::keys_exists(array('tools', 'slots'), $_POST))
                {
                        $this->action_services();
                        return;
                }
                
                $account = Identity::init()->getAccountInfo();

                extract(Arr::extract($_POST, array('tools', 'slots')));

                // Изменяем список активных услуг
                $result = Service::factory('Account')->updateTools($account, $tools, $slots);

                $this->template->content = View::factory('pages/settings/services', array('tools' => $result));
                $this->template->jsCode = View::factory('javascripts/settings/services', array('tools' => $result));
        }
}
