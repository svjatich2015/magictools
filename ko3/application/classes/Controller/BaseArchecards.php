<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Базовый контроллер "архекарт".
 * 
 * @category  Controller
 * @author    Svjat Maslov
 */
class Controller_BaseArchecards extends Controller_Base {
    
        /**
         * @var
         */
        public $tool_id = Model_Tool::ARCHECARDS;

        /**
         * Инициализирует переменные перед запуском методов контроллеров (экшенов),
         * поэтому они доступны в этих экшенах.
         */
        public function before()
        {
                parent::before();
                
                $service = Arr::get(Identity::init()->getAccountServicesList(), $this->tool_id);
                // Проверка на доступ к сервису
                if (! $service || ($service->freeUntil < time() && $service->expiried < time()))
                {
                        // Редиректим на страничку авторизации
                        HTTP::redirect('access/archecards/?ret_path=' . rawurlencode($_SERVER['REQUEST_URI']));
                }
                
                $this->template->menu = array('controller' => 'tools', 'action' => 'archecards');
        }
}
