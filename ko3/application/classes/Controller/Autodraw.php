<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Autodraw extends Controller_BaseAutodraw {

        public function action_index() 
        {
                $accountId = Identity::init()->getAccountInfo()->id;
                $clientsCnt = Service::factory('Client')->countAllByAccount($accountId, TRUE);
                $autodrawCnt = Service::factory('Autodraw')->countAllByAccount($accountId);
                                
                $clients = Service::factory('Client')->getByAccount($accountId, TRUE, 10, 0);
                $autodraw = Service::factory('Autodraw')->getByAccount($accountId, FALSE, 10, 0);
                $list = Service::factory('Autodraw')->convertListObjectToArray($autodraw);
                $cats = Service::factory('Draw')->getActiveTemplatesCat();
                $modules = Service::factory('DrawModule')->getAllActiveWithCardsCntByAccountId($accountId);
                
                $contentData = compact('cats', 'modules', 'clients', 'clientsCnt', 'list', 'autodrawCnt');
                
                $this->template->content = View::factory('pages/autodraw/index', $contentData);
                $this->template->jsCode = View::factory('javascripts/autodraw/index');
                $this->template->styles[] = '/assets/draw.css?v=1.1';
        }

} // End Welcome
