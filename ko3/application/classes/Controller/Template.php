<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Template extends Kohana_Controller_Template {
    
        /**
         * @var  View  Базовый шаблон всех страниц
         */
        public $template = 'templates/base';

        public function before()
        {
                if(Conf::get_option('maintenance_mode'))
                {
                        $this->template = 'templates/maintenance_mode';
                }
                
                parent::before();
                
                // Дефолтные переменные для рендеринга страницы
                if ($this->auto_render)
                {
                        $this->template->title      = '[Magic Tools]';
                        $this->template->content    = '';
                        $this->template->jsCode     = '';
                        $this->template->activeMenu = '';
                        $this->template->menu       = array(
                            'controller' => $this->request->controller(), 
                            'action'     => $this->request->action()
                        );
                        $this->template->styles     = array();
                        $this->template->scripts    = array();
                }
        }
}
