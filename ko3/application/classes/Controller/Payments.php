<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Контроллер, взаимодействующий с платежными системами.
 * 
 * @category  Controller
 * @author    Svjat Maslov
 */
class Controller_Payments extends Controller {

        public function action_index()
        {
                die('Hi, Hacker! ;-)');
        }     

        public function action_yandex()
        {
                $this->action_index();
        }

        public function action_prodamus()
        {
                $this->action_index();
        }

        public function action_cryptocloud()
        {
                $this->action_index();
        }
        
        /**************************************************************************************************************
         *                                                                                                            *
         *                                           POST-обработчики                                                 *
         *                                                                                                            *
         **************************************************************************************************************/   
        
        public function post_action_yandex()
        {                
                if (Arr::get($_POST, 'test_notification', 0) != 0) 
                {
                        die('Error: тестовый режим запрещён');
                }

                if (Arr::keys_exists(array('notification_type', 'operation_id', 'currency', 'datetime', 'sender', 
                    'sha1_hash', 'label', 'amount', 'withdraw_amount', 'codepro', 'unaccepted'), $_POST)) {
                
                        //Точно ли это платеж MagicTools?
                        if (strpos(Arr::get($_POST, 'label'), 'MagicTools::') === FALSE) {
                                die('Not MagicTools payment');
                        }

                        if (! Service::factory('Pay')->acceptYandex($_POST))
                        {
                                die ('Error: Неверная контрольная сумма');
                        }
                
                        die('OK');
                }
                                
                $this->action_yandex();
        }  
        
        public function post_action_prodamus()
        {
                $secret_key = Conf::get_option('ps.prodamus.secret_key');
                $headers = apache_request_headers();
                
                try {
                        if ( empty($_POST) ) 
                        {
                                throw new Exception('$_POST is empty');
                        }
                        elseif ( empty($headers['Sign']) ) 
                        {
                                throw new Exception('signature not found');
                        }
                        elseif ( !Hmac::verify($_POST, $secret_key, $headers['Sign']) ) 
                        {
                                throw new Exception('signature incorrect');
                        }

                        Service::factory('Pay')->acceptProdamus($_POST);
                        
                        http_response_code(200);
                        die('success');
                }
                catch (Exception $e) {
                        http_response_code($e->getCode() ? $e->getCode() : 400);
                        printf('error: %s', $e->getMessage());
                        die;
                }
        }
        
        public function post_action_cryptocloud()
        {
                try {
                        if (! isset($_POST['status']) || ! isset($_POST['order_id'])
                            || ! isset($_POST['invoice_id']))
                        {
                                throw new Exception('signature incorrect');
                        }

                        if (! Service::factory('Pay')->acceptCryptocloud($_POST))
                        {
                                throw new Exception('payment is not accepted');
                        }
                        
                        http_response_code(200);
                        die('success');
                }
                catch (Exception $e) {
                        http_response_code($e->getCode() ? $e->getCode() : 400);
                        printf('error: %s', $e->getMessage());
                        die;
                }
        }
}
