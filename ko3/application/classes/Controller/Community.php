<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Community extends Controller_Base {
        
        public function before() {
                parent::before();
                
                $user = array(
                    'id' => Identity::init()->getAccountInfo()->id, 
                    'name' => Identity::init()->getAccountInfo()->profile->name,
                    'avatar' => 'https://magic-tools.ru/' . Identity::init()->getAccountAvatar()
                );
                
                $siteApiKey = 'mRjhm6RzH9VOkS7ErJPiCO9bchdZrZRtT9Tw1FYdhH3KfO6hzpVFm2A1kMkQcHv7';
                $userData = base64_encode(json_encode($user));
                $timestamp = round(microtime(true) * 1000);
                $sign = md5($userData . $siteApiKey . $timestamp);
                
                $this->template->content = View::factory('pages/community/index', compact('userData', 'sign', 'timestamp'));
        }

        public function action_index()
	{
	}

	public function action_qa()
	{
	}

} // End Welcome
