<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Базовый контроллер авторисовалки.
 * 
 * @category  Controller
 * @author    Svjat Maslov
 */
class Controller_BaseAutodraw extends Controller_BaseDraw {

        /**
         * Инициализирует переменные перед запуском методов контроллеров (экшенов),
         * поэтому они доступны в этих экшенах.
         */
        public function before()
        {
                parent::before();
                
                $this->template->menu = array('controller' => 'tools', 'action' => 'autodraw');
        }
}
