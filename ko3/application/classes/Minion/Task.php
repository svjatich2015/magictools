<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Interface that all minion tasks must implement
 */
abstract class Minion_Task extends Kohana_Minion_Task {

	protected $_demonize = FALSE;

	protected $_terminate_signals = array(SIGTERM, SIGINT, SIGHUP, SIGUSR1);

	protected $_signal_handler;
        
	protected $_method = '_executable';

	protected function __construct()
	{
                parent::__construct();
                
		// Populate $_accepted_options based on keys from $_options
		$this->_accepted_options[] = 'mode';
	}

	protected function _executable(array $params)
        {
                if(isset($params['mode']) && in_array($params['mode'], array('demon', 'daemon')))
                {
                        // Ставим флажок, что скрипт работает в режиме "демон"
                        $this->_demonize = TRUE;
                        
                        // Создаем Signal Handler
                        $this->_signal_handler = new PCNTLSignalHandler;
                        
                        // Добавляем обработчики сигналов завершения
                        $this->_signal_handler->addHandler(SIGTERM, array($this, 'terminate'));
                        $this->_signal_handler->addHandler(SIGINT, array($this, 'terminate'));
                }
                
                return $this->_execute($params);
        }
        
        public function terminate()
	{
		// послать SIGTERM детям
		// ...
		Minion_CLI::write(Minion_CLI::color("\nTerminated", 'green'));
		exit(0);
	}

        protected function __isDemonize()
        {
                // Если скрипт запущен не в "демоническом" режиме
                if(! $this->_demonize)
                {
                        Minion_CLI::write(Minion_CLI::color('CLI-script worked only in demonize mode!', 'red'));
                        exit(0);
                }
        }
}
