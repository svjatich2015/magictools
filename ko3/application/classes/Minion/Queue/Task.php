<?php defined('SYSPATH') or die('No direct script access.');

abstract class Minion_Queue_Task extends Minion_Task {
        
        protected function __queueReserve()
        {
                // Если все ОК
                try
                {                        
                        // Резервируем за собой задачку
                        return Queue::instance()->reserve();
                }
                // Если Exception от Pheanstalk
                catch (Pheanstalk\Exception $e)
                {                        
                        // Реконнектимся
                        Queue::instance()->reconnect(TRUE);
                }
                // Если случилась какая-то другая странная и необъяснимая хуйня...
                catch (Exception $e)
                {
                        return NULL;
                }
        }
        
        protected function __queueAdd($queue, $data, $priority, $delay)
        {
                // Если все ОК
                try
                {                        
                        return Queue::instance()->add($queue, $data, $priority, $delay);
                }
                // Если Exception от Pheanstalk
                catch (Pheanstalk\Exception $e)
                {                        
                        // Реконнектимся
                        Queue::instance()->reconnect(TRUE);
                        
                        // Запускаем следующую итерацию
                        return $this->__queueAdd($queue, $data, $priority, $delay);
                }
        }
        
        protected function __queueRelease($jobId, $priority, $delay)
        {
                // Если все ОК
                try
                {                        
                        // Резервируем за собой задачку
                        return Queue::instance()->release($jobId, $priority, $delay);
                }
                // Если Exception от Pheanstalk
                catch (Pheanstalk\Exception $e)
                {                        
                        // Реконнектимся
                        Queue::instance()->reconnect(TRUE);

                        // Запускаем следующую итерацию
                        return $this->__queueRelease($jobId, $priority, $delay);
                }
        }
        
        protected function __queueBury($jobId, $priority)
        {
                // Если все ОК
                try
                {                        
                        // Резервируем за собой задачку
                        return Queue::instance()->bury($jobId, $priority);
                }
                // Если Exception от Pheanstalk
                catch (Pheanstalk\Exception $e)
                {                        
                        // Реконнектимся
                        Queue::instance()->reconnect(TRUE);

                        // Запускаем следующую итерацию
                        return $this->__queueBury($jobId, $priority);
                }
        }
        
        protected function __queueDelete($jobId)
        {
                // Если все ОК
                try
                {                        
                        // Резервируем за собой задачку
                        return Queue::instance()->delete($jobId);
                }
                // Если Exception от Pheanstalk
                catch (Pheanstalk\Exception $e)
                {                        
                        // Реконнектимся
                        Queue::instance()->reconnect(TRUE);

                        // Запускаем следующую итерацию
                        return $this->__queueDelete($jobId);
                }
        }
}
