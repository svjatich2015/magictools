<?php

class HTML extends Kohana_HTML {
    
    public static function tag($tag, $content = NULL, array $attributes = NULL)
    {
            return '<'.$tag.HTML::attributes($attributes).'>'.$content.'</'.$tag.'>';
    }
    
    public static function tag_open($tag, array $attributes = NULL)
    {
            return '<'.$tag.HTML::attributes($attributes).'>';
    }
    
    public static function tag_single($tag, array $attributes = NULL)
    {
            return '<'.$tag.HTML::attributes($attributes).'/>';
    }
    
    public static function tag_close($tag)
    {
            return '</'.$tag.'>';
    }
}
