<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Service Abstract class.
 *
 * @package   Toolsie
 * @category  Abstract 
 * @author    Svjat Maslov
 */
class Abstract_Service {}
