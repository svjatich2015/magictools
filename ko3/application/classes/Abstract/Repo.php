<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Repository Abstract class.
 *
 * @category  Abstract
 * @author    Svjat Maslov
 */
class Abstract_Repo {

    /**
     * @var  string  Имя объекта ORM
     */
    public $objectName;
    
    /**
     *  Конструктор
     * 
     * @throws Kohana_Exception
     */
    function __construct()
    {
        $class = get_called_class();
        
        if ($class == "Abstract_Repo")
        {
            throw new Kohana_Exception("Abstract has no relation to DataBase");
        }
        
        $this->objectName = str_replace("Repo_", "", $class);
        
    }

    /**
     * Возвращает имя объекта ORM
     * 
     * @return  string
     */
    public function getObjectName()
    {
        return $this->objectName;
    }
    
    /**
     * Возвращает правила валидации для модели.
     * 
     * @return array
     */
    public function getValidationRules()
    {
        return array();
    }

    /**
     * Возвращает новую модель ORM
     * 
     * @return  ORM_Model 
     */
    public function getNew()
    {
        return ORM::factory($this->objectName);
    }

    /**
     * Создает новую запись в БД.
     *
     * @param	array	$data	     Массив значений, которые нужно занести в таблицу.
     */
    public function create(array $data)
    {        
        return  ORM::factory($this->objectName)->values($data)->save();
    }

    /**
     * Обновляет существующую запись в БД.
     *
     * @param	array	$data	     Массив значений, которые нужно занести в таблицу.
     * @param	array	$where       Условия, по которым найти запись для обновления
     */
    public function update(array $data, $where = NULL)
    {        
        if(array_key_exists('id', $data))
        {
            //$where = array(array('id', Arr::pop_assoc($data, 'id')));
            $where = array(array(strtolower($this->objectName).'.id', Arr::pop_assoc($data, 'id')));
        }
        
        if(is_array($where))
        {
            if(isset($where[0]) && !is_array($where[0]))
            {
                $where = array($where);
            }
        }
        else
        {
            $where = array();
        }
        
        return $this->_set($data, $where, TRUE);
    }

    /**
     * Создает новую запись в БД или обновляет существующую.
     * 	
     * Также можно использовать в режиме: обнови сществующую, либо создай новую.
     *
     * @param	array	$data	     Массив значений, которые нужно занести в таблицу.
     * @param	array	$where       Условия, по которым найти запись для обновления
     */
    public function set(array $data, $where = NULL)
    {        
        if(array_key_exists('id', $data))
        {
            //$where = array(array('id', Arr::pop_assoc($data, 'id')));
            $where = array(array(strtolower($this->objectName).'.id', Arr::pop_assoc($data, 'id')));
        }
        
        if(is_array($where))
        {
            if(isset($where[0]) && !is_array($where[0]))
            {
                $where = array($where);
            }
        }
        else
        {
            $where = array();
        }
        
        return $this->_set($data, $where, FALSE);
    }

    /**
     * Добавляет несколько записей в таблицу БД одним запросом.
     */
    public function multiSet(array $columns, array $values)
    {
        $colCount = count($columns);
        $ormObj   = ORM::factory($this->objectName);
        $query    = DB::insert($ormObj->table_name(), $columns);  
                
        foreach($values as $vals)
        {
            if(is_array($vals) && $colCount == count($vals))
            {
                $query->values($vals);
            }
        }
        
        try {
            $return = $query->execute();
        } catch ( Database_Exception $e ) {   
            throw new Kohana_Exception($e->getMessage());
        }
        
        return $return;
    }
    
    /**
     * Делает запрос через ORM и возвращает результат
     * 
     * @param  array        $where      Условия поиска строк в таблице
     * @param  array        $relations  Связи, которые нужно подгрузить
     * @param  type         $all        Если FALSE, в запрос подставится "LIMIT 1"
     * @param  type         $orderBy    Условия сортировки результата
     * @param  array        $params     Доп. параметры запроса.
     * @return ORM_Model
     */
    public function find(array $where, array $relations = array(), $orderBy = NULL, array $params = array())
    {
        if(isset($where[0]) && !is_array($where[0]))
        {
            $where = array($where);
        }
            
        return $this->_find($where, $relations, $orderBy, $params);
    }

    /**
     * Делает запрос через ORM и возвращает результат
     * 
     * @param  array        $where      Условия поиска строк в таблице
     * @param  array        $relations  Связи, которые нужно подгрузить
     * @param  array        $orderBy    Условия сортировки результата
     * @param  array        $params     Доп. параметры запроса.
     * @return ORM_Model
     */
    public function findAll(array $where, array $relations = array(), $orderBy = NULL, array $params = array())
    {
        if(isset($where[0]) && !is_array($where[0]))
        {
            $where = array($where);
        }
        
        return $this->_findAll($where, $relations, $orderBy, $params);
    }

    /**
     * Удаляет записи из таблицы через ORM
     * 
     * @param  array    $where  Условия поиска записей для удаления
     * @param  boolean  $all     Если TRUE - синоним $this->deleteAll($where);
     */
    public function delete(array $where, $all = FALSE)
    {
        if(isset($where[0]) && !is_array($where[0]))
        {
            $where = array($where);
        }
        
        if(!$all)
        {
            $this->_delete($where);
        }
        else
        {
            $this->_deleteAll($where);
        }
    }

    /**
     * Удаляет записи из таблицы через ORM
     * 
     * @param  array    $where   Условия поиска записей для удаления
     * @param  boolean  $all     Если FALSE - удаляет только одну запись, соответствующую условиям
     */
    public function deleteAll(array $where)
    {
        if(isset($where[0]) && !is_array($where[0]))
        {
            $where = array($where);
        }
        
        $this->_deleteAll($where);
    }

    /**
     * Возвращает количество записей в таблице в соответствии с условиями.
     * 
     * @param   mixed    $fields  условия поиска
     * @return  integer 
     */
    public function countAll(array $fields = NULL)
    {
        $return = ORM::factory($this->objectName);

        if(!is_null($fields))
        {            
            if(isset($fields[0]) && !is_array($fields[0]))
            {
                $fields = array($fields);
            }
            
            foreach ($fields as $field)
            {
                $return->where($field[0], (isset($field[2]) ? $field[2] : '='), $field[1]);
            }
        }

        return $return->count_all();
    }

    /**
     * Проверяет, существует/существуют ли записи в БД, соответсвующие условиям.
     * 
     * @param   mixed    $fields  условия поиска
     * @return  integer 
     */
    public function exist(array $fields = NULL)
    {
        return ($this->countAll($fields) < 1) ? FALSE : TRUE;
    }

    /**
     * Возвращает первую найденную запись из таблицы.
     * 
     * @return ORM_Model
     */
    public function getOne()
    {
        $fields    = array('id', 0, '>=');
        $relations = array();
        
        return $this->find($fields, $relations);
    }

    /**
     * Возвращает конкретную запись из таблицы по ее ID
     * 
     *      Обертка для $this->find()
     * 
     * @param  integer   $id         ID записи
     * @param  array     $relations  Какие связанные таблицы подгрузить одним запросом?
     * @return ORM_Model
     */
    public function getOneById($id, $relations = NULL)
    {
        //$fields     = 'id', $id, '=');
        $fields     = array(strtolower($this->objectName).'.id', $id, '=');
        $relations  = is_array($relations) ? $relations : array();
        
        return $this->find($fields, $relations);
    }

    /**
     * Возвращает все найденные записи из таблицы.
     * 
     *      Обертка для $this->find()
     * 
     * @param  array      $relations  Какие связанные таблицы подгрузить одним запросом?
     * @param  array      $orderBy    Как упорядочить результаты?
     * @param  array      $params     Дполнительные параметры запроса
     * @return ORM_Model
     */
    public function getAll(array $relations = NULL, $orderBy = NULL, $params = NULL)
    {
        $fields    = array();
        $relations = is_array($relations) ? $relations : array();
        $params    = is_array($params) ? $params : array();
        
        return $this->findAll($fields, $relations, $orderBy, $params);
    }

    /**
     * Возвращат одну запись, соответствующую условиям.
     * 
     *      Обертка для $this->find()
     * 
     * @param  array      $fields     Условия поиска
     * @param  array      $relations  Связи, которые нужно подгрузить
     * @return ORM_Model
     */
    public function getOneByFields(array $fields, array $relations = NULL)
    {
        $relations = is_array($relations) ? $relations : array();
        
        return $this->find($fields, $relations);
    }

    /**
     * Возвращает все записи, соответствующие условиям.
     * 
     *      Обертка для $this->find()
     * 
     * @param  array      $fields     Условия поиска
     * @param  array      $orderBy    Сортировка результатов
     * @param  array      $relations  Связи, которые нужно подгрузить
     * @param  array      $params     Доп.параметры запроса
     * @return ORM_Model
     */
    public function getAllByFields(array $fields, $orderBy = NULL, $relations = NULL, $params = NULL)
    {
        $relations = is_array($relations) ? $relations : array();
        $params    = is_array($params) ? $params : array();
        
        return $this->findAll($fields, $relations, $orderBy, $params);
    }

    /**
     * Возвращат одну запись, соответствующую условию.
     * 
     *      Обертка для $this->find()
     * 
     * @param  string     $key        Имя столбца таблицы
     * @param  string     $value      Искомое значение
     * @param  string     $op         Отношение ('=', '<', '>' и т.д.)
     * @param  array      $relations  Связи, которые нужно подгрузить
     * @return ORM_Model
     */
    public function getOneByField($key, $value, $op = '=', $relations = NULL)
    {
        $fields    = array($key, $value, $op);
        $relations = is_array($relations) ? $relations : array();
        
        return $this->find($fields, $relations);
    }

    /**
     * Возвращает все записи, соответствующие условию.
     * 
     *      Обертка для $this->find()
     * 
     * @param  string     $key        Имя столбца таблицы
     * @param  string     $value      Искомое значение
     * @param  string     $op         Отношение ('=', '<', '>' и т.д.)
     * @param  array      $orderBy    Сортировка результатов
     * @param  array      $relations  Связи, которые нужно подгрузить
     * @param  array      $params     Доп.параметры запроса
     * @return ORM_Model
     */
    public function getAllByField($key, $value, $op = '=', $orderBy = NULL, $relations = NULL, $params = NULL)
    {
        $fields    = array($key, $value, $op);
        $relations = is_array($relations) ? $relations : array();
        $params    = is_array($params) ? $params : array();
        
        return $this->findAll($fields, $relations, $orderBy, $params);
    }

    /**
     * Удаляет записи из таблицы в соответствии с условиями
     * 
     *      Обертка для $this->delete()
     * 
     * @param  array  $fields  Массив условий поиска
     */
    public function deleteAllByFields(array $fields)
    {
        $this->delete($fields, TRUE);
    }

    /**
     * Удаляет одну запись из таблицы в соответствии с условиями.
     * 
     * Если условиям соответствует несколько записей, будет удалена 
     * (!) ПЕРВАЯ найденная запись.
     * 
     *      Обертка для $this->delete()
     * 
     * @param  array  $fields  Массив условий поиска
     */
    public function deleteOneByFields(array $fields)
    {
        $this->delete($fields, FALSE);
    }

    /**
     * Удаляет одну запись из таблицы в соответствии с условием.
     * 
     * Если условию соответствует несколько записей, будет удалена 
     * (!) ПЕРВАЯ найденная запись.
     * 
     *      Обертка для $this->delete()
     * 
     * @param  string  $key    Имя столбца таблицы
     * @param  string  $value  Искомое значение
     * @param  string  $op     Отношение ('=', '<', '>' и т.д.)
     */
    public function deleteOneByField($key, $value, $op = '=')
    {
        $fields[0] = array($key, $value, $op);
        $this->delete($fields, FALSE);
    }

    /**
     * Удаляет записи из таблицы в соответствии с условием.
     * 
     *      Обертка для $this->delete()
     * 
     * @param  string  $key    Имя столбца таблицы
     * @param  string  $value  Искомое значение
     * @param  string  $op     Отношение ('=', '<', '>' и т.д.)
     */
    public function deleteAllByField($key, $value, $op = '=')
    {
        $fields[0] = array($key, $value, $op);
        $this->delete($fields, TRUE);
    }
    
    protected function _set(array $data, array $where, $onlyUpdate)
    {
        $return = ORM::factory($this->objectName);
            
        foreach($where as $w)
        {
            $return->where($w[0], (isset($w[2]) ? $w[2] : '='), $w[1]);                
        }            

        $return->find();

        if(!$return->loaded() && $onlyUpdate == TRUE)
        {
            throw new Kohana_Exception("Updated entry not found");
        }

        $return->values($data)->save();  
        
        return $return;
    }
    
    protected function _find(array $fields, array $relations, $orderBy, array $params)
    {  
        $return = ORM::factory($this->objectName);   
        
        foreach ($relations as $with)
        {
            $return->with($with);
        }
        
        if(isset($orderBy[0]) && isset($orderBy[1]))
        {
            $return->order_by($orderBy[0], $orderBy[1]); // Example: order_by('timestamp', 'DESC');
        }
        
        foreach ($fields as $field)
        {
            $return->where($field[0], (isset($field[2]) ? $field[2] : '='), $field[1]);
        }

        $return->find();

        return $return;        
    }
    
    protected function _findAll(array $fields, array $relations, $orderBy, array $params)
    {        
        $return = ORM::factory($this->objectName);   
        
        foreach ($relations as $with)
        {
            $return->with($with);
        }
        
        if(isset($orderBy[0]) && isset($orderBy[1]))
        {
            $return->order_by($orderBy[0], $orderBy[1]); // Example: order_by('timestamp', 'DESC');
        }
        
        foreach ($fields as $field)
        {
            $return->where($field[0], (isset($field[2]) ? $field[2] : '='), $field[1]);
        }
        
        foreach ($params as $param)
        {
            $return->{$param[0]}($param[1]); 
        }

        return $return->find_all();
        
    }
    
    protected function _delete(array $fields)
    {
        $return = ORM::factory($this->objectName);

        foreach ($fields as $field)
        {
            $return->where($field[0], (isset($field[2]) ? $field[2] : '='), $field[1]);
        }
        
        $return->find();

        if (!$return->loaded())
        {
            throw new Kohana_Exception("Deleted entry not found");
        }

        $return->delete();
    }
    
    protected function _deleteAll(array $fields)
    {
        $return = ORM::factory($this->objectName);

        foreach ($fields as $field)
        {
            $return->where($field[0], (isset($field[2]) ? $field[2] : '='), $field[1]);
        }

        $return->find_all();

        if (count($return) < 1)
        {
            throw new Kohana_Exception("Deleted entry not found");
        }

        foreach ($return as $r)
        {
            $r->delete();
        }
    }

}