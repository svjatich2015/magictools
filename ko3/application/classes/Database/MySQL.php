<?php defined('SYSPATH') OR die('No direct script access.');

class Database_MySQL extends Kohana_Database_MySQL {

	public function connect()
	{
		if (is_resource($this->_connection) && ! mysql_ping($this->_connection))
			$this->_connection = NULL;

		parent::connect();
	}
}
