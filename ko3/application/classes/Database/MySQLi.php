<?php defined('SYSPATH') OR die('No direct script access.');

class Database_MySQLi extends Kohana_Database_MySQLi {
        
	public function connect()
	{
		if (is_resource($this->_connection) && ! mysqli_ping($this->_connection))
			$this->_connection = NULL;

		parent::connect();
	}
        
}
