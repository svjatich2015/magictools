<?php defined('SYSPATH') or die('No direct script access.');
/**
 *   Идентификация юзера
 * 
 * @category  Auth
 * @author    Svjat Maslov
 */
class Identity {

    /**
     * @var  instance  Контейнер для хранения ссылки на "одиночку"
     */
    protected static $_instance;
    
    /**
     *
     * @var array  Данные псевдо-аккаунта 
     */
    protected $_pseudo_account = FALSE;
    
    /**
     *
     * @var array  Данные аккаунта 
     */
    protected $_account = FALSE;
    
    /**
     *
     * @var array  Данные сотрудника 
     */
    protected $_staff = NULL;
    
    /**
     *
     * @var array  Аватар аккаунта 
     */
    protected $_avatar = FALSE;
    
    /**
     *
     * @var array  Настройки подключенных услуг
     */
    protected $_services = FALSE;
    
    /**
     *
     * @var array  Подключенные услуги
     */
    protected $_tools = FALSE;

    /**
     * Метод для доступа к синглтону
     * 
     * @return  instance
     */
    public static function init()
    {
        if (self::$_instance === null)
        {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    /**
     * Логинет юзера
     * 
     * @param    string   $account
     * @return   string
     */
    public function login($accountId)
    {
        // Устанавливаем аутентификационные куки
        $this->_setAuthCookie($accountId);  
    }

    /**
     * Логинет админа под личиной юзера
     * 
     * @param    string   $account
     * @return   string
     */
    public function pseudoLogin($accountId)
    {
        // Устанавливаем аутентификационные куки
        $this->_setPseudoAuthCookie($accountId);  
    }

    /**
     * Разлогинивает юзера
     * 
     * @param    string   $account
     * @return   string
     */
    public function logout()
    {
        $this->_deleteAuthCookie();  
    }

    /**
     * Разлогинивает админа под личиной юзера
     * 
     * @param    string   $account
     * @return   string
     */
    public function pseudoLogout()
    {
        $this->_deletePseudoAuthCookie();  
    }

    /**
     * Проверяет, залогинен ли юзер
     * 
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->_testAuthCookie();
    }

    public function getAccountInfo($isReal = FALSE)
    {
        if(! $this->isLoggedIn())
        {
                return FALSE;
        }
        
        if(! $isReal && $this->_testPseudoAuthCookie())
        {
            return $this->getPseudoAccountInfo();
        }
           
        if(! $this->_account)
        {
                $this->_account = Service::factory('Account')->getById(Cookie::get('user_id'), TRUE);
        }
        
        return $this->_account;
    }

    public function getPseudoAccountInfo()
    {
        if(! $this->_testPseudoAuthCookie())
        {
                return FALSE;
        }
           
        if(! $this->_pseudo_account)
        {
                $this->_pseudo_account = Service::factory('Account')->getById(Cookie::get('pseudo_user_id'), TRUE);
        }
        
        return $this->_pseudo_account;
    }

    public function getAccountBalance()
    {
        if(! $this->isLoggedIn())
        {
                return FALSE;
        }
           
        $balance = $this->getAccountInfo()->balance;  
        
        return ($balance !== 0) ? $balance / 100 : $balance;
    }

    public function getAccountAvatar()
    {
        if(! $this->isLoggedIn())
        {
                return FALSE;
        }
           
        if(! $this->_avatar)
        {
                $id = $this->getAccountInfo()->id;                
                $this->_avatar = 'assets/user_avatar.png';                
                $userAvatarPath = 'assets/avatars/' . substr(chunk_split(str_pad($id, 5, "0", STR_PAD_LEFT), 2, '/'), 0, -1) . '.png';
                
                if(file_exists(DOCROOT . $userAvatarPath)) 
                {
                        $this->_avatar = $userAvatarPath;
                }
        }
        
        return $this->_avatar;
    }

    public function getAccountServices()
    {
        if(! $this->isLoggedIn())
        {
                return FALSE;
        }
        
        if (! $this->_services)
        {
                $this->_services = $this->getAccountInfo()->services->find_all();
        }
        
        return $this->_services;
    }

    public function getAccountServicesList()
    {
        if(! $this->isLoggedIn())
        {
                return FALSE;
        }
        $services = array();        
        foreach ($this->getAccountServices() as $service) 
        {
                $services[$service->toolId] = $service;
        }
        
        return $services;
    }

    public function getAccountTools()
    {
        if(! $this->isLoggedIn())
        {
                return FALSE;
        }
        
        if (! $this->_tools)
        {
                $this->_tools = $this->getAccountInfo()->tools->find_all();
        }
        
        return $this->_tools;
    }

    public function getAccountToolsList()
    {
        if(! $this->isLoggedIn())
        {
                return FALSE;
        }
        
        $tools = array();
        
        foreach ($this->getAccountTools() as $tool) 
        {
                $tools[$tool->id] = $tool->code;
        }
        
        return $tools;
    }

    public function getStaffInfo()
    {
        if(! $this->isLoggedIn() || $this->_staff === FALSE)
        {
                return FALSE;
        }
        
        if(is_array($this->_staff))
        {
                return $this->_staff;
        }
        
        $staff = Repo::factory('Staff')->getOneByField('accountId', $this->getAccountInfo(TRUE)->id);
        
        if(! $staff->loaded())
        {
                $this->_staff = FALSE;
                
                return FALSE;
        }
        
        $this->_staff = array('isGod' => FALSE, 'permissions' => array());
        
        if($staff->status == Model_Staff::STATUS_GOD)
        {
                $this->_staff['isGod'] = TRUE;
                
                return $this->_staff;
        }
        
        foreach ($staff->permissions->find_all() as $perm) 
        {
                if(! isset($this->_staff['permissions'][$perm->controller]))
                {
                        $this->_staff['permissions'][$perm->controller] = array();
                }
                
                $this->_staff['permissions'][$perm->controller][$perm->action] = TRUE;
        }
        
        return $this->_staff;
    }

    public function isStaffAllowed($controller, $action = "index")
    {
        if(! $this->isLoggedIn() || $this->_staff === FALSE || (! is_array($this->_staff) && ! $this->getStaffInfo()))
        {
                return FALSE;
        }
        
        if($this->_staff['isGod'] || isset($this->_staff['permissions'][$controller][$action]) || 
                isset($this->_staff['permissions'][$controller]['*']))
        {
                return TRUE;
        }
        
        return FALSE;
    }
    
/* ---------------------------------------------------------------------------*/
    
    protected function _setAuthCookie($userId)
    {
        $pepperoni  = Conf::get_option('auth_pepper');
        $userHash   = md5(serialize(array(str_pad($userId, 5, '0', STR_PAD_LEFT), $pepperoni)));
        
        Cookie::set('user_id', $userId);
        Cookie::set('user_hash', $userHash);
    }
    
    protected function _testAuthCookie()
    {
        // Извлекаем из кук id юзверя
        $userId  = Cookie::get('user_id');
        
        $pepperoni  = Conf::get_option('auth_pepper');
        $userHash   = md5(serialize(array(str_pad($userId, 5, '0', STR_PAD_LEFT), $pepperoni)));
        
        return ($userHash == Cookie::get('user_hash'));
    }
    
    protected function _deleteAuthCookie()
    {
        Cookie::delete('user_id');
        Cookie::delete('user_hash');
    }
    
    protected function _setPseudoAuthCookie($userId)
    {
        $pepperoni  = Conf::get_option('auth_pepper');
        $userHash   = md5(serialize(array(str_pad($userId, 5, '0', STR_PAD_LEFT), $pepperoni)));
        
        Cookie::set('pseudo_user_id', $userId);
        Cookie::set('pseudo_user_hash', $userHash);
    }
    
    protected function _testPseudoAuthCookie()
    {
        // Извлекаем из кук id юзверя
        $userId  = Cookie::get('pseudo_user_id');
        
        $pepperoni  = Conf::get_option('auth_pepper');
        $userHash   = md5(serialize(array(str_pad($userId, 5, '0', STR_PAD_LEFT), $pepperoni)));
        
        return ($userHash == Cookie::get('pseudo_user_hash'));
    }
    
    protected function _deletePseudoAuthCookie()
    {
        Cookie::delete('pseudo_user_id');
        Cookie::delete('pseudo_user_hash');
    }

}
