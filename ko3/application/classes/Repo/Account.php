<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_Account extends Abstract_Repo {
    
    public function getAllByListId(array $list, $withProfile = TRUE)
    {
        $return = ORM::factory($this->objectName);     
        
        $return->where_open();
        for($i = 0; $i < count($list); $i++)
        {
            if($i == 0)
            {
                $return->where('account.id', '=', $list[$i]);
            }
            else
            {
                $return->or_where('account.id', '=', $list[$i]);
            }
        }
        $return->where_close(); 
        
        if($withProfile)
        {
            $return->with('profile');
        }
        
        return $return->find_all();
    }
}
