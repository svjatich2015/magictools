<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_DrawModuleCard extends Abstract_Repo {

    /**
     * Подсчитывает записи, для которых по расписанию наступило время обновления
     */
    public function countExpired()
    {
        $now = time();
        
        return ORM::factory($this->objectName)
                ->where('status', '=', Model_DrawModuleCard::STATUS_ENABLED)
                ->and_where_open()
                    ->where('updated', '=', NULL)
                    ->or_where('updated', '<', $now - 86400)
                ->and_where_close()
                ->count_all(); 
    }

    /**
     * Создает новую запись в БД.
     *
     * @param	array	$data	     Массив значений, которые нужно занести в таблицу.
     */
    public function create(array $data)
    {
        $data['created'] = time();
        
        return  ORM::factory($this->objectName)->values($data)->save();
    }

    /**
     * Подсчитывает записи, для которых по расписанию наступило время обновления
     */
    public function findExpired($limit = 100, $offset = 0)
    {
        $now = time();
        
        return ORM::factory($this->objectName)
                ->where('status', '=', Model_DrawModuleCard::STATUS_ENABLED)
                ->and_where_open()
                    ->where('updated', '=', NULL)
                    ->or_where('updated', '<', $now - 86400)
                ->and_where_close()
                ->limit($limit)
                ->offset($offset)
                ->find_all(); 
    }
        
}
