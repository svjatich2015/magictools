<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_SuperClean extends Abstract_Repo {
    
    public function countAllByAccountId($accountId)
    {
        return $this->countAll(array('accountId', $accountId));
    }
    
}
