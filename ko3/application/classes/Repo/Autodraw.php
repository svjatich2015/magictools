<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_Autodraw extends Abstract_Repo {

    /**
     * Находит забытые
     */
    public function findLost($limit = 100)
    {
        $now = time();
        
        return ORM::factory($this->objectName)
                ->where('status', '=', Model_Autodraw::STATUS_WAITING)
                ->and_where('processed', '<', $now - 3600)
                ->limit($limit)
                ->find_all(); 
    }
}
