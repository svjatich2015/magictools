<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_Slots extends Abstract_Repo {
    
    public function getAllByToolId($toolId)
    {
        return $this->findAll(array('toolId', $toolId));
    }
}
