<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_AccountsTool extends Abstract_Repo {
    
    public function getAllByListId(array $list)
    {
        $return = ORM::factory($this->objectName);     
        
        $return->where_open();
        for($i = 0; $i < count($list); $i++)
        {
            if($i == 0)
            {
                $return->where('accountstool.id', '=', $list[$i]);
            }
            else
            {
                $return->or_where('accountstool.id', '=', $list[$i]);
            }
        }
        $return->where_close(); 
        
        return $return->find_all();
    }
}
