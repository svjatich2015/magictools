<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_AutodrawClientSchedule extends Abstract_Repo {

    /**
     * Подсчитывает записи, для которых по расписанию наступило время обновления
     */
    public function countExpired()
    {
        $now = time();
        
        return ORM::factory($this->objectName)
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_HOURLY)
                    ->and_where('start', 'IS NOT', NULL)
                    ->and_where('start', '<', $now)
                    ->and_where('finish', 'IS NOT', NULL)
                    ->and_where('finish', '>', $now)
                    ->and_where('updated', '<', $now - 86400)
                    //->and_where('updated', '<', $now - 3600)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_HOURLY)
                    ->and_where('start', 'IS', NULL)
                    ->and_where('finish', 'IS', NULL)
                    ->and_where('updated', '<', $now - 86400)
                    //->and_where('updated', '<', $now - 3600)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_DAILY)
                    ->and_where('start', 'IS NOT', NULL)
                    ->and_where('start', '<', $now)
                    ->and_where('finish', 'IS NOT', NULL)
                    ->and_where('finish', '>', $now)
                    ->and_where('updated', '<', $now - 86400)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_DAILY)
                    ->and_where('start', 'IS', NULL)
                    ->and_where('finish', 'IS', NULL)
                    ->and_where('updated', '<', $now - 86400)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_WEEKLY)
                    ->and_where('start', 'IS NOT', NULL)
                    ->and_where('start', '<', $now)
                    ->and_where('finish', 'IS NOT', NULL)
                    ->and_where('finish', '>', $now)
                    ->and_where('updated', '<', $now - 604800)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_WEEKLY)
                    ->and_where('start', 'IS', NULL)
                    ->and_where('finish', 'IS', NULL)
                    ->and_where('updated', '<', $now - 604800)
                ->or_where_close()
                ->count_all(); 
    }

    /**
     * Находит записи, для которых по расписанию наступило время обновления
     */
    public function findExpired($limit = 100, $offset = 0)
    {
        $now = time();
        
        return ORM::factory($this->objectName)
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_HOURLY)
                    ->and_where('start', 'IS NOT', NULL)
                    ->and_where('start', '<', $now)
                    ->and_where('finish', 'IS NOT', NULL)
                    ->and_where('finish', '>', $now)
                    ->and_where('updated', '<', $now - 86400)
                    //->and_where('updated', '<', $now - 3600)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_HOURLY)
                    ->and_where('start', 'IS', NULL)
                    ->and_where('finish', 'IS', NULL)
                    ->and_where('updated', '<', $now - 86400)
                    //->and_where('updated', '<', $now - 3600)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_DAILY)
                    ->and_where('start', 'IS NOT', NULL)
                    ->and_where('start', '<', $now)
                    ->and_where('finish', 'IS NOT', NULL)
                    ->and_where('finish', '>', $now)
                    ->and_where('updated', '<', $now - 86400)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_DAILY)
                    ->and_where('start', 'IS', NULL)
                    ->and_where('finish', 'IS', NULL)
                    ->and_where('updated', '<', $now - 86400)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_WEEKLY)
                    ->and_where('start', 'IS NOT', NULL)
                    ->and_where('start', '<', $now)
                    ->and_where('finish', 'IS NOT', NULL)
                    ->and_where('finish', '>', $now)
                    ->and_where('updated', '<', $now - 604800)
                ->or_where_close()
                ->or_where_open()
                    ->where('period', '=', Model_AutodrawClientSchedule::PERIOD_WEEKLY)
                    ->and_where('start', 'IS', NULL)
                    ->and_where('finish', 'IS', NULL)
                    ->and_where('updated', '<', $now - 604800)
                ->or_where_close()
                ->limit($limit)
                ->offset($offset)
                ->order_by('period', 'ASC')
                ->find_all(); 
    }

    /**
     * Находит записи, которые имели ограниченный срок действия
     */
    public function findOverdue($limit = 100, $offset = 0)
    {
        $now = time();
        
        return ORM::factory($this->objectName)
                ->where('start', 'IS NOT', NULL)
                ->and_where('finish', 'IS NOT', NULL)
                ->and_where('finish', '<', $now)
                ->find_all(); 
    }
}
