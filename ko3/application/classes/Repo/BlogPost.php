<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @category  Repository 
 */
class Repo_BlogPost extends Abstract_Repo {
    
    public function findAllByEntity($entity, array $fields, array $relations, $orderBy, array $params)
    {
        foreach ($relations as $with)
        {
            $entity->with($with);
        }
        
        if(isset($orderBy[0]) && isset($orderBy[1]))
        {
            $entity->order_by($orderBy[0], $orderBy[1]); // Example: order_by('timestamp', 'DESC');
        }
        
        foreach ($fields as $field)
        {
            $entity->where($field[0], (isset($field[2]) ? $field[2] : '='), $field[1]);
        }
        
        foreach ($params as $param)
        {
            $entity->{$param[0]}($param[1]); 
        }

        return $entity->find_all();
        
    }
}
