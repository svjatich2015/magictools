<?php
/**
 * Базовый класс Service.
 *
 * @category  Service Layer
 * @author    Svjat Maslov
 */
class Service {

    /**
     * @var  array  Контейнер для хранения инстансов 
     */
    protected static $_instance = array();

    /**
     * Обертка для вызова Service, как синглтон.
     *
     *     $serviceInstance = Service::instance($name);
     *
     * @param   string  $name    Имя сервисного метода
     * @return instance
     */
    public static function instance($name)
    {
        $class = 'Service_' . $name;

        if (!isset($_instance[$name]))
        {
            $_instance[$name] = new $class();
        }


        return $_instance[$name];
    }

    /**
     * Обертка для вызова сервис-методов через единый интерфейс.
     *
     *     $service = Service::factory($name);
     *
     * @param   string  $name    Имя сервисного метода
     * @return  Service
     */
    public static function factory($name)
    {
        $class = 'Service_' . $name;
        return new $class();
    }

}
