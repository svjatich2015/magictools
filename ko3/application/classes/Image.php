<?php

abstract class Image extends Kohana_Image {

	/**
	 * @var  string  driver: GD, ImageMagick, etc
	 */
	protected static $_driver;

	/**
	 * Loads information about the image. Will throw an exception if the image
	 * does not exist or is not an image.
	 *
	 * @param   string  $file  image file path
	 * @return  void
	 * @throws  Kohana_Exception
	 */
	public function __construct($file)
	{
		try
		{
			// Get the real path to the file
			$file = realpath($file);
                        
                        // Image is WebP? 
                        $isWebp = substr($file, -5) == '.webp';

			// Get the image information
                        if($isWebp && Image::$_driver == 'Imagick')
                        {
                                $image = new Imagick($file);
                                $dims  = $image->getImageGeometry();
                                $info  = array($dims['width'], $dims['height'], self::IMAGETYPE_WEBP);

                                unset($image, $dims);
                        }
                        else if($isWebp && Image::$_driver == 'GD' && function_exists('imagecreatefromwebp'))
                        {
                                $image = imagecreatefromwebp($file);                                        
                                $info  = array(imagesx($image), imagesy($image), self::IMAGETYPE_WEBP);

                                unset($image);                                        
                        }
                        else
                        {
                                $info = getimagesize($file);
                        }
		}
		catch (Exception $e)
		{
			// Ignore all errors while reading the image
		}

		if (empty($file) OR empty($info))
		{
			throw new Kohana_Exception('Not an image or invalid image: :file',
				array(':file' => Debug::path($file)));
		}

		// Store the image information
		$this->file   = $file;
		$this->width  = $info[0];
		$this->height = $info[1];
		$this->type   = $info[2];
		$this->mime   = $this->image_type_to_mime_type($this->type);
	}

	/**
	 * Наносит текст на изображение.
	 *
	 *     // Нанести текст черным шрифтом Carlito 24px справа внизу
	 *     $image->scale('16:9', '#000');
	 *
	 * @param   string  $text     Text
	 * @param   string  $css      CSS
	 * @param   string  $gravity  Gravity
	 * @return  $this
	 * @uses    Image::_do_scale
	 */
	public function text($text, array $css = array(), $offset_x = NULL, $offset_y = NULL, $gravity = NULL)
	{
                if(! isset($css['color']))
                {
                        $css['color'] = '#000';
                }
                
                if(! isset($css['font-family']))
                {
                        $css['font-family'] = 'Carlito';
                }
                
                if(! isset($css['font-size']))
                {
                        $css['font-size'] = 12;
                }
                
                if(! isset($css['font-weight']))
                {
                        $css['font-weight'] = 300;
                }
                
                if($gravity < \Imagick::GRAVITY_NORTHWEST || $gravity > \Imagick::GRAVITY_SOUTHEAST)
                {
                        $gravity = \Imagick::GRAVITY_NORTHWEST;
                }
                
                $this->_do_text($text, $css, $offset_x, $offset_y, $gravity);

		return $this;
	}

	/**
	 * Масштабирует изображение с учетом определенного соотношения сторон. Если исходное
         * изображение имеет иное соотношение ширины и высоты, то заполняет пустоты фоном.
	 *
	 *     // Масштабировать до нужного размера и заполнить пустоты черным фоном
	 *     $image->scale('16:9', '#000');
	 *
	 * @param   string  $ratio  aspect ratio
	 * @param   string  $bg     background
	 * @return  $this
	 * @uses    Image::_do_scale
	 */
	public function scale($width, $height, $bg = '#000')
	{
                // Если картинка не соответствует нужным размерам
                if($this->width > $width && $this->height > $height)
                {
                        $this->resize($width, $height);
                }		
                else if($this->width > $width)
                {
                        $this->resize($width, NULL);
                }		
                else if($this->height > $height)
                {
                        $this->resize(NULL, $height);
                }		
                
                // Если картинка не соответствует нужному масштабу
                if($width !== $this->width || $height !== $this->height)
                {
                        // Remove the pound
                        $bg = ($bg[0] === '#') ? substr($bg, 1) : $bg;

                        // Convert shorthand into longhand hex notation
                        $bg = (strlen($bg) === 3) ? preg_replace('/./', '$0$0', $bg) : $bg;

                        // Convert the hex into RGB values
                        list ($r, $g, $b) = array_map('hexdec', str_split($bg, 2));
                        
                        $this->_do_scale($width, $height, compact('r', 'g', 'b'));
                }

		return $this;
	}
        
	public function over(Image $watermark, $offset_x = NULL, $offset_y = NULL, $opacity = 100)
	{
		if ($offset_x === NULL)
		{
			// Center the X offset
			$offset_x = round(($this->width - $watermark->width) / 2);
		}
		elseif ($offset_x === TRUE)
		{
			// Bottom the X offset
			$offset_x = $this->width - $watermark->width;
		}
		elseif ($offset_x < 0)
		{
			// Set the X offset from the right
			$offset_x = $this->width - $watermark->width + $offset_x;
		}

		if ($offset_y === NULL)
		{
			// Center the Y offset
			$offset_y = round(($this->height - $watermark->height) / 2);
		}
		elseif ($offset_y === TRUE)
		{
			// Bottom the Y offset
			$offset_y = $this->height - $watermark->height;
		}
		elseif ($offset_y < 0)
		{
			// Set the Y offset from the bottom
			$offset_y = $this->height - $watermark->height + $offset_y;
		}

		// The opacity must be in the range of 1 to 100
		$opacity = min(max($opacity, 1), 100);

		$this->_do_over($watermark, $offset_x, $offset_y, $opacity);

		return $this;
	}

	/**
	 * Render the image and return the binary string.
	 *
	 *     // Render the image at 50% quality
	 *     $data = $image->render(NULL, 50);
	 *
	 *     // Render the image as a PNG
	 *     $data = $image->render('png');
	 *
	 * @param   string   $type     image type to return: png, jpg, gif, etc
	 * @param   integer  $quality  quality of image: 1-100
	 * @return  string
	 * @uses    Image::_do_render
	 */
	public function render($type = NULL, $quality = 100)
	{
		if ($type === NULL)
		{
			// Use the current image type
			$type = $this->image_type_to_extension($this->type, FALSE);
		}

		return $this->_do_render($type, $quality);
	}

	/**
	 * Get file extension for image type
	 *
	 * @param   string      $type           One of the IMAGETYPE_XXX constant.
	 * @param   string      $include_dot    Whether to prepend a dot to the extension or not. Default to TRUE.
	 * @return  string
	 */
	protected function image_type_to_extension($type, $include_dot = TRUE)
	{
		if ($type === self::IMAGETYPE_WEBP)
			return $include_dot ? '.webp' : 'webp';

		return image_type_to_extension($type, $include_dot);
	}
}
