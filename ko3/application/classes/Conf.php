<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Конфигуратор фреймворка
 * 
 * С помощью данного метода загружается глобальный и локальный конфиг сайта,
 * что позволяет разделить настройки на глобальные и локальные.
 * 
 * @category  Configuration
 * @author    Svjat Maslov
 */
class Conf {

    /**
     * @var  instance  Контейнер для хранения ссылки на "одиночку"
     */
    protected static $_instance;
    
    /**
     * @var  array  Контейнер для хранения общего массива настроек
     */
    private static $_conf = array();

    /**
     * Метод для доступа к синглтону
     * 
     * @return  instance
     */
    public static function init()
    {
        if (self::$_instance === NULL)
        {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    /**
     * Возвращает определенный в переменной $name параметр настроек.
     * 
     * @param  string  $name  Имя параметра
     * @return mixed
     */
    public static function get_option($name, $default = NULL)
    {
        $conf = self::$_conf;
        $path = explode('.', $name);
        while ($key  = array_shift($path))
        {
            if (array_key_exists($key, $conf))
            {
                $conf = $conf[$key];
            }
            else
            {
                return $default;
            }
        }
        return $conf;
    }

    /**
     * Возвращает "перец" используемый при авторизации/регистрации
     * 
     * @return   string
     */
    public static function get_auth_pepper()
    {
        if (!isset(self::$_conf['auth_pepper']))
        {
            // генерируем системную ошибку
            throw new Kohana_Exception('В файле local_conf.php не установлена переменная "auth_pepper"');
        }

        return self::$_conf['auth_pepper'];
    }

    /**
     * Возвращает массив модулей, которые необходимо подключить
     * 
     * @return   array    Ассоциированный массив
     */
    public static function get_modules()
    {
        return self::$_conf['modules'];
    }

    /**
     * Устанавливает финальные настройки
     */
    public static function setup_final()
    {
        if (isset(self::$_conf['database']))
        {
            $db_config = Kohana::$config->load('database');

            foreach (self::$_conf['database'] as $k => $v)
            {
                $db_config->set($k, $v);
            }
        }
    }

    /**
     * Возвращает массив параметров для инициализации коханы
     * 
     * @return   array    Ассоциированный массив
     */
    public static function initialize_kohana()
    {
        return self::$_conf['kohana_init'];
    }

    private function __clone()
    {
        
    }

    private function __wakeup()
    {
        
    }

    private function __construct()
    {
        foreach (include APPPATH . '/conf/conf.php' as $k => $v)
        {
            self::$_conf[$k] = $v;
        }

        if (file_exists(APPPATH . '/conf/local_conf.php'))
        {
            $_conf_addon = array();

            foreach (include APPPATH . '/conf/local_conf.php' as $k => $v)
            {
                $_conf_addon[$k] = $v;
            }

            self::$_conf = array_replace_recursive(self::$_conf, $_conf_addon);
        }

        Cookie::$salt = self::$_conf['cookie_salt'];
    }

}
