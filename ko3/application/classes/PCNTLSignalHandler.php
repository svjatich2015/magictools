<?php defined('SYSPATH') OR die('No direct script access.');

class PCNTLSignalHandler {
	/**
	 * @var callable[]
	 */
	private $handlers = array();
	private $toDispatch = array();

	/**
	 * Добавление обработчика сигнала
	 *
	 * @param int       $signalNumber   номер сигнала, например SIGTERM
	 * @param callable  $handler        функция-обработчик игнала $signalNumber
	 * @param bool      $isAdd          если TRUE, то заменить текущие обработчики
	 */
	public function addHandler($signalNumber, $handler, $isAdd = TRUE)
	{
		$isHandlerNotAttached = empty($this->handlers[$signalNumber]);
		if($isAdd)
			$this->handlers[$signalNumber][] = $handler;
		else
			$this->handlers[$signalNumber] = array($handler);

		if($isHandlerNotAttached && function_exists('pcntl_signal'))
		{
			$this->toDispatch[$signalNumber] = false;
			pcntl_signal($signalNumber, array($this, 'handleSignal'));
		}
	}

	/**
	 * Добавление обработчика для группы сигналов
	 *
	 * @param array     $signals   массив сигналов
	 * @param callable  $handler   функция-обработчик игнала $signalNumber
	 * @param bool      $isAdd     если TRUE, то заменить текущие обработчики
	 */
	public function addGroupHandler(array $signals, $handler, $isAdd = TRUE)
	{
		foreach ($signals as $signalNumber)
                {
                        $this->addHandler($signalNumber, $handler, $isAdd);
                }
	}

	/**
	 * Обработать накопленные сигналы
	 */
	public function dispatch()
	{
		pcntl_signal_dispatch();
		foreach($this->toDispatch as $signalNumber => $isNeedDispatch)
		{
			if(!$isNeedDispatch)
				continue;
			$this->toDispatch[$signalNumber] = false;
			foreach($this->handlers[$signalNumber] as $handler)
				call_user_func($handler, $signalNumber);
		}
	}

	/**
	 * Поставнока обработки сигнала в очередь
	 *
	 * @param int $signalNumber номер сигнала, например SIGTERM
	 */
	private function handleSignal($signalNumber)
	{
		$this->toDispatch[$signalNumber] = TRUE;
	}
}
