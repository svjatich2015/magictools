<?php defined('SYSPATH') or die('No direct script access.');

class Task_Servicefee extends Minion_Task
{
        private $feeReports = array();
        
        protected function _execute(array $params)
	{
                $tools = Service::factory('Tool')->getAll('id');
            
                $where = array(
                    array('freeUntil', time()+86400, '<'), 
                    array('expiried', time()+86400, '<'), 
                    array('autopay', Model_AccountsTool::AUTOPAY_ON)
                );
                $atFee = array();
                
                foreach (Repo::factory('AccountsTool')->findAll($where) as $accTool) 
                {
                        if(! isset($tools[$accTool->toolId]))
                        {
                                continue;
                        }
                        if(! isset($atFee[$accTool->accountId]))
                        {
                            $atFee[$accTool->accountId] = [];
                        }
                        switch ($accTool->toolId) {
                                case Model_AccountsTool::TOOL_SUPERCLEAN:
                                        $atFee[$accTool->accountId][$accTool->toolId] = 
                                                $this->__calculateSupercleanFee(
                                                        $accTool, $tools[$accTool->toolId]);
                                        break;
                                default:
                                        $atFee[$accTool->accountId][$accTool->toolId] =
                                                $this->__calculateRegularFee($accTool, $tools[$accTool->toolId]);
                                        break;
                        }
                }
                        
                $this->__takeFeeFromAccounts($atFee);
        }
        
        private function __calculateRegularFee($accTool, $tool)
        {
                return ['expiried' => $accTool->expiried, 'fee' => $tool['cost'] * 100];
        }
        
        private function __calculateSupercleanFee($accTool, $tool)
        {
                $fee = 0;
                        
                if(! is_null($accTool->slotsId) && isset($tool['slots'][$accTool->slotsId]))
                {
                        $fee = $tool['slots'][$accTool->slotsId]['cost'] * 100;
                }
                
                return ['expiried' => $accTool->expiried, 'fee' => $fee];
        }
        
        private function __takeFeeFromAccounts($atFee)
        {
                $list = array_keys($atFee);
                $accounts = Service::factory('Account')->getByListIds($list);
                $results = [];
            
                // Снимаем плату с аккаунтов
                foreach ($atFee as $aId => $expTools)
                {
                    $result = $this->__takeFeeFromAccount($accounts[$aId], $expTools);
                    if(count($result))
                    {
                        $tools = array_keys($result);
                        Service::factory('Bill')->createAndAccept($aId, $tools, 1);
                    }
                }
        }
        
        private function __takeFeeFromAccount($account, $tools)
        {
                $balance = $account->balance;
                $updated = time();
                $result = [];
                                
                foreach ($tools as $tId => $toolInfo)
                {
                    if($toolInfo['fee'] > $balance)
                    {
                        continue;
                    }
                    // Снимаем плату с аккаунта
                    $balance -= $toolInfo['fee'];
                    $expiried = strtotime('+1 month', 
                            ($toolInfo['expiried'] > time() ? 
                                $toolInfo['expiried'] : time()));
                    $data = ['expiried' => $expiried, 'updated' => $updated];
                    $where = [['accountId', $account->id], ['toolId', $tId]];
                    $_result = Repo::factory('AccountsTool')->update($data, $where);
                    $result[$tId] = ['toolId' => $tId, 'expiried' => $expiried];                        
                }
                
                if($balance != $account->balance)
                {
                    $account->set('balance', $balance)->save();
                }
                                
                return $result;
        }
}
