<?php defined('SYSPATH') or die('No direct script access.');

setLocale( LC_ALL, "C" );

class Task_Superclean extends Minion_Queue_Task
{
        // Опции таска
        protected $_options = array(
            'action' => 'queue', 
            'queue'  => Model_SuperClean::QUEUE_NAME, 
            'job'    => NULL
        );
        
        protected $_params = array();

        protected function _execute(array $params) 
        {
                // Экшен
                $action = $params['action'];
                
                unset($params['action']);
                
                // Делаем параметры доступными из любого экшена
                $this->_params = $params;
                
                // Вызываем экшен
                $this->{$action}();
        }

        protected function queue()
        {
                $this->__isDemonize();
                
                // Название очереди
                $queue = $this->_params['queue'];
                                
                // Timestamp начала работы скрипта
                $startJob = time();
                
                Queue::instance()->watch($queue)->ignore('default');

                while(true)
                {
                        // Обрабатываем накопившиеся PCNTL-сигналы 
                        $this->_signal_handler->dispatch();
                        
                        // Если демон уже работает сутки - усыпляем его...
                        if(time() > $startJob + 86400)
                        {
                                $this->terminate();
                        }
                        
                        // Резервируем за собой задачку
                        $job = $this->__queueReserve();

                        // Если задачки пока нет - идем на новый круг (таймаут = 1 сек.)
                        if (! is_array($job) || ! isset($job['id']))
                        {
                                continue;
                        }
                        
                        // Запускаем обработку
                        $this->__processing($job);
                }
        }

        protected function update()
        {
                $toolId = Model_Tool::SUPERCLEAN;
                $accountsExpiried = Service::factory('Tool')->getAccountIdsByExpiriedList($toolId);
                $accountsSlotsNum = Service::factory('Superclean')->getSlotsNumByAccounts();
                                        
                $where = array('status', Model_SuperClean::STATUS_PROCESSED, '<=');                
                $msgs = array();
                
                foreach (Repo::factory('SuperClean')->findAll($where) as $slot) 
                {
                        $slotsNum = Arr::get($accountsSlotsNum, $slot->accountId);
                        
                        if(in_array($slot->accountId, $accountsExpiried) || $slot->slotNum > $slotsNum)
                        {
                                continue;
                        }
                        
                        $params = Arr::extract($slot->as_array(), array('id', 'accountId', 'slotNum', 'rawFileType'));
                        
                        // Add job in queue
                        Queue::instance()->add(Model_SuperClean::QUEUE_NAME, $params);
                        
                        $msgs[] = 'Slot #' . $slot->id . ' will be updated!';
                }
                
                Minion_CLI::write($msgs);
        }
        
        protected function kick()
        {                
                // Название очереди
                $queue = $this->_params['queue'];
                
                Queue::instance()->watch($queue)->ignore('default');
                
                if(! $this->_params['job'])
                {
                        Minion_CLI::write(Minion_CLI::color('Param "job" is required.', 'red'));
                        exit(0);
                }
                
                // Job
                $job = Queue::instance()->queue()->peek(intval($this->_params['job']));
                
                // Воскрешаем job
                Queue::instance()->queue()->kickJob($job);

                Minion_CLI::write(Minion_CLI::color('OK!', 'green'));
        }
        
        private function __processing($job)
        {
                $job_ = unserialize($job['body']);

                try
                {
                        // Запускаем "СуперЧистку"
                        $this->__job($job_);

                        // Меняем статус на "Готово"
                        $this->__updateDB($job_);

                        // Удаляем текущую задачку, с которой работали
                        $this->__queueDelete($job['id']);
                }
                catch (Exception $e)
                {
                        // Пишем в лог ошибку
                        Kohana::$log->add(Log::ERROR,
                                sprintf('Unable to execute Superclean job width id=%d. Buried with message "%s"',
                                        $job['id'], $e->getMessage()));
                        Kohana::$log->add(Log::ERROR,
                                $e->getTraceAsString());

                        // Пишем в лог инфу для дебага
                        Kohana::$log->add(Log::DEBUG,
                                sprintf('Job #%d debug data log: %s',
                                        $job['id'], $this->__log(array_merge(array('id' => $job['id']), $job_))));

                        // Активируем немедленную запись лога в файл на диске
                        Kohana::$log->write();

                        // Хороним задачку заживо
                        $this->__queueBury($job['id'], Queue::DEFAULT_PRIORITY);
                }

                // Удаляем из памяти ненужные переменные
                unset($job['id'], $job, $job_);
        }

        private function __job($slot)
        {
                try
                {
                        $rawSrc   = Service::factory('Superclean')->getSlotImgRaw($slot);
                        $cleanSrc = $this->__generateImgPath($slot);
                        
                        Service::factory('Superclean')->createHarmonizeImgByRaw($rawSrc, $cleanSrc);
                }
                catch (Exception $e)
                {
                        throw $e;
                }
        }

        private function __updateDB($job, $processed = NULL, $status = Model_SuperClean::STATUS_COMPLETE)
        {
                if(! $processed)
                {
                        $processed = time();
                }
                
                $slotId = Arr::get($job, 'id');
                $values = compact('status', 'processed');

                try 
                {
                        Database::instance()->connect();
                        
                        if($status == Model_SuperClean::STATUS_COMPLETE)
                        {
                                $values['incorrect'] = Service::factory('Superclean')->getIncorrectRate();
                        }
                        
                        DB::update('SuperClean')->set($values)->where('id', '=', $slotId)->execute();
                        
                        //Database::instance()->disconnect();
                }
                catch (Database_Exception $e) 
                {
                        return 0;
                }
                catch (Exception $e) 
                {
                        //Database::instance()->disconnect();
                        
                        throw $e;
                }

                return 1;
        }

        private function __generateImgPath($slot) 
        {
                $path = Service::factory('Dir')->createByAccount($slot['accountId'], Conf::get_option('superclean_dir'));
                $hash = md5(Arr::get($slot, 'id') . ':=:=:' . Arr::get($slot, 'slotNum'));
                
                return $path . $hash . '.jpg';
        }

        private function __log($data)
        {
                $slot = Arr::get($data, 'slot', FALSE);
                        
                if($slot)
                {
                        $data['slotId'] = $slot->id;
                        unset($slot, $data['slot']);
                }
                        
                $file  = APPPATH . 'logs' . DIRECTORY_SEPARATOR . 'custom' . DIRECTORY_SEPARATOR;
                $file .= md5(microtime()) . '.php';
                
                file_put_contents($file, serialize($data));
                
                return $file;
        }
}
