<?php defined('SYSPATH') or die('No direct script access.');

class Task_Reminder extends Minion_Task
{ 
	protected $_options = array('sendmail' => FALSE);
        
        private $messages = array();

        protected function _execute(array $params)
	{
                $params['sendmail'] ? $this->__sendmail() : $this->__queued();
                
                Minion_CLI::write($this->messages);
	}
        
        private function __queued()
        {
                Service::factory('AccountTools')->createNotificationsOfExpiration();
                
                $this->messages = Minion_CLI::color('Emails queued for sending.', 'green');
        }

        protected function __sendmail()
	{
                // Грузим мэйлер для отправки через ElasticEmail
                $mailer = Mailer::factory('notify');
                
                // Сканируем все файлы в папке не обработанных писем
                $raw = File::scandir(APPPATH . 'emails/raw_notify');

                foreach ($raw as $task) {
                        if($mailer->getLimit() < 1)
                        {
                                unlink(APPPATH . 'emails/raw_notify/' . $task);
                        }
                        
                        // Если файл успешно открылся...
                        if ($email_arr = @file_get_contents(APPPATH . 'emails/raw_notify/' . $task)) 
                        {
                                if ($this->__run(unserialize($email_arr), $task, $mailer))
                                {
                                        $finished[] = $task;
                                }
                                
                        } 
                        else 
                        {
                                $this->messages[] = Minion_CLI::color('Error reading file ' . $task, 'red');
                        }
                }
                
                if (count($this->messages) < 1)
                {
                        $this->messages = Minion_CLI::color('There is nothing to process.', 'green');
                }
	}
        
        private function __run($email, $task, $mailer) 
        {
                $status = $mailer->send($email);

                if ($status['error']) 
                {
                        // Пишем в лог
                        Kohana::$log->add(Log::ERROR, $status['error']);

                        $this->messages[] = Minion_CLI::color('A letter to ' . $email['to'][0]['email'] .
                                        ' is not sent.[timestamp ' . $task . ']', 'red');
                        $this->messages[] = Minion_CLI::color('Error: ' . $status['error'], 'red');
                        
                        return FALSE;
                }
                
                $this->messages[] = Minion_CLI::color('A letter to ' . $email['to'][0]['email'] .
                                ' is sent.[timestamp ' . $task . ']', 'green');

                // Переносим файл в папку отправленных
                unlink(APPPATH . 'emails/raw_notify/' . $task);

                return $task;                
        }
}
