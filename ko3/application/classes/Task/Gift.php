<?php defined('SYSPATH') or die('No direct script access.');

class Task_Gift extends Minion_Task
{
        protected $_options = array('account' => FALSE, 'gift' => FALSE);

        protected function _execute(array $params) 
        {
                if (! $params['account'] || ! $params['gift'])
                {
                        $message = Minion_CLI::color('Не указан обязательный параметр!', 'red');
                        Minion_CLI::write($message);
                        return;
                }
                Service::factory('AccountTools')->applyGift($params['account'], $params['gift']);
                $message = Minion_CLI::color('Письмо отправлено!', 'red');
                Minion_CLI::write($message);
        }
        
        protected function __invalid_action($action)
	{
                $message = Minion_CLI::color('CLI task has no action "'. $action .'"!', 'red');
                Minion_CLI::write($message);
	}
}
