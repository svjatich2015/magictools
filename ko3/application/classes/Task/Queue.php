<?php defined('SYSPATH') or die('No direct script access.');

class Task_Queue extends Minion_Task
{
	protected $_options = array('action' => FALSE, 'queue' => Model_Autodraw::QUEUE_NAME);

        protected function _execute(array $params) 
        {
                $actions = array('stats_tube', 'stats', 'peek_delayed');
                $action = ($params['action'] && in_array($params['action'], $actions)) ? $params['action'] : $actions[0];

                return $this->{'__' . $action}($params['queue']);
        }

        protected function __stats($queue) 
        {
                Queue::instance()->queue()->watch($queue);
                Queue::instance()->queue()->ignore('default');

                //$job = Queue::instance()->queue()->kickJob(1);
                $job = Queue::instance()->queue()->stats();
                var_dump($job);
                die();
        }

        protected function __stats_tube($queue) 
        {
                while(true)
                {
                    Queue::instance()->watch($queue)->ignore('default');

                    $stats = (array) Queue::instance()->queue()->statsTube($queue);
                    $write = array(
                        'Срочные: ' . $stats["current-jobs-urgent"],
                        'Ожидающие: ' . $stats["current-jobs-ready"],
                        'В работе: ' . $stats["current-jobs-reserved"],
                        'Отложенные: ' . $stats["current-jobs-delayed"],
                        'Похороненные: ' . $stats["current-jobs-buried"],
                        'Удаленные: ' . $stats["cmd-delete"],
                        '*Воркеры: ' . (intval($stats["current-watching"]) - 1) . ' '
                    );

                    Minion_CLI::write_replace(join(' | ', $write));
                    Minion_CLI::wait(3);
                }
        }

        protected function __peek_delayed($queue) 
        {
                Queue::instance()->queue()->watch($queue);
                Queue::instance()->queue()->ignore('default');

                $tube = Queue::instance()->queue()->statsTube($queue);
                $cnt = $tube["current-jobs-delayed"];
                Queue::instance()->queue()->kick($cnt);
                for ($i = 0; $i < $cnt; $i++) {
                        $peek = Queue::instance()->queue()->peekReady();
                        if (!$peek) {
                                $errors = Queue::instance()->queue()->errors();
                                $peek = array_pop($errors);
                        }
                        var_dump($peek);
                }
                die();
        }

}
