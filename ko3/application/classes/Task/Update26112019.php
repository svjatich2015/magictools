<?php defined('SYSPATH') or die('No direct script access.');

class Task_Update26112019 extends Minion_Task
{
        protected function _execute(array $params) 
        {
                $oldSlotsItems = array();
                
                foreach(Repo::factory('AccountsSuperCleanSlots')->getAll() as $oldSlotsItem)
                {
                        $oldSlotsItems[$oldSlotsItem->accountId] = $oldSlotsItem->slotsId;
                }
                
                foreach(Repo::factory('AccountTool')->findAll(array('toolId', 2)) as $tool)
                {
                        if(! isset($oldSlotsItems[$tool->accountId]))
                        {
                                continue;
                        }
                        
                        $tool->slotsId = $oldSlotsItems[$tool->accountId];
                        $tool->save();
                }
	}
}
