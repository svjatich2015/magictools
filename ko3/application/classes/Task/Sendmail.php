<?php defined('SYSPATH') or die('No direct script access.');

class Task_Sendmail extends Minion_Task
{
        private $messages = array();

        protected function _execute(array $params)
	{
                // Грузим мэйлер для отправки через Sendpulse
                $mailer = Mailer::factory();
                
                // Сканируем все файлы в папке обработанных писем, чтобы понять, не превышен ли лимит...
		$finished = File::scandir(APPPATH . 'emails/finished');
                
                // Если лимит не превышен...
                if(count($finished) < $mailer->getLimit())
                {
                        $this->__processing($mailer->getLimit(), $finished, $mailer);
                }
                else
                {
                        $this->messages = Minion_CLI::color('Reach the limit to send mails per hour.', 'red');
                }
                
                Minion_CLI::write($this->messages);
	}
        
        private function __processing($limit, $finished, $mailer)
        {
                // Сканируем все файлы в папке не обработанных писем
                $raw = File::scandir(APPPATH . 'emails/raw');

                foreach ($raw as $task) {
                        // Если файл успешно открылся...
                        if ($email_arr = @file_get_contents(APPPATH . 'emails/raw/' . $task)) 
                        {
                                $email = unserialize($email_arr);
                                
                                if ($this->__run($email, $task, $mailer))
                                {
                                        $finished[] = $task;

                                        if (count($finished) >= $limit)
                                        {
                                                break;
                                        }
                                }
                                
                        } 
                        else 
                        {
                                $this->messages[] = Minion_CLI::color('Error reading file ' . $task, 'red');
                        }
                }
                
                if (count($this->messages) < 1)
                {
                        $this->messages = Minion_CLI::color('There is nothing to process.', 'green');
                }
        }
        
        private function __run($email, $task, $mailer) 
        {
                $status = $mailer->send($email);

                if ($status['error']) 
                {
                        // Пишем в лог
                        Kohana::$log->add(Log::ERROR, $status['error']);

                        $this->messages[] = Minion_CLI::color('A letter to ' . $email['to'][0]['email'] .
                                        ' is not sent.[timestamp ' . $task . ']', 'red');
                        $this->messages[] = Minion_CLI::color('Error: ' . $status['error'], 'red');
                        
                        return FALSE;
                } 
                
                $this->messages[] = Minion_CLI::color('A letter to ' . $email['to'][0]['email'] .
                                ' is sent.[timestamp ' . $task . ']', 'green');

                // Переносим файл в папку отправленных
                rename(APPPATH . 'emails/raw/' . $task, APPPATH . 'emails/finished/' . $task);

                return $task;                
        }
}
