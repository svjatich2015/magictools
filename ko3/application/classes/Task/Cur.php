<?php defined('SYSPATH') or die('No direct script access.');

class Task_Cur extends Minion_Task
{
        protected function _execute(array $params) 
        {
                $i = 0;
                $ctx = stream_context_create(array('http'=>
                    array(
                        'timeout' => 30,  //30 Seconds
                    )
                ));
                do 
                {
                    $i = $i + 1;
                    $rates = file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js', false, $ctx);
                } 
                while(trim($rates) == '' && $i < 10);
                if(trim($rates) == '') {
                    exit;
                }
                file_put_contents(APPPATH . 'json/daily.json', $rates);
                
                Minion_CLI::color('The exchange rate has been updated.', 'green');
	}
}
