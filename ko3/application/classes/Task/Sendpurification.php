<?php defined('SYSPATH') or die('No direct script access.');

class Task_Sendpurification extends Minion_Task
{
        private $messages = array();
        
        protected function _execute(array $params)
	{                
                // Сканируем все файлы в папке обработанных писем
		$finished = File::scandir(APPPATH . 'emails/finished');
                
                // Последовательно удаляем каждое письмо...
                foreach ($finished as $file)
                {
                        if (@unlink(APPPATH . 'emails/finished/' . $file))
                        {
                                $this->messages[] = Minion_CLI::color('File ' . $file . ' deleted!', 'green');
                        }
                        else
                        {
                                $this->messages[] = Minion_CLI::color('File ' . $file . ' is not deleted!', 'red');
                                
                                // Пишем в лог
                                Kohana::$log->add(Log::ERROR, 'File ' . APPPATH . 'emails/finished/' . $file .
                                        ' is not deleted!');
                        }
                }
                
                Minion_CLI::write($this->messages);
	}
}
