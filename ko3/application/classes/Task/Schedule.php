<?php defined('SYSPATH') or die('No direct script access.');

setLocale( LC_ALL, "C" );

class Task_Schedule extends Minion_Queue_Task
{
        protected $_options = array(
            'action' => 'autodraw', 
        );
        
        protected $_params = array();
        
        protected $_updateTime;
        
        protected $_accountsExpiried = array();

        protected function _execute(array $params) 
        {
                // Время запуска
                $this->_updateTime = time();
                
                // Экшен
                $action = $params['action'];
                
                unset($params['action']);
                
                // Делаем параметры доступными из любого экшена
                $this->_params = $params;
                
                // Вызываем экшен
                $this->{$action}();
        }

        protected function autodraw()
        {                
                $this->__deleteOverdue();
                
                $msgs = array();                
                $toolId = Model_Tool::AUTODRAW;
                $this->_accountsExpiried['autodraw'] = Service::factory('Tool')->getAccountIdsByExpiriedList($toolId);   
                $count = Repo::factory('AutodrawClientSchedule')->countExpired();
                $reach = $this->__getReach($count);
                $successed = 0;
                $offset = 0;
                
                do 
                {
                        $limit = $reach - $successed;
                        
                        // Пробегаемся по всем запланированным автоотрисовкам
                        foreach($this->__autodrawExpired($limit, $offset) as $taskId)
                        {
                                $successed++;
                                
                                $msgs[] = 'Schedule Task #' . $taskId . ' will be activated!';
                        }
                        
                        $offset += $limit;
                        
                        $nullExpired = ($count < 1 || $offset >= $count) ? TRUE : FALSE;
                }
                while(! $nullExpired && $successed < $reach);
                
                Minion_CLI::write($msgs);
        }

        protected function autodraw_module()
        {
                $msgs = array();
                
                $toolId = Model_Tool::AUTODRAW;
                $this->_accountsExpiried['autodraw'] = Service::factory('Tool')->getAccountIdsByExpiriedList($toolId); 
                
                $count = Repo::factory('DrawModuleCard')->countExpired();
                $reach = $this->__getReach($count);
                $successed = 0;
                $offset = 0;
                
                do 
                {
                        $limit = $reach - $successed;
                        
                        // Пробегаемся по всем запланированным модульным автоотрисовкам
                        foreach($this->__autodrawModuleExpired($limit, $offset) as $taskId)
                        {
                                $successed++;
                                
                                $msgs[] = 'Autodraw Module Task #' . $taskId . ' will be activated!';
                        }
                        
                        $offset += $limit;
                        
                        $nullExpired = ($count < 1 || $offset >= $count) ? TRUE : FALSE;
                }
                while(! $nullExpired && $successed < $reach);
                
                Minion_CLI::write($msgs);
        }

        private function __autodrawExpired($limit, $offset)
        {
                $successed = array();
                
                // Пробегаемся по всем запланированным автоотрисовкам, которые пора запускать...
                foreach (Repo::factory('AutodrawClientSchedule')->findExpired($limit, $offset) as $task) 
                {                      
                        if(in_array($task->accountId, $this->_accountsExpiried['autodraw']))
                        {
                                continue;
                        }
                        
                        // Это временный лайфхак для снижения нагрузки
                        $minimize = (Model_AutodrawClientSchedule::PERIOD_HOURLY == $task->period) ? 
                                1 : $task->minimize;
                        
                        $result = Service::factory('Autodraw')->createByClientScheduled($task->accountId, $task->clientId, $minimize);
                        
                        if($result)
                        {
                                $task->set('updated', $this->_updateTime)->save();
                                $successed[] = $task->id;
                        }
                        else
                        {
                                $task->delete();
                        }
                }
                
                return $successed;
        }

        private function __autodrawModuleExpired($limit, $offset)
        {
                $successed = array();
                
                // Пробегаемся по всем запланированным автоотрисовкам, которые пора запускать...
                foreach (Repo::factory('DrawModuleCard')->findExpired($limit, $offset) as $card) 
                {                     
                        if(in_array($card->accountId, $this->_accountsExpiried['autodraw']))
                        {
                                continue;
                        }                        
                        $moduleName = Autodraw_Module::initById($card->moduleId)->getModuleTitle();
                        foreach (Autodraw_Module::initById($card->moduleId)->getScriptsByCard($card) as $script)
                        {
                                $result = Service::factory('Autodraw')->createByModuleCard($moduleName, $card, $script);
                        
                                if($result)
                                {
                                        $card->set('updated', $this->_updateTime)->save();
                                        $successed[] = $card->id;
                                }
                                
                        }
                }
                
                return $successed;
        }

        private function __getReach($count)
        {
                $dateG = date('G');
                $startsCnt = (($dateG > 22) ? 1 : 23 - $dateG) * floor(60 - date('i'));
                
                return ceil($count / $startsCnt);
        }

        private function __deleteOverdue()
        {
                $msgs = array(); 
                
                // Пробегаемся по всем неактуальным записям в расписание
                foreach(Repo::factory('AutodrawClientSchedule')->findOverdue() as $task)
                {
                        $msgs[] = 'Schedule Task #' . $task->id . ' will be deleted!';
                        $task->delete();
                }
                
                if(count($msgs) > 0)
                {
                    Minion_CLI::write($msgs);
                }
        }
}
