<?php defined('SYSPATH') or die('No direct script access.');

class Task_Tools extends Minion_Task
{
        protected $_options = array('action' => FALSE);
        
        // Тулзы
        protected $_actions = array(
            'calculate_account_balances',       // Подсчет баланса аккаунта по дате истечения аренды
            'calculate_account_expiries',       // Подсчет даты истечения аккаунта по балансу и списку подключенных услуг
            'remove_accounts_tools',            // Удаление услуг аккаунтов из таблицы AccountsToolsRemove + пересчет expiried
            'update_superclean_slots_by_accs'   // Переносит ссылки на слоты из старой таблицы AccountsSuperCleanSlots
        );

        protected function _execute(array $params) 
        {
                if (! $params['action'])
                {
                        return $this->__no_action();
                }
                else if (! in_array($params['action'], $this->_actions))
                {
                        return $this->__invalid_action($params['action']);
                }
                
                $action = $params['action'];
                
                unset($params['action']);

                return $this->{'__' . $action}($params);
        }
        
        protected function __calculate_account_balances(array $params)
	{
                $expiried = strtotime('+1 day midnight') - 1;
                $accounts = Repo::factory('Account')->findAll(array(array('expiried', $expiried, '>')));
                
                $a = 0;
                
                foreach ($accounts as $account)
                {
                        $balance   = 0;
                        $monthly   = 0;
                        $tools     = $account->tools->find_all();
                        $_expiried = $expiried;
                
                        foreach($tools as $tool)
                        {
                                $monthly = $monthly + ($tool->monthlyCost * 100);
                        }
                        
                        while($account->expiried > $_expiried) 
                        {
                                $_expiried = strtotime('+1 day', $_expiried);
                                $balance   = $balance + ($monthly / date("t", $_expiried));
                        }
                        
                        $account->set('balance', $balance)->save();
                        
                        $messages[] = 'Баланс аккаунта ' . $account->id . ': ' . (floor($balance) / 100) . ' руб.';
                                
                        $a++;
                }
                
                $messages[] = 'Миссия завершена! Обработано аккаунтов: ' . $a . '.';
                
                Minion_CLI::write($messages);
	}
        
        protected function __calculate_account_expiries(array $params)
	{
                $accounts = Repo::factory('Account')->findAll(array(array('balance', 0, '>')));
                
                $a = 0;
                
                foreach ($accounts as $account)
                {
                        Service::factory('Account')->increaseBalance($account, 0, FALSE);
                        
                        $messages[] = 'Аккаунт ' . $account->id . ' обработан.';
                                
                        $a++;
                }
                
                $messages[] = 'Миссия завершена! Обработано аккаунтов: ' . $a . '.';
                
                Minion_CLI::write($messages);
	}
        
        protected function __remove_accounts_tools(array $params)
	{
                // Сначала собираем список всех задач на удаление услуг
                $remove = Repo::factory('AccountsToolsRemove')->getAll();
                
                $query =  'DELETE AccountsTools, AccountsToolsRemove FROM AccountsTools '
                        . 'INNER JOIN AccountsToolsRemove ON AccountsToolsRemove.accountId = AccountsTools.accountId '
                        . 'WHERE AccountsToolsRemove.toolId = AccountsTools.toolId';

                // Удаляем разом все услуги из списка
                DB::query(Database::DELETE, $query)->execute();
                
                // Список аккаунтов с пересчитанным сроком окончания
                $increase = array();
                
                // Пробегаемся по списку
                foreach ($remove as $tool) 
                {
                        // Если аккаунт еще не обрабатывался
                        if (! in_array($tool->accountId, $increase))
                        {
                                // Пересчитываем срок окончания аккаунта
                                Service::factory('Account')->increaseBalance($tool->accountId, 0, FALSE);
                                
                                // Добавляем аккаунт в список обработанных
                                $increase[] = $tool->accountId;
                        }
                        
                        // Удаляем запись из БД
                        $tool->delete();
                }
	}
        
        protected function __update_superclean_slots_by_accs(array $params)
	{
                // Сначала достаем все связи из старой таблицы
                $oldLinks = Repo::factory('AccountsSuperCleanSlots')->getAll();
                
                // Загружаем старые слоты для суперчистки
                $oldSlots = array();
                
                foreach(Repo::factory('SuperCleanSlots')->getAll() as $_slots)
                {
                        $oldSlots[$_slots->id] = $_slots;
                }                
                
                // Находим тулзу "Суперчистка"
                $tool = Repo::factory('Tool')->getOneByField('code', 'superclean');
                
                // Создаем массив связей num => slotsId для новых слотов
                $slotsNumIds = array();
                
                foreach(Repo::factory('Slots')->findAll(array(array('toolId', $tool->id))) as $slots)
                {                        
                        $slotsNumIds[$slots->num] = $slots->id;
                }
                
                // Пробегаемся по списку
                foreach ($oldLinks as $link) 
                {
                        $accTool = Repo::factory('AccountsTool')->getOneByFields(array(
                            array('accountId', $link->accountId),
                            array('toolId', $tool->id),
                        ));
                        
                        if(! $accTool->loaded())
                        {
                                continue;
                        }
                        
                        $num = $oldSlots[$link->slotsId]->num;
                        
                        $accTool->set('slotsId', $slotsNumIds[$num])->set('updated', time())->save();
                }
	}
        
        protected function __no_action()
	{
                $message = Minion_CLI::color('CLI request has no action!', 'red');
                Minion_CLI::write($message);
	}
        
        protected function __invalid_action($action)
	{
                $message = Minion_CLI::color('CLI task has no action "'. $action .'"!', 'red');
                Minion_CLI::write($message);
	}
}
