<?php defined('SYSPATH') or die('No direct script access.');

class Task_Servicefee extends Minion_Task
{
        private $feeReports = array();
        
        protected function _execute(array $params)
	{
                $tools = Service::factory('Tool')->getAll('id');
            
                $where = array(
                    array('freeUntil', time()+86400, '<'), 
                    array('expiried', time()+86400, '<'), 
                    array('autopay', Model_AccountsTool::AUTOPAY_ON)
                );
                $atFee = array();
                
                foreach (Repo::factory('AccountsTool')->findAll($where) as $accTool) 
                {
                        if(! isset($tools[$accTool->toolId]))
                        {
                                continue;
                        }
                        if(! isset($atFee[$accTool->accountId]))
                        {
                            $atFee[$accTool->accountId] = [];
                        }
                        $atFee[$accTool->accountId][$accTool->toolId] = $accTool->expiried;
                }
                        
                var_dump($atFee);
        }
        
        private function __accountsFeeExecute666($tools, $slots)
        {
                $where = array(
                    array('freeUntil', time()+86400, '<'), 
                    array('expiried', time()+86400, '<'), 
                    array('autopay', Model_AccountsTool::AUTOPAY_ON)
                );
                $atFee = array();
                
                foreach (Repo::factory('AccountsTool')->findAll($where) as $accTool) 
                {
                        if(! isset($tools[$accTool->toolId]))
                        {
                                continue;
                        }                        
                        $atFee[$accTool->accountId] = isset($atFee[$accTool->accountId]) ?
                                $atFee[$accTool->accountId] : 0;
                        
                        switch ($accTool->toolId) {
                                case Model_AccountsTool::TOOL_AUTOCOACH:
                                        $atFee[$accTool->accountId] += 
                                                $this->__calculateAutocoachFee(
                                                        $accTool->accountId, 
                                                        Arr::get($autocoachInstances, $accTool->accountId));
                                        break;
                                case Model_AccountsTool::TOOL_SUPERCLEAN:
                                        $atFee[$accTool->accountId] += 
                                                $this->__calculateSupercleanFee(
                                                        $accTool->accountId, $accTool->toolId, $accTool->slotsId, $slots);
                                        break;
                                default:
                                        $atFee[$accTool->accountId] += 
                                                $this->__calculateRegularFee($accTool->accountId, $tools[$accTool->toolId]);
                                        break;
                        }
                }
                
                $this->__takeFeeFromAccounts($atFee);
        }
        
        private function __calculateDailyRegularFee($accountId, $tool)
        {
                // Количество дней в текущем месяце
                $monthDaysCnt = date("t");
                $fee = $tool->monthlyCost * 100;
                        
                switch ($tool->id) {
                        case Model_AccountsTool::TOOL_DRAW:
                                $toolName = 'Рисовалка';
                                break;

                        case Model_AccountsTool::TOOL_ARCHECARDS:
                                $toolName = 'Архетип-карты';
                                break;

                        default:
                                $toolName = 'Неизвестно';
                                break;
                }
                
                $dailyFee = floor($fee / $monthDaysCnt);
                                
                $this->__saveFeeReportByAccountId(
                        $accountId, 'Подсчитана оплата за инструмент "' . $toolName . '": ' . ($dailyFee / 100));
                
                return $dailyFee;
        }
        
        private function __calculateSupercleanFee($accountId, $toolId, $slotsId, $slots)
        {
                // Количество дней в текущем месяце
                $monthDaysCnt = date("t");
                $fee = 0;
                        
                if(! is_null($slotsId) && isset($slots[$slotsId]) 
                        && $slots[$slotsId]->toolId == $toolId)
                {
                        $fee = $slots[$slotsId]->monthlyCost * 100;
                }
                
                $dailyFee = floor($fee / $monthDaysCnt);
                                
                $this->__saveFeeReportByAccountId(
                        $accountId, 'Подсчитана оплата за инструмент "Суперчистка": ' . ($dailyFee / 100));
                
                return $dailyFee;
        }
        
        private function __calculateAutocoachFee($accountId, $cnt)
        {
                // Количество дней в текущем месяце
                $monthDaysCnt = date("t");
                $fee = 0;
                
                if(! is_null($cnt))
                {
                        $fee = Model_Autocoach::TOOL_INSTANCE_FEE * 100 * $cnt;
                }
                
                $dailyFee = floor($fee / $monthDaysCnt);
                
                $this->__saveFeeReportByAccountId(
                        $accountId, 'Подсчитана оплата за инструмент "Автокоуч": ' . ($dailyFee / 100));
                
                return $dailyFee;
        }
        
        private function __countAutocoachInstancesByAccounts()
        {
                $instances = array();
                $query = 'SELECT accountId, COUNT(*) as count FROM `Autocoach` '
                        . 'WHERE status = :status GROUP BY accountId;';                                
                $result = DB::query(Database::SELECT, $query)->param(':status', Model_Autocoach::STATUS_ACTIVE)->execute();
                
                foreach ($result as $r)
                {
                        $instances[$r['accountId']] = $r['count'];
                }
                
                return $instances;
        }
        
        private function __calculateDailyFee($monthlyCost)
        {
                // Количество дней в текущем месяце
                $monthDaysCnt = date("t");
                $fee = $monthlyCost * 100;
                
                $dailyFee = floor($fee / $monthDaysCnt);
                
                return $dailyFee;
        }
        
        private function __takeFeeFromAccounts($atFee)
        {
                // Снимаем плату с аккаунтов
                foreach ($atFee as $aId => $daily)
                {
                        $query = 'UPDATE Accounts AS a SET balance = balance - :daily '
                               . 'WHERE a.id = :aId AND a.balance > 0';
                                
                        DB::query(Database::UPDATE, $query)->param(':daily', $daily)->param(':aId', $aId)->execute();
                        
                        $this->__printFeeReportByAccount($aId, ($daily / 100));
                }
        }
        
        private function __saveFeeReportByAccountId($accountId, $msg)
        {                
                $this->feeReports[$accountId][] = $msg;
        }
        
        private function __printFeeReportByAccount($accountId, $dailyFee)
        {
                $msg = Minion_CLI::color('Считаем суточную абонплату для аккаунта #' . $accountId . ':', 'green');
                Minion_CLI::write($msg);
                foreach($this->feeReports[$accountId] as $report)
                {
                        Minion_CLI::write($report);
                }
                $msg = Minion_CLI::color('С аккаунта #' . $accountId . ' списана плата: ' . $dailyFee, 'red');
                Minion_CLI::write($msg);
        }
}
