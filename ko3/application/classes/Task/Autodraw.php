<?php defined('SYSPATH') or die('No direct script access.');

setLocale( LC_ALL, "C" );

class Task_Autodraw extends Minion_Queue_Task
{
        protected $_options = array(
            'action' => 'schedule', 
            'queue'  => Model_Autodraw::QUEUE_NAME, 
            'job'    => NULL
        );
        
        protected $_params = array();

        protected function _execute(array $params) 
        {
                // Экшен
                $action = $params['action'];
                
                unset($params['action']);
                
                // Делаем параметры доступными из любого экшена
                $this->_params = $params;
                
                // Вызываем экшен
                $this->{$action}();
        }

        /**
         * Обновляет в БД статусы и перезапускает автоорисовки.
         * 
         */
        protected function status_change() 
        {
                $this->__isDemonize();
                
                // Название очереди, с которой работаем
                if ($this->_params['queue'] == Model_Autodraw::QUEUE_NAME) 
                {
                        $queue = Model_Autodraw::QUEUE_STATUS_NAME;
                }
                else
                {
                        $queue =  $this->_params['queue'];
                }

                // Timestamp начала работы скрипта
                $startJob = time();

                Queue::instance()->watch($queue)->ignore('default');

                while (true) 
                {
                        // Обрабатываем накопившиеся PCNTL-сигналы 
                        $this->_signal_handler->dispatch();
                        
                        // Если демон уже работает сутки - усыпляем его...
                        if(time() > $startJob + 86400)
                        {
                                $this->terminate();
                        }
                        
                        // Резервируем за собой задачку
                        $job = $this->__queueReserve();

                        // Если задачки пока нет - идем на новый круг (таймаут = 1 сек.)
                        if (!is_array($job) || !isset($job['id'])) 
                        {
                                continue;
                        }
                        
                        // Запускаем обработку
                        $this->__processing($job);
                }
        }

        protected function schedule()
        {
                // Время запуска отрисовки по расписанию
                $updateTime = time();
                $toolId = Model_Tool::AUTODRAW;
                $accountsExpiried = Service::factory('Tool')->getAccountIdsByExpiriedList($toolId);
                
                $msgs = array();
                $limit = Conf::get_option('autodraw.schedule.limit');
                
                // Пробегаемся по всем запланированным автоотрисовкам, которые пора запускать...
                foreach (Repo::factory('AutodrawClientSchedule')->findExpired($limit) as $task) 
                {                        
                        if(in_array($task->accountId, $accountsExpiried))
                        {
                                continue;
                        }
                        
                        // Это временный лайфхак для снижения нагрузки
                        $minimize = (Model_AutodrawClientSchedule::PERIOD_HOURLY == $task->period) ? 
                                1 : $task->minimize;
                        $result = Service::factory('Autodraw')->createByClientScheduled($task->accountId, $task->clientId, $minimize);
                        
                        if($result)
                        {
                                $task->set('updated', $updateTime)->save();
                        }
                        else
                        {
                                $task->delete();
                        }
                        $msg  = 'Schedule Task #' . $task->id;                        
                        $msg .= $result ? ' will be activated!' : ' is return error!';
                        
                        $msgs[] = $msg;
                }
                
                Minion_CLI::write($msgs);
        }

        protected function kick()
        {                
                // Название очереди
                $queue = $this->_params['queue'];
                
                // JobID
                $job = $this->_params['job'];
                
                if(! $job)
                {
                        Minion_CLI::write(Minion_CLI::color('Param "job" is required.', 'red'));
                        exit(0);
                }
                
                Queue::instance()->watch($queue);
                Queue::instance()->ignore('default');
                
                // Воскрешаем job
                Queue::instance()->kickJob($job);

                Minion_CLI::write(Minion_CLI::color('OK!', 'green'));
        }

        protected function reanimate()
        {
                // Время запуска отрисовки по расписанию
                $updateTime = time();
                $toolId = Model_Tool::AUTODRAW;
                $accountsExpiried = Service::factory('Tool')->getAccountIdsByExpiriedList($toolId);
                
                $msgs = array();
                
                // Пробегаемся по всем забытым автоотрисовкам
                foreach (Repo::factory('Autodraw')->findLost() as $task) 
                {                        
                        if(in_array($task->accountId, $accountsExpiried))
                        {
                                continue;
                        }
                        
                        $result = Service::factory('Autodraw')->reanimate($task);
                        
                        $msg  = 'Autodraw #' . $task->id;                        
                        $msg .= $result ? ' will be reanimated!' : ' is return error!';
                        
                        $msgs[] = $msg;
                }
                
                Minion_CLI::write($msgs);
        }

        protected function garbage()
        {
                $this->__delOldAutodrawFromDb();
                $this->__delOldDirsRecursive();

                Minion_CLI::write(Minion_CLI::color('OK!', 'green'));
        }
        
        private function __processing($job)
        {
                $job_  = unserialize($job['body']);

                try
                {
                        // Обновляем инфо об автоотрисовке в БД и (при необходимости) перезапускаем ее.
                        if ($this->__update($job_))
                        {
                                // Удаляем текущую задачку
                                $this->__queueDelete($job['id']);
                        }
                        // Если вдруг (!) не удалось изменить статус...
                        else
                        {
                                // Снова ставим текущую задачку в очередь с задержкой 10 сек.
                                $this->__queueRelease($job['id'], Queue::DEFAULT_PRIORITY, 10);
                        }
                }
                catch (Exception $e)
                {
                        // Пишем в лог ошибку
                        Kohana::$log->add(Log::ERROR,
                                sprintf('Unable to execute update DB job width id=%d. Buried with message "%s"',
                                        $job['id'], $e->getMessage()));
                        Kohana::$log->add(Log::ERROR, $e->getTraceAsString());

                        // Активируем немедленную запись лога в файл на диске
                        Kohana::$log->write();

                        $this->__queueBury($job['id'], Queue::DEFAULT_PRIORITY);
                }

                // Удаляем из памяти ненужные переменные
                unset($job['id'], $job, $job_);
        }

        private function __job($job)
        {
                try
                {
                        $atrament = new Atrament(Arr::get($job, 'name', '[Без имени]'));
                        $themes = Arr::get($job, 'themes', array());
                        
                        if(count($themes) > 80)
                        {
                            $themes = array_slice($themes, -80);
                        }

                        foreach ($themes as $theme)
                        {
                                $atrament->draw(Arr::get($theme, 'timeout'), Arr::get($theme, 'color'));
                        }

                        $atrament->render();

                        $this->__generateImg($atrament->getImage(), $job);
                        
                        unset($atrament, $themes);
                }
                catch (Exception $e)
                {
                        throw $e;
                }
        }

        private function __changeStatus($job, $status = Model_Autodraw::STATUS_COMPLETE)
        {
                // Название очереди
                if ($this->_params['queue'] == Model_Autodraw::QUEUE_NAME) 
                {
                        $queue = Model_Autodraw::QUEUE_NAME . 'StatusChange';
                }
                else
                {
                        $queue =  Model_Autodraw::QUEUE_STATUS_NAME;
                }
                
                // Если статус не изменился, ничего не делаем!
                if(isset($job['status']) && $job['status'] == $status)
                {
                        return TRUE;
                }
                
                $data     = array_merge($job, compact('status'));
                $priority = Queue::DEFAULT_PRIORITY;
                $delay    = Queue::DEFAULT_DELAY;
                
                if ($status == Model_Autodraw::STATUS_COMPLETE)
                {
                        $delay = ceil(Arr::get($job, 'delay') / 2);
                }
                
                $this->__queueAdd($queue, $data, $priority, $delay);
                
                unset($queue, $data, $priority, $delay);
        }

        private function __update($job)
        {
                // Флажок, что запуск повторной отрисовки не требуется
                $repeat = FALSE;
                $minimize = Arr::get($job, 'minimize', FALSE);
                
                if($minimize && $job['strainLevel'] > 3 && $job['status'] == Model_Autodraw::STATUS_COMPLETE)
                {
                        $job = $this->__updateJob($job);
                        
                        // Меняем флажок
                        $repeat = TRUE;
                }              

                if (! $this->__updateInDB($job))
                {
                        return 0;
                }
                
                // Если установлен флажок, запускаем новый цикл отрисовки
                if ($repeat)
                {
                
                        $queue    = Model_Autodraw::QUEUE_NAME;
                        $priority = $job['strainLevel'] + (Queue::DEFAULT_PRIORITY - 9);
                        $delay    = Queue::DEFAULT_DELAY;
                        
                        $this->__queueAdd($queue, $job, $priority, $delay);
                
                        unset($queue, $priority, $delay);
                }
                
                unset($job, $minimize, $repeat);
                
                return 1;
        }

        private function __updateJob($job)
        {
                $job['status']      = Model_Autodraw::STATUS_PROCESSED;
                $job['strainLevel'] = mt_rand(1, 9);
                $job['delay']       = 0;
                
                foreach ($job['themes'] as $k => $theme) {                        
                        // Вычисляем новый таймаут для темы
                        $job['themes'][$k]['timeout'] = rand(1, $job['strainLevel'] * 10);
                        // Увеличиваем общий таймаут отрисовки
                        $job['delay'] = $job['delay'] + $job['themes'][$k]['timeout'];
                }

                return $job;
        }

        private function __updateInDB($job)
        {
                $aid       = Arr::get($job, 'autodrawId');
                $values    = array_merge(Arr::extract($job, array('status', 'strainLevel')), array('processed' => time()));
                
                if(isset($job['worker']))
                {
                    $values['worker'] = Arr::get($job, 'worker');
                }

                try 
                {
                        Database::instance()->connect();
                        DB::update('Autodraw')->set($values)->where('id', '=', $aid)->execute();
                        
                        //Database::instance()->disconnect();
                } 
                catch (Database_Exception $e) 
                {
                        //$this->__log(array($e->getMessage()));
                        return 0;
                }
                catch (Exception $e) 
                {
                        //Database::instance()->disconnect();
                        
                        throw $e;
                }
                finally 
                {
                        unset($aid, $values);
                }

                return 1;
        }

        private function __generateImg($img, $job)
        {
                $imgPath = $this->__generateImgDir(Conf::get_option('autodraw_dir'));
                $imgPath .= md5(Arr::get($job, 'autodrawId') . ':=:=:' . Arr::get($job, 'name', '[Без имени]')) . '.png';

                file_put_contents($imgPath, $img);
                
                unset($imgPath);
        }

        private function __generateImgDir($dir) 
        {
                $dir .= date('Y/m/d');

                if (!file_exists($dir) || !is_dir($dir)) 
                {
                        if (!mkdir($dir, 0777, true)) 
                        {
                                return FALSE;
                        }
                }

                return $dir . '/';
        }

        private function __delOldAutodrawFromDb()
        {
                $where[] = array('processed', strtotime('-1 week midnight'), '<');
                $params[] = array('limit', 100);
                
                while (true)
                {
                        $autodraw = Repo::factory('Autodraw')->findAll($where, array('themes'), NULL, $params);

                        if (! count($autodraw))
                        {
                            break;
                        }

                        foreach ($autodraw as $draw)
                        {
                                foreach ($draw->themes->find_all() as $theme)
                                {
                                        $theme->delete();
                                }

                                $draw->delete();
                                
                                unset($theme, $draw);
                        }
                        
                        unset($autodraw);
                }
        }

        private function __delOldDirsRecursive()
        {
                $baseDir = Conf::get_option('autodraw_dir');                
                $deadline = strtotime('-1 week midnight') - 1;
                
                foreach ($this->__scanDir($baseDir) as $dir)
                {
                        $dirY = $baseDir . $dir;
                        
                        if ((int) $dir < (int) date('Y', $deadline))
                        {
                                @shell_exec('rm -rf ' . $dirY);
                                continue;
                        }
                        else if ((int) $dir > (int) date('Y', $deadline))
                        {
                                continue;
                        }
                        
                        foreach ($this->__scanDir($dirY) as $dir)
                        {
                                $dirM = $dirY . '/' . $dir;
                                
                                if ((int) $dir < (int) date('n', $deadline))
                                {
                                        @shell_exec('rm -rf ' . $dirM);
                                        continue;
                                }
                                else if ((int) $dir > (int) date('n', $deadline))
                                {
                                        continue;
                                }

                                foreach ($this->__scanDir($dirM) as $dir)
                                {
                                        if ((int) $dir < (int) date('j', $deadline))
                                        {
                                                @shell_exec('rm -rf ' . $dirM . '/' . $dir);
                                        }
                                }
                        }
                }
        }

        private function __scanDir($dir)
        {
                return array_diff(scandir($dir), array('.', '..', '.gitkeep'));
        }

        private function __log($data)
        {
                $file  = APPPATH . 'logs' . DIRECTORY_SEPARATOR . 'custom' . DIRECTORY_SEPARATOR;
                $file .= md5(microtime()) . '.php';
                
                file_put_contents($file, serialize($data));
                
                return $file;
        }
}
