<?php

final class Mailer_Sendpulse extends Mailer {

    /**
     * @var string
     */
    private $__pubKey;

    /**
     * @var integer
     */
    private $__limit;

    /**
     * @var string
     */
    private $__apiUrl = 'https://login.sendpulse.com/api/smtp/1.0/';

    /**
     * @var string
     */
    private $__encMethod = 'aes-128-cbc';

    /**
     * @var array
     */
    private $__from = [];
    
    public function __construct($config) 
    {
        $this->__pubKey = str_replace("\r\n","\n",trim($config['pubkey']));
        $this->__from   = $config['from'];
        $this->__limit  = $config['limit'];
    }
    
    public function send($data) 
    {
        $request = array(
            'action'    => 'send_email',
            'message'   => $this->__combinePostData($data),
        );
        
        $result = $this->__callApi(json_encode($request));
        
        if ($result['error']) 
        {
                // Nothing TODO
        }
        else
        {
                $this->__limit--;
        }

        return $result;
    }
    
    public function getLimit()
    {
            return $this->__limit;
    }

    private function __combinePostData($data) 
    {        
        $data['from'] = $this->__from;
        
        $message = array();
        $message['html'] = (! empty($data['html'])) ? $data['html'] : '';
        $message['text'] = (! empty($data['text'])) ? $data['text'] : '';
        $message['subject'] = (! empty($data['subject'])) ? $data['subject'] : '';
        $message['encoding'] = (! empty($data['encoding'])) ? $data['encoding'] : '';
        if (! empty($data['from'])){
            $from = array();
            $from['name'] = (! empty($data['from']['name'])) ? $data['from']['name'] : '';
            $from['email'] = (! empty($data['from']['email'])) ? $data['from']['email'] : '';
            $message['from'] = $from;
        }
        if (! empty($data['to'])){
            $to = array();
            foreach($data['to'] as $recipient){
                $toSingle = array();
                $toSingle['name'] = (! empty($recipient['name'])) ? $recipient['name'] : '';
                $toSingle['email'] = (! empty($recipient['email'])) ? $recipient['email'] : '';
                $to[] = $toSingle;
                unset($toSingle);
            }
            $message['to'] = $to;
        }
        if (! empty($data['bcc'])){
            $to = array();
            foreach($data['bcc'] as $recipient){
                $toSingle = array();
                $toSingle['name'] = (! empty($recipient['name'])) ? $recipient['name'] : '';
                $toSingle['email'] = (! empty($recipient['email'])) ? $recipient['email'] : '';
                $to[] = $toSingle;
                unset($toSingle);
            }
            $message['bcc'] = $to;
        }


        if ( (! empty($message['encoding'])) && (! in_array(strtolower($message['encoding']),array('utf8','utf-8'))) ){
            $message['html'] = mb_convert_encoding($message['html'],'utf8',$message['encoding']);
            $message['text'] = mb_convert_encoding($message['text'],'utf8',$message['encoding']);
            $message['subject'] = mb_convert_encoding($message['subject'],'utf8',$message['encoding']);
            if ( (! empty($message['from'])) && (! empty($message['from']['name'])) ){
                $message['from']['name'] = mb_convert_encoding($message['from']['name'],'utf8',$message['encoding']);
            }
            if (! empty($message['to'])){
                foreach ($message['to'] as $key=>$to){
                    if (! empty($to['name'])){
                        $message['to'][$key]['name'] = mb_convert_encoding($to['name'],'utf8',$message['encoding']);
                    }
                }
            }
            $message['encoding'] = 'utf8';
        }
        
        return $message;
    }

    private function __callApi($sData='')
    {
        if (function_exists('curl_version')) {
            if (function_exists('openssl_public_encrypt')){

                $sPass = sha1(microtime(true));
                openssl_public_encrypt($sPass,$sEncPass,$this->__pubKey);

                $sIv = substr(md5(microtime(true)),0,16);
                openssl_public_encrypt($sIv,$sEncIv,$this->__pubKey);

                $sEncData = openssl_encrypt($sData,$this->__encMethod,$sPass,1,$sIv);

                $aPost = array(
                    'key'   => md5($this->__pubKey),
                    'pass'  => $sEncPass,
                    'iv'    => $sEncIv,
                    'data'  => $sEncData,
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($aPost));
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 100);
                curl_setopt($ch, CURLOPT_URL, $this->__apiUrl);
                $res = curl_exec($ch);
                curl_close($ch);

                unset($sPass);
                unset($sIv);

                $aAnswer = json_decode($res, TRUE);

                if (base64_decode($aAnswer['pass']) && base64_decode($aAnswer['iv'])){
                    openssl_public_decrypt(base64_decode($aAnswer['pass']),$sPass,$this->__pubKey);
                    openssl_public_decrypt(base64_decode($aAnswer['iv']),$sIv,$this->__pubKey);
                    $sDataAnswer = openssl_decrypt(base64_decode($aAnswer['data']),$this->__encMethod,$sPass,1,$sIv);
                } else {
                    $sDataAnswer = base64_decode($aAnswer['data_unencrypted']);
                }

                return json_decode($sDataAnswer, TRUE);
            } else {
                throw new Exception('OpenSSL required, but not found');
            }
        } else {
            throw new Exception('CURL required, but not found');
        }
    }
    
}
