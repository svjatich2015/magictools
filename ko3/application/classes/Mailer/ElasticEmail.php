<?php

final class Mailer_ElasticEmail extends Mailer {

    /**
     * @var string
     */
    private $__apiKey;

    /**
     * @var integer
     */
    private $__limit;

    /**
     * @var string
     */
    private $__apiUrl = 'https://api.elasticemail.com/v2/email/send';

    /**
     * @var array
     */
    private $__from = [];
    
    public function __construct($config) {
        $this->__apiKey = $config['apikey'];
        $this->__from   = $config['from'];
        $this->__limit  = $config['limit'];
    }
    
    public function send($data) {        
        $post   = $this->__combinePostData($data);        
        $result = $this->__callApi($post);
        
        if ($result['success']) 
        {
                $result['error'] = FALSE;
                $this->__limit--;
        }
        
        return $result;
    }
    
    public function getLimit()
    {
            return $this->__limit;
    }
    
    private function __combinePostData($data) {
        $post = array(
            'apikey'          => $this->__apiKey,
            'isTransactional' => TRUE,
            'to'              => $data['to'][0]['email'],
            'from'            => $this->__from['email'],
            'fromName'        => $this->__from['name'],
            'subject'         => $data['subject'],
            'bodyText'        => $data['text']
        );
        
        return $post;
    }

    private function __callApi(array $post)
    {   
        $ch = curl_init();
        $curl_opts = [
            CURLOPT_URL            => $this->__apiUrl,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => false,
            CURLOPT_SSL_VERIFYPEER => false
        ];
        curl_setopt_array($ch, $curl_opts);
		
        $result = json_decode(curl_exec($ch), TRUE);
        curl_close($ch);
        
        return $result;
    }
    
}
