<?php

class Mailer {
    
    public static function factory($settings = 'default') {       
        $config = Conf::get_option('mailer.' . $settings);
        
        if(is_null($config)) {
            throw new \Exception('Mailer config not found!');
        }
        
        $driver = Arr::pop_assoc($config, 'driver');
        
        if(is_null($driver)) {
            throw new \Exception('Mailer config has no driver option!');
        }
        
        $class  = 'Mailer_' . $driver;
        
        if(! class_exists($class)) {
            throw new \Exception('Mailer driver "' . $driver . '" not found!');
        }
        
        return new $class($config);
    }
}
