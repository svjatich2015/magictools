<?php defined('SYSPATH') or die('No direct script access.');
/**
 *   Хелпер, позволяющий создавать переменные-контейнеры для хранения любых данных
 * 
 * @category  Helper
 * @author    Svjat Maslov
 */
class Bag {

        /**
         * @var  variables  Контейнеры для хранения данных
         */
        protected static $_variables;

        /**
         * Возвращает сохраненные данные
         * 
         * @return  instance
         */
        public static function get($name) 
        {
                if (! isset(self::$_variables[$name])) 
                {
                        throw new Kohana_Exception("Variable does not exist!");
                }
                
                return self::$_variables[$name];
        }

        /**
         * Сохраняет данные
         * 
         * @param   string    $name
         * @param   mixed     $variable
         */
        public static function set($name, $variable) 
        {
                self::$_variables[$name] = $variable;
        }

}
