<?php defined('SYSPATH') OR die('No direct script access.');

class Date extends Kohana_Date {
        
        public static $rusMonthGenetive = array(
            '', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 
            'августа', 'сентября', 'октября', 'ноября', 'декабря', 
        );
        
        public static function genetiveRusDateByUT($unixtime = 'now')
        {
                $day = date('j', $unixtime);
                $month = self::$rusMonthGenetive[date('n', $unixtime)];
                $year = date('Y', $unixtime);
                
                return join(' ', [$day, $month, $year]);
        }
        
        public static function genetiveRusMonth($number = 'now')
        {
                if($number !== 'now' && !in_array($number, range(1, 12))) 
                {
                        $number = 'now';
                }                
                if($number == 'now')
                {
                        $number = date('n');
                }
                
                return self::$rusMonthGenetive[$number];
        }

        /**
	 * Возвращает разницу в днях между двумя метками timestamp в человекочитаемом формате.
	 *
	 *     $span = Date::spanRusDays(1495532884, 1495745999); // 2 дня
	 *
	 * @param   integer $remote timestamp to find the span of
	 * @param   integer $local  timestamp to use as the baseline
	 * @return  string   when only a single output is requested
	 */
	public static function spanRusDays($remote, $local = NULL)
	{
		return Date::decl(Date::span($remote, $local, 'days'), array('день', 'дня', 'дней'));
	}
        
        /**
         * Возвращает правильное склонение для даты/времени/количества чего-либо
         * 
         *      $decl = Date::decl(17, array('мгновение', 'мгновения', 'мгновений')); // 17 мгновений
         * 
         * @param integer $int
         * @param integer $expr
         * @return string
         */
        public static function decl($int, $expr = array()) 
        {
                settype($int, "integer");
                
                $count = $int % 100;
                
                if ($count >= 5 && $count <= 20) 
                {
                        $result = $int . " " . $expr['2'];
                }
                else
                {
                        $count = $count % 10;
                        if ($count == 1) 
                        {
                                $result = $int . " " . $expr['0'];
                        } 
                        elseif ($count >= 2 && $count <= 4) 
                        {
                                $result = $int . " " . $expr['1'];
                        } 
                        else 
                        {
                                $result = $int . " " . $expr['2'];
                        }
                }
                
                return $result;
        }

}
