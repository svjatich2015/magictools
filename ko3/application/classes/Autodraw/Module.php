<?php defined('SYSPATH') or die('No direct script access.');

/**
 * AutodrawModule Factory class.
 *
 * @category  Autodraw
 * @author    Svjat Maslov
 */
class Autodraw_Module {
        
        protected static $_instances = [];
        
        public static function factory($name)
        {
                $class = 'Autodraw_Module_' . $name;

                return new $class();
        }

        public static function initById($id)
        {                
                $register = Model_DrawModule::REGISTER;
                
                if(! isset(self::$_instances[$id]) && ! isset($register[$id]))
                {
                        return NULL;
                }
                
                if(! isset(self::$_instances[$id]))
                {
                        self::$_instances[$id] = self::factory($register[$id]);
                }
                
                return self::$_instances[$id];
        }
}