<?php defined('SYSPATH') or die('No direct script access.');

/**
 * AutodrawModule Interface.
 *
 * @category  AutodrawModule
 * @author    Svjat Maslov
 */
interface Autodraw_IModule {

    public function createCard($accountId, $data);
    public function getCardsByAccountId($accountId, $deleted = FALSE);
    public function getFields();
    public function getModuleId();
    public function getOptions();
    public function getScriptsByCard(Model_DrawModuleCard $card);
    
}