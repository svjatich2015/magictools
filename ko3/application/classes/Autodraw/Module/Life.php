<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  AutodrawModule 
 */
class Autodraw_Module_Life implements Autodraw_IModule {
        
        private $moduleId = 3;
        
        private $moduleTitle = 'Жизнь';
        
        private $fields = ['name', 'birthday'];
        
        private $options = [
            [
                'name' => 'name',
                'label' => 'Имя',
                'type' => 'text'
            ], 
            [
                'name' => 'birthday',
                'label' => 'Дата рождения',
                'type' => 'date'
            ]
        ];
        
        public function createCard($accountId, $data)
        {
                if(! Arr::keys_exists($this->fields, $data))
                {
                        throw new Exception_AutodrawModuleHandlerCreateCardException;
                }               
                
                $serializedData = serialize(Arr::extract($data, $this->fields));
                
                return $this->_createCard($accountId, $serializedData);
        }
        
        public function getCardsByAccountId($accountId, $deleted = FALSE)
        {
                $result = [];
                $where = [['accountId', $accountId], ['moduleId', $this->moduleId]];                
                if(! $deleted)
                {
                    $where[] = ['status', Model_DrawModuleCard::STATUS_DELETED, '<'];
                }
                
                foreach(Repo::factory('DrawModuleCard')->findAll($where) as $card)
                {
                        $_data = @unserialize($card->data);
                        if(! $_data)
                        {
                                continue;
                        }
                        $_data['birthday'] = $this->_generateRusBirthdayDateStr($_data['birthday']);
                        $result[] = [
                            'id' => $card->id, 
                            'data' => $_data, 
                            'status' => $card->status
                        ];
                }
                
                return $result;
        }
        
        public function getFields()
        {
                return $this->fields;
        }
        
        public function getModuleId()
        {
                return $this->moduleId;
        }
        
        public function getModuleTitle()
        {
                return $this->moduleTitle;
        }
        
        public function getOptions()
        {
                return $this->options;
        }
        
        public function getScriptsByCard(Model_DrawModuleCard $card)
        {                
                $data = @unserialize($card->data);                
                if(! is_array($data))
                {
                        return [];
                }
                $scripts = $this->_loadScripts();
                $scriptsCnt = count($scripts);
                $createdDayUts = strtotime('midnight', $card->created);
                $nowDayUts = strtotime('midnight');
                $daysPassed = floor(($nowDayUts - $createdDayUts) / 86400);
                if($daysPassed > $scriptsCnt)
                {
                    $daysPassed = $daysPassed % $scriptsCnt;
                }
                $actualScriptsKey = '+' . $daysPassed . ' day';  
                
                return isset($scripts[$actualScriptsKey]) ? 
                    $scripts[$actualScriptsKey] : [];
        }
        
        private function _createCard($accountId, $data)
        {
                $values = compact('accountId', 'data');
                $values['moduleId'] = $this->moduleId;
                
                return Repo::factory('DrawModuleCard')->create($values);
        }
        
        private function _generateRusBirthdayDateStr($dateStr)
        {
                $ut = strtotime($dateStr);
                $day = date('j', $ut);
                $month = date('n', $ut);
                $year = date('Y', $ut);
                
                return $day . ' ' . Date::genetiveRusMonth($month) . ' ' . $year;
        }
        
        private function _loadScripts()
        {
                return include APPPATH . '/autodraw/modules/scripts/life.php';
        }
}