<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @category  AutodrawModule 
 */
class Autodraw_Module_Newyear implements Autodraw_IModule {
        
        private $moduleId = 2;
        
        private $moduleTitle = 'Новый Год';
        
        private $fields = ['name', 'timezone'];
        
        private $options = [
            [
                'name' => 'name',
                'label' => 'Имя',
                'type' => 'text'
            ], 
            [
                'name' => 'timezone',
                'label' => 'Часовой пояс',
                'type' => 'timezone'
            ]
        ];
        
        public function createCard($accountId, $data)
        {
                if(! Arr::keys_exists($this->fields, $data))
                {
                        throw new Exception_AutodrawModuleHandlerCreateCardException;
                }               
                
                $serializedData = serialize(Arr::extract($data, $this->fields));
                
                return $this->_createCard($accountId, $serializedData);
        }
        
        public function getCardsByAccountId($accountId, $deleted = FALSE)
        {
                $result = [];
                $where = [['accountId', $accountId], ['moduleId', $this->moduleId]];                
                if(! $deleted)
                {
                    $where[] = ['status', Model_DrawModuleCard::STATUS_DELETED, '<'];
                }
                
                foreach(Repo::factory('DrawModuleCard')->findAll($where) as $card)
                {
                        $_data = @unserialize($card->data);
                        if(! $_data)
                        {
                                continue;
                        }
                        $_data['timezone'] = $this->_generateTimezoneStr($_data['timezone']);
                        $result[] = [
                            'id' => $card->id, 
                            'data' => $_data, 
                            'status' => $card->status
                        ];
                }
                
                return $result;
        }
        
        public function getFields()
        {
                return $this->fields;
        }
        
        public function getModuleId()
        {
                return $this->moduleId;
        }
        
        public function getModuleTitle()
        {
                return $this->moduleTitle;
        }
        
        public function getOptions()
        {
                return $this->options;
        }
        
        public function getScriptsByCard(Model_DrawModuleCard $card)
        {                
                $data = @unserialize($card->data);
                
                if(! is_array($data))
                {
                        return [];
                }
                
                $result = [];
                $scripts = $this->_loadScripts();
                $nyUts = strtotime('1 january', time() + ($data['timezone'] * 3600));
                $nowDate = date('j|n', time() + ($data['timezone'] * 3600));
                
                foreach ($scripts as $k => $v)
                {
                        $triggerDate = date('j|n', strtotime($k, $nyUts));
                        if($nowDate == $triggerDate)
                        {
                                foreach ($v as $script)
                                {
                                        $result[] = $this->_replaceMacrosInScript($script);
                                }
                        }
                }
                
                return $result;
        }
        
        private function _createCard($accountId, $data)
        {
                $values = compact('accountId', 'data');
                $values['moduleId'] = $this->moduleId;
                
                return Repo::factory('DrawModuleCard')->create($values);
        }
        
        private function _generateTimezoneStr($tzStr)
        {
                $symbol = (intval($tzStr) < 0) ? '' : '+';
                
                return 'UTC' . $symbol . $tzStr;
        }
        
        private function _loadScripts()
        {
                return include APPPATH . '/autodraw/modules/scripts/birthday.php';
        }
        
        private function _replaceMacrosInScript($script)
        {
                $search = ['{Y}', '{Y+1}', '{Y-1}'];
                $replace = [date('Y'), date('Y') + 1, date('Y') - 1];
                foreach ($script['themes'] as $k => $v)
                {
                        $script['themes'][$k] = str_replace($search, $replace, $v);
                }
                
                return $script;
        }
}