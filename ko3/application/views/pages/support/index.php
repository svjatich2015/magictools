                  
        <div class="page-header"><h1>Служба поддержки</h1></div>  
        <div class="col-md-12">
            <p>По любым вопросам, связанным с работой сайта или аккаунта, вы можете обратиться удобным способом:</p>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-6">
            <p><strong>Email:</strong> <a href="mailto:e@helpdesk.magic-tools.ru" target="_blank">e@helpdesk.magic-tools.ru</a></p>
        </div>
        <div class="col-md-6">
            <p><strong>Telegram:</strong> <a href="https://t.me/MagicToolsSupportBot">https://t.me/MagicToolsSupportBot</a></p>
        </div>