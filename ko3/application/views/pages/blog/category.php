    <!--<div class="page-header"><h1>Блог</h1></div> -->
        <ol class="breadcrumb">
            <li><a href="/">Блог</a></li>
            <li class="active">
                <small><i class="glyphicon glyphicon-folder-open" aria-hidden="true"></i></small>
                &nbsp;<?=$category->title?></li>
            <li class="active">Список постов</li>
        </ol>
    <div class="page-blog"> 
        <?php foreach ($posts as $post) : ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="blog-header">
                    <h1><a href="/blog/post/<?=$post->id?>" class="blog-header-link"><?=$post->title?></a></h1> 
                </div>
                <div class="blog-meta">     
                    <span class="blog-meta-timestamp">
                        <small><i class="glyphicon glyphicon-calendar text-muted" aria-hidden="true"></i></small>
                        <?=date('d.m.Y', $post->timestamp);?>
                    </span>
                    <a class="blog-meta-link" href="/blog/category/<?=$post->cat->id?>">
                        <small><i class="glyphicon glyphicon-folder-open" aria-hidden="true"></i></small>
                        &nbsp;<?=$post->cat->title?>
                    </a>
                    <a class="blog-meta-link" href="/blog/post/<?=$post->id?>#comments">
                        <small><i class="glyphicon glyphicon-comment" aria-hidden="true"></i></small>
                        &nbsp;<?=$post->commentsCnt?>
                    </a>
                </div>
                <div class="blog-content">        
                    <div class="row">
                        <div class="col-md-12">
                            <?=$post->excerpt?>
                            <div class="more">
                                <a class="btn btn-primary btn-lg" href="/blog/post/<?=$post->id?>" role="button">Читать полностью</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="tags">
                        <?php foreach ($post->tags as $tag) : ?>
                                <a class="blog-tag" href="/blog/tag/<?= $tag->id ?>">
                                    <small><i class="glyphicon glyphicon-tag" aria-hidden="true"></i></small>
                                    <?= $tag->name ?>
                                </a>
                        <?php endforeach; ?>
                    </div>
                </li>
            </ul>
        </div>
        <?php endforeach; ?>
            <div class="col-xs-12 col-md-6">
                <?php if(ceil($postsCnt/$limit) > 1) : ?>
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Страница </span></li>
                        <li<?=($pageNum == 1) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/blog') . 
                                URL::query(array('page' => NULL, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;">1</a>
                        </li>
                        <?php for($a=2; $a<=ceil($postsCnt/$limit); $a++) : ?>
                        <li<?=($pageNum == $a) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/blog') . 
                                URL::query(array('page' => $a, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;"><?=$a?></a>
                        </li>
                        <?php endfor; ?> 
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?php if($postsCnt > 10) : ?>
                <nav class="xs-left md-right" aria-label="Sort">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Отображать по </span></li>
                        <?php foreach(array(10, 20, 50, 100) as $_limit) :?>
                        <li<?=($limit == $_limit) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/blog') . URL::query(array('page' => NULL, 'rows' => $_limit))?>"><?=strval($_limit)?><?=($limit == $_limit) ? ' <span class="sr-only">(current)</span>':''?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
        <div class="clearfix">&nbsp;</div>
    </div> 

