    <!--<div class="page-header"><h1>Блог</h1></div> -->
    <ol class="breadcrumb">
        <li><a href="/">Блог</a></li>
        <li class="active"><?=$post->title?></li>
    </ol>
    <div class="page-blog">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="blog-header">
                    <h1><a href="/blog/post/<?=$post->id?>" class="blog-header-link"><?=$post->title?></a></h1> 
                </div>
                <div class="blog-meta">     
                    <span class="blog-meta-timestamp">
                        <small><i class="glyphicon glyphicon-calendar text-muted" aria-hidden="true"></i></small>
                        <?=date('d.m.Y', $post->timestamp);?>
                    </span>
                    <a class="blog-meta-link" href="/blog/category/<?=$post->cat->id?>">
                        <small><i class="glyphicon glyphicon-folder-open" aria-hidden="true"></i></small>
                        &nbsp;<?=$post->cat->title?>
                    </a>
                    <a class="blog-meta-link" href="/blog/post/<?=$post->id?>#comments">
                        <small><i class="glyphicon glyphicon-comment" aria-hidden="true"></i></small>
                        &nbsp;<?=$post->commentsCnt?>
                    </a>
                </div>
                <div class="blog-content">        
                    <div class="row">
                        <div class="col-md-12">
                            <?=$post->content?>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="tags">
                        <?php foreach ($post->tags as $tag) : ?>
                                <a class="blog-tag" href="/blog/tag/<?= $tag->id ?>">
                                    <small><i class="glyphicon glyphicon-tag" aria-hidden="true"></i></small>
                                    <?= $tag->name ?>
                                </a>
                        <?php endforeach; ?>
                    </div>
                </li>
            </ul>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="list-group">
            <div class="list-group-item">
                <div class="container">
                    <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                        <center><a id="addCommentBtn" class="btn btn-primary btn-lg btn-block" href="#" role="button">Добавить комментарий</a></center>
                    </div>
                </div>
            </div>
        </div>
        <form id="hiddenCommentForm" method="POST"><input type="hidden" name="comment"></form>
        <div class="clearfix">&nbsp;</div>
        <?php if(count($comments) > 0) : ?>
        <h3 id="comments">Комментарии</h3>
        <div class="clearfix">&nbsp;</div>
        <?php /*<div class="panel panel-warning panel-blog-comment">
            <div class="panel-heading"><strong>Ваш комментарий</strong></div>
            <div class="panel-body">
                <form method="post" class="blog-comment-form">
                    <div class="form-group">
                        <textarea name="comment" class="form-control" id="commentText" placeholder="Введите текст..."></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
        </div>   */ ?> 
        <?php foreach ($comments as $comment) : ?>
                <?php if(Identity::init()->getAccountInfo()->id == $comment->account->id) : ?>
                <div class="panel panel-warning panel-blog-comment" id="blogCommentId<?=$comment->id?>">
                <?php else : ?>
                <div class="panel panel-default">
                <?php endif; ?>
                    <div class="panel-heading">
                        <strong><?=$comment->profile->name ?> <?= $comment->profile->surname?></strong>
                        написал(а):
                        <?php if(Identity::init()->getAccountInfo()->id == $comment->account->id) : ?>
                        <button type="button" class="close pull-right btn-delete-comment" data-comment-id="<?=$comment->id?>" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php endif; ?>

                    </div>
                    <div class="panel-body">
                        <?= $comment->text ?>
                    </div>
                </div>
        <?php endforeach; ?>
        <div class="clearfix">&nbsp;</div>
        <?php endif; ?>
    </div> 

