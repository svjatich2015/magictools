
          <div class="page-header"><h1>Добавление клиента</h1></div>
          <form id="formDraw" method="POST" autocomplete="off">
              
              <table class="table">
                  <thead>
                      <tr>
                          <td>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputClient">Название карточки клиента *</label>
                                  <input type="text" name="title" class="form-control" id="inputClient" placeholder="Отображается в общем списке клиентов" required="required">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputWho">Кого отрисовываем? *</label>
                                  <input type="text" name="who" class="form-control" id="inputWho" placeholder="Имя человека" required="required">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                  <label for="inputBodys">Отрисовываем виды внимания?</label>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isbodys" id="inputBodys" value="1">
                                          Да
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isbodys" id="inputBodys2" value="0" checked="checked">
                                          Нет
                                      </label>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                  <label for="inputSpheres">Отрисовываем сферы жизнедеятельности?</label>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isspheres" id="inputSpheres" value="1">
                                          Да
                                      </label>
                                  </div>
                                  
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isspheres" id="inputSpheres2" value="0" checked="checked">
                                          Нет
                                      </label>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-md-6 form-group" id="inputWhatFormGroup">
                                  <label for="inputWhat">Что еще отрисовываем?</label>
                                  <div class="input-group">
                                      <input type="text" name="what[]" class="form-control" id="inputWhat" placeholder="Тема для отрисовки">
                                      <span class="input-group-btn">
                                          <button class="btn btn-success" type="button" id="inputWhatAdd">
                                              <i class="glyphicon glyphicon-plus"></i>
                                          </button>
                                      </span>
                                  </div><!-- /input-group -->
                              </div>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="button" class="btn btn-primary" id="formDrawSubmit">Добавить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
