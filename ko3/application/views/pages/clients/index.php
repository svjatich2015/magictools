          <div class="page-header"><h1>Клиенты</h1></div>
          
          <div class="table-responsive hidden-xs">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Название карточки клиента</th>
                  <th>Имя</th>
                  <th style="text-align: center;">Тела</th>
                  <th style="text-align: center;">Сферы</th>
                  <th style="text-align: center;">Темы</th>
                  <th style="width: 100px;">Отрисован</th>
                  <th style="width: 100px;"></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($clients as $client) : ?>
                <tr>
                  <td><?=$client['title']?></td>
                  <td><?=$client['name']?></td>
                  <td style="text-align: center;"><?=($client['isBodies']) ? 'Да' : 'Нет'?></td>
                  <td style="text-align: center;"><?=($client['isScheme']) ? 'Да' : 'Нет'?></td>
                  <td style="text-align: center;"><?=$client['themesCnt']?></td>
                  <td><?=is_null($client['processed']) ? '<em>Нет</em>' : date('d.m.Y H:i:s', $client['processed'])?></td>
                  <td style="text-align: center;">
                      <form action="<?= URL::site('/clients/delete/') ?>" method="POST" id="clientDelete<?=$client['id']?>"><input type="hidden" name="client" value="<?=$client['id']?>"></form>
                      <nobr>
                          <a href="<?=URL::site('clients/edit/'.$client['id'])?>" title="Редактировать"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                          &nbsp;&nbsp;
                          <a href="#" class="client-delete" data-client-id="<?=$client['id']?>" title="Удалить"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                      </nobr>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="panel-group hidden-sm hidden-md hidden-lg" id="accordionTable" role="tablist" aria-multiselectable="true">
              <?php foreach ($clients as $panelId => $client) : ?>
              <div class="panel panel-default panel-plus-minus">
                  <div class="panel-heading" role="tab" id="heading<?=$panelId?>">
                        <a class="no-link collapsed" role="button" data-toggle="collapse" data-parent="#accordionTable" href="#collapse<?=$panelId?>" aria-expanded="true" aria-controls="collapse<?=$panelId?>">
                            <span class="text-muted">
                                <h4 class="panel-title">
                                        <?=$client['title']?>
                                </h4>
                            </span>
                        </a>
                  </div>
                  <div id="collapse<?=$panelId?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$panelId?>">
                      <div class="panel-body">
                          <p><strong>Имя клиента:</strong> <?=$client['name']?></p>
                          <p><strong>Отрисовка тонких тел:</strong> <?=($client['isBodies']) ? 'Да' : 'Нет'?></p>
                          <p><strong>Отрисовка сфер жизни:</strong> <?=($client['isScheme']) ? 'Да' : 'Нет'?></p>
                          <p><strong>Тем для отрисовки:</strong> <?=$client['themesCnt']?></p>
                          <p><strong>Последняя отрисовка:</strong> <?=is_null($client['processed']) ? '<em>Нет</em>' : date('d.m.Y H:i:s', $client['processed'])?></p>
                          <form action="<?= URL::site('/clients/delete/') ?>" method="POST" id="clientDelete<?=$client['id']?>"><input type="hidden" name="client" value="<?=$client['id']?>"></form>                      
                          <nobr><a class="btn btn-default" href="<?=URL::site('clients/edit/'.$client['id'])?>" role="button" title="Редактировать"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Редактировать</a>
                          &nbsp;&nbsp;
                          <a class="btn btn-default client-delete" href="#" role="button" data-client-id="<?=$client['id']?>" title="Удалить"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Удалить</a></nobr>
                      </div>
                  </div>
              </div>
              <?php endforeach; ?>
          </div>
          <div class="row">
              <div class="col-xs-12 col-md-6">
                  <?php if(ceil($clientsCnt/$limit) > 1) : ?>
                  <nav aria-label="Page navigation">
                      <ul class="pagination">
                          <li class="disabled"><span style="cursor: default;">Страница </span></li>
                          <li<?=($pageNum == 1) ? ' class="active"':''?>>
                              <a href="<?=URL::site('clients') . 
                                  URL::query(array('page' => NULL, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;">1</a>
                          </li>
                          <?php for($a=2; $a<=ceil($clientsCnt/$limit); $a++) : ?>
                          <li<?=($pageNum == $a) ? ' class="active"':''?>>
                              <a href="<?=URL::site('clients') . 
                                  URL::query(array('page' => $a, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;"><?=$a?></a>
                          </li>
                          <?php endfor; ?> 
                      </ul>
                  </nav>
                  <?php endif; ?>
              </div>
              <div class="col-xs-12 col-md-6">
                  <?php if($clientsCnt > 10) : ?>
                  <nav class="xs-left md-right" aria-label="Sort">
                      <ul class="pagination">
                          <li class="disabled"><span style="cursor: default;">Отображать по </span></li>
                          <?php foreach(array(10, 20, 50, 100) as $_limit) :?>
                          <li<?=($limit == $_limit) ? ' class="active"':''?>>
                              <a href="<?=URL::site('clients') . URL::query(array('page' => NULL, 'rows' => $_limit))?>"><?=strval($_limit)?><?=($limit == $_limit) ? ' <span class="sr-only">(current)</span>':''?></a>
                          </li>
                          <?php endforeach; ?>
                      </ul>
                  </nav>
                  <?php endif; ?>
              </div>
          </div>
          <div class="clearfix">&nbsp;</div>
