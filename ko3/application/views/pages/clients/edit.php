
          <div class="page-header"><h1>Редактирование клиента</h1></div>
          <form id="formDraw" method="POST" autocomplete="off">
              
              <table class="table">
                  <thead>
                      <tr>
                          <td>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputClient">Название карточки клиента *</label>
                                  <input type="text" name="title" class="form-control" id="inputClient" placeholder="Отображается в общем списке клиентов" required="required" value="<?=$client->title?>">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputWho">Кого отрисовываем? *</label>
                                  <input type="text" name="who" class="form-control" id="inputWho" placeholder="Имя человека" required="required" value="<?=$client->name?>">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                  <label for="inputBodys">Отрисовываем виды внимания?</label>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isbodys" id="inputBodys" value="1"
                                                  <?=($client->isBodies) ? 'checked' : ''?>>
                                          Да
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isbodys" id="inputBodys2" value="0"
                                                  <?=($client->isBodies) ? '' : 'checked'?>>
                                          Нет
                                      </label>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                  <label for="inputSpheres">Отрисовываем сферы жизнедеятельности?</label>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isspheres" id="inputSpheres" value="1"
                                                  <?=($client->isScheme) ? 'checked' : ''?>>
                                          Да
                                      </label>
                                  </div>
                                  
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isspheres" id="inputSpheres2" value="0"
                                                  <?=($client->isScheme) ? '' : 'checked'?>>
                                          Нет
                                      </label>
                                  </div>
                              </div>
                              <?php $themesCnt = count($themes); ?>
                              <?php $themeNum = 1; ?>
                              <?php $lastTheme = ''; ?>
                              <?php foreach($themes as $theme) : ?>
                              <?php if($themesCnt <= $themeNum) : ?>
                              <?php $lastTheme = $theme->title; break; ?>
                              <?php endif; ?>
                              <?php $_themeNum = 1000 + $themeNum; ?>
                              <div class="col-xs-12 col-md-6 form-group form-group-input-what<?=($themeNum > Model_DrawClient::THEMES_MAX_CNT - 1) ? ' has-warning' : ''?>" id="inputWhatFormGroup<?=$_themeNum?>">
                                  <label for="inputWhat<?=$_themeNum?>">Что еще отрисовываем?</label>
                                  <div class="input-group">
                                      <input type="text" name="what[]" class="form-control" id="inputWhat<?=$_themeNum?>" placeholder="Тема для отрисовки" value="<?=$theme->title?>">
                                      <span class="input-group-btn">
                                          <button class="btn btn-danger btn-input-what-remove" type="button" data-input-what-id="<?=$_themeNum?>">
                                              <i class="glyphicon glyphicon-minus"></i>
                                          </button>
                                      </span>
                                  </div><!-- /input-group -->
                              </div>
                              <?php $themeNum++; ?>
                              <?php endforeach; ?>
                              <div class="col-xs-12 col-md-6 form-group" id="inputWhatFormGroup">
                                  <label for="inputWhat">Что еще отрисовываем?</label>
                                  <div class="input-group">
                                      <input type="text" name="what[]" class="form-control" id="inputWhat" placeholder="Тема для отрисовки" value="<?=$lastTheme?>">
                                      <span class="input-group-btn">
                                          <button class="btn btn-success<?=($themeNum >= Model_DrawClient::THEMES_MAX_CNT) ? ' disabled' : ''?>" type="button" id="inputWhatAdd">
                                              <i class="glyphicon glyphicon-plus"></i>
                                          </button>
                                      </span>
                                  </div><!-- /input-group -->
                              </div>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="button" class="btn btn-primary" id="formDrawSubmit">Отредактировать</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
