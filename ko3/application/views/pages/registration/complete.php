<div class="row">
  <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
      <div class="alert alert-success" role="alert"><center><strong>Регистрация завершена!</strong><br><br>В течении 5 минут на электронную почту придет пароль от личного кабинета.</center></div>
  </div>
</div>