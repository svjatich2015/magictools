
      <?php $request = Arr::extract(array_merge($_GET, $_POST), array('name', 'surname', 'email', 'invite'), ''); ?>
      <form class="form-signin" method="POST">
        <?=Form::input('agree', 0, array('type'=>'hidden'))?> 
        <h2 class="form-signin-heading">Регистрация</h2>
        <?php if (strtoupper(Request::current()->method()) == 'POST' && count($errors) > 0) : ?>        
        <?php $form_errors = (array) Kohana::$config->load('form_errors'); ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors as $e) : ?>
                <li><?=$form_errors[$e]?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?> 
        <label for="inputName" class="sr-only">Имя</label>
        <?=Form::input('name', $request['name'], array('type'=>'text', 'id'=>'inputName', 
            'class'=>'form-control', 'placeholder'=>'Имя', 'required'=>'required', 'pattern'=>'^[A-Za-zА-Яа-яЁё]+$',
            'autofocus'=>'autofocus', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 
            'title'=>'Только буквы русского и латинского алфавита'))?> 
        <label for="inputSurname" class="sr-only">Фамилия</label>
        <?=Form::input('surname', $request['surname'], array('type'=>'text', 'id'=>'inputSurname', 
            'class'=>'form-control', 'placeholder'=>'Фамилия', 'required'=>'required' , 'pattern'=>'^[A-Za-zА-Яа-яЁё]+$', 
            'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Только буквы русского и латинского алфавита'))?> 
        <label for="inputEmail" class="sr-only">Email</label>
        <?=Form::input('email', $request['email'], array('type'=>'email', 'id'=>'inputEmail', 
            'class'=>'form-control', 'placeholder'=>'Email', 'required'=>'required'))?> 
        <label for="inputInvite" class="sr-only">Инвайт (код приглашения)</label>
        <?=Form::input('invite', $request['invite'], array('type'=>'text', 'id'=>'inputInvite', 
            'class'=>'form-control', 'placeholder'=>'Инвайт (код приглашения)', 'required'=>'required', 'pattern'=>'^[0-9A-Za-z]+$'))?> 
        <div class="checkbox">
          <label>
            <input type="checkbox" name="agree" id="checkboxAgree" value="1" checked="checked"> Я согласен получать новостную рассылку
          </label>
        </div>
        <input class="btn btn-lg btn-primary btn-block" id="buttonSubmit" type="submit" value="Создать аккаунт">
        <div class="row">
            <div class="col-md-12"><br /></div>
            <div class="col-md-6"><a href="<?=URL::site('/invite/');?>">Нужен инвайт?</a></div>
            <div class="col-md-6 text-right"><a href="<?=URL::site('/login/');?>">Уже есть аккаунт?</a></div>
        </div>
      </form>
