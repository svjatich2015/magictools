
          <div class="page-header"><h1>Услуги приостановлены</h1></div>
          
          <table class="table">
              <thead>
                  <tr>
                      <td>

                          <p>Предоставление услуг приостановлено, поскольку средств на балансе аккаунта недостаточно для оплаты.</p>
                          <p>Для возобновления работы необходимо пополнить баланс.</p>
                          <p>Также вы можете изменить активные услуги и/или отключить ненужные.</p>
                      </td>
                  </tr>  
              </thead>
              <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <a href="<?=URL::site('pay')?>" class="btn btn-success">Пополнить баланс</a>&nbsp;
                              <a href="<?=URL::site('settings/services')?>" class="btn btn-primary">Мои услуги</a>
                          </td>
                      </tr> 
              </tbody>
          </table>
