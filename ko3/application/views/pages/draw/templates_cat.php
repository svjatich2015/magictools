<div class="page-header">
    <h1>
        Отрисовка шаблонов
        <small><span class="label label-lblack" style="vertical-align: text-top;"><?=$cat->title?></span></small>
    </h1>
</div>
          <div class="container">
              <div class="row row-fluid-xs">
                  <?php foreach($tpls as $template) : ?>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <div class="thumbnail draw-templates-thumbnail">
                          <div class="caption">
                              <h4><?=Text::limit_chars($template->title, 32, '...', TRUE);?></h4>
                              <p><?=$template->description?></p>
                               <a href="<?=URL::site('draw/template/' . $template->id)?>" class="btn btn-primary btn-sm" role="button"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a> <a href="<?=URL::site('draw/template/' . $template->id)?>" target="_blank" class="btn btn-light-blue btn-sm" role="button"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> В новом окне</a>
                          </div>
                      </div>
                  </div>
                  <?php endforeach; ?>
              </div><!--/row-->
          </div><!--/container -->
          <div class="clearfix">&nbsp;</div>
