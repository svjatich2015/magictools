<?php 
        $_clients = array(); 
        foreach ($clients as $client)
        {
                $_client = $client->as_array();                
                $_client['themesCnt'] = $client->themes->count_all();
                $_clients[] = $_client;
                
        }
?>

          <div class="page-header"><h1>Отрисовка клиентов</h1></div>
          
          <div class="table-responsive hidden-xs">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th style="width: 50%;">Название карточки клиента</th>
                  <th style="width: 50%;">Имя</th>
                  <th style="width: 100px;"></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($_clients as $client) : ?>
                <tr>
                  <td><?=$client['title']?></td>
                  <td><?=$client['name']?></td>
                  <td style="text-align: center;">
                      <nobr>
                          <a class="btn btn-default" role="button" href="<?= URL::site('draw/client/'.$client['id'])?>" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Отрисовать</span></a>
                          &nbsp;&nbsp;
                          <a class="btn btn-default" role="button" href="<?= URL::site('draw/client/'.$client['id'])?>" onclick="if(window.open('<?= URL::site('draw/clients/') . URL::query(array('page' => Arr::get($_GET, 'page'), 'rows' => Arr::get($_GET, 'rows')))?>')){location='<?= URL::site('draw/client/'.$client['id'])?>'; return false;}" title="Отрисовать в новом окне" target="_blank"><i class="glyphicon glyphicon-share" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> В новом окне</span></a>
                      </nobr>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="panel-group hidden-sm hidden-md hidden-lg" id="accordionTable" role="tablist" aria-multiselectable="true">
              <?php foreach ($_clients as $panelId => $client) : ?>
              <div class="panel panel-default panel-plus-minus">
                  <div class="panel-heading" role="tab" id="heading<?=$panelId?>">
                        <a class="no-link collapsed" role="button" data-toggle="collapse" data-parent="#accordionTable" href="#collapse<?=$panelId?>" aria-expanded="true" aria-controls="collapse<?=$panelId?>">
                            <span class="text-muted">
                                <h4 class="panel-title">
                                        <?=$client['title']?>
                                </h4>
                            </span>
                        </a>
                  </div>
                  <div id="collapse<?=$panelId?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$panelId?>">
                      <div class="panel-body">
                          <p><strong>Имя клиента:</strong> <?=$client['name']?></p>
                          <p><strong>Отрисовка тонких тел:</strong> <?=($client['isBodies']) ? 'Да' : 'Нет'?></p>
                          <p><strong>Отрисовка сфер жизни:</strong> <?=($client['isScheme']) ? 'Да' : 'Нет'?></p>
                          <nobr><a class="btn btn-default" href="<?= URL::site('draw/client/'.$client['id'])?>" role="button" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a>
                          &nbsp;&nbsp;
                          <a class="btn btn-default" href="<?= URL::site('draw/client/'.$client['id'])?>" role="button" onclick="if(window.open('<?= URL::site('draw/clients/') . URL::query(array('page' => Arr::get($_GET, 'page'), 'rows' => Arr::get($_GET, 'rows')))?>')){location='<?= URL::site('draw/client/'.$client['id'])?>'; return false;}" title="Отрисовать в новом окне" target="_blank"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> В новом окне</a></nobr>
                      </div>
                  </div>
              </div>
              <?php endforeach; ?>
          </div>
          <div class="row">
              <div class="col-xs-12 col-md-6">
                  <?php if(ceil($clientsCnt/$limit) > 1) : ?>
                  <nav aria-label="Page navigation">
                      <ul class="pagination">
                          <li class="disabled"><span style="cursor: default;">Страница </span></li>
                          <li<?=($pageNum == 1) ? ' class="active"':''?>>
                              <a href="<?=URL::site('draw/clients/') . 
                                  URL::query(array('page' => NULL, 'rows' => Arr::get($_GET, 'rows')))?>" style="cursor: pointer;">1</a>
                          </li>
                          <?php for($a=2; $a<=ceil($clientsCnt/$limit); $a++) : ?>
                          <li<?=($pageNum == $a) ? ' class="active"':''?>>
                              <a href="<?=URL::site('draw/clients/') . 
                                  URL::query(array('page' => $a, 'rows' => Arr::get($_GET, 'rows')))?>" style="cursor: pointer;"><?=$a?></a>
                          </li>
                          <?php endfor; ?> 
                      </ul>
                  </nav>
                  <?php endif; ?>
              </div>
              <div class="col-xs-12 col-md-6">
                  <?php if($clientsCnt > 10) : ?>
                  <nav class="xs-left md-right" aria-label="Sort">
                      <ul class="pagination">
                          <li class="disabled"><span style="cursor: default;">Отображать по </span></li>
                          <?php foreach(array(10, 20, 50, 100) as $_limit) :?>
                          <li<?=($limit == $_limit) ? ' class="active"':''?>>
                              <a href="<?=URL::site('draw/clients/') . URL::query(array('page' => NULL, 'rows' => $_limit))?>"><?=strval($_limit)?><?=($limit == $_limit) ? ' <span class="sr-only">(current)</span>':''?></a>
                          </li>
                          <?php endforeach; ?>
                      </ul>
                  </nav>
                  <?php endif; ?>
              </div>
          </div>
          <div class="clearfix">&nbsp;</div>
