
          <div class="page-header">
              <h1>
                  Отрисовка шаблона
                  <small><span class="label label-lblack" style="vertical-align: text-top;"><?=$tpl->title?></span></small>
              </h1>
          </div>
          <form id="draw" method="POST">  
              <table class="table">
                  <thead>
                      <tr>
                          <td>                              
                              <fieldset class="col-md-12">    	
                                  <legend>Основная информация</legend>

                                  <div class="panel panel-default panel-fieldset">
                                      <div class="panel-body">
                                          <div class="col-xs-12 col-md-6 form-group">
                                              <label for="inputWho">Кого отрисовываем? *</label>
                                              <input type="text" name="who" class="form-control" id="inputWho" placeholder="Имя человека" required="required">
                                          </div>
                                          <div class="col-xs-12 col-md-6 form-group">
                                              <label for="inputWhat">Что отрисовываем? *</label>
                                              <input type="text" name="what" class="form-control" id="inputWhat" placeholder="Тема для отрисовки" required="required">
                                          </div>
                                          <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                              <label for="inputBodys">Отрисовываем виды внимания?</label>
                                              <div class="radio">
                                                  <label>
                                                      <input type="radio" name="isbodys" id="inputBodys" value="1">
                                                      Да
                                                  </label>
                                              </div>
                                              <div class="radio">
                                                  <label>
                                                      <input type="radio" name="isbodys" id="inputBodys2" value="0" checked="checked">
                                                      Нет
                                                  </label>
                                              </div>
                                          </div>
                                          <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                              <label for="inputSpheres">Отрисовываем сферы жизнедеятельности?</label>
                                              <div class="radio">
                                                  <label>
                                                      <input type="radio" name="isspheres" id="inputSpheres" value="1">
                                                      Да
                                                  </label>
                                              </div>
                                              <div class="radio">
                                                  <label>
                                                      <input type="radio" name="isspheres" id="inputSpheres2" value="0" checked="checked">
                                                      Нет
                                                  </label>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                              </fieldset>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary">Отрисовать</button>&nbsp;&nbsp;
                              <?php /*<button type="button" class="btn btn-info btn-blank-submit">В новом окне</button>*/ ?>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
