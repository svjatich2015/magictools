        <div class="page-header">
            <h1>
                <?php 
                if(strtolower(Request::initial()->action()) == 'client')
                {
                        echo 'Отрисовка клиента';
                }
                else if(strtolower(Request::initial()->action()) == 'template')
                {
                        echo 'Отрисовка шаблона';
                }
                else
                {
                        echo 'Быстрая отрисовка';
                }
                ?>
            </h1>
        </div>
        <div class="col-md-12" style="padding-left: 0px;">
          <div id="drawmessage">
              <dl class="dl-horizontal"> 
                  <dt>Имя клиента:</dt> 
                  <dd><?=$info['title']?></dd>
                  <?php if(strtolower(Request::initial()->action()) == 'client') : ?>
                  <dt>Название карточки:</dt> 
                  <dd><?=$client->title;?></dd>
                  <?php elseif(strtolower(Request::initial()->action()) == 'template') : ?>
                  <dt>Название шаблона:</dt> 
                  <dd><?=$tpl->title;?></dd>
                  <?php if($tpl->type == Model_DrawTemplate::TYPE_USER_0_100 && isset($info['theme'])) : ?>
                  <dt>Название темы:</dt> 
                  <dd><?=$info['theme']?></dd>
                  <?php endif; ?>
                  <?php endif; ?>
                  <dt>Нагруженность:</dt> 
                  <dd id="drawStrainLevel"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></dd>
              </dl>
          </div>
          <div id="canvas">
                  <canvas id="sketcher"></canvas>
                  <?php foreach ($themes as $k => $v) : ?> 
                  <canvas id="draw<?=$k?>"></canvas> 
                  <?php endforeach; ?> 
                  <div id="drawstatus"></div>
          </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-12">
          <div class="well well-lg">
              <div class="row">
                  <?php /*
                  <div class="col-lg-4 col-md-6 col-xs-12"><strong>Имя клиента:</strong> <?=$title;?></div>
                  <?php if(strtolower(Request::initial()->action()) == 'client') : ?>
                  <div class="col-lg-4 col-md-6 col-xs-12"><strong>Название карточки:</strong> <?= $client->title; ?></div>
                  <?php elseif(strtolower(Request::initial()->action()) == 'template') : ?>
                  <div class="col-lg-4 col-md-6 col-xs-12"><strong>Название шаблона:</strong> <?= $tpl->title; ?></div>
                  <?php endif; ?>
                  <div class="col-lg-4 col-md-6 col-xs-12"><strong>Нагруженность отрисовки:</strong> 3 / 10</div>
                  <div class="clearfix"></div><hr /><div class="clearfix"></div>
                   * 
                   */ ?>
                  <?php foreach ($themes as $theme) : ?> 
                  <div class="col-md-4 col-sm-6 col-xs-12"><span class="example"><span class="color" style="background-color: <?=$theme["color"]?>;">&nbsp;</span> <?=$theme["title"]?></span></div> 
                  <?php endforeach; ?>
              </div>
          </div>
        </div>