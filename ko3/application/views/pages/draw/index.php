          <div class="page-header"><h1>Рисовалка</h1></div>
          
          <ul id="tabs" class="nav nav-tabs nav-draw" data-tabs="tabs">
              <li role="presentation" class="active" id="draw_fast"><a class="no-link" href="#draw_base" data-toggle="tab"><big><span class="glyphicon glyphicon-time visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Быстрая</span></a></li>
              <li role="presentation"><a class="no-link" href="#draw_clients" data-toggle="tab"><big><span class="glyphicon glyphicon-user visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Клиенты</span></a></li>
              <li role="presentation"><a class="no-link" href="#draw_templates" data-toggle="tab"><big><span class="glyphicon glyphicon-folder-open visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Шаблоны</span></a></li>
              <li role="presentation" class="dropdown" id="draw_ex" style="display: none;">
                  <a class="dropdown-toggle no-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <big><span class="glyphicon glyphicon-option-horizontal visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Отрисовки <span class="badge">1</span> <span class="caret text-muted"></span></span>
                  </a>
                  <ul class="dropdown-menu nav-draw" data-tabs="tabs"></ul>
              </li>
          </ul>
          <div class="clearfix">&nbsp;</div>
          <div class="tab-content" id="draw_tabs">
              <div class="tab-pane active" id="draw_base">
                  <form id="fastDraw" class="form-draw" method="POST" autocomplete="off">
                    <input type="hidden" name="target" value="_parent">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>                              
                                    <fieldset class="col-md-12">    	
                                        <legend>Основная информация</legend>

                                        <div class="panel panel-default panel-fieldset">
                                            <div class="panel-body">
                                                <div class="col-xs-12 col-md-6 form-group">
                                                    <label for="inputWho">Кого отрисовываем? *</label>
                                                    <input type="text" name="who" class="form-control" id="inputWho" placeholder="Имя человека" required="required">
                                                </div>
                                                <div class="col-xs-12 col-md-6 form-group">
                                                    <label for="inputWhat1">Что отрисовываем? *</label>
                                                    <input type="text" name="what[]" class="form-control" id="inputWhat1" placeholder="Тема для отрисовки" required="required">
                                                </div>
                                                <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                    <label for="inputBodys">Отрисовываем виды внимания?</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isbodys" id="inputBodys" value="1" checked="checked">
                                                            Да
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isbodys" id="inputBodys2" value="0">
                                                            Нет
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                    <label for="inputSpheres">Отрисовываем сферы жизнедеятельности?</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isspheres" id="inputSpheres" value="1" checked="checked">
                                                            Да
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isspheres" id="inputSpheres2" value="0">
                                                            Нет
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="clearfix">&nbsp;</div>
                                    <button type="submit" class="btn btn-primary">Отрисовать</button>&nbsp;&nbsp;
                                    <?php /*<button type="button" class="btn btn-info btn-blank-submit">В новом окне</button>*/ ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
              </div>
              <div class="tab-pane" id="draw_clients">
                <div class="table-responsive hidden-xs">
                    <div class="content-preloader table-preloader">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th style="width: 50%;">Название</th>
                              <th style="width: 50%;">Имя</th>
                              <th style="width: 100px;"></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($clients as $client) : ?>
                            <tr>
                              <td><?=$client['title']?></td>
                              <td><?=$client['name']?></td>
                              <td style="text-align: center;">
                                  <nobr>
                                      <a class="btn btn-default btn-client-draw" role="button" href="#" data-client-id="<?=$client['id']?>" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a>
                                      &nbsp;&nbsp;
                                      <a class="btn btn-default btn-client-draw" role="button" href="#" data-client-id="<?=$client['id']?>" data-target="_blank" title="Отрисовать в фоновой вкладке"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> В фоне</a>
                                  </nobr>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                          </tbody>
                        </table>    
                        <div class="loader">
                            <div class="loader-text">
                                <div class="preloader-css">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="panels hidden-sm hidden-md hidden-lg">
                    <div class="content-preloader tab-preloader">
                        <div class="panel-group" id="accordionTable" role="tablist" aria-multiselectable="true">
                            <?php foreach ($clients as $panelId => $client) : ?>
                            <div class="panel panel-default panel-plus-minus">
                                <div class="panel-heading" role="tab" id="heading<?=$panelId?>">
                                      <a class="no-link collapsed" role="button" data-toggle="collapse" data-parent="#accordionTable" href="#collapse<?=$panelId?>" aria-expanded="true" aria-controls="collapse<?=$panelId?>">
                                          <span class="text-muted">
                                              <h4 class="panel-title">
                                                      <?=$client['title']?>
                                              </h4>
                                          </span>
                                      </a>
                                </div>
                                <div id="collapse<?=$panelId?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$panelId?>">
                                    <div class="panel-body">
                                        <p><strong>Имя клиента:</strong> <?=$client['name']?></p>
                                        <p><strong>Отрисовка тонких тел:</strong> <?=($client['isBodies']) ? 'Да' : 'Нет'?></p>
                                        <p><strong>Отрисовка сфер жизни:</strong> <?=($client['isScheme']) ? 'Да' : 'Нет'?></p>
                                        <nobr>
                                            <a class="btn btn-default btn-client-draw" href="#" data-client-id="<?=$client['id']?>" role="button" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a>
                                            &nbsp;&nbsp;
                                            <a class="btn btn-default btn-client-draw" href="#" data-client-id="<?=$client['id']?>" data-target="_blank" role="button" title="Отрисовать в фоновой вкладке"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> Отрисовать в фоне</a>
                                        </nobr>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>   
                        </div>
                        <div class="loader">
                            <div class="loader-text">
                                <div class="preloader-css">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?php if(ceil($clientsCnt/10) > 1) : ?>
                        <nav aria-label="Page navigation">
                            <ul class="pagination" id="draw_clients_pagination">
                                <li class="disabled"><span style="cursor: default;">Страница </span></li>
                                <li class="active"><a href="#" style="cursor: pointer;" data-page="1">1</a></li>
                                <?php for($a=2; $a<=ceil($clientsCnt/10); $a++) : ?>
                                <li><a href="#" style="cursor: pointer;" data-page="<?=$a?>"><?=$a?></a></li>
                                <?php endfor; ?> 
                            </ul>
                        </nav>
                        <?php endif; ?>
                    </div>
                </div>
              </div>
              <div class="tab-pane" id="draw_templates">
                  <div class="content-preloader">
                      <div id="draw_templates_wrapper">
                          <ol class="breadcrumbs breadcrumb hidden-xs hidden"></ol>
                          <div class="breadcrumbs breadcrumb-sm hidden-sm hidden-md hidden-lg hidden">
                              <div class="list-group breadcrumb-list-group">
                                  <div class="list-group-item">
                                      <div class="container">
                                          <div class="col-xs-12 text-center"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="list-group breadcrumb-list-group-header">
                                  <div class="list-group-item">
                                      <h3 align="center"></h3>
                                  </div>
                              </div>
                          </div>
                          <div class="list-group draw-templates-list-group subtab-content">
                              <?php foreach ($cats as $cat) : ?>
                                      <?php if ($cat->templates->count_all() < 1) continue; ?>
                                      <div class="list-group-item">
                                          <div class="container">
                                              <div class="col-md-8 col-lg-9">
                                                  <h2 class="list-group-item-heading"> 
                                                      <a href="#" class="no-link templates-cat" data-templates-cat-id="<?= $cat->id ?>" data-templates-cat-title="<?= $cat->title ?>"><?= $cat->title; ?></a>
                                                  </h2>
                                                  <p class="list-group-item-text">
                                                      <?= $cat->description; ?>
                                                  </p>
                                              </div>
                                              <div class="col-md-4 col-lg-3 text-center">
                                                  <h2><a href="#" class="no-link templates-cat" data-templates-cat-id="<?= $cat->id ?>" data-templates-cat-title="<?= $cat->title ?>"><?= Date::decl($cat->templates->count_all(), array('<small>шаблон</small>', '<small>шаблона</small>', '<small>шаблонов</small>')) ?></a></h2>
                                                  <a href="#" class="btn btn-lg btn-block btn-light-blue templates-cat" role="button" data-templates-cat-id="<?= $cat->id ?>" data-templates-cat-title="<?= $cat->title ?>">Открыть</a>
                                              </div>
                                          </div>
                                      </div>
                              <?php endforeach; ?>
                          </div>
                      </div>
                      <div class="loader">
                          <div class="loader-text">
                              <div class="preloader-css">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                  <span></span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="clearfix">&nbsp;</div>
          <div id="drawTemplateHtmlCode" style="display: none;">
              <div class="col-md-12" style="padding-left: 0px;">
                  <div class="drawmessage">
                      <dl class="dl-horizontal"></dl>
                  </div>
                  <div class="canvas">
                      <canvas class="sketcher"></canvas>
                      <div class="drawstatus"></div>
                  </div>
              </div>
              <div class="clearfix">&nbsp;</div>
              <div class="col-md-12">
                  <div class="well well-lg"><div class="row"></div></div>
              </div>
          </div>
          <div id="drawTemplateFormSimple" style="display: none;">
              <form class="form-draw" method="POST" autocomplete="off">  
                  <table class="table">
                      <thead>
                          <tr>
                              <td>                              
                                  <fieldset class="col-md-12">    	
                                      <legend>Основная информация</legend>

                                      <div class="panel panel-default panel-fieldset">
                                          <div class="panel-body">
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Кого отрисовываем? *</label>
                                                  <input type="text" name="who" class="form-control" placeholder="Имя человека" required="required">
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Как отрисовываем? *</label>                                  
                                                  <select name="mode" class="form-control form-input-mode">
                                                      <option value="default">Полная отрисовка шаблона</option>
                                                      <option value="special">Выборочная отрисовка шаблона</option>
                                                  </select>
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем виды внимания?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем сферы жизнедеятельности?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                  </fieldset>                              
                                  <fieldset class="col-md-12 theme-specials" style="position: absolute; left: -100000px; top: -100000px;">    	
                                      <legend>Темы, включенные в шаблон</legend>
                                      <div class="panel panel-default panel-fieldset panel-checkbox">
                                          <div class="panel-body">
                                              <div class="col-xs-12 form-group form-checkbox theme-specials-items">
                                                  <input type="hidden" name="what" value="">
                                              </div>
                                          </div>
                                      </div>
                                  </fieldset>
                              </td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="clearfix">&nbsp;</div>
                                  <button type="submit" class="btn btn-primary btn-submit" data-target="_parent">Отрисовать</button>
                                  &nbsp;&nbsp;
                                  <button type="submit" class="btn btn-info btn-submit" data-target="_blank">В новом окне</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </form>
          </div>
          <div id="drawTemplateForm0100" style="display: none;">
              <form class="form-draw" method="POST" autocomplete="off">  
                  <table class="table">
                      <thead>
                          <tr>
                              <td>                              
                                  <fieldset class="col-md-12">    	
                                      <legend>Основная информация</legend>

                                      <div class="panel panel-default panel-fieldset">
                                          <div class="panel-body">
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Кого отрисовываем? *</label>
                                                  <input type="text" name="who" class="form-control" placeholder="Имя человека" required="required">
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Что отрисовываем? *</label>
                                                  <input type="text" name="theme" class="form-control" placeholder="Тема для отрисовки" required="required">
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем виды внимания?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем сферы жизнедеятельности?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </fieldset>                              
                                  <fieldset class="theme-specials" style="position: absolute; left: -100000px; top: -100000px;">
                                      <input type="text" name="what[]" style="width: 1px;" value="Зачатие">
                                      <input type="text" name="what[]" style="width: 1px;" value="1-9 месяцы беременности">
                                      <input type="text" name="what[]" style="width: 1px;" value="Роды">
                                      <input type="text" name="what[]" style="width: 1px;" value="1 год жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="2-5 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="6-10 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="11-15 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="16-20 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="21-25 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="26-30 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="31-35 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="36-40 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="41-45 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="46-50 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="51-55 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="56-60 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="61-65 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="66-70 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="71-75 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="76-80 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="81-85 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="86-90 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="91-95 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="96-100 годы жизни">
                                  </fieldset>
                              </td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="clearfix">&nbsp;</div>
                                  <button type="submit" class="btn btn-primary btn-submit" data-target="_parent">Отрисовать</button>
                                  &nbsp;&nbsp;
                                  <button type="submit" class="btn btn-info btn-submit" data-target="_blank">В новом окне</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </form>
          </div>
          <div id="tmpHtmlCode"></div>
          <form id="drawFormHtmlCode" class="form-draw" style="display: none;"></form>
