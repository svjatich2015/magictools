
          <div class="page-header"><h1>Быстрая отрисовка</h1></div>
          <form id="draw" method="POST">
              
              <table class="table">
                  <thead>
                      <tr>
                          <th>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputWho">Кого отрисовываем? *</label>
                                  <input type="text" name="who" class="form-control" id="inputWho" placeholder="Имя человека" required="required">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputWhat1">Что отрисовываем? *</label>
                                  <input type="text" name="what[]" class="form-control" id="inputWhat1" placeholder="Тема для отрисовки" required="required">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                  <label for="inputBodys">Отрисовываем виды внимания?</label>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isbodys" id="inputBodys" value="1">
                                          Да
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isbodys" id="inputBodys2" value="0" checked="checked">
                                          Нет
                                      </label>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                  <label for="inputSpheres">Отрисовываем сферы жизнедеятельности?</label>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isspheres" id="inputSpheres" value="1">
                                          Да
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="isspheres" id="inputSpheres2" value="0" checked="checked">
                                          Нет
                                      </label>
                                  </div>
                              </div>
                          </td>
                      </th>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary">Отрисовать</button>&nbsp;&nbsp;
                              <?php /*<button type="button" class="btn btn-info btn-blank-submit">В новом окне</button>*/ ?>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
