<div class="page-header"><h1>Отрисовка шаблонов</h1></div>
          <div class="list-group draw-templates-list-group">
              <?php foreach($cats as $cat) : ?>
              <?php if($cat->templates->count_all() < 1) continue; ?>
              <div class="list-group-item">
                  <div class="container">
                      <div class="col-md-8 col-lg-9">
                          <h2 class="list-group-item-heading"> 
                              <a href="<?=URL::site('draw/templates_cat/' . $cat->id)?>" class="no-link"><?=$cat->title;?></a>
                          </h2>
                          <p class="list-group-item-text">
                            <?=$cat->description;?>
                          </p>
                      </div>
                      <div class="col-md-4 col-lg-3 text-center">
                          <h2><a href="<?=URL::site('draw/templates_cat/' . $cat->id)?>" class="no-link"><?=Date::decl($cat->templates->count_all(), array('<small>шаблон</small>', '<small>шаблона</small>', '<small>шаблонов</small>'))?></a></h2>
                          <a href="<?=URL::site('draw/templates_cat/' . $cat->id)?>" class="btn btn-lg btn-block btn-light-blue" role="button">Открыть</a>
                      </div>
                  </div>
              </div>
              <?php endforeach; ?>
          </div>
          <div class="clearfix">&nbsp;</div>
