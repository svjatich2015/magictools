
          <div class="page-header"><h1>Настройки аккаунта</h1></div>
          <form id="profile" method="POST" autocomplete="off">
              <?php if(Arr::keys_exists(array('email', 'currentpassword', 'password'), $_POST)) : ?>
              <?php if($result === TRUE) : ?>
              <div class="alert alert-success">
                      <button class="close" data-close="alert"></button>
                      <span>
                      Профиль успешно обновлен. </span>
              </div>
              <?php else: ?>
              <?php $form_errors = (array) Kohana::$config->load('form_errors'); ?>
              <div class="alert alert-danger">
                      <ul>
                          <li><?=$form_errors[$result]?></li>
                      </ul>
              </div>
              <?php endif; ?>
              <?php endif; ?>
              <table class="table">
                  <thead>
                      <tr style="display: none;">
                          <td style="padding-bottom: 20px;">
                              <?=View::factory('templates/sub/services_menu')?> 
                          </td>
                      </tr>
                      <tr>
                          <td style="padding-top: 20px;">                            
                              <fieldset class="col-md-12">    	
                                  <legend>Профиль</legend>
                                  <div class="panel panel-default panel-fieldset">
                                      <div class="panel-body">
                                          <div class="col-xs-12 col-md-6 form-group">
                                              <label for="inputEmail">Ваш email *</label>
                                              <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Контактный email" required="required" value="<?= Identity::init()->getAccountInfo()->email; ?>">
                                              <p class="help-block text-weight-normal">&nbsp;</p>
                                          </div>
                                          <div class="col-xs-12 col-md-6 form-group">
                                              <label for="inputPassword">Ваш пароль *</label>
                                              <input type="text" name="password" class="form-control" id="inputPassword" placeholder="Желаемый пароль (необязательно)" pattern="[\w\W]{6,}">
                                              <p class="help-block text-weight-normal">Минимум 6 символов.</p>
                                          </div>
                                      </div>
                                  </div>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary" id="profileSubmit">Сохранить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
          <form id="profilePostForm" method="POST">
              <input type="hidden" name="email" id="profileEmail">
              <input type="hidden" name="password" id="profileNewPassword">
              <input type="hidden" name="currentpassword" id="profileCurrentPassword">
          </form>
