
<div class="page-header"><h1>Автокоуч</h1></div>
<!--<div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    Короткая инструкция по использованию данного инструмента 
    <a href="https://drive.google.com/open?id=1k4YJU_p02RgOd2zYMpewJFZb6imWPN2YHdVPzsjsvA8"
       class="alert-link" target="_blank" style="text-decoration: underline;">находится здесь</a>.
</div>-->
<?php if(count($autocoach) > 0) : ?>
<div class="panel panel-default">
    <table class="table table-striped table-panels">
        <!--
        <thead>
            <tr>
                <th>Тема</th>
                <th>Клиент</th>
                <th style="width: 100px;">Статус</th>
                <th style="width: 100px;"></th>
            </tr>
        </thead>
        -->
        <tbody>
            <?php foreach($autocoach as $_autocoach) : ?>
            <tr>
                <td>
                    <div class="col-xs-12 col-lg-6">
                        <strong>Клиент:</strong>
                        <?=$_autocoach->target?>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <strong>Статус:</strong>
                        <?php if ($_autocoach->status == Model_Autocoach::STATUS_ACTIVE) : ?>
                            <span class="text-green">В работе</span>
                        <?php else : ?>
                            <span class="text-orange">На паузе</span>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-lg-12">
                        <strong>Тема:</strong>
                        <?=$_autocoach->theme?>
                    </div>
                    <form action="/autocoach/delete" method="POST" id="autocoachDelete<?=$_autocoach->id?>"><input type="hidden" name="theme" value="<?=$_autocoach->id?>"></form>
                    <div class="col-xs-12 col-lg-12 cp-buttons">
                        <?php if($_autocoach->status == Model_Autocoach::STATUS_ACTIVE) : ?>
                        <a href="/autocoach/pause/<?=$_autocoach->id?>" class="btn btn-lorange" title="Приостановить">Приостановить</a>
                        <?php else : ?>
                        <a href="/autocoach/activate/<?=$_autocoach->id?>" class="btn btn-lgreen" title="Запустить">Запустить</a>
                        <?php endif; ?>
                        <a href="/autocoach/edit/<?=$_autocoach->id?>" class="btn btn-lblue" title="Редактировать">Изменить</a>
                        <a href="#" class="btn btn-lred theme-delete" data-theme-id="<?=$_autocoach->id?>" title="Удалить">Удалить</a>
                    </div>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<?php endif; ?>
<div class="list-group">
            <div class="list-group-item">
                <div class="container">
                    <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                        <center><a class="btn btn-primary btn-lg btn-block" href="/autocoach/add" role="button">Добавить тему</a></center>
                    </div>
                </div>
            </div>
        </div>
