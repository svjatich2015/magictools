
          <div class="page-header"><h1>Автокоуч</h1></div>
          <form id="draw" method="POST"> 
              <table class="table">
                  <thead>
                      <tr>
                          <td>
                              <fieldset class="col-md-12">   	
                                  <legend>Редактирование темы</legend>

                                  <div class="panel panel-default panel-fieldset">
                                      <div class="panel-body">
                                          <div class="col-xs-12 col-md-6 form-group" style="margin-bottom: 16px;">
                                              <label for="inputWho">Кого автокоучим? *</label>
                                              <input name="who" class="form-control" id="inputWho" placeholder="Имя человека" required="required" value="<?= $autocoach->target; ?>">
                                          </div>
                                          <div class="col-xs-12 col-md-6 form-group form-textarea-counter" style="margin-bottom: 16px;">
                                              <label for="inputDescription">Описание темы</label>
                                              <textarea name="theme" class="form-control" id="inputDescription" maxlength="200" required="required" style="height: 55px;"><?= $autocoach->theme; ?></textarea>
                                          </div>
                                      </div>
                                  </div>
                              </fieldset>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary">Изменить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
