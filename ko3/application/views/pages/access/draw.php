
          <div class="page-header"><h1>Услуга не подключена</h1></div>
          
          <table class="table">
              <thead>
                  <tr>
                      <td>

                          <p>Сервисы "Рисовалка" и "Авторисовалка" в данный момент не подключены.</p>
                          <p>Для того, чтобы начать использовать данные инструменты, нажмите на кнопку «Подключить» ниже.</p>
                      </td>
                  </tr>  
              </thead>
              <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <a class="btn btn-primary" href="<?=URL::site('services')?>">Подключить</a>
                          </td>
                      </tr> 
              </tbody>
          </table>