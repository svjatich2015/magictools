
          <div class="page-header"><h1>Услуга не подключена</h1></div>
          
          <table class="table">
              <thead>
                  <tr>
                      <td>

                          <p>Сервис "СуперЧистка" в данный момент не активен.</p>
                          <p>Для начала использования, подключите его в разделе "Инструменты".</p>
                      </td>
                  </tr>  
              </thead>
              <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <a class="btn btn-primary" href="<?=URL::site('services')?>">Подключить</a>
                          </td>
                      </tr> 
              </tbody>
          </table>

