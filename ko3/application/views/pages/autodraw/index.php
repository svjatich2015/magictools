          <div class="page-header"><h1>Авторисовалка</h1></div>
          
          <ul id="tabs" class="nav nav-tabs nav-draw" data-tabs="tabs">
              <li role="presentation" class="active" id="draw_fast"><a class="no-link" href="#draw_base" data-toggle="tab"><big><span class="glyphicon glyphicon-time visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Быстрая</span></a></li>
              <li role="presentation"><a class="no-link" href="#draw_clients" data-toggle="tab"><big><span class="glyphicon glyphicon-user visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Клиенты</span></a></li>
              <li role="presentation"><a class="no-link" href="#draw_templates" data-toggle="tab"><big><span class="glyphicon glyphicon-folder-open visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Шаблоны</span></a></li>
              <li role="presentation"><a class="no-link" href="#draw_modules" data-toggle="tab"><big><span class="glyphicon glyphicon-equalizer visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Модули</span></a></li>
              <li role="presentation" id="draw_logs"><a class="no-link" href="#draw_log" data-toggle="tab"><big><span class="glyphicon glyphicon-list-alt visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Журнал</span></a></li>

              <li role="presentation" class="dropdown" id="draw_ex" style="display: none;">
                  <a class="dropdown-toggle no-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <big><span class="glyphicon glyphicon-option-horizontal visible-xs text-muted" aria-hidden="true"></span></big><span class="hidden-xs">Отрисовки <span class="badge">1</span> <span class="caret text-muted"></span></span>
                  </a>
                  <ul class="dropdown-menu nav-draw" data-tabs="tabs"></ul>
              </li>
          </ul>
          <div class="clearfix">&nbsp;</div>
          <div class="tab-content" id="draw_tabs">
              <div class="tab-pane active" id="draw_base">
                <form id="fastDraw" class="form-draw" method="POST" autocomplete="off">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>                              
                                    <fieldset class="col-md-12">    	
                                        <legend>Основная информация</legend>

                                        <div class="panel panel-default panel-fieldset">
                                            <div class="panel-body">
                                                <div class="col-xs-12 col-md-6 form-group">
                                                    <label for="inputWho">Кого отрисовываем? *</label>
                                                    <input type="text" name="who" class="form-control" id="inputWho" placeholder="Имя человека" required="required">
                                                </div>
                                                <div class="col-xs-12 col-md-6 form-group">
                                                    <label for="inputWhat1">Что отрисовываем? *</label>
                                                    <input type="text" name="what[]" class="form-control" id="inputWhat1" placeholder="Тема для отрисовки" required="required">
                                                </div>
                                                <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                    <label for="inputBodys">Отрисовываем виды внимания?</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isbodys" id="inputBodys" value="1" checked="checked">
                                                            Да
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isbodys" id="inputBodys2" value="0">
                                                            Нет
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                    <label for="inputSpheres">Отрисовываем сферы жизнедеятельности?</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isspheres" id="inputSpheres" value="1" checked="checked">
                                                            Да
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="isspheres" id="inputSpheres2" value="0">
                                                            Нет
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="clearfix">&nbsp;</div>
                                    <button type="submit" class="btn btn-primary" data-loading-text="Загрузка...">Отрисовать</button>&nbsp;&nbsp;
                                    <?php /*<button type="button" class="btn btn-info btn-blank-submit">В новом окне</button>*/ ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
              </div>
              <div class="tab-pane" id="draw_clients">
                <div class="table-responsive">
                    <div class="content-preloader table-preloader">
                        <table class="table table-striped table-panels">
                          <tbody>
                            <?php foreach ($clients as $client) : ?>
                            <tr id="drawTblClient<?=$client['id']?>">
                              <td>
                                  <!--
                                  <span class="label" style="background-color: #FEEA3A; border: 1px solid #e4d235; color: darkgoldenrod; position: absolute; top: 0; right: 0px; border-radius: 0 0 0 0.25em; padding: 10px; font-size: 12px;">Модуль</span>
                                  -->
                                  <div class="col-xs-12 col-lg-6">
                                      <strong>Название карточки:</strong> 
                                      <?=$client['title']?>
                                  </div>
                                  <div class="col-xs-12 col-lg-6">
                                      <strong>Имя клиента:</strong> 
                                      <?=$client['name']?>
                                  </div>
                                  <div class="col-xs-12 col-lg-6">
                                      <strong>Тела (виды внимания):</strong> 
                                      <?=($client['isBodies']) ? 
                                        '<span class="text-green">Да</span>' : '<span class="text-red">Нет</span>'?> 
                                  </div>
                                  <div class="col-xs-12 col-lg-6">
                                      <strong>Сферы благополучия:</strong> 
                                      <?=($client['isScheme']) ? 
                                        '<span class="text-green">Да</span>' : '<span class="text-red">Нет</span>'?> 
                                  </div>
                                  <div class="col-xs-12 col-lg-6">
                                      <strong>Количество тем:</strong> 
                                      <?=$client['themesCnt']?>
                                  </div>
                                  <div class="col-xs-12 col-lg-6 cp-schedule">
                                      <strong>Расписание:</strong> 
                                      <?php $_period = 'null' ?>
                                      <?php if($client['schedule']['id']) : ?>
                                      <span>
                                          <button type="button" class="btn btn-link" style="padding: 0; text-decoration: none!important;" data-client-period="<?=$client['schedule']['period']?>" data-client-is-min="<?=($client['schedule']['minimize'])?'true':'false'?>" data-client-is-duration="<?=($client['schedule']['duration']['switch'] == 'on') ? 'true' : 'false'?>" data-client-duration-start="<?=$client['schedule']['duration']['start']?>" data-client-duration-finish="<?=$client['schedule']['duration']['finish']?>"><?=($client['schedule']['period'] == 'weekly') ? 'Каждую неделю' : 'Каждый день' ?></button>
                                      </span>
                                      <?php else : ?>
                                      <span>Не задано</span>
                                      <?php endif; ?>
                                  </div>
                                  <div class="col-xs-12 cp-buttons">
                                    <nobr>
                                        <a class="btn btn-default btn-client-draw" role="button" href="#" data-client-id="<?=$client['id']?>" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a>
                                        <a class="btn btn-info btn-client-schedule" role="button" href="#" data-client-id="<?=$client['id']?>" data-client-period="<?=$client['schedule']['period']?>" data-client-is-min="<?=($client['schedule']['minimize'])?'true':'false'?>" data-client-is-duration="<?=($client['schedule']['duration']['switch'] == 'on') ? 'true' : 'false'?>" data-client-duration-start="<?=$client['schedule']['duration']['start']?>" data-client-duration-finish="<?=$client['schedule']['duration']['finish']?>" title="Расписание"><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i> Расписание</a>
                                    </nobr>
                                  </div>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                          </tbody>
                        </table>    
                        <div class="loader">
                            <div class="loader-text">
                                <div class="preloader-css">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?php if(ceil($clientsCnt/10) > 1) : ?>
                        <nav aria-label="Page navigation">
                            <ul class="pagination" id="draw_clients_pagination">
                                <li class="disabled"><span style="cursor: default;">Страница </span></li>
                                <li class="active"><a href="#" style="cursor: pointer;" data-page="1">1</a></li>
                                <?php for($a=2; $a<=ceil($clientsCnt/10); $a++) : ?>
                                <li><a href="#" style="cursor: pointer;" data-page="<?=$a?>"><?=$a?></a></li>
                                <?php endfor; ?> 
                            </ul>
                        </nav>
                        <?php endif; ?>
                    </div>
                </div>
              </div>
              <div class="tab-pane" id="draw_templates">
                  <div class="content-preloader">
                      <div id="draw_templates_wrapper">
                          <ol class="breadcrumbs breadcrumb hidden-xs hidden"></ol>
                          <div class="breadcrumbs breadcrumb-sm hidden-sm hidden-md hidden-lg hidden">
                              <div class="list-group breadcrumb-list-group">
                                  <div class="list-group-item">
                                      <div class="container">
                                          <div class="col-xs-12 text-center"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="list-group breadcrumb-list-group-header">
                                  <div class="list-group-item">
                                      <h3 align="center"></h3>
                                  </div>
                              </div>
                          </div>
                          <div class="list-group draw-templates-list-group subtab-content">
                              <?php foreach ($cats as $cat) : ?>
                                      <?php if ($cat->templates->count_all() < 1) continue; ?>
                                      <div class="list-group-item">
                                          <div class="container">
                                              <div class="col-md-8 col-lg-9">
                                                  <h2 class="list-group-item-heading"> 
                                                      <a href="#" class="no-link templates-cat" data-templates-cat-id="<?= $cat->id ?>" data-templates-cat-title="<?= $cat->title ?>"><?= $cat->title; ?></a>
                                                  </h2>
                                                  <p class="list-group-item-text">
                                                      <?= $cat->description; ?>
                                                  </p>
                                              </div>
                                              <div class="col-md-4 col-lg-3 text-center">
                                                  <h2><a href="#" class="no-link templates-cat" data-templates-cat-id="<?= $cat->id ?>" data-templates-cat-title="<?= $cat->title ?>"><?= Date::decl($cat->templates->count_all(), array('<small>шаблон</small>', '<small>шаблона</small>', '<small>шаблонов</small>')) ?></a></h2>
                                                  <a href="#" class="btn btn-lg btn-block btn-light-blue templates-cat" role="button" data-templates-cat-id="<?= $cat->id ?>" data-templates-cat-title="<?= $cat->title ?>">Открыть</a>
                                              </div>
                                          </div>
                                      </div>
                              <?php endforeach; ?>
                          </div>
                      </div>
                      <div class="loader">
                          <div class="loader-text">
                              <div class="preloader-css">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                  <span></span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="draw_modules">
                  <div class="content-preloader">
                      <div id="draw_modules_wrapper">
                          <ol class="breadcrumbs breadcrumb hidden-xs hidden"></ol>
                          <div class="breadcrumbs breadcrumb-sm hidden-sm hidden-md hidden-lg hidden">
                              <div class="list-group breadcrumb-list-group">
                                  <div class="list-group-item">
                                      <div class="container">
                                          <div class="col-xs-12 text-center"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="list-group breadcrumb-list-group-header">
                                  <div class="list-group-item">
                                      <h3 align="center"></h3>
                                  </div>
                              </div>
                          </div>
                          <div class="list-group draw-modules-list-group subtab-content">
                              <?php foreach ($modules as $module_id => $module) : ?>
                                <div class="list-group-item" id="draw_modules_list_item<?=$module_id?>">
                                    <div class="container">
                                        <div class="col-md-8 col-lg-9">
                                            <h2 class="list-group-item-heading"> 
                                                <a href="#" class="no-link draw-module-href" data-draw-module-id="<?= $module_id ?>" data-draw-module-title="<?= $module['title'] ?>"><?= $module['title']; ?></a>
                                            </h2>
                                            <p class="list-group-item-text">
                                                <?= $module['description']; ?>
                                            </p>
                                        </div>
                                        <div class="col-md-4 col-lg-3 text-center">
                                            <h2><a href="#" class="no-link draw-module-href draw-module-decl-href" data-draw-module-id="<?= $module_id ?>" data-draw-module-title="<?= $module['title'] ?>" data-cards-cnt="<?=$module['count']?>"><?= Date::decl($module['count'], array('<small>карточка</small>', '<small>карточки</small>', '<small>карточек</small>')) ?></a></h2>
                                            <a href="#" class="btn btn-lg btn-block btn-light-blue draw-module-href" role="button" data-draw-module-id="<?= $module_id ?>" data-draw-module-title="<?= $module['title'] ?>">Открыть</a>
                                        </div>
                                    </div>
                                </div>
                              <?php endforeach; ?>
                          </div>
                      </div>
                      <div class="loader">
                          <div class="loader-text">
                              <div class="preloader-css">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                  <span></span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div> 
              <div class="tab-pane" id="draw_log">
                <div class="panel panel-default panel-draw-log-delete">
                    <div class="panel-body">
                    <form class="form-inline" id="draw_log_delete">
                        <div class="form-group">
                            <label>Очистить записи:</label>
                            <select name="period" class="form-control">
                                <option value="all">За неделю</option>
                                <option value="5days">За 5 дней</option>
                                <option value="3days">За 3 дня</option>
                                <option value="day">За сутки</option>
                                <option value="hour">За час</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary" data-loading-text="Подождите...">Применить</button>
                    </form>
                    </div>
                </div>
                <div class="table">
                    <div class="content-preloader table-preloader">
                        <table class="table table-striped">
                          <tbody>
                            <?php if($autodrawCnt) : ?>
                            <?php foreach ($list as $_autodraw) : ?>
                            <tr>
                              <td>
                                  <div class="row" id="autodraw<?=$_autodraw['id']?>_wrapper">
                                      <div class="col-xs-12 col-sm-7 col-md-4 col-lg-4">
                                          <div class="col-xs-12 col-md-12 autodraw-name-wrapper"><h3 class="autodraw-name"><?=$_autodraw['name']?></h3></div>
                                          <div class="col-xs-12 col-md-12 autodraw-title-wrapper"><p class="autodraw-title"><?=$_autodraw['title']?></p></div>
                                      </div>
                                      <div class="col-xs-12 col-sm-5 col-md-8 col-lg-3">
                                        <div class="col-xs-12 col-md-6 col-md-push-6 col-lg-12 col-lg-push-0 autodraw-status-wrapper">
                                          <p class="autodraw-status">
                                            <?php if($_autodraw['strainLevel'] < 4) : ?>
                                            <span class="autodraw-strain-level text-green">
                                            <?php elseif($_autodraw['strainLevel'] < 8) : ?>
                                            <span class="autodraw-strain-level text-orange">
                                            <?php else : ?>
                                            <span class="autodraw-strain-level text-red">
                                            <?php endif; ?>
                                              <span class="glyphicon glyphicon-flash"></span><?=$_autodraw['strainLevel']?>
                                            </span>
                                            <span class="autodraw-status-text">
                                                <?php if($_autodraw['status'] == Model_Autodraw::STATUS_COMPLETE) : ?>
                                                <span class="text-blue-gray">Отрисовано</span>
                                                <?php elseif($_autodraw['status'] == Model_Autodraw::STATUS_PROCESSED) : ?>
                                                <span class="text-gray">Отрисовывается</span>
                                                <?php else : ?>
                                                <span class="text-gray">В ожидании</span>
                                                <?php endif; ?>                                                        
                                            </span>
                                          </p>
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-md-pull-6 col-lg-12 col-lg-pull-0 autodraw-date-wrapper"><span class="badge"><?=$_autodraw['date']?></span></div>
                                      </div>
                                      <div class="col-xs-12 col-lg-5">
                                        <div class="col-md-12 autodraw-actions-wrapper">
                                          <?php if($_autodraw['status'] == Model_Autodraw::STATUS_COMPLETE) : ?>
                                          <a class="btn btn-default btn-autodraw-open" role="button" href="#" data-autodraw-id="<?=$_autodraw['id']?>" data-target="_parent" data-loading-text="Загрузка..." title="Открыть"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Открыть</a><a class="btn btn-default btn-autodraw-open" href="#" data-autodraw-id="<?=$_autodraw['id']?>" data-target="_blank" data-loading-text="Загрузка..." role="button" title="Открыть в фоновой вкладке"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> В фоне</a><a class="btn btn-danger btn-autodraw-delete" href="#" data-autodraw-id="<?=$_autodraw['id']?>" data-loading-text="Удаление..." role="button" title="Удалить"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Удалить</a>
                                          <?php else : ?>
                                          <a class="btn btn-default btn-autodraw-f5" role="button" href="#" data-autodraw-id="<?=$_autodraw['id']?>" data-loading-text="Загрузка..." title="Проверить"><i class="glyphicon glyphicon-repeat" aria-hidden="true"></i> Проверить</a>
                                          <?php endif; ?>
                                        </div>
                                      </div>
                                  </div>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php else : ?>
                            <tr>
                              <td>
                                <div class="row">
                                    <div class="col-md-12"><center>Журнал автоотрисовок пуст!</center></div>
                                </div>
                              </td>
                            </tr>
                            <?php endif; ?>
                          </tbody>
                        </table>    
                        <div class="loader">
                            <div class="loader-text">
                                <div class="preloader-css">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="row" id="autodraw_pagination">
                    <div class="col-xs-12 col-md-6">
                        <?php if(ceil($autodrawCnt/10) > 1) : ?>
                        <nav aria-label="Page navigation">
                            <ul class="pagination autodraw-pagination">
                                <li class="disabled"><span style="cursor: default;">Страница </span></li>
                                <li class="active"><a href="#" style="cursor: pointer;" data-page="1">1</a></li>
                                <?php for($a=2; $a<=ceil($clientsCnt/10); $a++) : ?>
                                <li><a href="#" style="cursor: pointer;" data-page="<?=$a?>"><?=$a?></a></li>
                                <?php endfor; ?> 
                            </ul>
                        </nav>
                        <?php endif; ?>
                    </div>
                </div>
              </div>
          </div>
          <div class="clearfix">&nbsp;</div>
          <div id="drawTemplateHtmlCode" style="display: none;">
              <div class="col-md-12" style="padding-left: 0px;">
                  <div class="drawmessage">
                      <dl class="dl-horizontal"></dl>
                  </div>
                  <div class="canvas">
                      <img class="sketcher" src="<?= URL::site('/assets/sketcher.png') ?>" />
                      <div class="drawstatus">
                          <a class="btn btn-default btn-autodraw-re" role="button" href="#" title="Отрисовать повторно" data-loading-text="Загрузка..."><i class="glyphicon glyphicon-repeat" aria-hidden="true"></i> Отрисовать повторно</a>
                      </div>
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="well well-lg"><div class="row"></div></div>
              </div>
          </div>
          <div id="drawTemplateFormSimple" style="display: none;">
              <form class="form-draw" method="POST" autocomplete="off">  
                  <table class="table">
                      <thead>
                          <tr>
                              <td>                              
                                  <fieldset class="col-md-12">    	
                                      <legend>Основная информация</legend>

                                      <div class="panel panel-default panel-fieldset">
                                          <div class="panel-body">
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Кого отрисовываем? *</label>
                                                  <input type="text" name="who" class="form-control" placeholder="Имя человека" required="required">
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Как отрисовываем? *</label>                                  
                                                  <select name="mode" class="form-control form-input-mode">
                                                      <option value="default">Полная отрисовка шаблона</option>
                                                      <option value="special">Выборочная отрисовка шаблона</option>
                                                  </select>
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем виды внимания?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем сферы жизнедеятельности?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                  </fieldset>                              
                                  <fieldset class="col-md-12 theme-specials" style="position: absolute; left: -100000px; top: -100000px;">    	
                                      <legend>Темы, включенные в шаблон</legend>
                                      <div class="panel panel-default panel-fieldset panel-checkbox">
                                          <div class="panel-body">
                                              <div class="col-xs-12 form-group form-checkbox theme-specials-items">
                                                  <input type="hidden" name="what" value="">
                                              </div>
                                          </div>
                                      </div>
                                  </fieldset>
                              </td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="clearfix">&nbsp;</div>
                                  <button type="button" class="btn btn-primary btn-submit" data-action="draw">Отрисовать</button>
                                  <button type="button" class="btn btn-success btn-submit" data-action="save">Сохранить</button>
                                  <button type="submit" style="display: none;" data-target="_parent"></button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </form>
          </div>
          <div id="drawTemplateForm0100" style="display: none;">
              <form class="form-draw" method="POST" autocomplete="off">  
                  <table class="table">
                      <thead>
                          <tr>
                              <td>                              
                                  <fieldset class="col-md-12">    	
                                      <legend>Основная информация</legend>

                                      <div class="panel panel-default panel-fieldset">
                                          <div class="panel-body">
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Кого отрисовываем? *</label>
                                                  <input type="text" name="who" class="form-control" placeholder="Имя человека" required="required">
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group">
                                                  <label>Что отрисовываем? *</label>
                                                  <input type="text" name="title" class="form-control" placeholder="Тема для отрисовки" required="required">
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем виды внимания?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isbodys" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                                  <label>Отрисовываем сферы жизнедеятельности?</label>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="1" checked="checked">
                                                          Да
                                                      </label>
                                                  </div>
                                                  <div class="radio">
                                                      <label>
                                                          <input type="radio" name="isspheres" value="0">
                                                          Нет
                                                      </label>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </fieldset>                              
                                  <fieldset class="theme-specials" style="position: absolute; left: -100000px; top: -100000px;">
                                      <input type="text" name="what[]" style="width: 1px;" value="Зачатие">
                                      <input type="text" name="what[]" style="width: 1px;" value="1-9 месяцы беременности">
                                      <input type="text" name="what[]" style="width: 1px;" value="Роды">
                                      <input type="text" name="what[]" style="width: 1px;" value="1 год жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="2-5 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="6-10 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="11-15 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="16-20 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="21-25 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="26-30 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="31-35 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="36-40 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="41-45 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="46-50 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="51-55 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="56-60 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="61-65 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="66-70 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="71-75 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="76-80 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="81-85 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="86-90 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="91-95 годы жизни">
                                      <input type="text" name="what[]" style="width: 1px;" value="96-100 годы жизни">
                                  </fieldset>
                              </td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="clearfix">&nbsp;</div>
                                  <button type="button" class="btn btn-primary btn-submit" data-action="draw">Отрисовать</button>
                                  <button type="button" class="btn btn-success btn-submit" data-action="save">Сохранить</button>
                                  <button type="submit" style="display: none;" data-target="_parent"></button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </form>
          </div>
          <div id="drawModuleForm" style="display: none;">
              <form class="form-draw" method="POST" autocomplete="off">  
                  <table class="table">
                      <thead>
                          <tr>
                              <td>                              
                                  <fieldset class="col-md-12">    	
                                      <legend>Основная информация</legend>
                                      <div class="panel panel-default panel-fieldset">
                                          <div class="panel-body"></div>
                                      </div>
                                  </fieldset>
                              </td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <div class="clearfix">&nbsp;</div>
                                  <button type="submit" class="btn btn-success btn-submit" data-target="_parent">Сохранить</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </form>
          </div>
          <div id="tmpHtmlCode"></div>
          <form id="drawFormHtmlCode" class="form-draw" style="display: none;" autocomplete="off"></form>
          <form id="browserInputTypeSupportTest" style="display: none;" autocomplete="off">
              <input name="date" type="date" value="<?=date('Y-m-d')?>">
          </form>
          <!-- Modal -->
          <div class="modal fade" id="clientsSchedule" tabindex="-1" role="dialog" aria-labelledby="clientsScheduleLabel">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Расписание</h4>
                </div>
                <div class="modal-body">
                  <form>
                      <div class="form-group">
                        <select name="period" class="bootbox-input bootbox-input-select form-control">
                          <option value="null">Не задано</option>
                          <!--<option value="hourly">Каждый час</option>-->
                          <option value="daily">Каждый день</option>
                          <option value="weekly">Каждую неделю</option>
                        </select>
                      </div>
                      <div class="form-group form-group-duration">
                          <div class="input-group">
                              <input class="form-control" name="durationStart" required="required" type="date" min="<?=date('Y-m-d')?>" value="<?=date('Y-m-d')?>" data-default-value="<?=date('Y-m-d')?>">
                              <span class="input-group-addon" id="basic-addon1">-</span>
                              <input class="form-control" name="durationFinish" required="required" type="date" min="<?=date('Y-m-d')?>" value="<?=date('Y-m-d', strtotime('+1 month'))?>" data-default-value="<?=date('Y-m-d', strtotime('+1 month'))?>">
                          </div>
                      </div>
                      <div class="form-group form-group-checkbox">
                        <div class="checkbox">
                          <label>
                              <input name="isDuration" type="checkbox" disabled="disabled"> В течение периода
                          </label>
                        </div>
                      </div>
                      <div class="form-group form-group-checkbox">
                        <div class="checkbox">
                          <label>
                              <input name="isMinVal" type="checkbox" disabled="disabled"> До минимальной нагрузки
                          </label>
                        </div>
                      </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-primary" data-callback="ok">OK</button>
                </div>
              </div>
            </div>
          </div>
