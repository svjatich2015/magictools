
      <form method="POST" class="form-signin">
        <h2 class="form-signin-heading">Забыли пароль?</h2>
        <?php if (strtoupper(Request::current()->method()) == 'POST' && ! is_null($error)) : ?>        
        <?php $form_errors = (array) Kohana::$config->load('form_errors'); ?>
        <div class="alert alert-danger">
            <ul>
                <li><?=$form_errors[$error]?></li>
            </ul>
        </div>
        <?php endif; ?> 
        <label for="inputEmail" class="sr-only">Email</label>
        <?=Form::input('email', Arr::get($_POST, 'email', ''), array('type'=>'email', 'id'=>'inputEmail', 
            'class'=>'form-control', 'placeholder'=>'Email', 'required'=>'required', 'autofocus'=>'autofocus'))?> 
        <button class="btn btn-lg btn-primary btn-block" type="submit">Получить новый пароль</button>
        <div class="row">
            <div class="col-md-12"><br /></div>
            <div class="col-md-6"><a href="<?=URL::site('/login/recovery/');?>">Забыли пароль?</a></div>
            <div class="col-md-6 text-right"><a href="<?=URL::site('/registration/');?>">Еще нет аккаунта?</a></div>
        </div>
      </form>
