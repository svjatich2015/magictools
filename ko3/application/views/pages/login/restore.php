<div class="row">
  <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
      <div class="alert alert-success" role="alert">
          <center>
              <strong>Запрос получен!</strong><br><br>Скоро на электронную почту придет письмо с дальнейшими инструкциями.
          </center>
      </div>
  </div>
</div>