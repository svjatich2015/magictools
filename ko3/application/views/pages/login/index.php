
      <form method="POST" class="form-signin">
        <?php if(Arr::keys_exists(array('email', 'password'), $_POST)) : ?>
        <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span>
                Неверный email или пароль. </span>
        </div>
        <?php endif; ?>
        <label for="inputEmail" class="sr-only">Email</label>
        <?=Form::input('email', Arr::get($_POST, 'email', ''), array('type'=>'email', 'id'=>'inputEmail', 
            'class'=>'form-control', 'placeholder'=>'Email', 'required'=>'required', 'autofocus'=>'autofocus'))?> 
        <label for="inputPassword" class="sr-only">Пароль</label>
        <?=Form::input('password', Arr::get($_POST, 'password', ''), array('type'=>'password', 'id'=>'inputPassword', 
            'class'=>'form-control', 'placeholder'=>'Пароль', 'required'=>'required'))?>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
        <div class="row">
            <div class="col-md-12"><br /></div>
            <div class="col-md-6"><a href="<?=URL::site('/login/recovery/');?>">Забыли пароль?</a></div>
            <div class="col-md-6 text-right"><a href="<?=URL::site('/registration/');?>">Еще нет аккаунта?</a></div>
        </div>
      </form>
