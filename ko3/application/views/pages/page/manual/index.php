
          <div class="page-header"><h1>С чего начать?</h1></div>

          <div class="row">
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                      <a href="<?=URL::site('pages/manual/1')?>">
                          <img src="<?=URL::site('assets/screenshots/video1.png')?>" alt="Основы работы с рисовалкой">
                      </a>
                      <div class="caption">
                          <h5>Основы работы с рисовалкой</h5>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                      <a href="<?=URL::site('pages/manual/2')?>">
                          <img src="<?=URL::site('assets/screenshots/video2.png')?>" alt="Отрисовка клиентов">
                      </a>
                      <div class="caption">
                          <h5>Отрисовка клиентов</h5>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                      <a href="<?=URL::site('pages/manual/3')?>">
                          <img src="<?=URL::site('assets/screenshots/video3.png')?>" alt="Отрисовка шаблонов">
                      </a>
                      <div class="caption">
                          <h5>Отрисовка шаблонов</h5>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                      <a href="<?=URL::site('pages/manual/4')?>">
                          <img src="<?=URL::site('assets/screenshots/video4.png')?>" alt="Основы работы с авторисовалкой">
                      </a>
                      <div class="caption">
                          <h5>Основы работы с авторисовалкой</h5>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                      <a href="<?=URL::site('pages/manual/5')?>">
                          <img src="<?=URL::site('assets/screenshots/video5.png')?>" alt="Основы работы с &quot;СуперЧисткой&quot;">
                      </a>
                      <div class="caption">
                          <h5>Основы работы с "СуперЧисткой"</h5>
                      </div>
                  </div>
              </div>
          </div>
