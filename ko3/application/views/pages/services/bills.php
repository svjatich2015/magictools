        <div class="page-header"><h1>Инструменты</h1></div>
        <div id="servicesForm">
            <div class="content-preloader table-preloader">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="padding-bottom: 20px;">
                                <?=View::factory('templates/sub/services_menu')?> 
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 20px; padding-left: 20px;"> 
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th style="width: 230px; text-align: center;">Создан</th>
                                            <th style="width: 100px; text-align: center;">Сумма</th>
                                            <th style="width: 100px; text-align: center;">Статус</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($bills as $bill) : ?>
                                        <tr>
                                            <td><?=$bill->id;?></td>
                                            <td style="text-align: center;"><?=($bill->created) ? date('d.m.Y H:i:s', $bill->created) : '-';?></td>
                                            <td style="text-align: center;"><?=$bill->amount;?></td>
                                            <td style="text-align: center;">
                                                <?php if($bill->status < Model_Bill::STATUS_ACCEPTED) : ?>
                                                <a href="<?=URL::site('/bill/' . $bill->id)?>" style="text-decoration: underline;">Оплатить</a>
                                                <?php else : ?>
                                                <span class="glyphicon glyphicon-ok text-success"></span></span>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="loader">
                    <div class="loader-text">
                        <div class="preloader-css">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
