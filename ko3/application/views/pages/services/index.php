        <div class="page-header"><h1>Инструменты</h1></div>
        <div id="servicesForm">
            <div class="content-preloader table-preloader">
                <table class="table">
                    <thead>
                        <tr>
                            <td style="padding-bottom: 20px;">
                                <?=View::factory('templates/sub/services_menu')?> 
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 20px; padding-left: 20px;"> 
                                <?php foreach($tools['list'] as $code => $tool) : ?>
                                <fieldset class="fieldset-services col-md-12" data-service-id="<?=$tool['id']?>" data-mode="<?=$tool['mode']?>">    	
                                    <legend>
                                        <input type="checkbox" name="groupServices[]"  
                                               value="<?=$tool['id']?>"<?=($tool['mode'] == 0) ? ' disabled' : ''?>>
                                    </legend>
                                    <div class="panel panel-default panel-fieldset panel-services">
                                        <div class="panel-body">
                                            <table class="table table-condensed table-services" id="service<?=ucfirst($code)?>">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">
                                                            <h4><?=$tool['title']?></h4>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="service-tr-status">
                                                        <td>
                                                            <div class="row">
                                                              <div class="col-sm-12 col-md-3">
                                                                  Состояние
                                                              </div>
                                                              <div class="col-sm-12 col-md-9 service-status">
                                                                  <?php if($tool['mode'] == 0) : ?>
                                                                  <span>Не подключено</span> (<a href="#" data-action="service_activate" data-service-id="<?=$tool['id']?>"><nobr>Активировать пробный</nobr> период</a>)
                                                                  <?php elseif($tool['mode'] == 1) : ?>
                                                                  <span class="text-success">Активно</span>  
                                                                  <?php else : ?>
                                                                  <span class="text-warning">Приостановлено</span>
                                                                  <?php endif; ?>
                                                              </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php if($tool['mode'] > 0) : ?>
                                                    <tr class="service-tr-expiried">
                                                        <td>
                                                            <?php if(time() < $tool['freeUntil']) : ?>
                                                            <div class="row">
                                                              <div class="col-sm-12 col-md-3">
                                                                  Бесплатный период
                                                              </div>
                                                              <div class="col-sm-12 col-md-9">
                                                                <nobr>
                                                                  до
                                                                  <?=HTML::tag('span', 
                                                                          Date::genetiveRusDateByUT(
                                                                                  $tool['freeUntil']))?>
                                                                </nobr>                                                                
                                                              </div>
                                                            </div>
                                                            <?php else: ?>
                                                            <div class="row">
                                                              <div class="col-sm-12 col-md-3">
                                                                  <?=($tool['expiried'] > time()) ? 'Истекает' : 'Истекло'?>
                                                              </div>
                                                              <div class="col-sm-12 col-md-9">
                                                                  <?=HTML::tag('span', 
                                                                          Date::genetiveRusDateByUT($tool['expiried']),
                                                                          [
                                                                            'style' => 'color: '. (time() < $tool['expiried'] ?
                                                                              'inherit' : 'red'),
                                                                            'class' => 'expiried-text-date'
                                                                          ])?>                                                                
                                                              </div>
                                                            </div>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>
                                                    <?php if($code == 'superclean') : ?>
                                                    <tr class="service-tr-slots">
                                                        <td>
                                                            <div class="row">
                                                              <div class="col-sm-12 col-md-3">
                                                                  Количество слотов
                                                              </div>
                                                              <div class="col-sm-12 col-md-9">
                                                                  <select name="slots" class="form-control slots"<?=($tool['mode']) ? '' : ' disabled="disabled"'?>>
                                                                      <?php foreach($tool['slots'] as $slot) : ?>
                                                                      <?php if(isset($slot['active'])) : ?>
                                                                      <option value="<?=$slot['num']?>" selected="selected"><?=$slot['num']?></option>
                                                                      <?php else : ?>
                                                                      <option value="<?=$slot['num']?>"><?=$slot['num']?></option>
                                                                      <?php endif; ?>
                                                                      <?php endforeach; ?>
                                                                  </select>
                                                              </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>
                                                    <tr>
                                                        <td>
                                                            <div class="row">
                                                              <div class="col-sm-12 col-md-3">
                                                                  Описание
                                                              </div>
                                                              <div class="col-sm-12 col-md-9">
                                                                <?=$tool['description']?>
                                                              </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="row">
                                                              <div class="col-sm-12 col-md-3">
                                                                  Цена в месяц
                                                              </div>
                                                              <div class="col-sm-12 col-md-9">
                                                                  <strong class="month-cost"><?=$tool['cost']?></strong> руб.
                                                              </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="row">
                                                              <div class="col-sm-12 col-md-3">
                                                                  Автопродление
                                                              </div>
                                                              <div class="col-sm-12 col-md-9">
                                                                    <div class="material-switch">
                                                                        <input class="service-autopay-switch" 
                                                                               id="service<?=$tool['id']?>AutopaySwitcher" 
                                                                               type="checkbox" 
                                                                               data-service-id="<?=$tool['id']?>"
                                                                               <?=($tool['autopay'] == Model_AccountsTool::AUTOPAY_ON)?' checked="checked"':''?>/>
                                                                        <label for="service<?=$tool['id']?>AutopaySwitcher" class="label-success"></label>
                                                                    </div>
                                                              </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer">
                                            <form class="form-inline form-service-prolong" data-service-id="<?=$tool['id']?>">
                                                <select class="form-control"<?=($tool['mode'] == 0) ? ' disabled' : ''?>>
                                                    <option value="1" selected="selected">Продлить на 1 мес.</option>
                                                    <option value="3">Продлить на 3 мес.</option>
                                                    <option value="6">Продлить на 6 мес.</option>
                                                    <option value="12">Продлить на 12 мес.</option>
                                                </select>
                                                <button type="submit" class="btn btn-default"<?=($tool['mode'] == 0) ? ' disabled' : ''?>>Применить</button>
                                            </form>
                                        </div>
                                    </div>
                                </fieldset>                             
                                <?php endforeach;?>                     
                                <fieldset class="fieldset-services col-md-12"> 
                                    <div class="row">
                                        <div class="col-sm-5 col-md-4 col-lg-3">
                                          <label>
                                            <input type="checkbox" id="servicesSelectCheckbox" value="1">
                                            Выбрать все
                                          </label>
                                        </div>
                                        <div class="col-sm-7 col-md-8 col-lg-9">

                                        </div>
                                    </div>
                                </fieldset> 
                                <p class="text-danger" style="display: none"><strong>Внимание!</strong> В случае отключения услуги, ее предоставление продолжается до 23:59 МСК текущего дня.</p>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <form class="form-inline form-group-prolong">
                                    <label>С выбранными:</label>
                                    <select class="form-control">
                                        <option value="1" selected="selected">Продлить на 1 мес.</option>
                                        <option value="3">Продлить на 3 мес.</option>
                                        <option value="6">Продлить на 6 мес.</option>
                                        <option value="12">Продлить на 12 мес.</option>
                                    </select>
                                    <button type="submit" class="btn btn-default">Применить</button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="loader">
                    <div class="loader-text">
                        <div class="preloader-css">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
