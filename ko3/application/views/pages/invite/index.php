
      <?php $request = Arr::extract(array_merge($_GET, $_POST), array('name', 'surname', 'email', 'text'), ''); ?>
      <form class="form-signin form-invite" method="POST">
        <h2 class="form-signin-heading">Запрос приглашения</h2>
        <?php if (strtoupper(Request::current()->method()) == 'POST' && count($errors) > 0) : ?>        
        <?php $form_errors = (array) Kohana::$config->load('form_errors'); ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors as $e) : ?>
                <li><?=$form_errors[$e]?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?> 
        <label for="inputName" class="sr-only">Имя</label>
        <?=Form::input('name', $request['name'], array('type'=>'text', 'id'=>'inputName', 
            'class'=>'form-control', 'placeholder'=>'Имя', 'required'=>'required', 'pattern'=>'^[A-Za-zА-Яа-яЁё]+$',
            'autofocus'=>'autofocus', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 
            'title'=>'Только буквы русского и латинского алфавита'))?> 
        <label for="inputSurname" class="sr-only">Фамилия</label>
        <?=Form::input('surname', $request['surname'], array('type'=>'text', 'id'=>'inputSurname', 
            'class'=>'form-control', 'placeholder'=>'Фамилия', 'required'=>'required' , 'pattern'=>'^[A-Za-zА-Яа-яЁё]+$', 
            'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Только буквы русского и латинского алфавита'))?> 
        <label for="inputEmail" class="sr-only">Email</label>
        <?=Form::input('email', $request['email'], array('type'=>'email', 'id'=>'inputEmail', 
            'class'=>'form-control', 'placeholder'=>'Email', 'required'=>'required'))?> 
        <label for="inputText" class="sr-only">Откуда узнали о нас?</label>
        <?=Form::textarea('text', $request['text'], array('id'=>'inputText', 'class'=>'form-control', 'required'=>'required', 
            'placeholder'=>'Расскажите, как вы узнали о нашем сервисе?'))?> 
        <input class="btn btn-lg btn-primary btn-block" id="buttonSubmit" type="submit" value="Отправить запрос">
        <div class="row">
            <div class="col-md-12"><br /></div>
            <div class="col-md-6"><a href="<?=URL::site('/registration/');?>">Уже есть инвайт?</a></div>
            <div class="col-md-6 text-right"><a href="<?=URL::site('/login/');?>">Уже зарегистрированы?</a></div>
        </div>
      </form>
