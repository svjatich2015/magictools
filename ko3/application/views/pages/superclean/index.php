
          <div class="page-header"><h1>СуперЧистка</h1></div>
          
          <div id="superCleanContainer">
              <div class="row">
                  <?php foreach ($slots as $slotNum => $slot) : ?>
                  <div class="col-sm-6 col-md-4">                      
                      <form class="superclean-uploader-form" method="POST" enctype="multipart/form-data" onsubmit="return false;">
                          <input type="hidden" name="MAX_FILE_SIZE" value="5000000">
                          <input type="file" id="superCleanSlot<?= $slotNum ?>Uploader" class="superclean-uploader" data-slot-id="<?= $slotNum ?>" accept="image/*,image/jpeg,image/pjpeg,image/webp,image/png">
                      </form>
                      <div class="thumbnail thumbnail-<?=$slot['status']?>" id="superCleanSlot<?=$slotNum?>">
                          <img src="<?=$slot['img']?><?=($slot['status'] == 'active') ? '?' . $slot['processed'] : ''?>" class="img-responsive">
                          <div class="caption">
                              <h4>Слот #<?=$slotNum?></h4>
                              <table class="table table-status">
                                  <tbody>
                                      <tr>
                                          <td>Обновлено:</td>
                                          <?php if ($slot['status'] == 'active') : ?>
                                                  <td><span class="processed-str"><?=date('d.m.Y H:i', $slot['processed']) ?></span></td>
                                          <?php else : ?>
                                                  <td><span class="processed-str">-</span></td>
                                          <?php endif; ?>
                                      </tr>
                                      <tr>
                                          <td>Искажение:</td>
                                          <?php if ($slot['status'] == 'active') : ?>
                                                  <td><span class="incorrect-percent"><?=$slot['incorrect'] ?>%</span></td>
                                          <?php else : ?>
                                                  <td><span class="incorrect-percent">-</span></td>
                                          <?php endif; ?>
                                      </tr>
                                  </tbody>
                              </table>
                              <div class="buttons">
                                  <button class="btn btn-primary btn-upload btn-upload-slot<?=$slotNum?>" data-slot-id="<?=$slotNum?>" role="button">Загрузить</button>
                                  <button class="btn btn-primary btn-update btn-update-slot<?=$slotNum?>" data-slot-id="<?=$slotNum?>" role="button">Обновить</button>
                                  <button class="btn btn-primary btn-test btn-test-slot<?=$slotNum?>" data-slot-id="<?=$slotNum?>" role="button">Проверить</button>
                                  <button class="btn btn-danger btn-delete btn-delete-slot<?=$slotNum?>" data-slot-id="<?=$slotNum?>" role="button">Удалить</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <?php endforeach; ?>
              </div>
          </div>
