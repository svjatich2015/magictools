
          <div class="page-header"><h1>Новая категория</h1></div>
          <form id="draw" method="POST"> 
              <table class="table">
                  <thead>
                      <tr>
                          <td>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputTitle">Название категории *</label>
                                  <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Отображается всюду" required="required" maxlength="128" value="">
                              </div>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary">Сохранить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
