
        <div class="page-header"><h1>Записи</h1></div> 
        <?php foreach ($posts as $post) : ?>
        <div class="panel panel-default panel-blog">
            <div class="panel-heading"><?=$post->title?></div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <strong>Создано:</strong> <?= date('d.m.Y', $post->timestamp) ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <strong>Категория:</strong> <?=$post->cat->title?>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <strong>Комментариев:</strong> <?= $post->comments->count_all() ?>
                </div>
                <div class="col-xs-12 cp-buttons">
                    <select class="form-control post-status-edit" data-post-id="<?=$post->id?>">
                        <option<?=($post->status == Model_BlogPost::STATUS_ACTIVE) ? ' selected="selected"' : ''?> value="1">Опубликовано</option>
                        <option<?=($post->status == Model_BlogPost::STATUS_DRAFT) ? ' selected="selected"' : ''?> value="0">Черновик</option>
                    </select>
                    <a class="btn btn-default account-edit" href="<?=URL::site('admin/blog/edit/' . $post->id)?>" title="Изменить">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        <span class="btn-text">Изменить</span>
                    </a>
                    <a href="#" class="btn btn-danger post-delete" data-post-id="<?=$post->id?>" data-loading-text="Удалить" title="Удалить">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        <span class="btn-text">Удалить</span>
                    </a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?php if(ceil($postsCnt/$limit) > 1) : ?>
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Страница </span></li>
                        <li<?=($pageNum == 1) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/blog') . 
                                URL::query(array('page' => NULL, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;">1</a>
                        </li>
                        <?php for($a=2; $a<=ceil($postsCnt/$limit); $a++) : ?>
                        <li<?=($pageNum == $a) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/blog') . 
                                URL::query(array('page' => $a, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;"><?=$a?></a>
                        </li>
                        <?php endfor; ?> 
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?php if($postsCnt > 10) : ?>
                <nav class="xs-left md-right" aria-label="Sort">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Отображать по </span></li>
                        <?php foreach(array(10, 20, 50, 100) as $_limit) :?>
                        <li<?=($limit == $_limit) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/blog') . URL::query(array('page' => NULL, 'rows' => $_limit))?>"><?=strval($_limit)?><?=($limit == $_limit) ? ' <span class="sr-only">(current)</span>':''?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>

