<?php 
if(! isset($post)) {
        $pageHeader = 'Создать запись';
        $title = $postId = $catId = $html = NULL;
} else {
        $pageHeader = 'Изменить запись';
        $title = $post->title;
        $postId = $post->id;
        $catId = $post->blogCatId;
        $html = $post->content;
}
?>
          <div class="page-header"><h1><?=$pageHeader?></h1></div>
          <form id="draw" method="POST"> 
              <input type="hidden" name="id" value="<?=$postId?>" />
              <input type="hidden" name="tags" value="0" />
              <table class="table">
                  <thead>
                      <tr>
                          <td>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputTitle">Название поста *</label>
                                  <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Отображается всюду" required="required" maxlength="128" value="<?=$title?>">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputCategory">Категория</label>
                                  <select name="cat" class="form-control" id="inputCategory">
                                  <?php foreach($cats as $cat) : ?> 
                                      <option<?=($cat->id == $catId) ? ' selected="selected"' : '';?> value="<?=$cat->id;?>">
                                        <?=$cat->title;?>
                                      </option>
                                  <?php endforeach; ?> 
                                  </select>
                              </div>
                              <div class="col-xs-12 col-md-12 form-group form-textarea-counter" style="margin-bottom: 16px;">
                                  <label for="inputDescription">HTML-код статьи</label>
                                  <textarea name="html" class="form-control" id="editor" maxlength="128" style="height: 55px;"><?=$html?></textarea>
                              </div>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary">Сохранить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
