
          <div class="page-header"><h1>Категории</h1></div>
          
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Название</th>
                  <th style="text-align: center; width: 100px;">Записей</th>
                  <th style="width: 100px;"></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($cats as $cat) : ?>
                <tr>
                  <td><?=$cat->title?></td>
                  <td style="text-align: center;"><?=$cat->posts->count_all()?></td>
                  <td style="text-align: center;">
                      <nobr>
                          <a href="<?=URL::site('clients/edit/'.$cat->id)?>" title="Редактировать"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                          &nbsp;&nbsp;
                          <a href="#" class="client-delete" data-client-id="<?=$cat->id?>" title="Удалить"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                      </nobr>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="row">
              <div class="col-xs-12 col-md-6">
                  <?php if(ceil($catsCnt/$limit) > 1) : ?>
                  <nav aria-label="Page navigation">
                      <ul class="pagination">
                          <li class="disabled"><span style="cursor: default;">Страница </span></li>
                          <li<?=($pageNum == 1) ? ' class="active"':''?>>
                              <a href="<?=URL::site('admin/blog/cats') . 
                                  URL::query(array('page' => NULL, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;">1</a>
                          </li>
                          <?php for($a=2; $a<=ceil($catsCnt/$limit); $a++) : ?>
                          <li<?=($pageNum == $a) ? ' class="active"':''?>>
                              <a href="<?=URL::site('admin/blog/cats') . 
                                  URL::query(array('page' => $a, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;"><?=$a?></a>
                          </li>
                          <?php endfor; ?> 
                      </ul>
                  </nav>
                  <?php endif; ?>
              </div>
              <div class="col-xs-12 col-md-6">
                  <?php if($catsCnt > 10) : ?>
                  <nav class="xs-left md-right" aria-label="Sort">
                      <ul class="pagination">
                          <li class="disabled"><span style="cursor: default;">Отображать по </span></li>
                          <?php foreach(array(10, 20, 50, 100) as $_limit) :?>
                          <li<?=($limit == $_limit) ? ' class="active"':''?>>
                              <a href="<?=URL::site('admin/blog/cats') . URL::query(array('page' => NULL, 'rows' => $_limit))?>"><?=strval($_limit)?><?=($limit == $_limit) ? ' <span class="sr-only">(current)</span>':''?></a>
                          </li>
                          <?php endforeach; ?>
                      </ul>
                  </nav>
                  <?php endif; ?>
              </div>
          </div>
          <div class="clearfix">&nbsp;</div>
