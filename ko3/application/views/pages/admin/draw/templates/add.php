
          <div class="page-header"><h1>Добавление шаблона рисовалки</h1></div>
          <form id="draw" method="POST"> 
              <table class="table">
                  <thead>
                      <tr>
                          <td>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputTitle">Название шаблона *</label>
                                  <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Отображается на странице отрисовки" required="required" maxlength="32" value="">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputCategory">Категория</label>
                                  <select name="cat" class="form-control" id="inputCategory">
                                  <?php foreach($cats as $cat) : ?> 
                                      <option value="<?=$cat->id;?>"><?=$cat->title;?></option>
                                  <?php endforeach; ?> 
                                  </select>
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-checkbox">
                                  <label for="inputType">Тип шаблона *</label>
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="type" id="inputType" value="1" checked="checked">
                                          Простой шаблон
                                      </label>
                                  </div>
                                  
                                  <div class="radio">
                                      <label>
                                          <input type="radio" name="type" id="inputType" value="3">
                                          Шаблон "от зачатия до 100 лет"
                                      </label>
                                  </div>
                              </div>
                              <?php /*<div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputType">Тип шаблона *</label>
                                  <select name="type" class="form-control" id="inputType">
                                      <option value="1">Простой шаблон</option>
                                      <option value="2">Шаблон "от зачатия до 100 лет"</option>
                                  </select>
                              </div> */ ?>
                              <div class="col-xs-12 col-md-6 form-group form-textarea-counter" style="margin-bottom: 16px;">
                                  <label for="inputDescription">Описание шабона</label>
                                  <textarea name="description" class="form-control" id="inputDescription" maxlength="128" style="height: 55px;"></textarea>
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-input-what" id="inputWhatFormGroup">
                                  <label for="inputWhat">Что отрисовываем?</label>
                                  <div class="input-group">
                                      <input type="text" name="what[]" class="form-control" id="inputWhat" placeholder="Тема для отрисовки">
                                      <span class="input-group-btn">
                                          <button class="btn btn-success" type="button" id="inputWhatAdd">
                                              <i class="glyphicon glyphicon-plus"></i>
                                          </button>
                                      </span>
                                  </div><!-- /input-group -->
                              </div>
                          </td>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary">Сохранить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
