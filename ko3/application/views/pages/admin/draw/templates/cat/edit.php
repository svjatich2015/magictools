
          <div class="page-header"><h1>Редактирование категории шаблонов</h1></div>
          <form id="draw" method="POST">
              <table class="table">
                  <thead>
                      <tr>
                          <td>
                              <div class="col-xs-12 col-md-6 form-group">
                                  <label for="inputTitle">Название категории *</label>
                                  <input type="text" name="title" class="form-control" id="inputTitle" placeholder="" required="required" maxlength="45" value="<?=$category->title?>">
                              </div>
                              <div class="col-xs-12 col-md-6 form-group form-textarea-counter" style="margin-bottom: 15px;">
                                  <label for="inputDescription">Описание категории *</label>
                                  <textarea name="description" class="form-control" id="inputDescription" required="required" maxlength="500" style="height: 55px;"><?=$category->description?></textarea>
                              </div>
                          </td>
                      </tк>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-primary">Сохранить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
