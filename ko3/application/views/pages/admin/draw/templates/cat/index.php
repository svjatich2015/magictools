<div class="page-header">
    <h1>
        Шаблоны рисовалки
        <small><span class="label label-lblack" style="vertical-align: text-top;"><?=$cat->title?></span></small>
    </h1> 
</div>
          <div class="container">
              <div class="row row-fluid-xs">
                  <?php foreach($tpls as $template) : ?>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <div class="thumbnail draw-templates-thumbnail">
                          <div class="caption">
                              <h4><?=Text::limit_chars($template->title, 32, '...', TRUE);?></h4>
                              <p><?=$template->description?></p>
                              <form action="<?=URL::site('admin/draw/templates/delete/')?>" method="POST" id="tplDelete<?=$template->id?>"><input type="hidden" name="cat" value="<?=$cat->id?>"><input type="hidden" name="tpl" value="<?=$template->id?>"></form>
                              <div class="btn-group btn-group-justified" role="group">
                                  <a href="<?= URL::site('admin/draw/templates/up/' . $cat->id . '/' . $template->id) ?>" class="btn btn-default" role="button">
                                      <span class="glyphicon glyphicon glyphicon-menu-left hidden-xs hidden-sm" aria-hidden="true"></span>
                                      <span class="glyphicon glyphicon glyphicon-menu-up visible-xs visible-sm" aria-hidden="true"></span>
                                  </a>
                                  <?php if ($template->status == Model_DrawTemplate::STATUS_DISABLED) : ?>
                                  <a href="<?= URL::site('admin/draw/templates/enable/' . $cat->id . '/' . $template->id) ?>" class="btn btn-success" role="button">
                                      <span class="glyphicon glyphicon glyphicon-play" aria-hidden="true"></span>
                                  </a>
                                  <?php else : ?>
                                  <a href="<?= URL::site('admin/draw/templates/disable/' . $cat->id . '/' . $template->id) ?>" class="btn btn-warning" role="button">
                                      <span class="glyphicon glyphicon glyphicon-pause" aria-hidden="true"></span>
                                  </a>
                                  <?php endif; ?>                                 
                                  <a href="#" class="btn btn-danger template-delete" role="button" data-template-id="<?= $template->id ?>">
                                      <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
                                  </a>
                                  <a href="<?= URL::site('admin/draw/templates/edit/' . $template->id) ?>" class="btn btn-info" role="button">
                                      <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                  </a>
                                  <a href="<?= URL::site('admin/draw/templates/down/' . $cat->id . '/' . $template->id) ?>" class="btn btn-default" role="button">
                                      <span class="glyphicon glyphicon glyphicon-menu-right hidden-xs hidden-sm" aria-hidden="true"></span>
                                      <span class="glyphicon glyphicon glyphicon-menu-down visible-xs visible-sm" aria-hidden="true"></span>
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
                  <?php endforeach; ?>
              </div><!--/row-->
          </div><!--/container -->
        <div class="list-group">
            <div class="list-group-item">
                <div class="container">
                    <div class="col-xs-12 col-md-6 text-center">
                            <a class="btn btn-primary btn-lg btn-block" href="<?= URL::site('/admin/draw/templates/add') ?>" role="button">Добавить шаблон</a>
                    </div>
                    <div class="clearfix visible-xs visible-sm">&nbsp;</div>
                    <div class="col-xs-12 col-md-6 text-center">
                            <a class="btn btn-default btn-lg btn-block" href="<?= URL::site('/admin/draw/templates_cat/edit/' . $cat->id) ?>" role="button">Редактировать категорию</a>
                    </div>
                </div>
            </div>
        </div>
          <div class="clearfix">&nbsp;</div>
