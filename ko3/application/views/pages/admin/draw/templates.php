
        <div class="page-header"><h1>Шаблоны рисовалки</h1></div>  
        <div class="list-group draw-templates-list-group">
            <?php foreach($cats as $cat) : ?>
            <div class="list-group-item">
                <div class="container">
                    <div class="col-md-8 col-lg-9">
                        <h2 class="list-group-item-heading"> 
                            <a href="<?=URL::site('admin/draw/templates_cat/' . $cat->id)?>" class="no-link"><?=$cat->title;?></a> 
                        </h2>
                        <p class="list-group-item-text"> 
                            <?=$cat->description;?>
                        </p>
                    </div>
                    <div class="col-md-4 col-lg-3 text-center">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><a href="<?=URL::site('admin/draw/templates_cat/' . $cat->id)?>" class="no-link"><?=Date::decl($cat->templates->count_all(), array('<small>шаблон</small>', '<small>шаблона</small>', '<small>шаблонов</small>'))?></a></h2>
                            </div>
                            <div class="col-md-12"> 
                                <?php if($cat->id > 1) : ?>
                                <form action="<?=URL::site('admin/draw/templates_cat/delete/')?>" method="POST" id="templateCatDelete<?=$cat->id?>"><input type="hidden" name="cat" value="<?=$cat->id?>"></form>
                                <?php endif; ?>
                                <div class="btn-group btn-group-justified" role="group">
                                    <a href="<?=URL::site('admin/draw/templates_cat/up/' . $cat->id)?>" class="btn btn-default" role="button">
                                        <span class="glyphicon glyphicon glyphicon-menu-up" aria-hidden="true"></span>
                                    </a>
                                    <?php if($cat->status == Model_DrawTemplatesCategory::STATUS_DISABLED) : ?>
                                    <a href="<?=URL::site('admin/draw/templates_cat/enable/' . $cat->id)?>" class="btn btn-success" role="button">
                                        <span class="glyphicon glyphicon glyphicon-play" aria-hidden="true"></span>
                                    </a>
                                    <?php else : ?>
                                    <a href="<?=URL::site('admin/draw/templates_cat/disable/' . $cat->id)?>" class="btn btn-warning" role="button">
                                        <span class="glyphicon glyphicon glyphicon-pause" aria-hidden="true"></span>
                                    </a>
                                    <?php endif; ?>
                                    
                                    <?php if($cat->id == 1) : ?>
                                    <a href="#" class="btn btn-danger disabled" role="button">
                                        <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                    <?php else : ?>                                    
                                    <a href="#" class="btn btn-danger template-cat-delete" role="button" data-template-cat-id="<?=$cat->id?>">
                                        <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                    <?php endif; ?>
                                    <a href="<?=URL::site('admin/draw/templates_cat/edit/' . $cat->id)?>" class="btn btn-info" role="button">
                                        <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                    <a href="<?=URL::site('admin/draw/templates_cat/down/' . $cat->id)?>" class="btn btn-default" role="button">
                                        <span class="glyphicon glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-md-4 col-lg-3 text-center">
                        <div class="row" style="margin-top: 15px;">
                            <div class="col-md-12">
                                    <a href="#" class="btn btn-default btn-lg btn-block" role="button"> Редактировать </a>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="col-md-12">
                            <?php if($cat->id == 1) : ?>
                                    <a href="#" class="btn btn-warning btn-lg btn-block disabled" role="button"> Отключить </a>
                            <?php elseif($cat->status == Model_DrawTemplatesCategory::STATUS_DISABLED) : ?>
                                    <a href="#" class="btn btn-success btn-lg btn-block" role="button"> Включить </a>
                            <?php else : ?>
                                    <a href="#" class="btn btn-warning btn-lg btn-block" role="button"> Отключить </a>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    -->
                </div>
            </div>
            <?php endforeach; ?>
        </div> 
        <div class="list-group">
            <div class="list-group-item">
                <div class="container">
                    <div class="col-xs-12 col-md-6 text-center">
                            <a class="btn btn-primary btn-lg btn-block" href="<?= URL::site('/admin/draw/templates/add') ?>" role="button">Добавить шаблон</a>
                    </div>
                    <div class="clearfix visible-xs visible-sm">&nbsp;</div>
                    <div class="col-xs-12 col-md-6 text-center">
                            <a class="btn btn-default btn-lg btn-block" href="<?= URL::site('/admin/draw/templates_cat/add') ?>" role="button">Добавить категорию</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
