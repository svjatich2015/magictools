
        <div class="page-header"><h1>Пользователи</h1></div>     
        <div class="panel-group" id="search" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default panel-plus-minus">
                <div class="panel-heading" role="tab" id="headingSearch">
                    <a class="no-link collapsed" role="button" data-toggle="collapse" data-parent="#search" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span class="text-muted">
                                <h4 class="panel-title"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Поиск пользователей</h4>
                            </span>
                        </a>
                </div>
                <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSearch">
                    <div class="panel-body">
                        <form class="form-draw" method="POST">  
                            <table class="table" style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <td>                              
                                            <fieldset class="col-md-12">    	
                                                <legend>Условия поиска</legend>

                                                <div class="panel panel-default panel-fieldset">
                                                    <div class="panel-body">
                                                        <div class="col-xs-12 col-md-6 form-group" style="margin-bottom: 25px;">
                                                            <label>Название поля</label>                              
                                                            <select name="name" class="form-control form-input-mode">
                                                                <option value="id">ID аккаунта</option>
                                                                <option value="email">Email пользователя</option>
                                                                <option value="name">Имя пользователя</option>
                                                                <option value="surname">Фамилия пользователя</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6 form-group" style="margin-bottom: 25px;">
                                                            <label>Значение</label>    
                                                            <input type="text" name="value" class="form-control" placeholder="Поисковый запрос" required="required">
                                                        </div>
                                                        <div class="col-xs-12 col-md-12 form-group" style="margin-bottom: 15px;">
                                                            <button type="submit" class="btn btn-primary btn-submit">Искать</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>   
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="panel panel-default">
          <table class="table table-striped table-admin-users">
            <thead style="display: none;">
              <tr>
                <th>ID</th>
                <th>Email</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th style="width: 100px; text-align: center;">Статус</th>
                <th style="width: 150px; text-align: center;"><nobr>Оплачено до</nobr></th>
                <th style="width: 100px;"></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($accounts as $acc) : ?>
              <?php if ($acc['balance'] !== 0) : ?>
              <?php $acc['balance'] = $acc['balance'] / 100; ?>
              <?php endif; ?>
              <tr id="account<?=$acc['id']?>Data">
                <td>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <strong>Имя:</strong> <?=$acc['profile']['name']?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <strong>Фамилия:</strong> <?=$acc['profile']['surname']?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <strong>ID:</strong> <?=$acc['id']?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <strong>Email:</strong> <?=$acc['email']?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <strong>Баланс:</strong>
                            <span<?= ($acc['balance'] < 1) ? ' style="color: red;"' : '' ?> id="account<?=$acc['id']?>Balance">
                                <nobr><?=$acc['balance'] ?> руб.</nobr>
                            </span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <strong>Статус:</strong>
                            <?php if($acc['status'] == Model_Account::STATUS_BLOCKED) :  ?>
                            <span style="color: red;">Заблокирован</span>
                            <?php else :  ?>
                            <span style="color: green;">Активен</span>
                            <?php endif; ?>
                            <?php if($acc['status'] == Model_Account::STATUS_ADMIN) :  ?>
                            <span class="glyphicon glyphicon-king" style="color: goldenrod;" title="Админ"></span>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-12 cp-buttons">
                            <a href="#" class="btn btn-lgreen account-increase" data-account-id="<?=$acc['id']?>" data-account-email="<?=$acc['email']?>" data-profile-name="<?=$acc['profile']['name']?>" data-profile-surname="<?=$acc['profile']['surname']?>" data-account-balance="<?=number_format($acc['balance'], 2, ',', '')?>" title="Продлить">Пополнить</a>
                            <a class="btn btn-lblue-gray account-pseudologin" data-account-id="<?=$acc['id']?>" href="#" title="Войти">Аккаунт</a>
                            <a class="btn btn-default account-edit" href="#" title="Редактировать">Редактировать</a>                            
                        </div>
                </td>
              </tr>
              <?php /*
              <tr style="display: none;">
                <td><?=$acc['id']?></td>
                <td><?=$acc['email']?></td>
                <td><?=$acc['profile']['name']?></td>
                <td><?=$acc['profile']['surname']?></td>
                <td style="text-align: center;">
                    <?php if($acc['status'] == Model_Account::STATUS_ADMIN) :  ?>
                    <span class="glyphicon glyphicon-king" style="color: gold;"> </span>
                    <?php elseif($acc['status'] == Model_Account::STATUS_BLOCKED) :  ?>
                    <span class="glyphicon glyphicon-ban-circle" style="color: red;"></span>
                    <?php else :  ?>
                    <span class="glyphicon glyphicon-ok" style="color: green;"></span>
                    <?php endif; ?>
                </td>
                <td style="text-align: center;">
                        <a href="#" class="account-increase" data-account-id="<?=$acc['id']?>" data-account-email="<?=$acc['email']?>" data-profile-name="<?=$acc['profile']['name']?>" data-profile-surname="<?=$acc['profile']['surname']?>" data-account-expiried="<?=$acc['expiried']?>" title="Продлить"<?=($acc['expiried'] < time()) ? 'style="color: red;"' : ''?>><?=date('d.m.Y', $acc['expiried'])?></a>
                </td>
                <td style="text-align: center;">
                    <a class="btn btn-default" href="<?=URL::site('admin/users/edit/'.$acc['id'])?>" title="Редактировать">Редактировать</a>
                </td>
              </tr>
              */ ?>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?php if(ceil($accountsCnt/$limit) > 1) : ?>
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Страница </span></li>
                        <li<?=($pageNum == 1) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/users') . 
                                URL::query(array('page' => NULL, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;">1</a>
                        </li>
                        <?php for($a=2; $a<=ceil($accountsCnt/$limit); $a++) : ?>
                        <li<?=($pageNum == $a) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/users') . 
                                URL::query(array('page' => $a, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;"><?=$a?></a>
                        </li>
                        <?php endfor; ?> 
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?php if($accountsCnt > 10) : ?>
                <nav class="xs-left md-right" aria-label="Sort">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Отображать по </span></li>
                        <?php foreach(array(10, 20, 50, 100) as $_limit) :?>
                        <li<?=($limit == $_limit) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/users') . URL::query(array('page' => NULL, 'rows' => $_limit))?>"><?=strval($_limit)?><?=($limit == $_limit) ? ' <span class="sr-only">(current)</span>':''?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>

