
        <div class="page-header"><h1>Платежи</h1></div> 
        <div class="panel panel-default">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>AccountID</th>
                <th>BillID</th>
                <th>Сумма</th>
                <th>Дата</th>
                <th style="width: 100px; text-align: center;">Способ</th>
                <th style="width: 130px; text-align: center;">Статус</th>
                <th style="width: 100px;"></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($payments as $payment) : ?>
              <tr>
                <td><?=$payment['id']?></td>
                <td><?=$payment['accountId']?></td>
                <td><?=$payment['billId']?></td>
                <td><?=$payment['amount']?></td>
                <td><?=date('d.m.Y H:i:s', $payment['datetime'])?></td>
                <td style="text-align: center;">
                    <?php if(in_array($payment['type'], array(Model_Payment::TYPE_YANDEX_CARD, Model_Payment::TYPE_YANDEX_MONEY))) :  ?>
                    Яндекс
                    <?php elseif($payment['type'] == Model_Payment::TYPE_PRODAMUS) :  ?>
                    Продамус
                    <?php else :  ?>
                    Перевод
                    <?php endif; ?>
                </td>
                <td style="text-align: center;">
                    <?php if($payment['status'] == Model_Payment::ACCEPTED) :  ?>
                    Подтвержден
                    <?php elseif($payment['status'] == Model_Payment::NOT_ACCEPTED) :  ?>
                    Не подтвержден
                    <?php else :  ?>
                    Отменен
                    <?php endif; ?>
                </td>
                <td style="text-align: center;">
                    <a href="<?=$payment['acceptLink']?>">Подтвердить</a>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?php if(ceil($paymentsCnt/$limit) > 1) : ?>
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Страница </span></li>
                        <li<?=($pageNum == 1) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/payments') . 
                                URL::query(array('page' => NULL, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;">1</a>
                        </li>
                        <?php for($a=2; $a<=ceil($paymentsCnt/$limit); $a++) : ?>
                        <li<?=($pageNum == $a) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/payments') . 
                                URL::query(array('page' => $a, 'rows' => Arr::get($_POST, 'rows')))?>" style="cursor: pointer;"><?=$a?></a>
                        </li>
                        <?php endfor; ?> 
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?php if($paymentsCnt > 10) : ?>
                <nav class="xs-left md-right" aria-label="Sort">
                    <ul class="pagination">
                        <li class="disabled"><span style="cursor: default;">Отображать по </span></li>
                        <?php foreach(array(10, 20, 50, 100) as $_limit) :?>
                        <li<?=($limit == $_limit) ? ' class="active"':''?>>
                            <a href="<?=URL::site('admin/payments') . URL::query(array('page' => NULL, 'rows' => $_limit))?>"><?=strval($_limit)?><?=($limit == $_limit) ? ' <span class="sr-only">(current)</span>':''?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>

