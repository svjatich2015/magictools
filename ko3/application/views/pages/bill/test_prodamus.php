
          <div class="page-header"><h1>Пополнение баланса аккаунта</h1></div>
          <form action="/pay/prodamus" method="POST" data-account-id="<?=Identity::init()->getAccountInfo()->id?>" id="accountPay">
              <input type="hidden" name="receiver" value="41001556421641">           
              <input type="hidden" name="sum" value="1020.41">
              <input type="hidden" name="formcomment" value="Magic Tools. Пополнение баланса на 1000 руб.">
              <input type="hidden" name="short-dest" value="Magic Tools. Пополнение баланса на 1000 руб.">
              <input type="hidden" name="label" value="MagicTools::<?=Identity::init()->getAccountInfo()->id?>::1000">
              <input type="hidden" name="quickpay-form" value="shop">
              <input type="hidden" name="targets" value="Пополнение баланса аккаунта {Email: <?=Identity::init()->getAccountInfo()->email?>}">  
              <input type="hidden" name="paymentType" value="AC"> 
              
              <table class="table">
                  <thead>
                      <tr>
                          <th>
                              <div class="form-group">
                                  <label for="inputSum">Сумма (рублей)</label>
                                  <input type="number" class="form-control" id="inputSum" placeholder="Сумма" min="1000" style="max-width: 200px;"
                                         name="cash" value="1000" data-type="number">
                                  <span class="help-block">Минимум: 1000 руб.</span>
                              </div>
                              <div class="clearfix">&nbsp;</div>
                              <div class="form-group">
                                  <label for="inputMethod">Способ оплаты</label>
                                  <!--<div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodYandex" value="yasha" checked="checked">
                                          Visa/MasterCard
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodSBRF" value="sbrf">
                                          Сбербанк.Онлайн
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodPayPal" value="paypal">
                                          PayPal (для нерезидентов РФ)
                                      </label>
                                  </div>-->
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusCard" value="AC" checked="checked">
                                          Банковская карта
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusSberOnline" value="sbol">
                                          Сбербанк.Онлайн
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusYandex" value="PC">
                                          ЮMoney (ex. Яндекс.Деньги)
                                      </label>
                                  </div>
                              </div>
                          </th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-success">Оплатить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </form>
