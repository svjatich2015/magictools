<div class="row">
    <div class="col-md-12">
        <div class="page-header"><h1>Оплата вручную Сбербанк.Онлайн</h1></div>
        <div class="alert alert-warning alert-dismissible" role="alert" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Внимание!</h4> 
            <p>С <strong>01.08.2022</strong> года <u>изменились реквизиты</u> получателя денежных средств через Сбербанк.Онлайн. Обратите на это внимание при оплате.</p>
            <div class="clearfix"><br /></div>
            <p><button type="button" class="btn btn-warning" data-dismiss="alert" aria-label="Close">Понятно, спасибо!</a></button>
        </div>
        <div class="alert alert-info alert-dismissible" role="alert" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Важное напоминание!</h4> 
            <p>После перевода денег в системе "Сбербанк.Онлайн" <strong>обязательно</strong> вернитесь <nobr>на данную</nobr> страницу 
            <nobr>и <strong>подтвердите</strong></nobr> <strong>оплату</strong>, следуя пошаговой инструкции ниже.</p>
            <div class="clearfix"><br /></div>
            <p><button type="button" class="btn btn-primary" data-dismiss="alert" aria-label="Close">Понятно, спасибо!</a></button>
        </div>
        <div id="accordion" class="checkout">
            <div class="panel checkout-step">
                <div> <span class="checkout-step-number">1</span>
                    <h4 class="checkout-step-title">Перевод</h4>
                </div>
                <div id="collapseOne" class="collapse in">
                    <div class="checkout-step-body">
                        <p>Переведите вручную <u><?= $cash ?> руб.</u> через систему "Сбербанк.Онлайн" на карту: <nobr><strong>4276 1609 4310 6832</strong></nobr> <nobr>(получатель Святослав Геннадьевич М.).</nobr></p>
                        <p>В комментарии к платежу напишите: <nobr>"ID аккаунта: <?= Identity::init()->getAccountInfo()->id ?>. Пополнение баланса на <?= $cash ?> руб."</nobr>.</p>
                        <div class="clearfix"><br /></div>
                        <p><a class="collapsed btn btn-default" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Далее</a></p>
                    </div>
                </div>
            </div>
            <div class="panel checkout-step">
                <div role="tab" id="headingTwo"> <span class="checkout-step-number">2</span>
                    <h4 class="checkout-step-title">Подтверждение</h4>
                </div>
                <div id="collapseTwo" class="collapse">
                    <div class="checkout-step-body">
                        <p>После оплаты нажмите на кнопку ниже...</p>
                        <div class="clearfix"><br /></div>
                        <p><a href="#" class="btn btn-success pay-accept" data-cash="<?=$cash?>" data-method="sbrf" data-loading-text="Подождите...">Я оплатил</a></p>
                    </div>
                </div>
            </div>
            <div class="panel checkout-step">
                <div role="tab" id="headingThree"> <span class="checkout-step-number">3</span>
                    <h4 class="checkout-step-title">Статус оплаты</h4>
                </div>
                <div id="collapseThree" class="collapse">
                    <div class="checkout-step-body">
                        <p>В течение 48 часов (обычно раньше!) баланс будет пополнен службой поддержки.</p>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>