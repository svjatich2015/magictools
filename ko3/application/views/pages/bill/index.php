
          <div class="page-header"><h1>Счет на оплату #<?=$bill->id?></h1></div>
          <form action="https://yoomoney.ru/quickpay/confirm.xml" method="POST" data-account-id="<?=Identity::init()->getAccountInfo()->id?>" id="accountPay">
            <div class="col-sm-12 col-md-12">
              <?php if($bill->status == Model_Bill::STATUS_ACCEPTED) : ?>
              <div class="alert alert-success" role="alert"><strong>Счет оплачен!</strong></div>
              <?php endif; ?>
              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Описание</th>
                          <th class="text-center">Стоимость</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php foreach($bill->items->find_all() as $i => $item) : ?>
                      <tr>
                          <td class="col-sm-1 col-md-1"><?=$i+1?></td>
                          <td class="col-sm-8 col-md-6">
                              <?php if($item->accountTool->toolId == Model_Tool::AUTODRAW) : ?>
                              Рисовалка и авторисовалка (продление на <?=$item->value?> мес.)
                              <?php elseif($item->accountTool->toolId == Model_Tool::SUPERCLEAN) : ?>
                              Суперчистка
                                <?php if($item->action == Model_AccountsTool::ACTION_PROLONG) : ?>
                                (продление на <?=$item->value?> мес.)
                                <?php elseif($item->action == Model_AccountsTool::ACTION_UPDATE_SLOTS) : ?>
                                (увеличение слотов до: <?=$item->value?>)
                                <?php endif; ?>
                              <?php elseif($item->accountTool->toolId == Model_Tool::ARCHECARDS) : ?>
                              Архетип-карты (продление на <?=$item->value?> мес.)
                              <?php elseif($item->accountTool->toolId == Model_Tool::AUTOCOACH) : ?>
                              Автокоуч (продление на <?=$item->value?> мес.)
                              <?php endif; ?>
                          </td>
                          <td class="col-sm-1 col-md-1 text-center"><strong><?=$item->amount?> руб.</strong></td>
                      </tr>
                      <?php endforeach; ?>
                      <?php if($bill->discount !== NULL) : ?>
                      <tr>
                          <td class="col-sm-1 col-md-1"></td>
                          <td class="col-sm-8 col-md-6">
                              <strong>Скидка</strong>
                          </td>
                          <td class="col-sm-1 col-md-1 text-center"><strong><span class="text-red">-<?=$bill->discount?> руб.</span></strong></td>
                      </tr>
                      <?php endif; ?>
                       <tr>
                          <td colspan="3" class="text-right">
                              <h4><strong>Итог: </strong> <?=$bill->amount?> руб.</h4>
                          </td>
                      </tr>
                  </tbody>
              </table> 
              <?php if($bill->status < Model_Bill::STATUS_ACCEPTED) : ?>
              <input type="hidden" name="cash" value="<?=$bill->amount?>">
              <input type="hidden" name="bill" value="<?=$bill->id?>">
              <input type="hidden" name="receiver" value="41001556421641">          
              <input type="hidden" name="sum" value="<?=$bill->amount?>">
              <input type="hidden" name="formcomment" value="Magic Tools. Оплата счета #<?=$bill->id?>">
              <input type="hidden" name="short-dest" value="Magic Tools. Оплата счета #<?=$bill->id?>">
              <input type="hidden" name="label" value="MagicTools::<?=Identity::init()->getAccountInfo()->id?>::<?=$bill->amount?>::<?=$bill->id?>">
              <input type="hidden" name="quickpay-form" value="shop">
              <input type="hidden" name="targets" value="Оплата счета #<?=$bill->id?> {Email: <?=Identity::init()->getAccountInfo()->email?>}">  
              <input type="hidden" name="paymentType" value="AC"> 
              <table class="table">
                  <thead>
                      <tr>
                          <th>
                              <div class="form-group">
                                  <label for="inputMethod">Способ оплаты</label>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodYandexCard" value="yacard" checked="checked">
                                          Банковская карта российского банка
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusCardEUR" value="ACeuruk">
                                          Банковская карта, <u>выпущенная НЕ в России</u> (EUR)
                                      </label>
                                  </div>
                                  <!--<div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusCardEUR" value="ACeur">
                                          Банковская карта, <u>выпущенная НЕ в России</u> (EUR)
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusCardUSD" value="ACusd">
                                          Банковская карта, <u>выпущенная НЕ в России</u> (USD)
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusCardUSD" value="ACUSDXP">
                                          Банковская карта, <u>выпущенная НЕ в России</u> (USD)
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusCardUSD" value="ACusduk">
                                          Банковская карта, <u>выпущенная НЕ в России</u> (USD)
                                      </label>
                                  </div>-->
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodProdamusCardKZT" value="ACkztjp">
                                          Банковская карта, <u>выпущенная НЕ в России</u> (оплата в тенге)
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodSBRF" value="sbrf">
                                          Сбербанк.Онлайн
                                      </label>
                                  </div>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodYandex" value="yasha">
                                          ЮMoney (ex. Яндекс.Деньги)
                                      </label>
                                  </div>
                                  <!--<div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodPayPal" value="paypal">
                                          PayPal (для нерезидентов РФ)
                                      </label>
                                  </div>-->
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodCryptocloud" value="crypto">
                                          Крипта (Bitcoin, Litecoin, Ethereum, USDT TRC20, Стейблкоины)
                                      </label>
                                  </div>
                                  <?php if(Identity::init()->getAccountBalance() >= $bill->amount) : ?>
                                  <div class="radio">
                                      <label>
                                          <input name="method" type="radio" id="payMethodBalance" value="balance">
                                          <strong>Баланс аккаунта (моментально)</strong>
                                      </label>
                                  </div>
                                  <?php endif; ?>
                              </div>
                          </th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>
                              <div class="clearfix">&nbsp;</div>
                              <button type="submit" class="btn btn-success">Оплатить</button>
                          </td>
                      </tr>
                  </tbody>
              </table>
              <?php endif; ?>
            </div>     
          </form>
