<div class="row">
    <div class="col-md-12">
        <div class="page-header"><h1>Оплата вручную PayPal</h1></div>
        <div class="alert alert-info alert-dismissible" role="alert" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Важное напоминание!</h4> 
            <p>После перевода денег в системе PayPal <strong>обязательно</strong> вернитесь на данную страницу 
            <nobr>и <strong>подтвердите</strong></nobr> <strong>оплату</strong>, следуя пошаговой инструкции ниже.</p>
            <div class="clearfix"><br /></div>
            <p><button type="button" class="btn btn-primary" data-dismiss="alert" aria-label="Close">Понятно, спасибо!</a></button>
        </div>
        <div id="accordion" class="checkout">
            <div class="panel checkout-step">
                <div> <span class="checkout-step-number">1</span>
                    <h4 class="checkout-step-title">Перевод в PayPal</h4>
                </div>
                <div id="collapseOne" class="collapse in">
                    <div class="checkout-step-body">
                        <p>Переведите вручную <?= $cash ?> руб. через систему PayPal. Для перевода <a href="https://paypal.me/svjatich/<?= $cash ?>RUB" target="_blank">нажмите здесь</a>.</p>
                        <p>В комментарии к платежу напишите: <strong><nobr>"ID аккаунта: <?= Identity::init()->getAccountInfo()->id ?>. Пополнение баланса на <?= $cash ?> руб."</nobr></strong>.</p>
                        <div class="clearfix"><br /></div>
                        <p><a class="collapsed btn btn-default" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Далее</a></p>
                    </div>
                </div>
            </div>
            <div class="panel checkout-step">
                <div role="tab" id="headingTwo"> <span class="checkout-step-number">2</span>
                    <h4 class="checkout-step-title">Подтверждение</h4>
                </div>
                <div id="collapseTwo" class="collapse">
                    <div class="checkout-step-body">
                        <p>После оплаты нажмите на кнопку ниже...</p>
                        <div class="clearfix"><br /></div>
                        <p><a href="#" class="btn btn-success pay-accept" data-cash="<?=$cash?>" data-method="paypal" data-loading-text="Подождите...">Я оплатил</a></p>
                    </div>
                </div>
            </div>
            <div class="panel checkout-step">
                <div role="tab" id="headingThree"> <span class="checkout-step-number">3</span>
                    <h4 class="checkout-step-title">Статус оплаты</h4>
                </div>
                <div id="collapseThree" class="collapse">
                    <div class="checkout-step-body">
                        <p>В течение 48 часов (обычно раньше!) баланс будет пополнен службой поддержки.</p>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>