
<div class="page-header"><h1>Архетип-карты</h1></div>
<div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    Короткая инструкция по использованию данного инструмента 
    <a href="https://drive.google.com/open?id=1k4YJU_p02RgOd2zYMpewJFZb6imWPN2YHdVPzsjsvA8"
       class="alert-link" target="_blank" style="text-decoration: underline;">находится здесь</a>.
</div>
<div id="archeCardsContainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="magic-cards">
                        <div class="magic-card magic-card-shirt"></div>
                        <div class="magic-card magic-card-face"></div>
                    </div>
                    <div class="well well-sm magic-cards-well">
                        <a class="btn btn-default magic-cards-btn" role="button" href="#" title="Вытащить карту"><i class="glyphicon glyphicon-repeat" aria-hidden="true"></i> Вытащить карту</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
