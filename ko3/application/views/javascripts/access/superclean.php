
<?php $activeSlotsSelected = FALSE; ?>
<?php $varSlots = ''; ?>
<?php foreach($slots as $slotsData) : ?>
<?php $activeSlotsDefault = isset($activeSlotsDefault) ? $activeSlotsDefault : $slotsData->monthlyCost; ?>
<?php if(! $activeSlotsSelected && $slotsData->num >= $activeSlotsNum) : ?>
<?php $activeSlotsSelected = TRUE; ?>
<?php $activeSlots = $slotsData->num; ?>
<?php endif; ?>
<?php $varSlots .= $slotsData->num . ": '" . $slotsData->monthlyCost . "', ";?>
<?php endforeach; ?> 
jQuery(function($) {
    
    var slots = {<?=$varSlots?>};
    
    var activeSlots = <?=isset($activeSlots) ? $activeSlots : $activeSlotsDefault;?>;
        
    $("#serviceSuperClean select.slots").on("change", function(){
        var $this = $(this);
        
        activeSlots = parseInt($this.val());
        
        $('#serviceSuperClean .month-cost').text(slots[activeSlots]);
    });
});