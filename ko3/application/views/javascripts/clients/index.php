jQuery(function($) {
    $(".client-delete").on("click", function(){
        var clientId = $(this).data("clientId");
    
        bootbox.confirm({
            size: "small",
            title: "Удалить клиента?",
            message: "Это действие необратимо.",
            buttons: {
                confirm: {
                    label: 'Удалить',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#clientDelete' + clientId).submit();
                }
            }
        }); 
    });
});