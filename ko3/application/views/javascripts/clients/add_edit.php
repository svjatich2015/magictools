var inputWhatUid = 0;
    var inputWhatCnt = <?=isset($themes) ? count($themes) : 1?>;
    var inputWhatLmt = <?=Model_DrawClient::THEMES_MAX_CNT?>;
  
    jQuery(function($) {
    $('#formDraw').delegate(".btn-input-what-remove", "click", function(){
        var $inputWhatElm = $('#inputWhatFormGroup' + $(this).data('inputWhatId'));
        var $hasWarning = $inputWhatElm.hasClass('has-warning');
    
        $inputWhatElm.remove();
        
        if(inputWhatCnt > inputWhatLmt && ! $hasWarning) {
           $('#formDraw .form-group-input-what').removeClass('has-warning').filter(function(i){
               return (inputWhatLmt > i + 1) ? false : true;
           }).addClass('has-warning');
        }
        
        if(inputWhatCnt <= inputWhatLmt) {
            $('#inputWhatAdd').removeClass('disabled');
        }
        
        inputWhatCnt--;
    });
  
    $('#inputWhatAdd').on('click', function(){
        if(inputWhatCnt >= inputWhatLmt) {
            createNoty('Максимальное количество тем для отрисовки: ' + inputWhatLmt, 'danger', 5);
            return;
        }
  
        var uid = inputWhatUid + 1;
        var val = $('#inputWhat').val();

        $('#inputWhatFormGroup').before('<div class="col-xs-12 col-md-6 form-group form-group-input-what" id="inputWhatFormGroup' + uid + '"><label for="inputWhat' + uid + '">Что еще отрисовываем?</label><div class="input-group"><input type="text" name="what[]" class="form-control" id="inputWhat' + uid + '" placeholder="Тема для отрисовки" value="' + val + '"><span class="input-group-btn"><button class="btn btn-danger btn-input-what-remove" type="button" data-input-what-id="' + uid + '"><i class="glyphicon glyphicon-minus"></i></button></span></div></div>');

        $('#inputWhat').val(null);

        inputWhatUid++;
        inputWhatCnt++;
        
        if(inputWhatCnt >= inputWhatLmt) {
            $('#inputWhatAdd').addClass('disabled');  
        }
    }); 
    
    $('#formDrawSubmit').on('click', function(){
        var $form = $('#formDraw');
        
        if(inputWhatCnt >= inputWhatLmt) {
            createNoty('Максимальное количество тем для отрисовки: ' + inputWhatLmt, 'danger', 5);
            return;
        }
        
        $form.submit();
    });
});