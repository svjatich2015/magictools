<?php $utime = time(); ?>
jQuery(function($) {

    var tools = {
            <?php foreach($tools['list'] as $tool) : ?>
            <?=$tool['id'];?>: {'mode': <?=$tool['mode'];?>, 'value': <?=($tool['freeUntil'] > $utime) ? 0 : $tool['cost'];?>},
            <?php endforeach; ?> 
    };
    
    var slots = {
            <?php foreach($tools['list']['superclean']['slots'] as $slot) : ?>
            <?php if(isset($slot['active'])) : ?>
            <?php $activeSlots = $slot['num']; ?>
            <?php endif; ?>
            <?=$slot['num'];?>: <?=$slot['cost'];?>,
            <?php endforeach; ?> 
    };
    
    var activeSlots = <?=isset($activeSlots) ? $activeSlots : 0;?>;
    var activeSlotsFree = <?=($tools['list']['superclean']['freeUntil'] > $utime) ? 'true' : 'false';?>;
    
    var preloaderEnable = function() {
        $('#servicesForm .content-preloader').addClass('active');
    }
    
    var preloaderDisable = function() {
        $('#servicesForm .content-preloader').removeClass('active');
    }

    var activateFreeUntil = function(toolId) {
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/activate_free_until') ?>',
            data: 'tool=' + toolId,
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                dfd.resolve({expiriedStr: data.expiriedRusStr});
              } else {
                dfd.reject();
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                dfd.reject();
            }
        });
        
        return dfd.promise();
    }
    
    var switchAutopay = function(toolId, condition) {
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/service_autopay_switch') ?>',
            data: 'tool=' + toolId + '&cond=' + condition,
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                dfd.resolve();
              } else {
                dfd.reject();
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                dfd.reject();
            }
        });
        
        return dfd.promise();
    }
    
    var changeActiveSlots = function(slots) {
        var dfd = new jQuery.Deferred();        
        bootbox.confirm({
            size: "medium",
            title: "Изменение количества слотов",
            message: "Будет пересчитан срок окончания действия услуги. Продолжить?",
            buttons: {
                confirm: {
                    label: 'Пересчитать',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url: '<?= URL::site('/ajax/superclean_recalculate_expiried') ?>',
                        data: 'slotsNum=' + slots,
                        type:     'POST',
                        dataType: 'json',
                        success:  function(data){
                          if(data.status == 'ok') {
                            dfd.resolve({result: 'confirm', expiriedStr: data.expiriedRusStr});
                          } else {
                            dfd.reject({result: 'error'});
                          }
                        },
                        error: function(xhr, ajaxOptions, thrownError){
                            dfd.reject({result: 'error'});
                        }
                    });
                } else {
                    dfd.reject({result: 'cancel'});
                }
            }
        });         
        return dfd.promise();
    }
    
    var changeActiveSlotsFree = function(slots) {
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/superclean_change_active_slots') ?>',
            data: 'slotsNum=' + slots,
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                dfd.resolve({result: 'confirm'});
              } else {
                dfd.reject({result: 'error'});
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                dfd.reject({result: 'error'});
            }
        });
        
        return dfd.promise();
    }

    var createBill = function(period, tools) {
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/bill_create') ?>',
            data: {period: period, tools: tools},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                dfd.resolve({billId: data.billId});
              } else {
                dfd.reject();
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                dfd.reject();
            }
        });
        
        return dfd.promise();
    }

    var prolong = function(period, tools) {
        preloaderEnable();
        $.when( createBill(period, tools) )
            .done(function(response) { 
                preloaderDisable();
                window.location = "<?=URL::site('/bill')?>/" + response.billId;
            })
            .fail(function() { 
                preloaderDisable();
                createNoty('Что-то пошло не так!', 'danger', 5);
            });
    }
    
    $("#serviceSuperclean select.slots").on('change', function(){
        var $this = $(this); 
        var $fieldset = $this.closest('fieldset');
        var slotsVal = parseInt($this.val()); 
        var actionFunc = (activeSlotsFree || parseInt($fieldset.data('mode')) == <?=Model_AccountsTool::MODE_EXPIRIED?>) ? 
            changeActiveSlotsFree : changeActiveSlots;
        if(slotsVal == activeSlots) {
            return;
        }
        $.when( actionFunc(slotsVal) )
            .done(function(response) {
                if(response.result == 'confirm') {
                    activeSlots = slotsVal;
                    $('#serviceSuperclean .month-cost').text(slots[activeSlots]);
                    console.log(response);
                    if(response.expiriedStr !== undefined) {
                        $this
                            .closest('tbody')
                                .find('tr.service-tr-expiried .expiried-text-date')
                                    .text(response.expiriedStr);
                    }
                    createNoty('Изменения сохранены!', 'success', 5);
                }
            })
            .fail(function(response) { 
                $this.val(activeSlots);
                if(response.result == 'error') {
                    createNoty('Что-то пошло не так!', 'danger', 5);
                }
            });
    });
    
    $("#servicesSelectCheckbox").on('change', function(){
        var $this = $(this);
        
        if($this.prop('checked')) {
            $('#servicesForm fieldset>legend>input:not(:disabled)').prop('checked', true);
        } else {
            $('#servicesForm fieldset>legend>input:not(:disabled)').prop('checked', false);
        }
    });
    
    $('#servicesForm fieldset>legend>input').on('change', function(){
        $("#servicesSelectCheckbox").prop('checked', false);
    });
    
    $("#servicesForm a[data-action='service_activate']").on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var serviceId = $this.data('serviceId');
        var $fieldset = $("fieldset[data-service-id='" + serviceId + "']");
        var title = $fieldset.find('.table>thead>tr>th>h4').text();
        
        bootbox.confirm({
            size: "small",
            title: title,
            message: "Активировать пробный период?",
            buttons: {
                confirm: {
                    label: 'Активировать',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $.when( activateFreeUntil(serviceId) )
                        .done(function(response) { 
                            $fieldset.find('.service-status')
                                .html('<span class="text-success">Активно</span>');
                            $fieldset.find('.service-tr-status')
                                .after('<tr><td><div class="row">' +
                                       '<div class="col-sm-12 col-md-3">Бесплатный период</div>' +
                                       '<div class="col-sm-12 col-md-9"><nobr>до '+ response.expiriedStr + '</nobr></div>' +                                                              
                                       '</div></td></tr>');
                            $fieldset.find('legend>input').prop('disabled', false);
                            if(parseInt(serviceId) == <?=Model_Tool::SUPERCLEAN?>) {
                                $fieldset.find('.service-tr-slots select')
                                    .prop('disabled', false);
                            }
                            createNoty('Пробный период активирован!', 'success', 5);
                        })
                        .fail(function() {        
                            createNoty('Что-то пошло не так!', 'danger', 5);
                        });
                }
            }
        }); 
    });
    
    $(".service-autopay-switch").on('change', function(e){
        var $this = $(this);
        var serviceId = $this.data('serviceId');
        console.log([ $this.data('serviceId'), $this.prop('checked') ]);         
        
        bootbox.confirm({
            size: "small",
            title: 'Автопродление',
            message: $this.prop('checked') ? "Включить автопродление?" : "Отключить автопродление?",
            buttons: {
                confirm: {
                    label: 'Подтвердить',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (! result) {
                    var _switch = $this.prop('checked') ? false : true;
                    $this.prop('checked', _switch);
                    return;
                }
                $this.prop('disabled', true);
                var _cond = $this.prop('checked') ? '<?=Model_AccountsTool::AUTOPAY_ON?>' : '<?=Model_AccountsTool::AUTOPAY_OFF?>';
                $.when( switchAutopay(serviceId, _cond) )
                    .done(function() { 
                        $this.prop('disabled', false);
                        var _msg = $this.prop('checked') ? "Автопродление включено" : "Автопродление выключено"
                        createNoty(_msg, 'success', 5);
                    })
                    .fail(function() {
                        $this.prop('disabled', false);
                        var _switch = $this.prop('checked') ? false : true;
                        $this.prop('checked', _switch);
                        createNoty('Что-то пошло не так!', 'danger', 5);
                    });
            }
        }); 
    });
    
    $("#servicesForm .form-group-prolong").on('submit', function(e) {
        e.preventDefault();        
        var $this = $(this);
        var period = $this.children('select').val();
        var tools = [];
        $('#servicesForm fieldset>legend>input:checked').each(function(i, elm) {
            var val = $(elm).val();
            tools.push(val);
        });
        if(tools.length) {
            prolong(period, tools);
        } else {
            createNoty('Выберите хотя бы один инструмент!', 'danger', 5);
        }
    });
    
    $("#servicesForm .form-service-prolong").on('submit', function(e) {
        e.preventDefault();        
        var $this = $(this);
        var period = $this.children('select').val();
        var tools = [$this.data('serviceId')];
        prolong(period, tools);
    });
});