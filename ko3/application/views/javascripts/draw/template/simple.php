jQuery(function($) {
  $('#inputMode').on('change', function(){
    if($(this).val() == 'special') {
        $('#themesSpecials').show();
    } else {
        $('#themesSpecials').hide();
    }
  });
});