jQuery(function($) {
                         
    Array.prototype.shuffle = function(){
        var newArray = [];
        var index = {};
        var lenArray = this.length;

        for (var x = (lenArray - 1); x >= 0; x--){
            var rand = Math.round(Math.random() * (lenArray - 1));
            var key = 'key_' + rand;
            if (key in index){
                x++;
                continue;
            } else {
                index[key] = this[rand];
                newArray.push(this[rand]);
            }
        }

        return newArray;
    };
        
    var draws = [], iframeDrawCntr = 0, iframeFastDrawNum = 1, iframeDrawActive = [];

    var _colors = ['blue', 'orange', 'peru', 'darkred', 'darkgreen', 'darkmagenta', 'crimson', 
                   'darkviolet', 'darkorchid', 'indigo', 'slateblue', 'darkslateblue', 'navy', 
                   'violet', 'royalblue', 'steelblue', 'cadetblue', 'darkolivegreen', 'olive', 
                   'palevioletred', 'darkgoldenrod', 'purple', 'mediumorchid', 'springgreen',
                   'darkkhaki', 'mediumvioletred', 'deeppink', 'goldenrod', 'sienna'];
        
    var _bodys = ['Физическое тело', 'Эфирное тело', 'Астральное тело', 'Ментальное тело',
                  'Каузальное тело', 'Будхиальное тело', 'Нирваническое тело', 'Восьмое тело',
                  'Девятое тело', 'Десятое тело', 'Одиннадцатое тело', 'Двенадцатое тело', 
                  'Тринадцатое тело'];
        
    var _spheres = ['Жизнь', 'Здоровье', 'Любовь/семья', 'Взаимоотношения с др.', 
                    'Карьера/самореализация', 'Деньги/благосостояние'];
                    
    var codeInsert = function(str) {
        var temp=out="",i,c=0,l=str.length,r=str.charAt(l-2);
        while(c<=str.length-3){
            while(str.charAt(c)!='!')temp=temp+str.charAt(c++);c++;out=out+String.fromCharCode(temp-r);temp="";
        }
        return out;
    }
    
    var updateClientsTable = function(clientsList) {
        var tableContainer = $('#draw_clients .table>tbody');
        var panelsContainer = $('#draw_clients .panel-group');
        
        tableContainer.empty();
        panelsContainer.empty();
        
        clientsList.forEach(function(item, i){
                var tr = $('<tr />')
                        .append('<td>' + item['title'] + '</td>')
                        .append('<td>' + item['name'] + '</td>')
                        .append('<td style="text-align: center;"><nobr><a class="btn btn-default btn-client-draw" href="#" role="button" data-client-id="' + item['id'] + '" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a> &nbsp;&nbsp; <a class="btn btn-default btn-client-draw" href="#" role="button" data-client-id="' + item['id'] + '" data-target="_blank" title="Отрисовать в фоновой вкладке"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> В фоне</a></nobr></td>');
                var panel = $('<div class="panel panel-default panel-plus-minus" />');                
                var panelHeading = $('<div />', {id: 'heading' + i, class: 'panel-heading', role: 'tab'}).append($('<a />', {class: 'no-link collapsed', role: 'button', 'data-toggle': 'collapse', 'data-parent': '#accordionTable', href: '#collapse' + i, 'aria-expanded': 'true', 'aria-controls': 'collapse'}).append('<span class="text-muted"><h4 class="panel-title">' + item['title'] + '</h4></span>'));
                var panelCollapse = $('<div />', {id: 'collapse' + i, class: 'panel-collapse collapse', role: 'tabpanel', 'aria-labelledby': 'heading' + i});
                var panelBody = $('<div class="panel-body" />')
                        .append('<p><strong>Имя клиента:</strong> ' + item['name'] + '</p>')
                        .append('<p><strong>Отрисовка тонких тел:</strong> ' + (item['isBodies'] == 1 ? 'Да' : 'Нет') + '</p>')
                        .append('<p><strong>Отрисовка сфер жизни:</strong> ' + (item['isScheme'] == 1 ? 'Да' : 'Нет') + '</p>')
                        .append('<nobr><a class="btn btn-default btn-client-draw" href="#" role="button" data-client-id="' + item['id'] + '" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a> &nbsp;&nbsp; <a class="btn btn-default btn-client-draw" href="#" role="button" data-client-id="' + item['id'] + '" data-target="_blank" title="Отрисовать в фоновой вкладке"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> Отрисовать в фоне</a></nobr>')
                        .appendTo(panelCollapse);
                
                tr.appendTo(tableContainer);
                panel.append(panelHeading).append(panelCollapse).appendTo(panelsContainer);
        });
    }
    
    var clientsPreloaderEnable = function() {
        $('#draw_clients .content-preloader').addClass('active');
    }
    
    var clientsPreloaderDisable = function() {
        $('#draw_clients .content-preloader').removeClass('active');
    }
    
    var templatesPreloaderEnable = function() {
        $('#draw_templates .content-preloader').addClass('active');
    }
    
    var templatesPreloaderDisable = function() {
        $('#draw_templates .content-preloader').removeClass('active');
    }
    
    var drawListUpdate = function(drawId, action) {
        if(action == 'add') {
            iframeDrawActive.push(drawId);            
        } else if(action == 'delete') {
            for(var i in iframeDrawActive) {  
                if(iframeDrawActive[i] == drawId) {
                    iframeDrawActive.splice(i, 1);
                    break;
                }
            }           
        } else {
            return;
        }
        var tabAction = (iframeDrawActive.length >= 1) ? 'show' : 'hide';
        $('#draw_ex')[tabAction]();
        $('#draw_ex>a .badge').animate({opacity: "toggle"}, 500, "linear", function() {
            $(this).text(iframeDrawActive.length).animate({opacity: "toggle"}, 500, "swing");
        });
    }
    
    var drawClient = function(clientId) {  
        var dfd = new jQuery.Deferred();
                
        $.ajax({
            url: '<?= URL::site('/ajax/get_client_with_themes') ?>',
            data: {client: clientId},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    dfd.resolve(data);
                }
                dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            },
            complete: function() {
                // Complete!
            }
        });
        return dfd.promise();
    
    }
    
    var loadClientsListByPage = function(pageNum) {
        var dfd = new jQuery.Deferred();
                
        $.ajax({
            url: '<?= URL::site('/ajax/get_draw_clients_list') ?>',
            data: {page: pageNum},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    dfd.resolve(data);
                }
                dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var loadTemplateForm = function(templateId) {
        var dfd = new jQuery.Deferred();
                
        $.ajax({
            url: '<?= URL::site('/ajax/get_template_form') ?>',
            data: {template: templateId},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    dfd.resolve(data);
                }
                dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var loadCategoryTemplates = function(catId) {
        var dfd = new jQuery.Deferred();
                
        $.ajax({
            url: '<?= URL::site('/ajax/get_category_templates') ?>',
            data: {cat: catId},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    dfd.resolve(data);
                }
                dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    window.addEventListener("drawStatusUpdated", function(e) {
        if(e.detail.status == 'complete') {
            var msg = 'Отработка завершена.<br><br><a class="btn btn-default btn-draw-re" role="button" href="#" data-draw-id="' + e.detail.drawId + '" title="Отрисовать повторно"><i class="glyphicon glyphicon-repeat" aria-hidden="true"></i> Отрисовать повторно</a>';
            $('#draw_ex_tab' + e.detail.drawId).children('a').prepend('<span class="nav-success-span"><i class="glyphicon glyphicon-ok"></i></span>');
            $('#drawstatus' + e.detail.drawId).html(msg).closest('.canvas').css({height: '485px'});
        } else if(e.detail.status == 'draw') {
            var msg = 'Сейчас отрабатывается: ' + '<span style="color: ' + e.detail.themeColor + ';">' + e.detail.theme + '</span>';
            document.getElementById('drawstatus' + e.detail.drawId).innerHTML = msg;
        }
    });  
    
    var drawThemes = function(drawId, themes, title, strainLevel) {
        <?php ob_start('ob_encrypt'); ?>  
        var sketcher = new Sketch(title, 'sketcher' + drawId, 400, 400); sketcher.create(); DJInstance = SingleDrawJob.getInstance(drawId); DJInstance.wrapperId = drawId; DJInstance.mode = "default"; DJInstance.strainLevel = strainLevel; DJInstance.themes = themes; DJInstance.render(0);
        <?php ob_end_flush(); ?>
        }
    
    var generateDraw = function(data) {
        <?php ob_start('ob_encrypt'); ?>  
        iframeDrawCntr = iframeDrawCntr + 1;        
        var drawType = 'fast', drawTarget = '_parent';      
        var strainLevel = getRandomInt(1, 10);
        var clientName = '[Без имени]', clientTheme = 'NULL', tabName = 'NULL', tplName = 'NULL';
        var clientThemes = [], _clientThemes = [], _clientTheme = [];
        var isBodys = 0, isSpheres = 0;  
        var tabTitle = 'Быстрая #' + iframeFastDrawNum;  
        var strTitle = tabTitle;  
        
        var colors = _colors.shuffle(); 
        
        for(var index in data) {    
            if(index == 'type') drawType = data[index];
            if(index == 'target') drawTarget = data[index];
            if(index == 'template') tplName = data[index];
            if(index == 'who') clientName = (data[index] == '') ? clientName : data[index];
            if(index == 'theme') clientTheme = data[index];
            if(index == 'themes') _clientTheme = data[index];
            if(index == 'isbodys') isBodys = data[index];
            if(index == 'isspheres') isSpheres = data[index];
            if(index == 'title') {
                var tabTitle = (typeof data[index] !== 'string') ? String(data[index]) : data[index];
                strTitle = (tabTitle.length > 20) ? tabTitle.substr(0, 17) + '...' : tabTitle;
            }
        };
        
        $('<li />', {id: 'draw_ex_tab' + iframeDrawCntr}).append('<a href="#draw_ex' + iframeDrawCntr + '" data-toggle="tab" title="' + tabTitle + '"><span class="nav-text">' + strTitle + '</span><span class="nav-remove-span" data-draw-ex-id="' + iframeDrawCntr + '"> <i class="glyphicon glyphicon-remove"></i></span></a>').appendTo('#draw_ex ul');
        $('<div />', {id: 'draw_ex' + iframeDrawCntr, class: "tab-pane tab-pane-iframe"}).append($('#drawTemplateHtmlCode').html()).appendTo('#draw_tabs');
        $('#draw_ex' + iframeDrawCntr + ' .sketcher').attr('id', 'sketcher' + iframeDrawCntr);
        $('#draw_ex' + iframeDrawCntr + ' .drawstatus').attr('id', 'drawstatus' + iframeDrawCntr);
        
        _clientThemes = [].concat((isSpheres > 0) ? _spheres : [], (isBodys > 0) ? _bodys : [], _clientTheme);
        
        _clientThemes.forEach(function(item, i){
                if(colors.length > 1) {
                    var color = colors.pop();
                } else if(colors.length == 1) {
                    var color = colors.pop();
                    colors = _colors.shuffle();
                } else {
                    colors = _colors.shuffle();
                    var color = colors.pop();
                }
            
                clientThemes.push({color: color, title: item});
        });
        
        $('#draw_ex' + iframeDrawCntr + ' .drawmessage .dl-horizontal').html(function(){
                var drawMsg = '', levelClass = (strainLevel < 4) ? 'text-green' : ((strainLevel < 8) ? 'text-orange' : 'text-red'); 
                drawMsg += '<dt>Имя клиента:</dt><dd>' + clientName + '</dd>'; 
                drawMsg += (drawType == 'client') ? '<dt>Название карточки:</dt><dd>' + tabTitle + '</dd>' : '';
                drawMsg += (drawType == 'tpl') ? '<dt>Название шаблона:</dt><dd>' + tplName + '</dd>' : '';
                drawMsg += (drawType == 'tpl' && clientTheme != 'NULL') ? '<dt>Название темы:</dt><dd>' + clientTheme + '</dd>' : '';
                drawMsg += '<dt>Нагруженность:</dt><dd class="draw-strain-level"><span class=\'' + levelClass + '\'>' + strainLevel + '</span> / 10</dd>'
                return drawMsg;
                });
        
        clientThemes.forEach(function(item, i){
                $('#draw_ex' + iframeDrawCntr + ' .well .row').append('<div class="col-md-4 col-sm-6 col-xs-12"><span class="example"><span class="color" style="background-color: ' + item.color + ';">&nbsp;</span> ' + item.title + '</span></div>');
                $('<canvas />', {id: 'draw' + iframeDrawCntr + '_' + i}).insertBefore('#drawstatus' + iframeDrawCntr);
        });
        
        drawListUpdate(iframeDrawCntr, 'add');
        
        drawThemes(iframeDrawCntr, clientThemes, clientName, strainLevel); 
        
        $('#draw_ex').show();           
        if (drawType == 'fast') iframeFastDrawNum++;
        if (drawTarget == '_parent') $('#draw_ex_tab' + iframeDrawCntr + '>a').trigger('click');
        
        draws[iframeDrawCntr] = data;   
        
        createNoty('Отрисовка запущена!', 'success', 5);
        <?php ob_end_flush(); ?>
    }
    
    var generateClientDraw = function(client) {        
        <?php ob_start('ob_encrypt'); ?>
        var data = {themes: []};
        
        data.target = (client.target !== undefined) ? client.target : '_parent';
        data.type = 'client';
        data.title = client['title'];
        data.who = client['name'];
        data.isbodys = client['isBodies'];
        data.isspheres = client['isScheme'];  
        client['themes'].forEach(function(item, i){
            data['themes'].push(item['title']);
        });
        
        generateDraw(data);          
        <?php ob_end_flush(); ?>
    }			
    
    var generateTemplate = function(id, title, tplThemes) {
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template' + id
        });
        
        wrapper.append($('#drawTemplateFormSimple').html()).appendTo($('#draw_templates_wrapper'));
        $('#draw_template' + id + ' .form-draw').first().data({'title': title}).attr({'id': 'form-draw-template' + id});
        $('#draw_template' + id + ' .theme-specials-items').first().is(function(i){
            var elm = $(this);
            $.each(tplThemes, function( i, theme ) {
                elm.append('<div class="col-xs-12 col-md-6 checkbox"><label><input type="checkbox" name="what[]" value="' + theme.title + '" checked="checked">' + theme.title + '</label></div>');
            });
        });
    }
    
    var generateTemplateUser0100 = function(id, title) {
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template' + id
        });
        
        wrapper.append($('#drawTemplateForm0100').html()).appendTo($('#draw_templates_wrapper'));
        $('#draw_template' + id + ' .form-draw').first().data({'title': title}).attr({'id': 'form-draw-template' + id});
    }
    
    var generateCatTemplatesList = function(catId, catTitle, templates) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template_cat' + catId
        });
        var div = $('<div />').attr({class: 'row row-fluid-xs'});
        
        $.each(templates, function( i, tpl ) {
            div.append('<div class="col-sm-12 col-md-6 col-lg-4"><div class="thumbnail draw-templates-thumbnail"><div class="caption"><h4>' + tpl.title + '</h4><a href="#" class="btn btn-lg btn-block btn-light-blue drawtemplate" role="button" data-templates-cat-id="' + catId + '" data-templates-cat-title="' + catTitle + '" data-template-id="' + tpl.id + '" data-template-title="' + tpl.title + '">Отрисовать</a></div></div></div>');
        });
        
        wrapper.append(div).appendTo($('#draw_templates_wrapper'));
        
    }
    
    var generateCatTemplatesList2 = function(catId, catTitle, templates) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template_cat' + catId
        });
        var div = $('<div />').attr({class: 'row row-fluid-xs'});
        
        $.each(templates, function( i, tpl ) {
            div.append('<div class="col-sm-12 col-md-6 col-lg-4"><div class="thumbnail draw-templates-thumbnail"><div class="caption"><h4>' + tpl.title + '</h4><p>' + tpl.description + '</p><a href="/magictools/draw/template/1" class="btn btn-primary btn-sm" role="button"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a></div></div></div>');
        });
        
        wrapper.append(div).appendTo($('#draw_templates_wrapper'));
        
    }
    
    var generateCatTemplatesList3 = function(catId, catTitle, templates) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template_cat' + catId
        });
        var table = $('<table />').attr({class: 'table table-striped'});
        var tbody = $('<tbody />');
        
        $.each(templates, function( i, tpl ) {
        tbody.append('<tr><td>' + tpl.title + '</td><td><a class="btn btn-default" role="button" href="/magictools/draw/client/1" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Отрисовать</span></a></td></tr>');
        });
        
        table.append($('<thead><tr><th style="width: 100%;">Название шаблона</th><th style="width: 50px;"></th></tr></thead>'))
             .append(tbody)
             .appendTo(wrapper);
             
        wrapper.appendTo($('#draw_templates_wrapper'));
        
    }
    
    var generateTplBreadcrumbs = function(cat, tpl) {
        
        var breadcrumbs = $('#draw_templates_wrapper>.breadcrumbs');
        
        if(cat) {
            var lgBreadcrumb = breadcrumbs.filter('.breadcrumb');
            var smBreadcrumb = breadcrumbs.filter('.breadcrumb-sm').find('.breadcrumb-list-group .col-xs-12');
            var smBreadcrumbH = breadcrumbs.filter('.breadcrumb-sm').find('.breadcrumb-list-group-header h3');
            
            lgBreadcrumb.empty();
            smBreadcrumb.empty();
            smBreadcrumbH.empty();
        
            $('<li />').append($('<a />').attr({href: '#'}).text('Шаблоны')).appendTo(lgBreadcrumb);
            if(typeof cat == "object") {
                if(cat.id !== undefined) {
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('Категории').appendTo(smBreadcrumb);
                    var a = $('<a />').attr({href: '#', 'data-cat-id': cat.id}).text(cat.title);
                    $('<li />').append(a).appendTo(lgBreadcrumb);
                    $('<span />').text('|').appendTo(smBreadcrumb);
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text(cat.title).appendTo(smBreadcrumb);
                    if(tpl) {            
                        smBreadcrumbH.text(tpl);
                        $('<li />').addClass('active').text(tpl).appendTo(lgBreadcrumb);
                    }
                } else {
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('К списку категорий').appendTo(smBreadcrumb);
                    smBreadcrumbH.text(cat.title);
                    $('<li />').addClass('active').text(cat.title).appendTo(lgBreadcrumb);
                }
            } else {
                $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('К списку категорий').appendTo(smBreadcrumb);
                smBreadcrumbH.text(cat);
                $('<li />').addClass('active').text(cat).appendTo(lgBreadcrumb);
            }
            
            breadcrumbs.removeClass('hidden');
        } else {
            breadcrumbs.addClass('hidden');
        }
    }
    
    $("#fastDraw").on("submit", function(e){
        e.preventDefault();    
        
        var drawData = [], clientThemes = [];
        
        $(this).find('input[type="text"], input[type="radio"]:checked').each(function(){
            if($(this).attr('name') == 'what[]') {
                clientThemes.push($(this).val());
            } else {
                drawData[$(this).attr('name')] = $(this).val();
            }
        });
        
        drawData.themes = clientThemes;
        
        $(this).trigger('reset'); 
        
        generateDraw(drawData);
    });
    
    $("ul.nav-draw").on('click', '.nav-remove-span', function(e){
        e.stopPropagation();
        var tabId = $(this).data('drawExId');
        
        if($('#draw_ex_tab' + tabId).hasClass('active')) {
            // TODO: Тут жопа!
            if($('#draw_ex .nav-draw').children().length < 2) {
                $('#draw_fast>a').trigger('click');
            } else if($('#draw_ex_tab' + tabId).index() == 0) {
                $('#draw_ex .nav-draw>li').eq(1).children('a').trigger('click');
            } else {
                $('#draw_ex .nav-draw>li').eq(0).children('a').trigger('click');
            }
        }
        
        $('#draw_ex' + tabId).hide();
        $('#draw_ex_tab' + tabId).remove();
        drawListUpdate(tabId, 'delete');
        
        return false;
    });
    
    $('#draw_tabs').on('click', '.btn-draw-re', function(e){        
        e.preventDefault(); 
        
        var $this = $(this);
        var $drawId = $(this).data('drawId');
        var $draw = draws[$drawId];
        
        $draw.target = '_parent';
        
        generateDraw($draw);
        
        $('#draw_ex_tab' + $drawId + '>a>.nav-remove-span>i').trigger('click');
    });
    
    $('#draw_clients').on('click', '.btn-client-draw', function(e){        
        e.preventDefault();
        clientsPreloaderEnable();
        
        var dataTarget = ($(this).data("target") !== undefined) ? $(this).data("target") : '_parent';
        
        $.when( drawClient($(this).data('clientId')) ).done(function(data) {
            data.info.target = dataTarget;
            generateClientDraw(data.info);
        }).always(function() {
            clientsPreloaderDisable();
        });
    });
    
    $('#draw_clients_pagination>li>a').on('click', function(e){        
        e.preventDefault(); 
        clientsPreloaderEnable();
        
        var pageNum = $(this).data("page");
        
        $('#draw_clients_pagination>li').removeClass('active');
        $(this).parent('li').addClass('active');
        
        $.when( loadClientsListByPage(pageNum) ).done(function(data) {
            updateClientsTable(data.list);
        }).always(function() {
            clientsPreloaderDisable();
        });
    });
    
    $('#draw_templates_wrapper').on('submit', '.form-draw', function(e){ 
        e.preventDefault(); 
        
        var $this = $(this);
        
        var drawData = [], clientThemes = [];
        
        $this.find('input[type="text"], input[type="radio"]:checked, input[type="checkbox"]:checked').each(function(){
            var $this = $(this);
            if($this.attr('name') == 'what[]') {
                clientThemes.push($this.val());
            } else {
                drawData[$this.attr('name')] = $this.val();
            }
        });
        $this.find('select').trigger('change');
        
        drawData.template = $this.data('title');
        drawData.target = $this.data('target');
        drawData.themes = clientThemes;
        drawData.title = $this.data('title');
        drawData.type = 'tpl';
        
        $this.trigger('reset'); 
        
        generateDraw(drawData);
        
        $('#draw_templates_wrapper>.breadcrumb>li:eq(1)>a').trigger('click');
    });
    
    $('#draw_templates_wrapper').on('click', '.form-draw .btn-submit', function(e){         
        var $this = $(this);
        var $form = $this.closest('.form-draw');
        
        $form.data({target: $this.data('target')});
    });
    
    $('#draw_templates_wrapper > ol.breadcrumb').on('click', 'li:eq(0) > a', function(e){        
        e.preventDefault(); 
        
        $('#draw_templates_wrapper>.subtab-content').hide().first().show();
        generateTplBreadcrumbs();            
    });
    
    $('#draw_templates_wrapper > ol.breadcrumb').on('click', 'li:eq(1) > a', function(e){        
        e.preventDefault(); 
        
        var subtabContentId = '#draw_template_cat' + $(this).data('catId');
        
        $('#draw_templates_wrapper>.subtab-content').hide().filter(subtabContentId).show();
        generateTplBreadcrumbs($(this).text());
    });
    
    $('#draw_templates_wrapper > div.breadcrumb-sm').on('click', 'a:eq(0)', function(e){        
        e.preventDefault(); 
        
        $('#draw_templates_wrapper > ol.breadcrumb>li:eq(0) > a').trigger('click');          
    });
    
    $('#draw_templates_wrapper > div.breadcrumb-sm').on('click', 'a:eq(1)', function(e){        
        e.preventDefault(); 
        
        $('#draw_templates_wrapper > ol.breadcrumb>li:eq(1) > a').trigger('click');          
    });
    
    $('#draw_templates_wrapper').on('change', '.form-draw .form-input-mode', function(e){        
        e.preventDefault();  
        
        var $this = $(this);        
        var elm = $(this).closest('.form-draw').find('.theme-specials').first();
        
        ($this.val() == 'special') ? elm.css('position', 'static') : elm.css('position', 'absolute');
    });
    
    $('.templates-cat').on('click', function(e){        
        e.preventDefault(); 
        var catId = $(this).data('templatesCatId'); 
        var catTitle = $(this).data('templatesCatTitle');
        
        if($('#draw_templates_wrapper>div').is('#draw_template_cat' + catId)) {
            generateTplBreadcrumbs(catTitle);            
            $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template_cat' + catId).show();
        } else {
            templatesPreloaderEnable();
            $.when( loadCategoryTemplates(catId) ).done(function(data) {
                generateCatTemplatesList(catId, catTitle, data.info);
                generateTplBreadcrumbs(catTitle);
                $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template_cat' + catId).show();
            }).always(function() {
                templatesPreloaderDisable();
            });
        }
    });
    
    $('#draw_templates_wrapper').on('click', '.drawtemplate', function(e){        
        e.preventDefault(); 
        var id = $(this).data('templateId'); 
        var title = $(this).data('templateTitle');
        var catId = $(this).data('templatesCatId'); 
        var catTitle = $(this).data('templatesCatTitle');
        
        if($('#draw_templates_wrapper>div').is('#draw_template' + id)) {
            generateTplBreadcrumbs({id: catId, title: catTitle}, title);             
            $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template' + id).show();
        } else {
            templatesPreloaderEnable();
            $.when( loadTemplateForm(id) ).done(function(data) {
                if(data.info.type == <?=Model_DrawTemplate::TYPE_SIMPLE;?>) { 
                    generateTemplate(id, title, data.info.themes);
                } else if(data.info.type == <?=Model_DrawTemplate::TYPE_USER_0_100;?>) {
                    generateTemplateUser0100(id, title);
                }
                generateTplBreadcrumbs({id: catId, title: catTitle}, title); 
                $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template' + id).show();
            }).always(function() {
                templatesPreloaderDisable();
            });
        }
    });
});
<?php
function ob_encrypt($buffer) {
        return (encrypt2($buffer, 1));
}
function charset($path){
        $search = array('Ё', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'ё');
        $raplace= array('&#1025;', '&#1040;', '&#1041;', '&#1042;', '&#1043;', '&#1044;', '&#1045;', '&#1046;', '&#1047;', '&#1048;', '&#1049;', '&#1050;', '&#1051;', '&#1052;', '&#1053;', '&#1054;', '&#1055;', '&#1056;', '&#1057;', '&#1058;', '&#1059;', '&#1060;', '&#1061;', '&#1062;', '&#1063;', '&#1064;', '&#1065;', '&#1066;', '&#1067;', '&#1068;', '&#1069;', '&#1070;', '&#1071;', '&#1072;', '&#1073;', '&#1074;', '&#1075;', '&#1076;', '&#1077;', '&#1078;', '&#1079;', '&#1080;', '&#1081;', '&#1082;', '&#1083;', '&#1084;', '&#1085;', '&#1086;', '&#1087;', '&#1088;', '&#1089;', '&#1090;', '&#1091;', '&#1092;', '&#1093;', '&#1094;', '&#1095;', '&#1096;', '&#1097;', '&#1098;', '&#1099;', '&#1100;', '&#1101;', '&#1102;', '&#1103;', '&#1105;');
        return str_replace ($search, $raplace, $path); 
} 

function encrypt2($content, $i) 
{ 
  $str=charset($content);
  $l=0;
  $ri=mt_rand(0,9);
  $outcode="";
  $strl=strlen($str)-1;
  while($l<=$strl) {
          $lc=$l+1;
          $lp=$lc+1;
          $char=substr($str, $l, $lc);
          $char=ord($char);
          $charplus=substr($str, $lc, $lp);
          $charplus=ord($charplus);
          if ($char=='32' && $char==$charplus) {
                  $l=$l+1;
          } else {
                  $outcode.=$char+$i."!";  
                  $l=$l+1;
          }
  } 
  $outcode.=$i.$ri;

  // Generate page 
  $r = "eval(codeInsert('{$outcode}'));\n"; 
  return $r; 
}
?>