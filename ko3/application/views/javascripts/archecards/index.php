
jQuery(function($) {

    var cardsNums = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', 
                     '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', 
                     '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', 
                     '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', 
                     '41', '42', '43', '44', '45'];
                     
    var shuffle = function(numPool) {
        for(var j, x, i = numPool.length; i; j = parseInt(Math.random() * i), x = numPool[--i], numPool[i] = numPool[j], numPool[j] = x);
        return numPool;
    };
    
    $('#archeCardsContainer .magic-cards-btn').on('click', function(e){
        e.preventDefault();
    
        cardsNums = shuffle(cardsNums);
        
        var cardFace = $('#archeCardsContainer .magic-cards .magic-card-face');
        var cardClass = null;
        
        cardFace.hide();
        
        if(cardFace.data("cardNum") !== undefined) {
            cardClass = 'magic-card-' + cardFace.data("cardNum");            
            cardFace.removeClass(cardClass);
        }

        cardClass = 'magic-card-' + cardsNums[0];
        cardFace.addClass(cardClass).data("cardNum", cardsNums[0]).fadeIn(500);
    });
});