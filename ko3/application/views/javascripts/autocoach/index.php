
jQuery(function($) {
    $(".theme-delete").on("click", function(e){
        e.preventDefault();
        var themeId = $(this).data("themeId");
    
        bootbox.confirm({
            size: "small",
            title: "Удалить тему?",
            message: "Это действие необратимо.",
            buttons: {
                confirm: {
                    label: 'Удалить',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#autocoachDelete' + themeId).submit();
                }
            }
        }); 
    });
});