
jQuery(function($) {  
    $('.form-textarea-counter > textarea[maxlength]').after(function(i){
        return '<span class="help-block help-block-counter" style="font-size: smaller; margin: 1px; text-align: right;"><span class="cnt">0</span> / ' + $(this).attr('maxlength')  + '</span>';
    });

    $('.form-textarea-counter > textarea[maxlength]').on('change keyup keydown', function(){
        $('.form-textarea-counter > .help-block-counter > .cnt').text($(this).val().length);
    }).trigger('change');
});