
jQuery(function($) {
    $("#addCommentBtn").on("click", function(e){
        e.preventDefault();
        var $form = $('#hiddenCommentForm');
        var $input = $form.children('input[type="hidden"]');
    
        bootbox.prompt({
            title: "Добавление комментария",
            placeholder: "Текст комментария",
            inputType: "textarea",
            buttons: {
                confirm: {
                    label: 'Опубликовать',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                var comment = (result !== null) ? $.trim(result) : null;                
                if (comment === '') {
                    createNoty('Введите текст комментария!', 'danger', 5);
                    $("#addCommentBtn").trigger('click');
                    
                } else if (comment !== null) {
                    $input.val(comment);
                    $form.submit();
                }
            }
        }); 
    });
    
    $(".btn-delete-comment").on("click", function(e){
        e.preventDefault();
        var $this = $(this);
    
        bootbox.confirm({
            size: "small",
            title: "Удалить комментарий?",
            message: "Это действие необратимо.",
            buttons: {
                confirm: {
                    label: 'Удалить',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    ajaxDeleteComment($this.data('commentId'));
                }
            }
        }); 
    });
    
    var ajaxDeleteComment = function(id) {
        var $comment = $('#blogCommentId' + id);
        var $deleteBtn = $comment.find('.panel-heading > button');
        
        $deleteBtn.prop('disabled', true);
        
        // Отправляем AJAX-запрос
        $.ajax({
            url:      '<?=URL::site('ajax/blog_delete_comment')?>',
            data:     {commentId: id},
            type:     'POST',
            dataType: 'json',
            
            success: function(data){
                if(data.status == 'ok') {
                    $comment.fadeOut();
                    createNoty('Комментарий удален.', 'info', 5);
                    return;
                } else {
                    createNoty('Ошибка запроса! Повторите попытку или обратитесь в поддержку!', 'danger', 5);
                    return;
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function(data){
                $deleteBtn.prop('disabled', false);
            }
        });
    }
});