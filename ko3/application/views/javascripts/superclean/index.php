
jQuery(function($) {

    var $techImages = {
        empty:      '/assets/superclean_empty.jpg?v=1.0.1',
        processed:  '/assets/superclean_processed.jpg?v=1.0',
        download:   '/assets/superclean_download.jpg?v=1.0',
    };
    
    var $validImgTypes = ['image/jpeg', 'image/pjpeg', 'image/png', 'image/tiff'];
    
    var validateImgFile = function(file) {
        if($.inArray(file.type, $validImgTypes) < 0) {
            createNoty('Недопустимый формат файла.', 'danger', 5);
            return false;
        }
        
        if(file.size > 5000000) {
            createNoty('Файл должен быть не больше 5 мб.', 'danger', 5);
            return false;
        }
        
        return true;
    }
    
    var clearSlotConfirmation = function(slotId) {
        var dfd = new jQuery.Deferred();
        
        bootbox.confirm({
            title: 'Освобождение слота #' + slotId,
            message: 'Вы уверены?',
            size: 'small',
            buttons: {
                confirm: {
                    label: 'Да',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Нет',
                    className: 'btn-default'
                }
            },
            callback: function(result){
                if(result) {
                    dfd.resolve();
                }
                dfd.reject();
            }
        });
        
        return dfd.promise();
    }
    
    var clearSlot = function(slotId) {
        var $slot   = $('#superCleanSlot' + slotId);
        var $btn    = $slot.find('.btn-delete');
        var $img    = $slot.find('.img-responsive');
        
        // Отключаем кнопку
        $btn.prop('disabled', true);
        
        // Отправляем AJAX-запрос
        $.ajax({
            url:      '<?=URL::site('ajax/superclean_slot_clear')?>',
            data:     {slot: slotId},
            type:     'POST',
            dataType: 'json',
            
            success: function(data){
                if(data.status == 'ok') {
                    $img.attr('src', $techImages.empty);
                    createNoty('Слот #' + slotId + ' освобожден.', 'success', 5);
                    $slot.removeClass('thumbnail-active').addClass('thumbnail-empty');
                    $slot.find('.incorrect-percent').text('-');
                    $slot.find('.processed-str').text('-');
                    return;
                }
                if(data.status == 'error') {
                    createNoty('Ошибка запроса! Повторите попытку или обратитесь в поддержку!', 'danger', 5);
                    return;
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function(data){
                $btn.prop('disabled', false);
            }
        });
    
    }
        
    $('.superclean-uploader').on('change', function(e){
        // ничего не делаем если files пустой или выбранный файл - не картинка
	if( typeof this.files == 'undefined' || ! validateImgFile(this.files[0])) {
            $(this).closest('form')[0].reset();
            return;
        }
        
        var $this   = $(this);
        var $slotId = $this.data('slotId');
        var $slot   = $('#superCleanSlot' + $slotId);
        var $btn    = $slot.find('.btn-upload');
        var $img    = $slot.find('.img-responsive');
	var $data   = new FormData();
        
        // Отключаем кнопку
        $btn.prop('disabled', true);
        
        // Заменяем превью
        $img.attr('src', $techImages.download);
        
        // Заполняем объект данных формы
        $data.append('slot', $slotId);
        $data.append('file', this.files[0]);
        
        // Отправляем AJAX-запрос
        $.ajax({
            url:         '<?=URL::site('ajax/superclean_slot_upload')?>',
            type:        'POST',
            data:        $data,
            dataType:    'json',
            cache:       false,
            contentType: false,
            processData: false,
            
            success: function(data){
                if(data.status == 'ok') {
                    $img.attr('src', $techImages.processed);
                    createNoty('Слот #' + $slotId + ' обновлен. Началась обработка фото.', 'success', 5);
                    $slot.removeClass('thumbnail-empty').addClass('thumbnail-processed');
                    return;
                }
                if(data.status == 'error') {
                    createNoty('Ошибка запроса! Повторите попытку или обратитесь в поддержку!', 'danger', 5);
                    return;
                }
                if(data.status == 'error1') {
                    createNoty('Ошибка загрузки! Повторите попытку или обратитесь в поддержку!', 'danger', 5);
                    return;
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                $img.attr('src', $techImages.empty);
            },
            complete: function(data){
                $btn.prop('disabled', false);
            }
        });
    });
    
    $('#superCleanContainer .btn-upload').on('click', function(e){
        e.preventDefault();
    
        var $this = $(this);
        var $slotId = $this.data('slotId');
        var $uploader = $('#superCleanSlot' + $slotId + 'Uploader');
        
        if(! window.FormData || ! window.File || ! window.FileList) {
            createNoty('Ошибка! Устаревший Интернет-браузер!', 'danger', 5);
            return;
        }
        $uploader.closest('form')[0].reset();
        $uploader.trigger('click');
    });
    
    $('#superCleanContainer .btn-update').on('click', function(e){
        e.preventDefault();
    
        var $this   = $(this);
        var $slotId = $this.data('slotId');
        var $slot   = $('#superCleanSlot' + $slotId);
        var $img    = $slot.find('.img-responsive');
        var $btn    = $slot.find('.btn-update');
        
        // Отключаем кнопку
        $btn.prop('disabled', true);
        
        // Отправляем AJAX-запрос
        $.ajax({
            url:      '<?=URL::site('ajax/superclean_slot_info')?>',
            data:     {slot: $slotId},
            type:     'POST',
            dataType: 'json',
            
            success: function(data){
                if(data.status == 'ok' && data.info.status == 'active') {
                    $img.attr('src', data.info.img);
                    $slot.removeClass('thumbnail-processed').addClass('thumbnail-active');
                    $slot.find('.incorrect-percent').text(data.info.incorrect + '%');
                    $slot.find('.processed-str').text(data.info.processed);
                    createNoty('Слот #' + $slotId + ' обновлен. Обработка фото завершена.', 'success', 5);
                    return;
                }
                if(data.status == 'ok' && data.info.status == 'processed') {
                    createNoty('Слот #' + $slotId + ' в процессе. Идет обработка фото.', 'info', 5);
                    return;
                }
                if(data.status == 'error') {
                    createNoty('Ошибка запроса! Повторите попытку или обратитесь в поддержку!', 'danger', 5);
                    return;
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function(data){
                $btn.prop('disabled', false);
            }
        });
    });
    
    $('#superCleanContainer .btn-test').on('click', function(e){
        e.preventDefault();
    
        var $this   = $(this);
        var $slotId = $this.data('slotId');
        var $slot   = $('#superCleanSlot' + $slotId);
        var $img    = $slot.find('.img-responsive');
        var $btn    = $slot.find('.btn-test');
        
        // Отключаем кнопку
        $btn.prop('disabled', true);
        
        // Отправляем AJAX-запрос
        $.ajax({
            url:      '<?=URL::site('ajax/superclean_slot_info')?>',
            data:     {slot: $slotId},
            type:     'POST',
            dataType: 'json',
            
            success: function(data){
                if(data.status == 'ok') {
                    $img.attr('src', data.info.img);
                    $slot.find('.incorrect-percent').text(data.info.incorrect + '%');
                    $slot.find('.processed-str').text(data.info.processed);
                    createNoty('Информация о слоте #' + $slotId + ' обновлена.', 'info', 5);
                    return;
                }
                if(data.status == 'error') {
                    createNoty('Ошибка запроса! Повторите попытку или обратитесь в поддержку!', 'danger', 5);
                    return;
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function(data){
                $btn.prop('disabled', false);
            }
        });
    });
    
    $('#superCleanContainer .btn-delete').on('click', function(e){
        e.preventDefault();
    
        var $this   = $(this);
        var $slotId = $this.data('slotId');
        
        $.when( clearSlotConfirmation($slotId) ).done(function() {
            clearSlot($slotId);
        });
    });
});