jQuery(function($) {
    $("#profile").on("submit", function(){
        
        bootbox.prompt({
            size: "small",
            title: "Укажите действующий пароль",
            inputType: 'password',
            inputPattern: 'password',
            callback: function (result) {
                if(result === null) return true;
                if(result === '') return false;
                $('#profileEmail').val($('#inputEmail').val());
                $('#profileNewPassword').val($('#inputPassword').val());
                $('#profileCurrentPassword').val(result);
                $('#profilePostForm').submit();
            }
        }); 
        
        return false;
    });
});