jQuery(function($) {
    var __GLOBALS__ = {
        nowDate: '<?=date('Y-m-d')?>',
        timeZones: {
            '-12': '[UTC-11] — Эниветок, Кваджалейн',
            '-11': '[UTC-11] — Американское Самоа, Ниуэ',
            '-10': '[UTC-10] — Гавайи, Алеутские острова',
            '-9': '[UTC-9] — Аляска, Острова Гамбье',
            '-8': '[UTC-8] — Вашингтон, Калифорния, Острова Питкэрн, Юкон',
            '-7': '[UTC-7] — Сонора, Аризона, Нижняя Калифорния',
            '-6': '[UTC-6] — Гватемала, Гондурас, Коста-Рика, Никарагуа, Сальвадор',
            '-5': '[UTC-5] — Гаити, Ямайка, Колумбия, Перу',
            '-4': '[UTC-4] — Венесуэла, Гайана, Бразилия, Боливия, Парагвай, Чили',
            '-3': '[UTC-3] — Бразилия, Буэнос-Айрес, Джорджтаун',
            '-2': '[UTC-2] — Южная Георгия и Южные Сандвичевы Острова',
            '-1': '[UTC-1] — Азорские острова, Кабо-Верде',
            '0': '[UTC] — Лондон, Лиссабон, Касабланка',
            '1': '[UTC+1] — Брюссель, Копенгаген, Мадрид, Париж',
            '2': '[UTC+2] — Калининград, Южная Африка',
            '3': '[UTC+3] — Москва, Санкт-Петербург, ДНР, ЛНР, Багдад, Эр-Рияд',
            '4': '[UTC+4] — Абу-Даби, Маскат, Баку, Тбилиси',
            '5': '[UTC+5] — Екатеринбург, Ташкент, Исламабад, Карачи',
            '6': '[UTC+6] — Алматы, Дакка, Коломбо',
            '7': '[UTC+7] — Бангкок, Ханой, Джакарта',
            '8': '[UTC+8] — Пекин, Перт, Сингапур, Гонконг',
            '9': '[UTC+9] — Токио, Сеул, Осака, Саппоро, Якутск',
            '10': '[UTC+10] — Восточная Австралия, Гуам, Владивосток',
            '11': '[UTC+11] — Магадан, Соломоновы острова, Новая Каледония',
            '12': '[UTC+12] — Окленд, Веллингтон, Фиджи, Камчатка'
        }
    };

    var schedulePeriods = {
        <?=Model_AutodrawClientSchedule::PERIOD_HOURLY?>: {'code': 'hourly', 'value': 'Каждый час'},
        <?=Model_AutodrawClientSchedule::PERIOD_DAILY?>: {'code': 'daily', 'value': 'Каждый день'},
        <?=Model_AutodrawClientSchedule::PERIOD_WEEKLY?>: {'code': 'weekly', 'value': 'Каждую неделю'}
    };

    var schedulePeriodsMin = {
        <?=Model_AutodrawClientSchedule::PERIOD_DAILY?>: {'code': 'daily_min', 'value': 'Каждый день (до мин. нагруженности)'},
        <?=Model_AutodrawClientSchedule::PERIOD_WEEKLY?>: {'code': 'weekly_min', 'value': 'Каждую неделю (до мин. нагруженности)'}
    };

    var schedulePeriodsCodes = {
        'null': 'Не задано',
        'hourly': 'Каждый час',
        'daily': 'Каждый день',
        'weekly': 'Каждую неделю',
        'daily_min': 'Каждый день (до мин. нагруженности)',
        'weekly_min': 'Каждую неделю (до мин. нагруженности)'
    };
    
    var rusMonthsGenitive = {
        1: 'января',
        2: 'февраля',
        3: 'марта',
        4: 'апреля',
        5: 'мая',
        6: 'июня',
        7: 'июля',
        8: 'августа',
        9: 'сентября',
        10: 'октября',
        11: 'ноября',
        12: 'декабря',
    }
    
    var iframeDrawActive = [];
    
    var genPrettyDateFromISO = function(iso_date) {
        var parts = iso_date.split('-');
        var monthNum = Number(parts[1]);
        var month = rusMonthsGenitive[monthNum];
        
        return parts[2] + ' ' + month + ' ' + parts[0];
    }
    
    var clientsPreloaderEnable = function() {
        $('#draw_clients .content-preloader').addClass('active');
    }
    
    var clientsPreloaderDisable = function() {
        $('#draw_clients .content-preloader').removeClass('active');
    }
    
    var templatesPreloaderEnable = function() {
        $('#draw_templates .content-preloader').addClass('active');
    }
    
    var templatesPreloaderDisable = function() {
        $('#draw_templates .content-preloader').removeClass('active');
    }
    
    var autodrawPreloaderEnable = function() {
        $('#draw_log .content-preloader').addClass('active');
    }
    
    var autodrawPreloaderDisable = function() {
        $('#draw_log .content-preloader').removeClass('active');
    }
    
    var updateClientsTable = function(clientsList) {
        var tableContainer = $('#draw_clients .table>tbody');
        
        console.log(clientsList);
        
        tableContainer.empty();
        
        clientsList.forEach(function(item, i){
            var isBodies = (item['isBodies']) ? 
                '<span class="text-green">Да</span>' : '<span class="text-red">Нет</span>';
            var isScheme = (item['isScheme']) ? 
                '<span class="text-green">Да</span>' : '<span class="text-red">Нет</span>';
            var schedule = 'Не задано';
            
            if(item.schedule.period != 'null') {
                var btnData = 'data-client-period="' + item.schedule.period + '"' +
                              'data-client-is-min="' + (item.schedule.minimize == '1' ? 'true' : 'false') + '"' +
                              'data-client-is-duration="' + (item.schedule.duration.switch == 'on' ? 'true' : 'false') + '"';
                if(item.schedule.duration.switch == 'on') {
                    btnData += 'data-client-duration-start="' + item.schedule.duration.start + '"' +
                                  'data-client-duration-finish="' + item.schedule.duration.finish + '"';
                }
                var schedule = '<button type="button" class = "btn btn-link" style="padding: 0; text-decoration: none!important;"' +
                                        btnData + '>' + schedulePeriodsCodes[item.schedule.period] + '</button>';
            }
            
            var tr = $('<tr />').attr({id: 'drawTblClient' + item['id']});
            var td = $('<td />')
                    .append('<div class="col-xs-12 col-lg-6"><strong>Название карточки:</strong> ' + item['title'] + '</div>')
                    .append('<div class="col-xs-12 col-lg-6"><strong>Имя клиента:</strong> ' + item['name'] + '</div>')
                    .append('<div class="col-xs-12 col-lg-6"><strong>Тела (виды внимания):</strong> ' + isBodies + '</div>')
                    .append('<div class="col-xs-12 col-lg-6"><strong>Сферы благополучия:</strong> ' + isScheme + '</div>')
                    .append('<div class="col-xs-12 col-lg-6"><strong>Количество тем:</strong> ' + item['themesCnt'] + '</div>')
                    .append('<div class="col-xs-12 col-lg-6 cp-schedule"><strong>Расписание:</strong> <span>' + schedule + '</span></div>')
                    .append('<div class="col-xs-12 cp-buttons"><nobr><a class="btn btn-default btn-client-draw" href="#" role="button" data-client-id="' + item['id'] + '" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a> <a class="btn btn-info btn-client-schedule" href="#" role="button" data-client-id="' + item['id'] + '" data-client-period="' + item.schedule.period + '" data-client-is-min="' + (item.schedule.minimize == '1' ? 'true' : 'false') + '" data-client-is-duration="' + (item.schedule.duration.switch == 'on' ? 'true' : 'false') + '" data-client-duration-start="' + item.schedule.duration.start + '" data-client-duration-finish="' + item.schedule.duration.finish + '" title="Расписание"><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i> Расписание</a></nobr></div>');

            tr.append(td).appendTo(tableContainer);
        });
    }
    
    var drawListUpdate = function(drawId, action) {
        if(action == 'add') {
            iframeDrawActive.push(drawId);            
        } else if(action == 'delete') {
            for(var i in iframeDrawActive) {  
                if(iframeDrawActive[i] == drawId) {
                    iframeDrawActive.splice(i, 1);
                    break;
                }
            }           
        } else if(action == 'reload') {
            // Nothing to do...
        } else {
            return;
        }
        var tabAction = (iframeDrawActive.length >= 1) ? 'show' : 'hide';
        $('#draw_ex')[tabAction]();
        $('#draw_ex>a .badge').animate({opacity: "toggle"}, 500, "linear", function() {
            $(this).text(iframeDrawActive.length).animate({opacity: "toggle"}, 500, "swing");
        });
    } 
    
    var autodrawStatusUpdateById = function(id, data) {
        var $wrap = $('#autodraw' + id + '_wrapper');
        var $stat = $wrap.find('.autodraw-status-text');
        var $slvl = $wrap.find('.autodraw-strain-level');
        var $dbdg = $wrap.find('.autodraw-date-wrapper > .badge');
        
        $slvl.removeClass('text-green text-orange text-red');
        $slvl.html('<span class="glyphicon glyphicon-flash"></span>' + data.strainLevel);
        
        if(data.strainLevel < 4) {
            $slvl.addClass('text-green');
        } else if(data.strainLevel < 8) {
            $slvl.addClass('text-orange');
        } else {
            $slvl.addClass('text-red');
        }
        
        $dbdg.text(data.date);
        
        if(data.status == <?=Model_Autodraw::STATUS_COMPLETE?>) {
            var $btns = $wrap.find('.autodraw-actions-wrapper');
            var $attr = {'class': 'btn btn-default btn-autodraw-open', 'role': 'button', href: '#', 'data-autodraw-id': id, 'data-target': '_parent', 'data-loading-text': 'Загрузка...', 'title': 'Открыть'};
            
            $stat.html('<span class="text-blue-gray">Отрисовано</span>');            
            $btns.empty();
            
            $('<a />').attr($attr).html('<i class="glyphicon glyphicon-check" aria-hidden="true"></i> Открыть').appendTo($btns);
            
            $attr['title'] = 'Открыть в фоновой вкладке';
            $attr['data-target'] = '_blank';
            
            $('<a />').attr($attr).html('<i class="glyphicon glyphicon-share" aria-hidden="true"></i> В фоне').appendTo($btns);
            
            $attr['title'] = 'Удалить';
            $attr['class'] = 'btn btn-danger btn-autodraw-delete';
            $attr['data-target'] = '';
            $attr['data-loading-text'] = 'Удаление...';
            
            $('<a />').attr($attr).html('<i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Удалить').appendTo($btns);
            
            createNoty('Автоотрисовка завершена!', 'success', 5);
            
            return 1;
        } 
        
        if(data.status == <?=Model_Autodraw::STATUS_PROCESSED?>) {            
            $stat.html('<span class="text-gray">Отрисовывается</span>');            
            createNoty('Идет процесс автоотрисовки...', 'info', 5);
        } else {
            $stat.html('<span class="text-gray">В ожидании</span>');            
            createNoty('Автоотрисовка ожидает очереди!', 'info', 5);
        }
        
        return false;
    }
    
    var autodrawStatusUpdate = function(id) {  
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_status_update') ?>',
            data: {drawId: id},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    autodrawStatusUpdateById(id, data.autodraw);
                    dfd.resolve();
                }
                dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            },
            complete: function() {
                // Complete!
            }
        });
        return dfd.promise();
    }
    
    var updateAutodrawTable = function(autodrawList) {
        var tableContainer = $('#draw_log .table>tbody');
        
        tableContainer.empty();
        
        if(autodrawList.length < 1) {
            tableContainer.html('<tr><td><div class="row"><div class="col-md-12"><center>Журнал автоотрисовок пуст!</center></div></div></td></tr>');
            return;
        }
        
        autodrawList.forEach(function(item, i){
                var row = $('<div />').attr({class: 'row', id: 'autodraw' + item['id'] + '_wrapper'});
                
                $('<div />').attr({class: 'col-xs-12 col-sm-7 col-md-4 col-lg-4'}).append('<div class="col-xs-12 col-md-12 autodraw-name-wrapper"><h3 class="autodraw-name">' + item['name'] + '</h3></div><div class="col-xs-12 col-md-12 autodraw-title-wrapper"><p class="autodraw-title">' + item['title'] + '</p></div>').appendTo(row);
                $('<div />').attr({class: 'col-xs-12 col-sm-5 col-md-8 col-lg-3'}).append('<div class="col-xs-12 col-md-6 col-md-push-6 col-lg-12 col-lg-push-0 autodraw-status-wrapper"><p class="autodraw-status"><span class="autodraw-strain-level ' + ((item['strainLevel'] < 4) ? 'text-green' : ((item['strainLevel'] < 8) ? 'text-orange' : 'text-red')) + '"><span class="glyphicon glyphicon-flash"></span>' + item['strainLevel'] + '</span> <span class="autodraw-status-text">' + ((item['status'] == <?=Model_Autodraw::STATUS_COMPLETE?>) ? '<span class="text-blue-gray">Отрисовано</span>' : ((item['status'] == <?=Model_Autodraw::STATUS_PROCESSED?>) ? '<span class="text-gray">Отрисовывается</span>' : '<span class="text-gray">В ожидании</span>')) + '</span></p></div><div class="col-xs-12 col-md-6 col-md-pull-6 col-lg-12 col-lg-pull-0 autodraw-date-wrapper"><span class="badge">' + item['date'] + '</span></div>').appendTo(row);
                $('<div />').attr({class: 'col-xs-12 col-lg-5'}).append('<div class="col-md-12 autodraw-actions-wrapper">' + ((item['status'] == <?=Model_Autodraw::STATUS_COMPLETE?>) ? '<a class="btn btn-default btn-autodraw-open" role="button" href="#" data-autodraw-id="' + item['id'] + '" data-target="_parent" data-loading-text="Загрузка..." title="Открыть"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Открыть</a><a class="btn btn-default btn-autodraw-open" role="button" href="#" data-autodraw-id="' + item['id'] + '" data-target="_blank" data-loading-text="Загрузка..." title="Открыть в фоновой вкладке"><i class="glyphicon glyphicon-share" aria-hidden="true"></i> В фоне</a><a class="btn btn-danger btn-autodraw-delete" role="button" href="#" data-autodraw-id="' + item['id'] + '" data-loading-text="Удаление..." title="Удалить"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Удалить</a>' : '<a class="btn btn-default btn-autodraw-f5" role="button" href="#" data-autodraw-id="' + item['id'] + '" data-loading-text="Загрузка..." title="Проверить"><i class="glyphicon glyphicon-repeat" aria-hidden="true"></i> Проверить</a>') + '</div>').appendTo(row);
                
                var td = $('<td />').append(row);
                
                $('<tr />').append(td).appendTo(tableContainer);
        });
    }
    
    var updateAutodrawPagination = function(counter, active) {    
        var paginationContainer = $('#autodraw_pagination>div');        
        
        paginationContainer.empty();
        
        if(counter <= 10) {
            return;
        }
        
        paginationContainer.html('<nav><ul class="pagination autodraw-pagination"><li class="disabled"><span style="cursor: default;">Страница </span></li></ul></nav>');
                
        var nav = paginationContainer.find('nav>ul.autodraw-pagination');
        
        for(var i = 1; i<=Math.ceil(counter/10); i++){
            var li = $('<li />');
            
            if(i == active) {
                li.addClass('active');
            }
            
            li.append('<a href="#" style="cursor: pointer;" data-page="' + i + '">' + i + '</a>').appendTo(nav);
        }
    }
    
    var autodrawLogUpdate = function(i) {
        var dfd = new jQuery.Deferred();
        if (i === undefined) {
            var i = 1;
        }
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_get_log10') ?>',
            data: {page: i},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                updateAutodrawPagination(data.counter, data.page);
                updateAutodrawTable(data.list);
                dfd.resolve();
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var autodrawRe = function(id) {  
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_re') ?>',
            data: {drawId: id},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    createNoty('Автоотрисовка поставлена в очередь!', 'success', 5);
                    dfd.resolve();
                } else {
                    createNoty('Ошибка! Автоотрисовка не найдена!', 'danger', 5);
                    dfd.reject();
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var autodrawRemoveRowById = function(id) {
        var $pgs = $('#autodraw_pagination>div');
        var $pgn = 1;
        
        $('#autodraw' + id + '_wrapper').closest('tr').hide(1000, function(){
            $(this).remove();
            if($pgs.find('nav>ul>li.active').length) {
                $pgn = Number($pgs.find('nav>ul>li.active>a').attr('data-page'));
            }
            $pgs.empty();
            autodrawLogUpdate($pgn);
        });
    }
    
    var autodrawDelete = function(id) {  
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_delete') ?>',
            data: {drawId: id},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                autodrawRemoveRowById(id);
                if(data.status == 'ok') {
                    createNoty('Автоотрисовка удалена!', 'success', 5);
                    dfd.resolve();
                } else {
                    createNoty('Ошибка! Автоотрисовка не найдена!', 'danger', 5);
                    dfd.reject();
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var autodrawGroupDelete = function(period) {  
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_group_delete') ?>',
            data: {period: period},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    autodrawLogUpdate(1);
                    createNoty('Автоотрисовки удалены!', 'success', 5);
                    dfd.resolve();
                } else {
                    createNoty('За выбранный период записей нет!', 'danger', 5);
                    dfd.reject();
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var autodrawGetData = function(drawId) {
        var dfd = new jQuery.Deferred();
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_get') ?>',
            data: {id: drawId},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                dfd.resolve(data.data);
              }
              dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var autodrawCalculateTaskData = function(data) { 
        var drawData = {
            type:      'fast', 
            clientId:  null,
            tplId:     null,   
            title:     null,
            name:      '[Без имени]',
            themes:    [],
            isbodys:   0,
            isspheres: 0
        }
        
        for(var index in data) {    
            if(index == 'type') drawData.type = data[index];
            if(index == 'clientId') drawData.clientId = data[index];
            if(index == 'tplId') drawData.tplId = data[index];
            if(index == 'title') drawData.title = data[index];
            if(index == 'name') drawData.name = data[index];
            if(index == 'themes') drawData.themes = data[index];
            if(index == 'isbodys') drawData.isbodys = data[index];
            if(index == 'isspheres') drawData.isspheres = data[index];
        };
        
        if(drawData.type == 'client') {
            drawData.themes = null;
        }
        
        return drawData;
    }
    
    var autodrawSendTask = function(data, drawData) { 
        autodrawPreloaderEnable();
                
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw') ?>',
            data: drawData,
            type:     'POST',
            dataType: 'json',
            success:  function(d){
                if(d.status == 'ok') {
                    if('formObj' in data) {
                        data.formObj.trigger('reset'); 
                        data.formObj.find('[type="submit"]').button('reset');
                    }
                    autodrawLogUpdate();
                    createNoty('Автоотрисовка поставлена в очередь!', 'success', 5);
                }
                console.log(d);
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function() {
                autodrawPreloaderDisable();
            }
        });
    }
    
    var autodrawSaveTask = function(data, drawData) {
        autodrawPreloaderEnable();
                
        $.ajax({
            url: '<?= URL::site('/ajax/create_client_by_template') ?>',
            data: drawData,
            type:     'POST',
            dataType: 'json',
            success:  function(d){
                if(d.status == 'ok') {
                    if('formObj' in data) {
                        data.formObj.trigger('reset');
                    }
                    createNoty('Карточка клиента создана!', 'success', 5);
                }
                console.log(d);
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function() {
                autodrawPreloaderDisable();
            }
        });
    }
    
    var clientsScheduleUpdate = function(schedule) {
        var dfd = new jQuery.Deferred();
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_client_schedule_update') ?>',
            data: schedule,
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              console.log(data);
              if(data.status == 'ok') {
                createNoty('Расписание обновлено!', 'success', 5);
                dfd.resolve(schedule);
              } else {
                createNoty('Ошибка! Не удалось обновить расписание!', 'danger', 5);
                dfd.reject();
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        return dfd.promise();
    }
    
    var generateDraw = function(data) {  
        var drawId = 1;        
        var drawType = 'fast', drawTarget = '_parent'; 
        var drawSrc = '<?= URL::site('/assets/sketcher.png') ?>';
        var strainLevel = 1;
        var clientName = '[Без имени]', clientTheme = 'NULL', tabName = 'NULL', tplName = 'NULL';
        var clientThemes = []; 
        var tabTitle = 'Отрисовка темы';  
        var strTitle = tabTitle;   
        
        for(var index in data) {    
            if(index == 'id') drawId = data[index];    
            if(index == 'src') drawSrc = data[index];    
            if(index == 'type') drawType = data[index];
            if(index == 'target') drawTarget = data[index];
            if(index == 'strainLevel') strainLevel = data[index];
            if(index == 'name') clientName = (data[index] == '') ? clientName : data[index];
            if(index == 'themes') clientThemes = data[index];
            if(index == 'title') {
                var tabTitle = (typeof data[index] !== 'string') ? String(data[index]) : data[index];
                strTitle = (tabTitle.length > 20) ? tabTitle.substr(0, 17) + '...' : tabTitle;
            }
        };
        
        for(var i in iframeDrawActive) {  
            if(iframeDrawActive[i] == drawId) {
                drawListUpdate(drawId, 'reload');
                if (drawTarget == '_parent') $('#draw_ex_tab' + drawId + '>a').trigger('click');
                return;
            }
        }
        
        $('<li />', {id: 'draw_ex_tab' + drawId}).append('<a href="#draw_ex' + drawId + '" data-toggle="tab" title="' + tabTitle + '"><span class="nav-text">' + strTitle + '</span><span class="nav-remove-span" data-draw-ex-id="' + drawId + '"> <i class="glyphicon glyphicon-remove"></i></span></a>').appendTo('#draw_ex ul');
        $('<div />', {id: 'draw_ex' + drawId, class: "tab-pane tab-pane-iframe tab-pane-autodraw"}).append($('#drawTemplateHtmlCode').html()).appendTo('#draw_tabs');
        $('#draw_ex' + drawId + ' .sketcher').attr('src', drawSrc);
        $('#draw_ex' + drawId + ' .drawstatus>a').attr('data-autodraw-id', drawId);
        
        $('#draw_ex' + drawId + ' .drawmessage .dl-horizontal').html(function(){
                var drawMsg = '', levelClass = (strainLevel < 4) ? 'text-green' : ((strainLevel < 8) ? 'text-orange' : 'text-red'); 
                drawMsg += '<dt>Имя клиента:</dt><dd>' + clientName + '</dd>'; 
                drawMsg += (drawType == 'client') ? '<dt>Название карточки:</dt><dd>' + tabTitle + '</dd>' : '';
                drawMsg += (drawType == 'tpl') ? '<dt>Название шаблона:</dt><dd>' + tabTitle + '</dd>' : '';
                drawMsg += (drawType == 'tpl' && clientThemes.length == 1) ? '<dt>Название темы:</dt><dd>' + clientThemes[0] + '</dd>' : '';
                drawMsg += '<dt>Нагруженность:</dt><dd class="draw-strain-level"><span class=\'' + levelClass + '\'>' + strainLevel + '</span> / 10</dd>'
                return drawMsg;
        });
        
        clientThemes.forEach(function(item, i){
                $('#draw_ex' + drawId + ' .well .row').append('<div class="col-md-4 col-sm-6 col-xs-12"><span class="example"><span class="color" style="background-color: ' + item.color + ';">&nbsp;</span> ' + item.title + '</span></div>');
        });
        
        drawListUpdate(drawId, 'add');
        
        $('#draw_ex').show();
        if (drawTarget == '_parent') $('#draw_ex_tab' + drawId + '>a').trigger('click');
    }		
    
    var generateTemplate = function(id, title, tplThemes) {
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template' + id
        });
        
        wrapper.append($('#drawTemplateFormSimple').html()).appendTo($('#draw_templates_wrapper'));
        $('#draw_template' + id + ' .form-draw').first().data({'id': id}).attr({'id': 'form-draw-template' + id});
        $('#draw_template' + id + ' .theme-specials-items').first().is(function(i){
            var elm = $(this);
            $.each(tplThemes, function( i, theme ) {
                elm.append('<div class="col-xs-12 col-md-6 checkbox"><label><input type="checkbox" name="what[]" value="' + theme.title + '" checked="checked">' + theme.title + '</label></div>');
            });
        });
    }
    
    var generateTemplateUser0100 = function(id, title) {
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template' + id
        });
        
        wrapper.append($('#drawTemplateForm0100').html()).appendTo($('#draw_templates_wrapper'));
        $('#draw_template' + id + ' .form-draw').first().data({'id': id}).attr({'id': 'form-draw-template' + id});
    }
    
    var generateCatTemplatesList = function(catId, catTitle, templates) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template_cat' + catId
        });
        var div = $('<div />').attr({class: 'row row-fluid-xs'});
        
        $.each(templates, function( i, tpl ) {
            div.append('<div class="col-sm-12 col-md-6 col-lg-4"><div class="thumbnail draw-templates-thumbnail"><div class="caption"><h4>' + tpl.title + '</h4><a href="#" class="btn btn-lg btn-block btn-light-blue drawtemplate" role="button" data-templates-cat-id="' + catId + '" data-templates-cat-title="' + catTitle + '" data-template-id="' + tpl.id + '" data-template-title="' + tpl.title + '">Открыть</a></div></div></div>');
        });
        
        wrapper.append(div).appendTo($('#draw_templates_wrapper'));
        
    }
    
    var generateCatTemplatesList2 = function(catId, catTitle, templates) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template_cat' + catId
        });
        var div = $('<div />').attr({class: 'row row-fluid-xs'});
        
        $.each(templates, function( i, tpl ) {
            div.append('<div class="col-sm-12 col-md-6 col-lg-4"><div class="thumbnail draw-templates-thumbnail"><div class="caption"><h4>' + tpl.title + '</h4><p>' + tpl.description + '</p><a href="/magictools/draw/template/1" class="btn btn-primary btn-sm" role="button"><i class="glyphicon glyphicon-check" aria-hidden="true"></i> Отрисовать</a></div></div></div>');
        });
        
        wrapper.append(div).appendTo($('#draw_templates_wrapper'));
        
    }
    
    var generateCatTemplatesList3 = function(catId, catTitle, templates) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_template_cat' + catId
        });
        var table = $('<table />').attr({class: 'table table-striped'});
        var tbody = $('<tbody />');
        
        $.each(templates, function( i, tpl ) {
        tbody.append('<tr><td>' + tpl.title + '</td><td><a class="btn btn-default" role="button" href="/magictools/draw/client/1" title="Отрисовать"><i class="glyphicon glyphicon-check" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Отрисовать</span></a></td></tr>');
        });
        
        table.append($('<thead><tr><th style="width: 100%;">Название шаблона</th><th style="width: 50px;"></th></tr></thead>'))
             .append(tbody)
             .appendTo(wrapper);
             
        wrapper.appendTo($('#draw_templates_wrapper'));
        
    }
    
    var generateTplBreadcrumbs = function(cat, tpl) {
        
        var breadcrumbs = $('#draw_templates_wrapper>.breadcrumbs');
        
        if(cat) {
            var lgBreadcrumb = breadcrumbs.filter('.breadcrumb');
            var smBreadcrumb = breadcrumbs.filter('.breadcrumb-sm').find('.breadcrumb-list-group .col-xs-12');
            var smBreadcrumbH = breadcrumbs.filter('.breadcrumb-sm').find('.breadcrumb-list-group-header h3');
            
            lgBreadcrumb.empty();
            smBreadcrumb.empty();
            smBreadcrumbH.empty();
        
            $('<li />').append($('<a />').attr({href: '#'}).text('Шаблоны')).appendTo(lgBreadcrumb);
            if(typeof cat == "object") {
                if(cat.id !== undefined) {
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('Категории').appendTo(smBreadcrumb);
                    var a = $('<a />').attr({href: '#', 'data-cat-id': cat.id}).text(cat.title);
                    $('<li />').append(a).appendTo(lgBreadcrumb);
                    $('<span />').text('|').appendTo(smBreadcrumb);
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text(cat.title).appendTo(smBreadcrumb);
                    if(tpl) {            
                        smBreadcrumbH.text(tpl);
                        $('<li />').addClass('active').text(tpl).appendTo(lgBreadcrumb);
                    }
                } else {
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('К списку категорий').appendTo(smBreadcrumb);
                    smBreadcrumbH.text(cat.title);
                    $('<li />').addClass('active').text(cat.title).appendTo(lgBreadcrumb);
                }
            } else {
                $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('К списку категорий').appendTo(smBreadcrumb);
                smBreadcrumbH.text(cat);
                $('<li />').addClass('active').text(cat).appendTo(lgBreadcrumb);
            }
            
            breadcrumbs.removeClass('hidden');
        } else {
            breadcrumbs.addClass('hidden');
        }
    }
    
    $("#fastDraw").on("submit", function(e){
        e.preventDefault();
        
        $(this).find('[type="submit"]').button('loading');
        
        var readyData;
        var drawData = {
            type:      'fast', 
            name:      '[Без имени]',
            themes:    [],
            isbodys:   0,
            isspheres: 0
        }
        
        clientThemes = [];
        
        $(this).find('input[type="text"], input[type="radio"]:checked').each(function(){
            if($(this).attr('name') == 'who') {
                drawData.name = $(this).val();
            } else if($(this).attr('name') == 'what[]') {
                clientThemes.push($(this).val());
            } else {
                drawData[$(this).attr('name')] = $(this).val();
            }
        });
        
        drawData.themes = clientThemes;
        drawData.formObj = $(this);
        
        readyData = autodrawCalculateTaskData(drawData);
        
        autodrawSendTask(drawData, readyData);
    });
    
    $("ul.nav-draw").on('click', '.nav-remove-span', function(e){
        e.stopPropagation();
        var tabId = $(this).data('drawExId');
        
        if($('#draw_ex_tab' + tabId).hasClass('active')) {
            // TODO: Тут жопа!
            if($('#draw_ex .nav-draw').children().length < 2) {
                $('#draw_fast>a').trigger('click');
            } else if($('#draw_ex_tab' + tabId).index() == 0) {
                $('#draw_ex .nav-draw>li').eq(1).children('a').trigger('click');
            } else {
                $('#draw_ex .nav-draw>li').eq(0).children('a').trigger('click');
            }
        }
        
        $('#draw_ex' + tabId).remove();
        $('#draw_ex_tab' + tabId).remove();
        drawListUpdate(tabId, 'delete');
        
        return false;
    });
    
    $('#draw_clients').on('click', '.btn-client-draw', function(e){
        e.preventDefault(); 
        clientsPreloaderEnable();
        
        var $this = $(this);   
        var readyData;
        var drawData = {type: 'client', clientId: $this.data('clientId')};
        
        readyData = autodrawCalculateTaskData(drawData);
        
        autodrawSendTask(drawData, readyData);
        
        clientsPreloaderDisable();
    });
    
    $('#draw_clients').on('click', '.btn-client-schedule', function(e){
        e.preventDefault();
        
        var $this = $(this);
        var $clientId = $this.data('clientId');
        var $clientPeriod = $this.data('clientPeriod');
        var $clientIsMin = $this.data('clientIsMin');
        var $clientIsDuration = $this.data('clientIsDuration');
        var $clientDurationStart = $this.data('clientDurationStart');
        var $clientDurationFinish = $this.data('clientDurationFinish');
        var $modal = $('#clientsSchedule');
        var $form = $modal.find('form');
        
        var isMinVal = ($clientIsMin == 'true' || $clientIsMin == true) ? true : false;
        var isDuration = ($clientIsDuration == 'true' || $clientIsDuration == true) ? true : false;
        
        $form.data('clientId', $clientId);
        $form[0].reset();
        
        if ($clientPeriod === null) { 
            $clientPeriod ='null';
        } else {
            $form.find('input[name="durationStart"]').val($clientDurationStart);
            $form.find('input[name="durationFinish"]').val($clientDurationFinish);
        }
        
        $form.find('select').val($clientPeriod).trigger('change');
        $form.find('input[name="isDuration"]').prop('checked', isDuration).trigger('change');
        $form.find('input[name="isMinVal"]').prop('checked', isMinVal);

        $modal.modal('show');
        
        return false;
    });
    
    $('#clientsSchedule form select').on('change', function(e){
        var $this = $(this);
        var $form = $this.closest('form');
        var $checkbox = $form.find('input[type="checkbox"]');
        var $checkboxWrapper = $form.find('div.form-group-checkbox');
        
        var disabled = false;
        var checked = $checkbox.prop('checked');
        var displayCheckbox = true;
        
        if($this.val() == 'null' || $this.val() == 'hourly') {
            disabled = true;
            checked = false;
            displayCheckbox = false;
        }
        
        $checkbox.prop('disabled', disabled).prop('checked', checked).trigger('change');
        
        (displayCheckbox) ? $checkboxWrapper.show() : $checkboxWrapper.hide();
    });
    
    $('#clientsSchedule form input[name="isDuration"]').on('change', function(e){
        var $this = $(this);
        var $form = $this.closest('form');
        var $modal = $this.closest('.modal-dialog');
        var $durationWrapper = $form.find('div.form-group-duration');
        
        if($this.prop('checked')) {
            $modal.addClass('modal-md').removeClass('modal-sm');
            $durationWrapper.show()
        } else {
            $modal.addClass('modal-sm').removeClass('modal-md');
            $durationWrapper.hide()
        }
    });
    
    $('#clientsSchedule form input[type="date"]').on('change', function(e){
        var $this = $(this);
        var $type = $this.attr('type');
        var $default = $this.data('defaultValue');
        var $val = $this.val();
        
        if($type == 'text' && ! /^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$/.test($val)) {
            $this.val($default);
        }
    });
    
    $('#clientsSchedule [data-callback="ok"]').on('click', function(e){
        var $form = $('#clientsSchedule form');
        var $modal = $('#clientsSchedule');
        
        var schedule = {
            clientId:       $form.data('clientId'),
            period:         $form.find('select').val(),
            isMin:          $form.find('input[name="isMinVal"]').prop('checked') ? 1 : 0,
            isDuration:     $form.find('input[name="isDuration"]').prop('checked') ? 1 : 0,
            durationStart:  $form.find('input[name="durationStart"]').val(),
            durationFinish: $form.find('input[name="durationFinish"]').val(),
        };
        
        clientsPreloaderEnable();
        
        $.when( clientsScheduleUpdate(schedule) ).done(function(schedule) {
            $('#drawTblClient' + schedule.clientId).find('.btn-client-schedule')
                .data('clientPeriod', schedule.period)
                .data('clientIsMin', schedule.isMinVal ? 'true' : 'false')
                .data('clientIsDuration', schedule.isDuration ? 'true' : 'false')
                .data('clientDurationStart', schedule.durationStart)
                .data('clientDurationFinish', schedule.durationFinish);
                var btnData = 'data-client-period="' + schedule.period + '"' +
                              'data-client-is-min="' + (schedule.isMinVal == '1' ? 'true' : 'false') + '"' +
                              'data-client-is-duration="' + (schedule.isDuration ? 'true' : 'false') + '"';
                if(schedule.isDuration) {
                    btnData += 'data-client-duration-start="' + schedule.durationStart + '"' +
                                  'data-client-duration-finish="' + schedule.durationFinish + '"';
                }
                var btn_html = '<button type="button" class = "btn btn-link" style="padding: 0; text-decoration: none!important;"' +
                                        btnData + '>' + schedulePeriodsCodes[schedule.period] + '</button>';
            $('#drawTblClient' + schedule.clientId).find('.cp-schedule > span').html(btn_html);
        }).always(function() {        
            clientsPreloaderDisable();
        });

        $modal.modal('hide');
    });
    
    $('#draw_clients_pagination>li>a').on('click', function(e){        
        e.preventDefault(); 
        clientsPreloaderEnable();
        
        var pageNum = $(this).data("page");
        
        $('#draw_clients_pagination>li').removeClass('active');
        $(this).parent('li').addClass('active');
                
        $.ajax({
            url: '<?= URL::site('/ajax/get_draw_clients_list') ?>',
            data: {page: pageNum},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                updateClientsTable(data.list);
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function() {
                clientsPreloaderDisable();
            }
        });
    });
    
    $('#autodraw_pagination').on('click', 'ul.autodraw-pagination>li>a', function(e){        
        e.preventDefault(); 
        autodrawPreloaderEnable();
        
        var pageNum = $(this).data("page");
        
        $.when( autodrawLogUpdate(pageNum) ).always(function() {
            autodrawPreloaderDisable();
        });
    });
    
    $('#draw_templates_wrapper').on('click', '.btn-submit', function(e){ 
        e.preventDefault(); 
        
        var $this = $(this);
        var $form = $this.closest('.form-draw');
        
        $form.data('action', $this.data('action'));        
        $form.find('button[type="submit"]').trigger('click');
    });
    
    $('#draw_templates_wrapper').on('submit', '.form-draw', function(e){ 
        e.preventDefault();
        
        var $this = $(this);
        
        var readyData, drawData = [], clientThemes = [];
        
        $this.find('input[type="text"], input[type="radio"]:checked, input[type="checkbox"]:checked').each(function(){
            var $this = $(this);
            if($this.attr('name') == 'who') {
                drawData.name = $this.val();
            } else if($this.attr('name') == 'what[]') {
                clientThemes.push($this.val());
            } else {
                drawData[$this.attr('name')] = $this.val();
            }
        });
        $this.find('select').trigger('change');
        
        drawData.type = 'tpl';
        drawData.tplId = $this.data('id');
        drawData.themes = clientThemes;
        drawData.formObj = $this;
        
        readyData = autodrawCalculateTaskData(drawData);
        
        if($this.data('action') == 'draw') {
            autodrawSendTask(drawData, readyData);
        } else {
            autodrawSaveTask(drawData, readyData);
        }
        //console.log($this.data('action'));
    });
    
    $('#draw_templates_wrapper > ol.breadcrumb').on('click', 'li:eq(0) > a', function(e){        
        e.preventDefault(); 
        
        $('#draw_templates_wrapper>.subtab-content').hide().first().show();
        generateTplBreadcrumbs();            
    });
    
    $('#draw_templates_wrapper > ol.breadcrumb').on('click', 'li:eq(1) > a', function(e){        
        e.preventDefault(); 
        
        var subtabContentId = '#draw_template_cat' + $(this).data('catId');
        
        $('#draw_templates_wrapper>.subtab-content').hide().filter(subtabContentId).show();
        generateTplBreadcrumbs($(this).text());
    });
    
    $('#draw_templates_wrapper > div.breadcrumb-sm').on('click', 'a:eq(0)', function(e){        
        e.preventDefault(); 
        
        $('#draw_templates_wrapper > ol.breadcrumb>li:eq(0) > a').trigger('click');          
    });
    
    $('#draw_templates_wrapper > div.breadcrumb-sm').on('click', 'a:eq(1)', function(e){        
        e.preventDefault(); 
        
        $('#draw_templates_wrapper > ol.breadcrumb>li:eq(1) > a').trigger('click');          
    });
    
    $('#draw_templates_wrapper').on('change', '.form-draw .form-input-mode', function(e){        
        e.preventDefault();  
        
        var $this = $(this);        
        var elm = $(this).closest('.form-draw').find('.theme-specials').first();
        
        ($this.val() == 'special') ? elm.css('position', 'static') : elm.css('position', 'absolute');
    });
    
    $('.templates-cat').on('click', function(e){        
        e.preventDefault(); 
        var catId = $(this).data('templatesCatId'); 
        var catTitle = $(this).data('templatesCatTitle');
        
        if($('#draw_templates_wrapper>div').is('#draw_template_cat' + catId)) {
            generateTplBreadcrumbs(catTitle);            
            $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template_cat' + catId).show();
        } else {
            templatesPreloaderEnable();
            $.ajax({
                url: '<?= URL::site('/ajax/get_category_templates') ?>',
                data: {cat: catId},
                type:     'POST',
                dataType: 'json',
                success:  function(data){
                  if(data.status == 'ok') {
                    generateCatTemplatesList(catId, catTitle, data.info);
                    generateTplBreadcrumbs(catTitle);
                    $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template_cat' + catId).show();
                  }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                },
                complete: function() {
                  templatesPreloaderDisable();
                }
            });
        }
    });
    
    $('#draw_templates_wrapper').on('click', '.drawtemplate', function(e){        
        e.preventDefault(); 
        var id = $(this).data('templateId'); 
        var title = $(this).data('templateTitle');
        var catId = $(this).data('templatesCatId'); 
        var catTitle = $(this).data('templatesCatTitle');
        
        if($('#draw_templates_wrapper>div').is('#draw_template' + id)) {
            generateTplBreadcrumbs({id: catId, title: catTitle}, title);             
            $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template' + id).show();
        } else {
            templatesPreloaderEnable();
            $.ajax({
                url: '<?= URL::site('/ajax/get_template_form') ?>',
                data: {template: id},
                type:     'POST',
                dataType: 'json',
                success:  function(data){
                  if(data.status == 'ok') {
                    if(data.info.type == <?=Model_DrawTemplate::TYPE_SIMPLE;?>) { 
                        generateTemplate(id, title, data.info.themes);
                    } else if(data.info.type == <?=Model_DrawTemplate::TYPE_USER_0_100;?>) {
                        generateTemplateUser0100(id, title);
                    }
                    generateTplBreadcrumbs({id: catId, title: catTitle}, title); 
                    $('#draw_templates_wrapper>.subtab-content').hide().filter('#draw_template' + id).show();
                  }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                },
                complete: function() {
                  templatesPreloaderDisable();
                }
            });
        }
    });  
    
    $('#draw_tabs').on('click', '.btn-autodraw-re', function(e){        
        e.preventDefault();
        var $this = $(this);
        //var $drawId = $this.attr('data-autodraw-id');
        
        $this.button('loading');
        
        $.when( autodrawRe($this.data('autodrawId')) ).done(function(data) {
            $('#draw_logs>a').trigger('click');
            autodrawLogUpdate(1);
        }).always(function() {
            $this.button('reset');
        });
    });
    
    $('#draw_log').on('click', '.btn-autodraw-f5', function(e){        
        e.preventDefault();
        var $this = $(this);
        
        $this.button('loading');
        
        $.when( autodrawStatusUpdate($this.data('autodrawId')) ).always(function() {
            $this.button('reset');
        });
    });
    
    $('#draw_log').on('click', '.btn-autodraw-open', function(e){        
        e.preventDefault();
        var $this = $(this);
        
        $this.button('loading');
        
        $.when( autodrawGetData($this.data('autodrawId')) ).done(function(data) {
            data.target = $this.data('target');
            generateDraw(data);
        }).always(function() {
            $this.button('reset');
        });
    });
    
    $('#draw_log').on('click', '.btn-autodraw-delete', function(e){        
        e.preventDefault();
        var $this = $(this);
        
        $this.button('loading');
        
        $.when( autodrawDelete($this.data('autodrawId')) ).fail(function() {
            $this.button('reset');
        });
    });
    
    $('#draw_log_delete').on('submit', function(e){        
        e.preventDefault();
        var $this = $(this);
        var $select = $(this).find('select');
        var $btn = $(this).find('button');
        
        $btn.button('loading');
        
        $.when( autodrawGroupDelete($select.val()) ).always(function() {
            $btn.button('reset');
        });
    });
    
    /**
      * AutoDraw Modules
      */
    
    var autodrawModuleCardCreate = function(moduleId, serializedData) {
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_module_card_create') ?>',
            data: 'module=' + moduleId + '&' + serializedData,
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                createNoty('Карточка сохранена!', 'success', 5);
                dfd.resolve();
              } else {
                createNoty('Ошибка! Не удалось создать карточку!', 'danger', 5);
                dfd.reject();
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        
        return dfd.promise();
    }
    
    var autodrawModuleCardChangeStatus = function(cardId, action) {
        var dfd = new jQuery.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/ajax/autodraw_module_card_status_change') ?>',
            data: {card: cardId, status: action},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
              if(data.status == 'ok') {
                if(action == 'activate') {
                    createNoty('Карточка запущена в работу!', 'success', 5);
                } else if(action == 'pause') {
                    createNoty('Работа карточки приостановлена!', 'success', 5);
                } else {
                    createNoty('Карточка удалена!', 'success', 5);
                }
                dfd.resolve();
              } else {
                createNoty('Действие привело к неопознанной ошибке!', 'danger', 5);
                dfd.reject();
              }
            },
            error: function(xhr, ajaxOptions, thrownError){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                dfd.reject();
            }
        });
        
        return dfd.promise();
    }
    
    var autodrawModuleCardDelete = function(cardId) {
    
        bootbox.confirm({
            size: "small",
            title: "Удалить карточку?",
            message: "Это действие необратимо.",
            buttons: {
                confirm: {
                    label: 'Удалить',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    modulesPreloaderEnable();

                    $.when( autodrawModuleCardChangeStatus(cardId, 'delete') ).done(function(data) { 
                        $('#draw_module_card' + cardId).remove();
                    }).always(function() {        
                        modulesPreloaderDisable();
                    });
                }
            }
        });
            
    }
    
    var modulesPreloaderEnable = function() {
        $('#draw_modules .content-preloader').addClass('active');
    }
    
    var modulesPreloaderDisable = function() {
        $('#draw_modules .content-preloader').removeClass('active');
    }
    
    var generateModuleCardsList = function(modId, modTitle, data) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_module' + modId
        });
        var table = $('<table />').attr({class: 'table table-striped table-panels'});
        var tbody = $('<tbody />');
        
        $.each(data.cards, function( id, card ) {
            var tr = $('<tr />').attr({id: 'draw_module_card' + id});
            var td = $('<td />');
            if(card.status == <?=Model_DrawModuleCard::STATUS_ENABLED?>) {
                var activateBtnStyle = 'display: none';
                var pauseBtnStyle = '';
                var label = '<span class="label label-module-card label-module-card-enable">Работает</span>';
            } else {
                var activateBtnStyle = '';
                var pauseBtnStyle = 'display: none';
                var label = '<span class="label label-module-card label-module-card-pause">На паузе</span>';
            }
            td.append(label);
            $.each(card.data, function( i, option ) {
                td.append('<div class="col-xs-12 col-lg-6"><strong>' + data.options[i].label + ':</strong> ' + option + '</div>');
            });
            td.append('<div class="col-xs-12 col-lg-12 cp-buttons"><a href="#" class="btn btn-lgreen btn-action btn-action-switch" title="Запустить" data-action="activate" data-card-id="' + id + '" style="' + activateBtnStyle + '">Запустить</a><a href="#" class="btn btn-lorange btn-action btn-action-switch" title="Приостановить" data-action="pause" data-card-id="' + id + '" style="' + pauseBtnStyle + '">Приостановить</a><a href="#" class="btn btn-lred btn-action" title="Удалить" data-action="delete" data-card-id="' + id + '">Удалить</a></div>');
            tr.append(td).appendTo(tbody);
        });
        
        table.append(tbody).appendTo(wrapper);
        wrapper.append('<div class="list-group"><div class="list-group-item"><div class="container"><div class="col-xs-12 col-md-6 col-md-offset-3 text-center"><center><a class="btn btn-primary btn-lg btn-block btn-card-add" href="#" role="button" data-module-id="' + modId + '" data-module-title="' + modTitle + '" id="draw_module' + modId + '_card_add">Добавить карточку</a></center></div></div></div></div>').appendTo($('#draw_modules_wrapper'));
        $('#draw_module' + modId + '_card_add').data('options', data.options);
    }
    
    var generateModuleCardCreateForm = function(modId, options) {
        
        var wrapper = $('<div />').css('display', 'none').attr({
            class: 'subtab-content', 
            id: 'draw_module' + modId + '_card_create'
        });
        
        wrapper.append($('#drawModuleForm').html()).appendTo($('#draw_modules_wrapper'));
        $('#draw_module' + modId + '_card_create .form-draw').first().data({'id': modId}).attr({'id': 'form-draw-module' + modId});
        $('#draw_module' + modId + '_card_create .panel-body').first().is(function(i){
            var elm = $(this);
            $.each(options, function( i, option ) {
                if(option.type == 'date') {
                    var inputHtml = generateModuleCardCreateFormDateInput(modId, option, i);
                    elm.append(inputHtml);
                    initModuleCardCreateFormDateInput(modId, i);
                } else if(option.type == 'timezone') {
                    var inputHtml = generateModuleCardCreateFormTimezoneInput(modId, option, i);
                    elm.append(inputHtml);
                    initModuleCardCreateFormDateInput(modId, i);
                } else {
                    var inputHtml = generateModuleCardCreateFormTextInput(modId, option, i);
                    elm.append(inputHtml);
                }
            });
        });
    }
    
    var generateModuleCardCreateFormTextInput = function(modId, option, i) {
        
        var wrapper = $('<div />').attr({class: 'col-xs-12 col-md-6 form-group'}); 
        
        var input = $('<input />').attr({
            class: 'form-control', 
            id: 'draw_module' + modId + '_card_text_input' + i,
            name: option.name, 
            required: 'required', 
            type: 'text'
        });  
        
        wrapper.append('<label>' + option.label + '</label>');
        wrapper.append(input);
        
        return wrapper;
    }
    
    var generateModuleCardCreateFormDateInput = function(modId, option, i) {
        
        var wrapper = $('<div />').attr({class: 'col-xs-12 col-md-6 form-group'}); 
        
        var input = $('<input />').attr({
            class: 'form-control', 
            id: 'draw_module' + modId + '_card_date_input' + i,  
            max: __GLOBALS__.nowDate, 
            min: '1900-01-01',
            name: option.name, 
            required: 'required', 
            type: 'date'
        });  
        
        wrapper.append('<label>' + option.label + '</label>');
        wrapper.append(input);
        
        return wrapper;
    }
    
    var generateModuleCardCreateFormTimezoneInput = function(modId, option, i) {
        
        var wrapper = $('<div />').attr({class: 'col-xs-12 col-md-6 form-group'}); 
        
        var input = $('<select />').attr({
            class: 'form-control', 
            id: 'draw_module' + modId + '_card_date_input' + i, 
            name: option.name,  
            required: 'required'
        });  
        
        for (var tz in __GLOBALS__.timeZones) {
            var opt = $('<option />').val(tz).text(__GLOBALS__.timeZones[tz]); 
            if(tz == '3') {
                opt.attr('selected', true);
            }
            input.append(opt);
        }
        
        wrapper.append('<label>' + option.label + '</label>');
        wrapper.append(input);
        
        return wrapper;
    }
    
    var isLeapYear = function(year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }
    
    var getDaysInMonth = function(year, month) {
        year = Number(year);
        month = Number(month);
        
        var months31 = new Array(1, 3, 5, 7, 8, 10, 12);
        
        if(month == 2)
            return isLeapYear(year) ? 29 : 28;
        
        return (months31.indexOf(month) != -1) ? 31 : 30;
    }
    
    var generateDateInputFallbackDaysSelectOptions = function(year, month, _default) {
        year = Number(year);
        month = Number(month);
        _default = typeof _default !== 'undefined' ?  Number(_default) : 1;
        
        var max = getDaysInMonth(year, month);
        var options = '';
        
        if(max < _default)
            _default = max;
        
        for (i = 1; i <= max; i++) {
            var val = (i < 10) ? '0' + i : String(i);
            options += '<option value="' + val + '"' + ((i == _default) ? ' selected="selected"' : '') +  '>' + val + '</option>';
        }
        
        return options;
    }
    
    var generateDateInputFallbackMonthsSelectOptions = function(_default) {        
        _default = typeof _default !== 'undefined' ?  Number(_default) : 1;
        
        var months = {
            '01': 'Январь',
            '02': 'Февраль',
            '03': 'Март',
            '04': 'Апрель',
            '05': 'Май',
            '06': 'Июнь',
            '07': 'Июль',
            '08': 'Август',
            '09': 'Сентябрь',
            '10': 'Октябрь',
            '11': 'Ноябрь',
            '12': 'Декабрь',
        };
        
        var options = '';
        
        for (i = 1; i <= 12; i++) {
            var val = (i < 10) ? '0' + i : String(i);
            options += '<option value="' + val + '"' + ((i == _default) ? ' selected="selected"' : '') +  '>' + months[val] + '</option>';
        }
        
        return options;
    }
    
    var generateDateInputFallbackYearsSelectOptions = function(max, count, _default) {
        max = Number(max);
        count = typeof count !== 'undefined' ?  Number(count) : 100;
        _default = typeof _default !== 'undefined' ?  Number(_default) : max;
        
        var options = '';
        
        for (i = max - count; i <= max; i++) {
            options += '<option value="' + i + '"' + ((i == _default) ? ' selected="selected"' : '') +  '>' + i + '</option>';
        }
        
        return options;
    }
    
    var initDateInputFallback = function(id) {
        var dateArr = __GLOBALS__.nowDate.split('-');
        
        var $inputGroupDate = $('#' + id);
        var $yearsSelect = $inputGroupDate.children('.form-control-date-year').first();
        var $monthSelect = $inputGroupDate.children('.form-control-date-month').first();
        var $daysSelect = $inputGroupDate.children('.form-control-date-day').first();
        
        $yearsSelect.append(generateDateInputFallbackYearsSelectOptions(dateArr[0]));
        $monthSelect.append(generateDateInputFallbackMonthsSelectOptions(dateArr[1]));
        $daysSelect.append(generateDateInputFallbackDaysSelectOptions(dateArr[0], dateArr[1], dateArr[2]));
    }
    
    var onChangeDateInputFallbackTrigger = function(modId, optionIndex, param) {
        var $input = $('#draw_module' + modId + '_card_date_input' + optionIndex);
        
        var $inputGroupDate = $('#draw_module' + modId + '_card_date_input_group' + optionIndex);
        var $yearsSelect = $inputGroupDate.children('.form-control-date-year').first();
        var $monthSelect = $inputGroupDate.children('.form-control-date-month').first();
        var $daysSelect = $inputGroupDate.children('.form-control-date-day').first();
        
        var year = $yearsSelect.val();
        var month = $monthSelect.val();
        var day = $daysSelect.val();
        
        if(param == 'month' || (param == 'year' && month == '02')) {
            $daysSelect.empty().append(generateDateInputFallbackDaysSelectOptions(year, month, day));
            day = $daysSelect.val();
        }
        
        var dateValue = $yearsSelect.val() + '-' + $monthSelect.val() + '-' + $daysSelect.val();
        
        $input.val(dateValue);
        
        console.log($('#draw_module1_card_date_input1').val());
    }
    
    var initModuleCardCreateFormDateInputFallback = function(modId, i) {
        
        var input = $('#draw_module' + modId + '_card_date_input' + i);  
        input.attr('style', 'display: none').val(__GLOBALS__.nowDate);
        var inputWrapper = $('<div />').attr({
            class: 'input-group-date', 
            id: 'draw_module' + modId + '_card_date_input_group' + i
        });  
                        
        inputWrapper.append('<select class="form-control form-control-date form-control-date-day" data-module-id="' + modId + '" data-module-option-index="' + i + '" data-param="day"></select>');                
        inputWrapper.append('<select class="form-control form-control-date form-control-date-month" data-module-id="' + modId + '" data-module-option-index="' + i + '" data-param="month"></select>');                
        inputWrapper.append('<select class="form-control form-control-date form-control-date-year" data-module-id="' + modId + '" data-module-option-index="' + i + '" data-param="year"></select>');
        
        input.after(inputWrapper);
        
        initDateInputFallback('draw_module' + modId + '_card_date_input_group' + i);
    }
    
    var initModuleCardCreateFormDateInput = function(modId, i) {
        var browserSupportForm = $('#browserInputTypeSupportTest');
        var inputDate = browserSupportForm.children('input[name="date"]').first();
        
        if(inputDate.attr('type') == 'text') {
            initModuleCardCreateFormDateInputFallback(modId, i);
        }
    }
    
    var generateModuleBreadcrumbs = function(mod, card) {
        
        var breadcrumbs = $('#draw_modules_wrapper>.breadcrumbs');
        
        if(mod) {
            var lgBreadcrumb = breadcrumbs.filter('.breadcrumb');
            var smBreadcrumb = breadcrumbs.filter('.breadcrumb-sm').find('.breadcrumb-list-group .col-xs-12');
            var smBreadcrumbH = breadcrumbs.filter('.breadcrumb-sm').find('.breadcrumb-list-group-header h3');
            
            lgBreadcrumb.empty();
            smBreadcrumb.empty();
            smBreadcrumbH.empty();
        
            $('<li />').append($('<a />').attr({href: '#'}).text('Модули')).appendTo(lgBreadcrumb);
            if(typeof mod == "object") {
                if(mod.id !== undefined) {
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('Модули').appendTo(smBreadcrumb);
                    var a = $('<a />').attr({href: '#', 'data-mod-id': mod.id}).text(mod.title);
                    $('<li />').append(a).appendTo(lgBreadcrumb);
                    $('<span />').text('|').appendTo(smBreadcrumb);
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text(mod.title).appendTo(smBreadcrumb);
                    if(card) {            
                        smBreadcrumbH.text(card);
                        $('<li />').addClass('active').text(card).appendTo(lgBreadcrumb);
                    }
                } else {
                    $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('К списку модулей').appendTo(smBreadcrumb);
                    smBreadcrumbH.text(mod.title);
                    $('<li />').addClass('active').text(mod.title).appendTo(lgBreadcrumb);
                }
            } else {
                $('<a />').attr({href: '#', 'class': 'btn btn-link'}).text('К списку модулей').appendTo(smBreadcrumb);
                smBreadcrumbH.text(mod);
                $('<li />').addClass('active').text(mod).appendTo(lgBreadcrumb);
            }
            
            breadcrumbs.removeClass('hidden');
        } else {
            breadcrumbs.addClass('hidden');
        }
    }
    
    $('#draw_modules_wrapper').on('click', '.btn-card-add', function(e){
        e.preventDefault();
        var $this = $(this);
        
        var modId = $this.data('moduleId');
        var modTitle = $this.data('moduleTitle');
        var options = $this.data('options');
        
        generateModuleBreadcrumbs({id: modId, title: modTitle}, 'Добавление карточки');
        
        if(! $('#draw_modules_wrapper>div').is('#draw_module' + modId + '_card_create')) {
            generateModuleCardCreateForm(modId, options);
        }      
        $('#draw_modules_wrapper>.subtab-content').hide().filter('#draw_module' + modId + '_card_create').show();
    });
    
    $('#draw_modules_wrapper').on('submit', '.form-draw', function(e){ 
        e.preventDefault();
        
        var $this = $(this);
        
        var moduleId = $this.data('id');
        var cardData = $this.serialize();
        
        var $drawModuleListItemCntrLink = $('#draw_modules_list_item' + moduleId + ' .draw-module-decl-href').first();
        
        modulesPreloaderEnable();
        
        $.when( autodrawModuleCardCreate(moduleId, cardData) ).done(function() { 
            var modCardsCntr = Number($drawModuleListItemCntrLink.data('cardsCnt'));
            var declExpr = ['<small>карточка</small>', '<small>карточки</small>', '<small>карточек</small>']; 
            $('#draw_modules_wrapper>.subtab-content').filter('#draw_module' + moduleId).remove();
            $drawModuleListItemCntrLink.data('cardsCnt', modCardsCntr + 1).html(decl(modCardsCntr + 1, declExpr)).trigger('click');  
            $this.trigger("reset");
        }).always(function() {        
            modulesPreloaderDisable();
        });
    }); 
    
    $('#draw_modules_wrapper').on('change', '.form-control-date', function(e){
        e.preventDefault();
        var $this = $(this);
        
        var modId = $this.data('moduleId');
        var optionIndex = $this.data('moduleOptionIndex');
        var param = $this.data('param');
        
        onChangeDateInputFallbackTrigger(modId, optionIndex, param);
    });
    
    $('#draw_modules_wrapper').on('click', '.btn-action', function(e){
        e.preventDefault();
        var $this = $(this);
        
        var cardId = $this.data('cardId');
        var action = $this.data('action');
        
        if(action == 'delete') {
            autodrawModuleCardDelete(cardId);
        } else {
            modulesPreloaderEnable();

            $.when( autodrawModuleCardChangeStatus(cardId, action) ).done(function(data) { 
                $('#draw_module_card' + cardId + ' .btn-action-switch').show().filter('[data-action="' + action + '"]').hide();
            }).always(function() {        
                modulesPreloaderDisable();
            });
        }
    });
    
    $('.draw-module-href').on('click', function(e){        
        e.preventDefault(); 
        var modId = $(this).data('drawModuleId'); 
        var modTitle = $(this).data('drawModuleTitle');
        
        if($('#draw_modules_wrapper>div').is('#draw_module' + modId)) {
            generateModuleBreadcrumbs(modTitle);            
            $('#draw_modules_wrapper>.subtab-content').hide().filter('#draw_module' + modId).show();
        } else {
            modulesPreloaderEnable();
            $.ajax({
                url: '<?= URL::site('/ajax/autodraw_module_get_cards') ?>',
                data: {module: modId},
                type:     'POST',
                dataType: 'json',
                success:  function(data){
                  if(data.status == 'ok') {
                    generateModuleCardsList(modId, modTitle, data.info);
                    if(data.info.length > 0) {
                        generateModuleBreadcrumbs(modTitle);
                        $('#draw_modules_wrapper>.subtab-content').hide().filter('#draw_module' + modId).show();
                    } else {
                        $('#draw_module' + modId + ' .btn-card-add').trigger('click');
                    }
                  }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                },
                complete: function() {
                    modulesPreloaderDisable();
                }
            });
        }
    });
    
    $('#draw_modules_wrapper > ol.breadcrumb').on('click', 'li:eq(0) > a', function(e){        
        e.preventDefault(); 
        
        $('#draw_modules_wrapper>.subtab-content').hide().first().show();
        generateModuleBreadcrumbs();            
    });
    
    $('#draw_modules_wrapper > ol.breadcrumb').on('click', 'li:eq(1) > a', function(e){        
        e.preventDefault(); 
        
        var subtabContentId = '#draw_module' + $(this).data('modId');
        
        $('#draw_modules_wrapper>.subtab-content').hide().filter(subtabContentId).show();
        generateModuleBreadcrumbs($(this).text());
    });
    
    $('#draw_modules_wrapper > div.breadcrumb-sm').on('click', 'a:eq(0)', function(e){        
        e.preventDefault(); 
        
        $('#draw_modules_wrapper > ol.breadcrumb>li:eq(0) > a').trigger('click');          
    });
    
    $('#draw_modules_wrapper > div.breadcrumb-sm').on('click', 'a:eq(1)', function(e){        
        e.preventDefault(); 
        
        $('#draw_modules_wrapper > ol.breadcrumb>li:eq(1) > a').trigger('click');          
    });
    
    $('#draw_clients').on('click', '.cp-schedule > span > button', function(e){        
        e.preventDefault();
        
        var $this = $(this);
        var $isMin = $this.data('clientIsMin');
        var $isDuration = $this.data('clientIsDuration');
        
        var content = '<p><b>До минимальной нагрузки:</b> ' + ($isMin ? '<span class="text-success">да</span>' : '<span class="text-danger">нет</span>') + '</p>';
        
        if($isDuration){
            var durationStart = genPrettyDateFromISO($this.data('clientDurationStart'));
            var durationFinish = genPrettyDateFromISO($this.data('clientDurationFinish'));
            content = '<p><b>Начало периода:</b> ' + durationStart + '</p>' +
                      '<p><b>Конец периода:</b> ' + durationFinish + '</p>' + content;
        }
        
        $(this).popover({
            'trigger': 'focus',
            'placement': 'auto',
            'html': true,
            'content': content,
            'container': '#draw_clients'
        }).popover('toggle');
    });
});