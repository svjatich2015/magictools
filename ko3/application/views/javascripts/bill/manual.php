jQuery(function($) {
    $(".pay-accept").on("click", function(e){
        e.preventDefault();
        
        var $this = $(this);
        var $cash = $this.data('cash');
        var $method = $this.data('method');
        
        $this.button('loading');
        
        $.ajax({
            url:      '<?= URL::site('/ajax/pay_confirm') ?>',
            data:     {cash: $cash, method: $method},
            type:     'POST',
            dataType: 'json',
            success: function(data){
                $('#alert').alert('close');
                $('#accordion .collapse').collapse('hide').filter('#collapseThree').collapse('show');
            },
            error: function(data){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function() {
                $this.button('reset');
            }
        });
    });
});