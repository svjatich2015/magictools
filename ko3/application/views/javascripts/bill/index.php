jQuery(function($) {    
    $("#payMethodYandexCard").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', 'https://yoomoney.ru/quickpay/confirm.xml');
        $form.find('input[name="paymentType"]').val('AC');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodYandex").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', 'https://yoomoney.ru/quickpay/confirm.xml');
        $form.find('input[name="paymentType"]').val('PC');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodSBRF").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/sbrf')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodPayPal").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/paypal')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodProdamusCardEUR").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/prodamus')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodProdamusCardUSD").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/prodamus')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodProdamusCardKZT").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/prodamus')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodCryptocloud").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/cryptocloud')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodBalance").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/bill/' . $bill->id . '/pay_from_balance')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
});