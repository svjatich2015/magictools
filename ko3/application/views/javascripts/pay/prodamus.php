jQuery(function($) {
    $("#inputSum").on("change", function(){
        var $this = $(this);
        var $cash = parseInt($this.val(), 10);
        
        if(isNaN($cash) || $cash < 1000) {
            $this.val(1000).triggerHandler('change');
            return;
        }
        
        var $form = $('#accountPay');
        var $dest = 'MagicTools. Пополнение баланса на ' + $cash + ' руб.';
        var $labl = 'MagicTools::' + $form.data('accountId') + '::' + $cash;
        var $ysum = Math.ceil($cash * 102.041) / 100;
        
        $form.find('input[name="formcomment"]').val($dest);
        $form.find('input[name="short-dest"]').val($dest);
        $form.find('input[name="label"]').val($labl);
        $form.find('input[name="cash"]').val($cash);
        $form.find('input[name="sum"]').val($ysum);
    });
    
    $("#payMethodYandex").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', 'https://money.yandex.ru/quickpay/confirm.xml');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodSBRF").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/sbrf')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodPayPal").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/paypal')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodProdamusCard").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/prodamus')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodProdamusYandex").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/prodamus')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
    
    $("#payMethodProdamusSberOnline").on("click", function(){
        var $this = $(this);
        var $form = $('#accountPay');
        
        $form.attr('action', '<?=URL::site('/pay/prodamus')?>');
        $form.find('input[type="radio"]').prop('checked', false);
        $this.prop('checked', true)
    });
});