jQuery(function($) {
    $(".pay-accept").on("click", function(e){
        e.preventDefault();
        
        var $this = $(this);
        var $bill = $this.data('bill');
        var $cash = $this.data('cash');
        var $method = $this.data('method');
        var data = {cash: $cash, method: $method};
        
        if($bill !== undefined) {
            data.bill = $bill;
        }
        
        $this.button('loading');
        
        $.ajax({
            url:      '<?= URL::site('/ajax/pay_confirm') ?>',
            data:     data,
            type:     'POST',
            dataType: 'json',
            success: function(data){
                $('#alert').alert('close');
                $('#accordion .collapse').collapse('hide').filter('#collapseThree').collapse('show');
            },
            error: function(data){
                createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
            },
            complete: function() {
                $this.button('reset');
            }
        });
    });
});