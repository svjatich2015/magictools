jQuery(function($) {    
    $(".post-status-edit").on("change", function(e){
        e.preventDefault();
        
        var $this = $(this);
        var $postId = $this.data("postId");
        var $oldVal = ($this.val() == 1) ? 0 : 1;
        var $title = ($this.val() == 1) ? 'Опубликовать?' : 'Снять с публикации?';
        var $text = ($this.val() == 1) ? 'Пост станет виден всем остальным.' : 'Пост перестанет быть виден всем остальным.';
    
        bootbox.confirm({
            size: "small",
            title: $title,
            message: $text,
            buttons: {
                confirm: {
                    label: 'Подтвердить',
                    className: 'btn-default'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    createNoty('Изменение статуса поста...', 'info', 5);
                
                    $.ajax({
                        url:      '<?= URL::site('/admin/ajax/blog_post_edit_status') ?>',
                        data:     {id: $postId, published: $this.val()},
                        type:     'POST',
                        dataType: 'json',
                        success: function(data){
                            createNoty('Статус поста изменен.', 'info', 5);
                        },
                        error: function(data){
                            createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                        }
                    });
                } else {
                    $this.val($oldVal);
                }
            }
        }); 
    });
    
    $(".post-delete").on("click", function(e){
        e.preventDefault();
        
        var $this = $(this);
        var $postId = $this.data("postId");
        
        $this.button('loading');
    
        bootbox.confirm({
            size: "small",
            title: "Удалить пост?",
            message: "Впоследствии вы сможете восстановить его из корзины.",
            buttons: {
                confirm: {
                    label: 'Удалить',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    createNoty('Идет удаление поста...', 'info', 5);
                
                    $.ajax({
                        url:      '<?= URL::site('/admin/ajax/blog_post_trash') ?>',
                        data:     {id: $postId},
                        type:     'POST',
                        dataType: 'json',
                        success: function(data){
                            window.location.reload();
                        },
                        error: function(data){
                            createNoty('Ошибка! Нет связи с сервером!', 'danger', 5);
                        },
                        complete: function() {
                            $this.button('reset');
                        }
                    });
                } else {
                    $this.button('reset');
                }
            }
        }); 
    });
});