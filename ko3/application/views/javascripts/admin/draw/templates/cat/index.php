jQuery(function($) {
    $(".template-delete").on("click", function(){
        event.preventDefault();
        var tplId = $(this).data("templateId");
    
        bootbox.confirm({
            size: "small",
            title: "Удалить шаблон?",
            message: "Это действие необратимо.",
            buttons: {
                confirm: {
                    label: 'Удалить',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#tplDelete' + tplId).submit();
                }
            }
        }); 
    });
});