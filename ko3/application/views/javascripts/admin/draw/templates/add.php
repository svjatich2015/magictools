jQuery(function($) {

  var data = [<?php foreach ($themes as $theme) {echo '{label: "' . $theme->title . '", category: "Похожие названия:"}, ';} ?>]

  var inputWhatCnt = $('.form-input-what').length;
  
  $('#inputWhatAdd').on('click', function(){
      var cnt = inputWhatCnt;
      var val = $('#inputWhat').val();
  
      $('#inputWhatFormGroup').before('<div class="col-xs-12 col-md-6 form-group form-input-what" id="inputWhatFormGroup' + cnt + '"><label for="inputWhat' + cnt + '">Что отрисовываем?</label><div class="input-group"><input type="text" name="what[]" class="form-control" id="inputWhat' + cnt + '" placeholder="Тема для отрисовки" value="' + val + '"><span class="input-group-btn"><button class="btn btn-danger btn-input-what-remove" type="button" data-input-what-id="' + cnt + '"><i class="glyphicon glyphicon-minus"></i></button></span></div></div>');
      
      $('#inputWhat' + cnt).catcomplete({delay: 0, source: data});
    
      $('#inputWhat').val(null);
    
      inputWhatCnt++;
  });
  
  $('#draw').delegate('.btn-input-what-remove', 'click', function(){
        $('#inputWhatFormGroup' + $(this).data('inputWhatId')).remove();
  });  
  
    $('.form-textarea-counter > textarea[maxlength]').after(function(i){
        return '<span class="help-block help-block-counter" style="font-size: smaller; margin: 1px; text-align: right;"><span class="cnt">0</span> / ' + $(this).attr('maxlength')  + '</span>';
    });

    $('.form-textarea-counter > textarea[maxlength]').on('change keyup keydown', function(){
        $('.form-textarea-counter > .help-block-counter > .cnt').text($(this).val().length);
    }).trigger('change');
  
  $('#draw input[name="type"]').on("click", function(){
        if($(this).val() == 1) {
            $('.form-input-what').show();
        } else {
            $('.form-input-what').hide();
        }
  });
  
  $( function() {
    $.widget( "custom.catcomplete", $.ui.autocomplete, {
      _create: function() {
        this._super();
        this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
      },
      _renderMenu: function( ul, items ) {
        var that = this,
          currentCategory = "";
        $.each( items, function( index, item ) {
          var li;
          if ( item.category != currentCategory ) {
            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
            currentCategory = item.category;
          }
          li = that._renderItemData( ul, item );
          if ( item.category ) {
            li.attr( "aria-label", item.category + " : " + item.label );
          }
        });
      }
    });
 
    $('.form-input-what>.input-group>input').each(function(i,elem) {
        $(this).catcomplete({delay: 0, source: data});
    });
  });
  
});