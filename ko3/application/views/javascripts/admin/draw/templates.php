jQuery(function($) {
    $(".template-cat-delete").on("click", function(){
        event.preventDefault();
        var tplCatId = $(this).data("templateCatId");
    
        bootbox.confirm({
            size: "small",
            title: "Удалить категорию?",
            message: "Также будут удалены все шаблоны в этой категории.",
            buttons: {
                confirm: {
                    label: 'Удалить',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#templateCatDelete' + tplCatId).submit();
                }
            }
        }); 
    });
});