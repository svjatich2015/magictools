jQuery(function($) {
    var todayMidnight = <?=strtotime('tomorrow midnight') - 1;?>;

    $(".account-increase").on("click", function(e){
        e.preventDefault();
        
        var $this = $(this);
        var $data = {
            accountId:  $this.data('accountId'),
            email:      $this.data('accountEmail'),
            name:       $this.data('profileName'),
            surname:    $this.data('profileSurname'),
            balance:    $this.data('accountBalance'),
            method:     null,
            amount:     null
        }
        
        bootbox.prompt({
            size: "small",
            title: "Как пополнять?",
            inputType: 'select',
            inputOptions: [
                {text: '--- Выберите способ ---', value: ""},
                {text: 'Бесплатно', value: 'free'},
                {text: 'Сбербанк', value: 'sbrf'},
                {text: 'PayPal', value: 'paypal'},
                {text: 'Яндекс.Деньги', value: 'yamoney'},
                {text: 'Наличные', value: 'cash'},
                {text: 'Другой способ', value: 'custom'}
            ],
            callback: function (result) {
                if(result === null) return true;
                if(result === '') return false;
                $data.method = result;
                increaseRequestPeriod($data);
            }
        }); 
        
        return false;
    });
    
    var pseudologin = function(accountId) {
        var dfd = $.Deferred();
        
        $.ajax({
            url: '<?= URL::site('/admin/ajax/user_pseudologin') ?>',
            data: {id: accountId},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    dfd.resolve();
                }
                dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                dfd.reject();
            }
        });
        
        return dfd.promise();
    };

    var increaseRequestPeriod = function(data) {
        
        bootbox.prompt({
            size: "small",
            title: "На какую сумму?",
            inputType: 'number',
            callback: function (result) {
                if(result === null) return true;
                if(result === '') return false;
                data.amount = parseFloat(result.split(',').join('.'));
                data.increase = String(parseFloat(data.balance.split(',').join('.')) + data.amount).split('.').join(',');
                increaseConfirmation(data);
            }
        }); 
    };
    
    var increaseConfirmation = function(data) {
        var $methods = {free: 'Бесплатно', sbrf: 'Сбербанк', paypal: 'PayPal', yamoney: 'Яндекс.Деньги', cash: 'Наличные', custom: 'Другой способ'};
        var $data = '<table><tr><td style="padding-right: 15px;"><strong>ID:</strong></td><td>' + data.accountId + '</td></tr><tr><td style="padding-right: 15px;"><strong>Имя:</strong></td><td>' + data.name + '</td></tr><tr><td style="padding-right: 15px;"><strong>Фамилия:</strong></td><td>' + data.surname + '</td></tr><tr><td style="padding-right: 15px;"><strong>Email:</strong></td><td>' + data.email + '</td></tr><tr><td style="padding-right: 15px;"><strong>Способ:</strong></td><td>' + $methods[data.method] + '</td></tr><tr><td style="padding-right: 15px;"><strong>Сумма:</strong></td><td>' + data.amount + ' руб.</td></tr><tr><td style="padding-right: 15px;"><strong>Баланс:</strong></td><td><nobr>' + data.increase + ' руб.</nobr> <nobr>(после зачисления)</nobr></td></tr></table>';
        
        bootbox.confirm({
            title: "Точно пополняем?",
            message: $data,
            callback: function (result) {
                if(result) {
                    increase(data);
                }
            }
        });
    };
    
    var increase = function(data) {
        var dfd = $.Deferred();
        
        var dialog = bootbox.dialog({
            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Идёт пополнение баланса...</p>',
            closeButton: false
        });
        
        // Если баланс успешно пополнен...
        dfd.done(function(){
            $('#account' + data.accountId + 'Balance').css('color', 'inherit').text(data.increase + ' руб.');
            $('#account' + data.accountId + 'Data a.account-increase').data('accountBalance', data.increase);
        
            bootbox.alert({
                size: "small",
                title: "Пополнение",
                message: 'Баланс аккаунта пополнен!'
            });
        })
        // Если баланс не пополнен ;(
        .fail(function(){
            bootbox.alert({
                size: "small",
                title: "Пополнение",
                message: 'Ошибка! Баланс не пополнен!'
            });
        });
        
        $.ajax({
            url: '<?= URL::site('/admin/ajax/user_increase') ?>',
            data: {id: data.accountId, amount: data.amount, method: data.method},
            type:     'POST',
            dataType: 'json',
            success:  function(data){
                if(data.status == 'ok') {
                    dfd.resolve();
                }
                dfd.reject();
            },
            error: function(xhr, ajaxOptions, thrownError){
                dfd.reject();
            },
            complete: function() {
                // Complete!
                dialog.modal('hide');
            }
        });
    };

    $(".account-edit").on("click", function(e){
        e.preventDefault();
        bootbox.alert({
            size: "small",
            title: "Редактирование",
            message: 'Функция в процессе разработки!'
        });
    });

    $(".account-pseudologin").on("click", function(e){
        e.preventDefault();
        
        var $this = $(this);
        var accountId = $this.data('accountId');
        
        $.when(pseudologin(accountId))
            .done(function(){
                window.location.href = "<?=URL::site('')?>";
            })
            .fail(function(){
                console.log('Что-то пошло не так ;-(');
            });
    });
});