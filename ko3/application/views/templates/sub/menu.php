<div class="navbar-collapse sidebar-navbar-collapse">
                            <ul class="nav navbar-nav" id="sidenav01">
                                <li><br></li>
                                <?php foreach ((array) $menu as $controller => $action) : ?>
                                <?php if(isset($action['sub'])) : ?>
                                <?php $subactions = array(); ?>
                                <?php foreach($action['sub'] as $_action => $subelement) : ?>
                                <?php if(isset($adminArea)) : ?>
                                <?php $controllerfull = 'Controller_Admin_' . ucfirst($controller); ?>
                                <?php $allowed = FALSE; ?>
                                <?php $actionarr = explode(',', $_action); ?>
                                <?php foreach ($actionarr as $aarr) : ?>
                                <?php if (Identity::init()->isStaffAllowed($controllerfull, $aarr)) : ?>
                                <?php $allowed = TRUE; ?>
                                <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if (! $allowed) : continue; endif; ?>
                                <?php endif; ?>
                                <?php $subactions[$_action] = $subelement; ?>
                                <?php endforeach; ?>
                                <?php if (! count($subactions)) : continue; endif; ?>
                                <?php if(! isset($action['access']) || in_array(Identity::init()->getAccountInfo()->status, $action['access'])) : ?>
                                <li>
                                    <a onclick="return false;" href="#"  
                                       data-toggle="collapse" 
                                       data-target="#toggle_<?=$controller?>" 
                                       data-parent="#sidenav01" 
                                       class="<?=($controller == strtolower(Request::initial()->controller())) ? '' : ' collapsed';?>"<?=isset($action['style']) ? ' style="' . $action['style'] . '"' : '';?>>
                                        <span class="glyphicon <?=$action['icon']?>"></span><?=$action['title']?>
                                        <span class="caret pull-right"></span>
                                    </a>
                                    <div id="toggle_<?=$controller?>" <?=($controller == $menuController) ? 'class="collapse in"' : 'style="height: 0px;" class="collapse"';?>>
                                        <ul class="nav nav-list">
                                            <?php foreach ($subactions as $_action => $subelement) : ?>
                                            <?php if(! isset($subelement['access']) || in_array(Identity::init()->getAccountInfo()->status, $subelement['access'])) : ?>
                                            <?php $actionarr = explode(',', $_action); ?>
                                            <li class="<?=($controller == $menuController && in_array($menuAction, $actionarr)) ? 'active' : '';?>">
                                                <a href="<?=$subelement['href']?>"<?=isset($subelement['style']) ? ' style="' . $subelement['style'] . '"' : '';?>>
                                                    <?=isset($subelement['icon']) ? '<span class="glyphicon ' . $subelement['icon'] . '"></span>' : '';?><?=$subelement['title']?>
                                                </a>
                                            </li>
                                            <?php endif; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </li>
                                <?php endif; ?>
                                <?php else : ?>
                                <?php foreach($action as $_action => $element) : ?>
                                <?php if(! isset($element['access']) || in_array(Identity::init()->getAccountInfo()->status, $element['access'])) : ?>
                                <?php $actionarr = explode(',', $_action); ?>
                                <?php if(isset($adminArea)) : ?>
                                <?php $controllerfull = 'Controller_Admin_' . ucfirst($controller); ?>
                                <?php $allowed = FALSE; ?>
                                <?php foreach ($actionarr as $aarr) : ?>
                                <?php if (Identity::init()->isStaffAllowed($controllerfull, $aarr)) : ?>
                                <?php $allowed = TRUE; ?>
                                <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if (! $allowed) : continue; endif; ?>
                                <?php endif; ?>
                                <li class="<?=($controller == strtolower(Request::initial()->controller()) &&
                                        in_array(strtolower(Request::initial()->action()), $actionarr)) ? 
                                        'active' : '';?>">
                                    <a href="<?=$element['href']?>"<?=isset($element['style']) ? ' style="' . $element['style'] . '"' : '';?>>
                                        <span class="glyphicon <?=$element['icon']?>"></span><?=$element['title']?>
                                    </a>
                                </li>
                                <?php endif; ?>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <?php endforeach; ?>                                
                                <li class="divider"><hr style="margin: 10px 0" /></li>
                                <li>
                                    <a href="<?=URL::site('services')?>">
                                        <span class="glyphicon glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                                        &nbsp;&nbsp;Управление инструментами
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=URL::site('settings')?>">
                                        <span class="glyphicon glyphicon glyphicon-cog" aria-hidden="true"></span>
                                        &nbsp;&nbsp;Настройки аккаунта
                                    </a>
                                </li>
                                <li class="divider visible-xs-block"><hr style="margin: 10px 0" /></li>
                                <li class="visible-xs-block">
                                    <a href="<?=URL::site('login/logout')?>">
                                        <span class="glyphicon glyphicon glyphicon-off" aria-hidden="true"></span>
                                        &nbsp;&nbsp;Выйти из аккаунта
                                    </a>
                                </li>
                            </ul>
                        </div><!--/.nav-collapse -->