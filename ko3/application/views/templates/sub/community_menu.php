
          <div class="page-header"><h1>Обсуждения</h1></div>
          
          <ul id="tabs" class="nav nav-tabs nav-justified nav-chat">
              <li role="presentation" <?=(strtolower(Request::initial()->action()) == 'index') ? 'class="active"' : ''?>>
                  <a class="no-link" href="<?=URL::site('community')?>"><big></big><span>Общие темы</span></a>
              </li>
              <li role="presentation" <?=(strtolower(Request::initial()->action()) == 'qa') ? 'class="active"' : ''?>>
                  <a class="no-link" href="<?=URL::site('community/qa')?>"><big></big><span>Вопрос-ответ</span></a>
              </li>
          </ul>
          