<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?=$title?></title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?= URL::site('/assets/bootstrap/css/bootstrap.min.css') ?>">

        <!-- Custom styles for this template -->
        <link href="<?= URL::site('/assets/dashboard.css?v=1.2.6') ?>" rel="stylesheet">        
        <link href="<?= URL::site('/assets/bettercolors.css') ?>" rel="stylesheet">
        <?php foreach ($styles as $style) : ?><link href="<?= URL::site($style) ?>" rel="stylesheet"> 
        <?php endforeach; ?> 
        <style>
            .navbar-login
            {
                width: 305px;
                padding: 10px 15px 5px;
                padding-bottom: 0px;
            }

            .navbar-login-session
            {
                padding: 10px;
                padding-bottom: 0px;
                padding-top: 0px;
            }

            .icon-size
            {
                font-size: 87px;
            }
        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>


        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".affix-row" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Аккаунт</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                  
                    <a class="navbar-brand" href="<?= URL::site()?>"><img style="height: 32px; margin-top: -6px;" alt="Brand" src="<?=URL::site('assets/logo_new_header.png')?>"></a>
                    <p class="navbar-text navbar-mobile-block visible-xs-block">
                        <span class="xs-right">
                            <a class="no-link" href="/pay">
                                <strong><?=Identity::init()->getAccountBalance()?></strong>
                                <small><span class="glyphicon glyphicon glyphicon-rub" aria-hidden="true"></span></small>
                            </a>
                            <a href="/pay" class="btn btn-success btn-xs btn-round-xs"><i class="glyphicon glyphicon-plus"></i></a>
                        </span>
                    <?php /*
                        <a href="<?=URL::site('login/logout')?>" title="Выход" class="navbar-link xs-right"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a>
                        <a href="<?=URL::site('settings')?>" title="Настройки аккаунта" class="navbar-link xs-right"><span class="glyphicon glyphicon glyphicon-cog" aria-hidden="true"></span></a>
                    */ ?>
                    </p>
                </div>
                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-right"> 
                        <?php /*<li class="visible-xs-block"><a href="<?=URL::site('login/logout')?>">Выход</a></li>*/ ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <strong><?= Identity::init()->getAccountInfo()->profile->name ?></strong>
                                <span class="glyphicon glyphicon-menu-down"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="navbar-login">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <p class="text-center">
                                                    <?=HTML::image(Conf::get_option('site_url') . Identity::init()->getAccountAvatar(), 
                                                            array('width' => '90'))?>
                                                </p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p class="text-left">
                                                    <strong>
                                                        <?= Identity::init()->getAccountInfo()->profile->name ?><br />
                                                        <?= Identity::init()->getAccountInfo()->profile->surname ?>
                                                    </strong>
                                                </p>
                                                <p class="text-left small"><?=View::factory('templates/sub/account_expiried')?></p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php /*<li class="divider"></li>
                                <li>
                                    <div class="navbar-login navbar-login-session">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>
                                                    <a href="<?=URL::site('settings')?>" class="btn btn-primary btn-block">Настройки профиля</a>
                                                </p>
                                            </div>
                                            <div class="col-lg-12">
                                                <p>
                                                    <a href="<?=URL::site('services')?>" class="btn btn-primary btn-block">Управление инструментами</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>*/ ?>
                                <li class="divider"></li>
                                <li>
                                    <div class="navbar-login navbar-login-session">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>
                                                    <a href="<?=URL::site('login/logout')?>" class="btn btn-danger btn-block">Выйти из аккаунта</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>    
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row affix-row collapse in">
            <div class="col-sm-3 col-lg-2 affix-sidebar">
                <div class="sidebar-nav">
                    <div class="navbar navbar-default" role="navigation" style="min-height: 0!important;">
                        <?php /*<div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" 
                                    data-target=".sidebar-navbar-collapse">
                                <span class="sr-only">Меню</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                         */ ?>
                        <?=View::factory('templates/sub/menu', 
                                array(
                                    'menu'            => Kohana::$config->load('menu'),
                                    'menuController'  => $menu['controller'],
                                    'menuAction'      => $menu['action']
                                                            ))?>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-lg-10 affix-content">
                <div class="container">
                  <?=$content;?>
                </div>
            </div>
        </div>
        <div id="noty-holder"></div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?= URL::site('/assets/formdata.min.js') ?>"></script>
        <script src="<?= URL::site('/assets/jquery/1.12.4/jquery.min.js') ?>"></script>
        <script src="<?= URL::site('/assets/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?= URL::site('/assets/bootbox.min.js') ?>"></script>
        <script src="<?= URL::site('/assets/dashboard.js?v=1.0.1') ?>"></script>
        <?php foreach ($scripts as $script) : ?><script src="<?= URL::site($script) ?>"></script> 
        <?php endforeach; ?> 
        <script type="text/javascript"> 
        <?=$jsCode?> 
        </script>
    </body>
</html>