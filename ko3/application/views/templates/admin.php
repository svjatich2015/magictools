<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?=$title?></title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?= URL::site('/assets/bootstrap/css/bootstrap.min.css') ?>">

        <!-- Custom styles for this template -->
        <link href="<?= URL::site('/assets/dashboard.css?v=1.1.5') ?>" rel="stylesheet">
        <link href="<?= URL::site('/assets/admin.css?v=1.0.0') ?>" rel="stylesheet">        
        <link href="<?= URL::site('/assets/bettercolors.css') ?>" rel="stylesheet">
        <?php foreach ($styles as $style) : ?><link href="<?= URL::site($style) ?>" rel="stylesheet"><?php endforeach; ?> 
        <style>
            .navbar-login
            {
                width: 305px;
                padding: 10px;
                padding-bottom: 0px;
            }

            .navbar-login-session
            {
                padding: 10px;
                padding-bottom: 0px;
                padding-top: 0px;
            }

            .icon-size
            {
                font-size: 87px;
            }
        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">                   
                    <a class="navbar-brand" href="<?= URL::site('admin')?>"><img style="height: 32px; margin-top: -6px;" alt="Brand" src="<?=URL::site('assets/logo_new_header.png')?>"></a>
                    <p class="navbar-text hidden-xs"> 
                        <a href="<?= URL::site()?>" class="navbar-link">
                            <span class="glyphicon glyphicon-log-out"></span>
                            Вернуться в пользовательскую зону сайта
                        </a>
                    </p>
                    <p class="navbar-text navbar-mobile-block visible-xs-block">
                        <a href="<?=URL::site('login/logout')?>" title="Выход" class="navbar-link xs-right"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a>
                        <a href="<?=URL::site('profile')?>" title="Профиль" class="navbar-link xs-right"><span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"></span></a>
                        <a href="http://helpdesk.magic-tools.ru" title="Служба поддержки" target="_blank" class="navbar-link xs-right"><span class="glyphicon glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>
                    </p>
                </div>
                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-right">          
                        <li class="hidden-xs">
                            <a href="http://helpdesk.magic-tools.ru" target="_blank">
                                <span class="glyphicon glyphicon-question-sign"></span> 
                                <strong class="hidden-sm">Поддержка</strong>
                            </a>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-user"></span> 
                                <strong class="hidden-sm"><?= Identity::init()->getAccountInfo(TRUE)->profile->name ?></strong>
                                <span class="glyphicon glyphicon-menu-down"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="navbar-login">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <p class="text-center">
                                                    <span class="glyphicon glyphicon-user icon-size"></span>
                                                </p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p class="text-left">
                                                    <strong>
                                                        <?= Identity::init()->getAccountInfo(TRUE)->profile->name ?>
                                                        <?= Identity::init()->getAccountInfo(TRUE)->profile->surname ?>
                                                    </strong>
                                                </p>
                                                <p class="text-left small"><?= Identity::init()->getAccountInfo(TRUE)->email ?></p>
                                                <p class="text-left">
                                                    <a href="<?=URL::site('profile')?>" class="btn btn-primary btn-block btn-sm">Редактировать профиль</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="navbar-login navbar-login-session">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>
                                                    <a href="<?=URL::site('login/logout')?>" class="btn btn-danger btn-block">Выйти из аккаунта</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>    
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row affix-row">
            <div class="col-sm-3 col-lg-2 affix-sidebar">
                <div class="sidebar-nav">
                    <div class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" 
                                    data-target=".sidebar-navbar-collapse">
                                <span class="sr-only">Меню</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="container">
                                <p class="visible-xs navbar-text">
                                        <a href="<?= URL::site()?>" class="navbar-link">
                                            <span class="glyphicon glyphicon-log-out"></span>
                                            Вернуться в пользовательскую зону сайта
                                        </a>
                                </p>
                            </div>
                        </div>
                        <?=View::factory('templates/sub/menu', 
                                array(
                                    'menu'            => Kohana::$config->load('admin_menu'),
                                    'menuController'  => $menu['controller'],
                                    'menuAction'      => $menu['action'], 
                                    'adminArea'       => TRUE
                                ))?>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-lg-10 affix-content">
                <div class="container">
                  <?=$content;?>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?= URL::site('/assets/jquery/1.12.4/jquery.min.js') ?>"></script>
        <script src="<?= URL::site('/assets/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?= URL::site('/assets/bootbox.min.js') ?>"></script>
        <?php foreach ($scripts as $script) : ?><script src="<?= URL::site($script) ?>"></script><?php endforeach; ?>         
        <script type="text/javascript"> 
        <?=$jsCode?> 
        </script>
    </body>
</html>