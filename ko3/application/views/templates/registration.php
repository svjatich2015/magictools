<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Регистрация</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?= URL::site('/assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom styles for this template -->
    <link href="<?=URL::site('/assets/signin.css')?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
        <?=$content?>
    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= URL::site('/assets/jquery/1.12.4/jquery.min.js') ?>"></script>
    <script src="<?= URL::site('/assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    
    <script type="text/javascript">
            jQuery(function($) {
                $('[data-toggle="tooltip"]').tooltip();
                $('#checkboxAgree').on('click', function(){
                        $('#buttonSubmit').prop('disabled', $(this).prop('checked') ? false : true);
                });
            });
    </script>
  </body>
</html>