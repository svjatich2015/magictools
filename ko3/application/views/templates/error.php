<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Error [<?= $code ?>]</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?= URL::site('/assets/bootstrap/css/bootstrap.min.css') ?>">
        <style>
            .page h1
            {
                color:#ff5c33;
                font-size:120px;
                text-align:center;
            }
            .page h3
            {
                color: #ff4d4d;
                text-align:center;
            }
        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="container">
            <div class="row page">
                <h1><?=$code?></h1>
                <h3><?=($code === 404) ? 'Страница не найдена' : 'Что-то сломалось! :-('?></h3>
            </div>
        </div>
    </body>
</html>