<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Технические работы</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?= URL::site('/assets/bootstrap/css/bootstrap.min.css') ?>">
        <style>
            .page h1
            {
                font-size:36px;
                text-align:center;
            }
            .page h3
            {
                font-size:18px;
                text-align:center;
            }

            @media (min-width: 768px){
                .page h1
                {
                    font-size:60px;
                    text-align:center;
                }
                .page h3
                {
                    font-size:30px;
                    text-align:center;
                }
            }

            @media (min-width: 992px){
                .page h1
                {
                    font-size:70px;
                    text-align:center;
                }
                .page h3
                {
                    font-size:35px;
                    text-align:center;
                }
            }

            @media (min-width: 1220px){
                .page h1
                {
                    font-size:80px;
                    text-align:center;
                }
                .page h3
                {
                    font-size:40px;
                    text-align:center;
                }
            }
        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="container">
            <div class="row page">
                <div class="col-xs-12">
                    <h1>Технические работы</h1>
                    <h3>Приносим извинения, работа сервиса возобновится в самое ближайшее время!</h3>
                </div>
            </div>
        </div>
    </body>
</html>