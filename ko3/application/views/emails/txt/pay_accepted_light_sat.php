Здравствуйте, <?=$name?>.

На связи Святослав, автор и разработчик инструментов "Magic Tools".

От вас получен платеж: <?=Date::decl($clearAmount, array('рубль', 'рубля', 'рублей'))?>.

В честь "Светлого Уикенда" мы также зачислили бонус в размере 30% от суммы.

Баланс аккаунта пополнен на <?=Date::decl($amount, array('рубль', 'рубля', 'рублей'))?>.

С уважением, Маслов Святослав.
<?=Conf::get_option('site_url')?> 

P.S. Если баланс до оплаты был нулевым или отрицательным, то после
зачисления средств сразу же будет произведено списание за остаток 
текущего дня.
