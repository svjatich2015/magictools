-----------------------------------------
 Запрос на пополнение баланса аккаунта
----------------------------------------- 
ID аккаунта: <?=$account->id?> 
Имя: <?=$account->profile->name?> 
Фамилия: <?=$account->profile->surname?> 
Email: <?=$account->email?> 

Способ: <?=strtoupper($provider)?> 
Сумма: <?=$amount?> руб.

-----------------------------------------
ОДОБРИТЬ: <?=Conf::get_option('site_url')?>payment/confirm/?sha=<?=substr(md5($account->id.'::'.$payId.'::'.$amount.'::'.Conf::get_option('auth_pepper')), 0, 8)?>&account=<?=$account->id?>&pay=<?=$payId?>&amount=<?=$amount?> 
-----------------------------------------

Если деньги не были получены или вы знаете, что это ошибочный платеж, отмените его!

-----------------------------------------
ОТМЕНИТЬ: <?= Conf::get_option('site_url') ?>payment/cancel/?sha=<?= substr(md5($account->id . '::' . $payId . '::' . $amount . '::' . Conf::get_option('auth_pepper')), 0, 8) ?>&account=<?= $account->id ?>&pay=<?= $payId ?>&amount=<?= $amount ?> 
-----------------------------------------

С уважением, робот MagicTools.
