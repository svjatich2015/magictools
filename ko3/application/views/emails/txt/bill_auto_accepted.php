Здравствуйте, <?=$name?>.

На связи Святослав, автор и разработчик MagicTools.

Автоматически создан и оплачен счет #<?=$bill_id?> на сумму 
<?=$bill_amount?> рублей.

Продлен срок действия следующих инструментов:

<?php foreach($tools as $tool) : ?>
- <?=$tool['title']?> (до <?=$tool['expiried']?>)
<?php endforeach; ?>

С уважением, Маслов Святослав.
<?=Conf::get_option('site_url')?>
