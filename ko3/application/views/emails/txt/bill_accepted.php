Здравствуйте, <?=$name?>.

На связи Святослав, автор и разработчик MagicTools.

Счет #<?=$bill_id?> был успешно оплачен.

Продлен срок действия следующих инструментов:

<?php foreach($tools as $tool) : ?>
- <?=$tool['title']?> (до <?=$tool['expiried']?>)
<?php endforeach; ?>

С уважением, Маслов Святослав.
<?=Conf::get_option('site_url')?>
