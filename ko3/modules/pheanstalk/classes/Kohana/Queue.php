<?php defined('SYSPATH') or die('No direct script access.');

use Pheanstalk\Pheanstalk;

/**
 * Обработка очередей
 *
 * @author nergal
 * @package beanstalk
 */
abstract class Kohana_Queue
{
    /**
     * Задержка при выбрке
     * @const
     */
    const DEFAULT_DELAY = 0;       // без задержки

    /**
     * Приоритет сообщения
     * @const
     */
    const DEFAULT_PRIORITY = 1024; // 0 - самый срочный, макс. значение - 4294967295

    /**
     * Время на выполнение задачи, Time To Run
     * @const
     */
    const DEFAULT_TTR = 60;        // 1 минута

    /**
     * Время ожидания новой задачи, если очередь пуста
     * @const
     */
    const DEFAULT_TIMEOUT = 1;     // 1 секунда

    /**
     * Пулл инстанций
     *
     * @static
     * @access protected
     * @var array of Kohana_Queue
     */
    protected static $_instance = array();

    /**
     * Конфигурация по-умолчанию
     *
     * @static
     * @access protected
     * @var $_default_config array
     */
    protected static $_default_config = array();
    
    protected $_jobs = array();
    protected $_conf = array();

    /**
     * Инстанцирование объекта очереди
     *
     * @param string $name
     * @param array $config
     * @throws Kohana_Exception
     * @return Kohana_Queue
     */
    public static function instance($name = 'default', $config = array())
    {
        if ( ! isset(self::$_instance[$name])) {
            if (empty(self::$_default_config)) {
                self::$_default_config = Kohana::$config->load('pheanstalk')->as_array();
            }

            $config = array_merge(self::$_default_config, $config);

            self::$_instance[$name] = new Queue($config);
        }

        return self::$_instance[$name];
    }

    /**
     * Создание очереди
     *
     * @constructor
     * @access protected
     * @return void
     */
    protected function __construct($config)
    {
        $this->_conf = $config;
            
        extract(Arr::extract($this->_conf, array('host', 'port', 'timeout', 'persistent')));
        
        $this->_beans = new Pheanstalk($host, $port, $timeout, $persistent);
    }

    /**
     * @return Pheanstalk\Connection
     */
    public function connection()
    {
        return $this->_beans->getConnection();
    }

    /**
     * @return $this
     */
    public function reconnect($tubes = FALSE)
    {
        if($tubes)
        {
                $tubesList = $this->_beans->listTubesWatched();
        }
            
        extract(Arr::extract($this->_conf, array('host', 'port', 'timeout', 'persistent')));
        
        $this->_beans = new Pheanstalk($host, $port, $timeout, $persistent);
        
        if(isset($tubesList))
        {
                $this->watch($tubesList, !in_array('default', $tubesList));
        }
        
        return $this;
    }

    /**
     * @return $this
     */
    public function ignore($tube)
    {
        $this->_beans->ignore($tube);
            
        return $this;
    }

    /**
     * @return $this
     */
    public function watch($tubes, $ignoreDefault = FALSE)
    {
        if(!is_array($tubes))
        {
                $tubes = array($tubes);
        }
        
        foreach($tubes as $tube)
        {
                $this->_beans->watch($tube);
        }
        
        if($ignoreDefault)
        {
                $this->_beans->ignore('default');
        }
            
        return $this;
    }

    /**
     * @return $this
     */
    public function touch($jobId)
    {
        $this->_beans->touch($this->_jobs[$jobId]);
            
        return $this;
    }

    /**
     * @return $this
     */
    public function delete($jobId)
    {
        $this->_beans->delete($this->_jobs[$jobId]);
        
        unset($this->_jobs[$jobId]);
            
        return $this;
    }

    /**
     * @return $this
     */
    public function release($jobId, $priority = Queue::DEFAULT_PRIORITY, $delay = Queue::DEFAULT_DELAY)
    {
        $this->_beans->release($this->_jobs[$jobId], $priority, $delay);
            
        return $this;
    }

    /**
     * @return none
     */
    public function bury($jobId, $priority = Queue::DEFAULT_PRIORITY)
    {
        $this->_beans->bury($this->_jobs[$jobId], $priority);
    }

    /**
     * @return mixed
     */
    public function reserve($timeout = Queue::DEFAULT_TIMEOUT)
    {
        $job = $this->_beans->reserve($timeout);
        
        if($job) {
            $jobId = $job->getId();
            $data = $job->getData();
            
            $this->_jobs[$jobId] = $job;
            
            return array('id' => $jobId, 'body' => $data);
        }
            
        return FALSE;
    }

    /**
     * Установка задачи в очередь
     *
     * @param string $queue
     * @param mixed $params
     * @param integer $priority
     * @param integer $delay
     * @param integer $ttr
     * @throws Kohana_Exception
     * @return Ambigous <number, boolean>
     */
    public function add($queue, $params, $priority = Queue::DEFAULT_PRIORITY, $delay = Queue::DEFAULT_DELAY, $ttr = Queue::DEFAULT_TTR)
    {
        $this->_beans->useTube($queue);
        $params = serialize($params);

        if ($job_id = $this->_beans->put($params, $priority, $delay, $ttr)) {
            return $job_id;
        }

        throw new Kohana_Exception('Cant set new job in '.$queue);
    }

    /**
     * Возврат объекта очереди
     *
     * @return Beanstalk
     */
    public function queue()
    {
        return $this->_beans;
    }
}
