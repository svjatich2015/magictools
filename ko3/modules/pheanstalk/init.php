<?php
/**
 * Pheanstalk compatibility with Kohana's autoload implementation.
 */

if ($path = Kohana::find_file('vendor', 'autoload')) {
	require_once $path;
}