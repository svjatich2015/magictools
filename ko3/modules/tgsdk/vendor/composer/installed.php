<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'e254ec40d1b1fd33100e69d83d42456166dc19ea',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'e254ec40d1b1fd33100e69d83d42456166dc19ea',
            'dev_requirement' => false,
        ),
        'telegram-bot/api' => array(
            'pretty_version' => 'v2.3.23',
            'version' => '2.3.23.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../telegram-bot/api',
            'aliases' => array(),
            'reference' => '4565a9f1339d9f584bc9d9073feab1bfe6b0f7ea',
            'dev_requirement' => false,
        ),
    ),
);
